#!/usr/bin/env perl

# randomize a sequence of sentences in Conll format (separated by blank lines)

use strict;
use List::Util qw(shuffle);

my @data = ();
my @sentence = ();

my $i=0;

while (<>) {
  if (/^\s*$/) {
    # end of sentence
    push(@data,[$i++,@sentence]);
    @sentence=();
    next;
  }
  push(@sentence,$_);
}

# emit in random order
@data = shuffle @data;

while (@data) {
  my $sentence = shift @data;
  my $i = shift @$sentence;
  print "## shuffle $i\n";
  foreach my $l (@$sentence) {
    print $l;
  }
  print "\n";
}
