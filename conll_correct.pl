#!/usr/bin/env perl

## correct some errors, such as multiroot

use strict;

my $root;
my $head = {};

while (<>) {
  chomp;
  if (/^\s*$/) {
    $root = undef;
    $head = {};
  }
  if (/^\d+\t/) {
    my @f = split(/\t/,$_);
    if (!$f[6]) {
      if (defined $root) {
	# no multiroot: we reroot to first root
	$f[6] = $root;
	$_ = join("\t",@f);
      } else {
	$root = $f[0];
      }
    } elsif ($f[6] =~ s/^(\d+)\.\d+$/$1/) {
      # no phantom gov
      if ($f[0] == $f[6]) {
	# we get a cycle !
	$f[6] = $head->{$f[0]};
      }
      $_ = join("\t",@f);
    } else {
      $head->{$f[0]} = $f[6];
    }
  }
  print "$_\n";
}
