/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  nn.pl -- neurol networks
 *
 * ----------------------------------------------------------------
 * Description
 *  use of a neural network layer in dyalog-sr
 * ----------------------------------------------------------------
 */

:-require('format.pl').
:-require('utils.pl').
:-require('features.pl').

:-extensional dynet_map/2.
:-extensional label2dynet/2.
:-extensional action2dynet/2.
:-extensional dynet2label/2.
:-extensional dynet2action/2.
:-extensional dynet!epoch/1.
:-extensional dynet!lstm_layers/1. % number of layer in word LSTM
:-extensional dynet!lstm_dim/1.	   % dimension of word lstm output
:-extensional dynet!tag_dim/1.	   % dimension of tag vectors
:-extensional dynet!label_dim/1.	   % dimension of label vectors
:-extensional dynet!state_dim/1.	   % dimension of state hidden layers
:-extensional dynet!weight_coef/1.	   % ponderation of dynet weight (when combined with DSR Weight)
:-extensional dynet!char_lstm_layers/1. % number of layer in char LSTM
:-extensional dynet!char_dim/1.	   % dimension of char vectors
:-extensional dynet!char_lstm_dim/1.	   % dimension of char lstm output
:-extensional dynet!state_words/1.	   % number of words by states (in 10, 16, 19)
:-extensional dynet!hinge_loss/0.	   % use of hinge loss
:-extensional dynet!weight_cx/1.
:-extensional dynet!freeze_cf/0. % freeze value of weeight_coef
:-extensional dynet!dropout/1.	 % 100 * dropout
:-extensional dynet!edge_hdim/1.
:-extensional dynet!label_hdim/1.
:-extensional dynet!use_fullcat/0.
  
:-extensional dynet!mstag_vocab/2.

:-extensional pos_oracle/2.
:-extensional mstag_oracle/3.

:-extensional we_approx/2.

:-extensional no_space_after/1.

:-extensional beam/1.

:-extensional mst!in/4.

dynet!init :-
    (dynet!epoch(Epoch) xor Epoch = 0),
    '$interface'('Dynet_Init'(Epoch:int),[return(none)]).

:-light_tabular dynet!w2i/2.
:-mode(dynet!w2i/2,+(+,-)).

dynet!w2i(Form,Id) :-
    '$interface'('Dynet_Form'(Form:string),[return(Id:int)])
.

:-xcompiler
dynet!w2i(Form,Freq,Id) :-
    '$interface'('Dynet_Form_Freq'(Form:string,Freq:int),[return(Id:int)])
.

:-light_tabular dynet!l2i/2.
:-mode(dynet!l2i/2,+(+,-)).

dynet!l2i(Label,Id) :-
    '$interface'('Dynet_Label'(Label:string),[return(Id:int)]),
    record(label2dynet(Label,Id)),
    record(dynet2label(Id,Label)),
    true
.

:-light_tabular dynet!t2i/2.
:-mode(dynet!t2i/2,+(+,-)).

dynet!t2i(Tag,Id) :-
    '$interface'('Dynet_Tag'(Tag:string),[return(Id:int)])
.

:-light_tabular dynet!a2i/2.
:-mode(dynet!a2i/2,+(+,-)).

dynet!a2i(Action,Id) :-
    '$interface'('Dynet_Action'(Action:string),[return(Id:int)]),
    record(action2dynet(Action,Id)),
    record(dynet2action(Id,Action)),
    true
.

:-light_tabular dynet!register_mstag_vocab/2.
:-mode(dynet!mstag_vocab/2,+(+,+)).

dynet!register_mstag_vocab(Mstag,Id) :-
    '$interface'('Dynet_New_Mstag_Vocab'(),[return(Id:int)]),
    record(dynet!mstag_vocab(Mstag,Id))
.

:-light_tabular dynet!mstag/3.
:-mode(dynet!mstag/3,+(+,+,-)).

dynet!mstag(Mstag,Value,Id) :-
    dynet!mstag_vocab(Mstag,VId),
    '$interface'('Dynet_Mstag'(VId:int,Value:string),[return(Id:int)]),
    format('register mstag ~w ~w => ~w ~w\n',[Mstag,Value,VId,Id]),
    true
.


:-light_tabular dynet!add_parameters/3.

dynet!add_parameters(MaxActions,NSimple,NComplex) :-
    (dynet!lstm_layers(Layers) xor Layers = 2),
    (dynet!lstm_dim(LSTM_Dim) xor LSTM_Dim = 100),
    (dynet!tag_dim(Tag_Dim) xor Tag_Dim = 32),
    (dynet!label_dim(Label_Dim) xor Label_Dim = 16),
    (dynet!state_dim(State_Dim) xor State_Dim = 100),
    (dynet!weight_coef(Coef) xor Coef = 3),
    (dynet!dropout(Dropout) xor Dropout = 20),
    (dynet!freeze_cf -> FreezeCf = 1 ; FreezeCf = 0 ),
    (dynet!char_dim(Char_Dim) xor Char_Dim = 8),
    (dynet!char_lstm_dim(Char_LSTM_Dim) xor Char_LSTM_Dim = 64),
    (dynet!char_lstm_layers(Char_LSTM_Layers) xor Char_LSTM_Layers = 2),
    (dynet!state_words(State_Words) xor State_Words = 10, record(dynet!state_words(10))),
    (dynet!hinge_loss -> Hinge_Loss = 1 ; Hinge_Loss = 0),
    (dynet!edge_hdim(Edge_HDim) xor Edge_HDim = 150),
    (dynet!label_hdim(Label_HDim) xor Label_HDim = 50),
    '$interface'( 'Dynet_Add_Parameters'(MaxActions:int,
					 NSimple: int,
					 NComplex: int,
					 Layers: int,
					 LSTM_Dim: int,
					 Tag_Dim: int,
					 Label_Dim: int,
					 State_Dim: int,
					 Coef: int,
					 Char_Dim: int,
					 Char_LSTM_Dim: int,
					 Char_LSTM_Layers: int,
					 State_Words: int,
					 Hinge_Loss: int,
					 FreezeCf: int,
					 Dropout: int,
					 Edge_HDim: int,
					 Label_HDim: int
					),
		  [return(none)])
.

:-std_prolog dynet!save_model.

dynet!save_model :-
    opt(save_model(Model)),
    name_builder('~w.dynet',[Model],FName),
    ('$interface'( 'Dynet_Save_Model'(FName: string), [return(none)]) xor true)
.

:-std_prolog dynet!load_model/1.

dynet!load_model(Model) :-
    name_builder('~w.dynet',[Model],FName),
    '$interface'( 'Dynet_Load_Model'(FName: string), [return(none)])
.


:-std_prolog dynet!register_embedding/1.

dynet!register_embedding(FName) :-
    '$interface'( 'Dynet_Register_Embedding'(FName: string), [return(none)])
.

:-std_prolog dynet!sentence_init/0.

dynet!sentence_init :-
    '$interface'( 'Dynet_Sentence_Init'(),[return(none)]),
    %% the first 4 "words" are actually special ones
    dynet!register_sentence_word(0,'<root>','<none>','<none>',0), % virtual root
    dynet!register_sentence_word('<null>','<none>','<none>','<none>',0), % null node
    dynet!register_sentence_word(start,'<s>','<none>','<none>',0), % sentence start
    dynet!register_sentence_word(end,'</s>','<none>','<none>',0),
    %% true words start now
    dynet!sentence_init_aux(0)
.

:-std_prolog dynet!sentence_init_aux/1.

dynet!sentence_init_aux(Pos) :-
    ('N'(Pos) ->
	 (opt(train:_) -> Train = 1 ; Train = 0),
	 '$interface'('Dynet_Run_BiLSTM'(Train:int),[return(none)]),
	 true
    ;
    'C'(Pos,Id,lemma{ lex => Lex, cat => Cat, fullcat => FullCat },NPos),
    (pos_oracle(Id,GoldCat) xor GoldCat = '<none>'),
    (no_space_after(Id) -> SpaceAfter = 0 ; SpaceAfter = 1),
    (dynet!use_fullcat -> XCat = FullCat ; XCat = Cat ),
    dynet!register_sentence_word(Id,Lex,XCat,GoldCat,SpaceAfter),
    every(( recorded(oracle!redge(Id,GovId,Label)),
	    (label2dynet(Label,LabelId) xor LabelId = 0),
	    '$interface'('Dynet_Oracle_Edge'(Id:int,GovId:int,LabelId:int),[return(none)])
	  )),
    dynet!sentence_init_aux(NPos)
    )
.

:-std_prolog dynet!register_sentence_word/5.

dynet!register_sentence_word(Id,Form,Cat,GoldCat,SpaceAfter) :-
    (we_approx(Id,Form2) xor Form2 = Form),
    '$interface'( 'Dynet_Register_Sentence_Word'(Form: string, Cat: string, GoldCat: string, Form2: string, SpaceAfter: int), [return(XId:int)]),
%    format('dynet register ~w ~w ~w => ~w\n',[Id,Form,Cat,XId]),
    record( dynet_map(Id,XId) ),
    every(( tagset!train_on_gold_mstag(MSTag),
	    dynet!mstag_vocab(MSTag,VId),
	    (mstag_oracle(Id,MSTag,Value) xor Value = '<none>'),
%%	    format('dynet register mstag ~w ~w ~w ~w\n',[Id,Form,MStag,Value]),
	    '$interface'('Dynet_Register_Sentence_Mstag'(VId:int,Value:string), [return(none)])
	  ))
.

:-extensional action_decompose/3.

%% prediction of next action given current state features
:-std_prolog dynet!state/6.

dynet!state(
    [ Delta0,Delta1,Delta01,
      Id1 : lemma{ lex => LexI, lemma=> LemmaI, cat => CatI, xfullcat =>FullCatI, mstag => MstagI, dict => DictI, length => LI },
      Id2 : lemma{ lex => LexI2, lemma=> LemmaI2, cat => CatI2, xfullcat =>FullCatI2, mstag => MstagI2, dict => DictI2, length => LI2},
      Id3 : lemma{ lex => LexI3, lemma=> LemmaI3, cat => CatI3, xfullcat =>FullCatI3, mstag => MstagI3, dict => DictI3, length => LI3},
      Id4 : lemma{ lex => LexI4, lemma=> LemmaI4, cat => CatI4, xfullcat =>FullCatI4, mstag => MstagI4, dict => DictI4, length => LI4},
      [ Left0, Pos0,
	S0Id : lemma{ lex => Lex0, lemma => Lemma0, cat => Cat0, xfullcat => FullCat0, mstag => Mstag0, dict => Dict0, length => L0},
	BLabel0, B0Id : lemma{ lex => BLex0, lemma => BLemma0, cat => BCat0, xfullcat => BFullCat0, dict => BDict0, length => BL0},BV0,BD0,BGov0,NBGov0,
	B2Label0, B02Id : lemma{ lex => B2Lex0, lemma => B2Lemma0, cat => B2Cat0, xfullcat => B2FullCat0, dict => B2Dict0, length => B2L0},
	ALabel0, A0Id : lemma{ lex => ALex0, lemma => ALemma0, cat => ACat0, xfullcat => AFullCat0, dict => ADict0, length => AL0},AV0,AD0,AGov0,NAGov0,
	A2Label0, A02Id : lemma{ lex => A2Lex0, lemma => A2Lemma0, cat => A2Cat0, xfullcat => A2FullCat0, dict => A2Dict0, length => A2L0},
	Gov0,
	NGov0,
	Gov0Id : lemma{ lex => GLex0, lemma => GLemma0, cat => GCat0, xfullcat => GFullCat0, dict => GDict0, length => GLength0 },
	LeftHead0Id : lemma{ lex => LeftHeadLex0, lemma => LeftHeadLemma0, cat => LeftHeadCat0, xfullcat => LeftHeadFullCat0, dict => LeftHeadDict0, length => LeftHeadLength0 },
	RightHead0Id : lemma{ lex => RightHeadLex0, lemma => RightHeadLemma0, cat => RightHeadCat0, xfullcat => RightHeadFullCat0, dict => RightHeadDict0, length => RightHeadLength0 },
	PrevLeftHead0Id : lemma{ lex => PrevLeftHeadLex0, lemma => PrevLeftHeadLemma0, cat => PrevLeftHeadCat0, xfullcat => PrevLeftHeadFullCat0, dict => PrevLeftHeadDict0, length => PrevLeftHeadLength0 },
	NextRightHead0Id : lemma{ lex => NextRightHeadLex0, lemma => NextRightHeadLemma0, cat => NextRightHeadCat0, xfullcat => NextRightHeadFullCat0, dict => NextRightHeadDict0, length => NextRightHeadLength0 },
	Span0
      ],
      [ Left1, Pos1,
	S1Id : lemma{ lex => Lex1, lemma => Lemma1, cat => Cat1, xfullcat => FullCat1, mstag => Mstag1, dict => Dict1, length => L1},
	BLabel1, B1Id : lemma{ lex => BLex1, lemma => BLemma1, cat => BCat1, xfullcat => BFullCat1, dict => BDict1, length => BL1},BV1,BD1,BGov1,NBGov1,
	B2Label1, B12Id : lemma{ lex => B2Lex1, lemma => B2Lemma1, cat => B2Cat1, xfullcat => B2FullCat1, dict => B2Dict1, length => B2L1},
	ALabel1, A1Id : lemma{ lex => ALex1, lemma => ALemma1, cat => ACat1, xfullcat => AFullCat1, dict => ADict1, length => AL1},AV1,AD1,AGov1,NAGov1,
	A2Label1, A12Id : lemma{ lex => A2Lex1, lemma => A2Lemma1, cat => A2Cat1, xfullcat => A2FullCat1, dict => A2Dict1, length => A2L1},
	Gov1,
	NGov1,
	Gov1Id : lemma{ lex => GLex1, lemma => GLemma1, cat => GCat1, xfullcat => GFullCat1, dict => GDict1, length => GLength1 },
	LeftHead1Id : lemma{ lex => LeftHeadLex1, lemma => LeftHeadLemma1, cat => LeftHeadCat1, xfullcat => LeftHeadFullCat1, dict => LeftHeadDict1, length => LeftHeadLength1 },
	RightHead1Id : lemma{ lex => RightHeadLex1, lemma => RightHeadLemma1, cat => RightHeadCat1, xfullcat => RightHeadFullCat1, dict => RightHeadDict1, length => RightHeadLength1 },
	PrevLeftHead1Id : lemma{ lex => PrevLeftHeadLex1, lemma => PrevLeftHeadLemma1, cat => PrevLeftHeadCat1, xfullcat => PrevLeftHeadFullCat1, dict => PrevLeftHeadDict1, length => PrevLeftHeadLength1 },
	NextRightHead1Id : lemma{ lex => NextRightHeadLex1, lemma => NextRightHeadLemma1, cat => NextRightHeadCat1, xfullcat => NextRightHeadFullCat1, dict => NextRightHeadDict1, length => NextRightHeadLength1 },
	Span1
      ],
      [ Left2, Pos2,
	S2Id : lemma{ lex => Lex2, lemma => Lemma2, cat => Cat2, xfullcat => FullCat2, mstag => Mstag2, dict => Dict2, length => L2},
	BLabel2, B2Id : lemma{ lex => BLex2, lemma => BLemma2, cat => BCat2, xfullcat => BFullCat2, dict => BDict2, length => BL2},BV2,BD2,BGov2,NBGov2,
	B2Label2, B22Id : lemma{ lex => B2Lex2, lemma => B2Lemma2, cat => B2Cat2, xfullcat => B2FullCat2, dict => B2Dict2, length => B2L2},
	ALabel2, A2Id : lemma{ lex => ALex2, lemma => ALemma2, cat => ACat2, xfullcat => AFullCat2, dict => ADict2, length => AL2},AV2,AD2,AGov2,NAGov2,
	A2Label2, A22Id : lemma{ lex => A2Lex2, lemma => A2Lemma2, cat => A2Cat2, xfullcat => A2FullCat2, dict => A2Dict2, length => A2L2},
	Gov2,
	NGov2,
	Gov2Id : lemma{ lex => GLex2, lemma => GLemma2, cat => GCat2, xfullcat => GFullCat2, dict => GDict2, length => GLength2 },
	LeftHead2Id : lemma{ lex => LeftHeadLex2, lemma => LeftHeadLemma2, cat => LeftHeadCat2, xfullcat => LeftHeadFullCat2, dict => LeftHeadDict2, length => LeftHeadLength2 },
	RightHead2Id : lemma{ lex => RightHeadLex2, lemma => RightHeadLemma2, cat => RightHeadCat2, xfullcat => RightHeadFullCat2, dict => RightHeadDict2, length => RightHeadLength2 },
	PrevLeftHead2Id : lemma{ lex => PrevLeftHeadLex2, lemma => PrevLeftHeadLemma2, cat => PrevLeftHeadCat2, xfullcat => PrevLeftHeadFullCat2, dict => PrevLeftHeadDict2, length => PrevLeftHeadLength2 },
	NextRightHead2Id : lemma{ lex => NextRightHeadLex2, lemma => NextRightHeadLemma2, cat => NextRightHeadCat2, xfullcat => NextRightHeadFullCat2, dict => NextRightHeadDict2, length => NextRightHeadLength2 },
	Span2
      ],
      Guide,
      DeltaStep,
      [ FormI5, AllCatsI5 ],
      [ PoncFeatsI1 ],
      [ LastBase, LastLabel ],
      PredAction,
      PredLabel,
      PredSecondAction,
      PredSecondLabel
    ],
    Update,
    Action,
    Label,
    Weight,
    NAction
) :-
%    format('dynet state update=~w action=~w label=~w actionbase=~w\n',[Update,Action,Label,ActionBase]),
    '$interface'( 'Dynet_State_Reset'(), [return(none)]),
    (mst!in(S0Id,GovS0Id,_,_), GovS0Label=0 xor dynet!head(S0Id,GovS0Id,GovS0Label) xor GovS0Id = -1, GovS0Label=0),
    (mst!in(S1Id,GovS1Id,_,_), GovS1Label=0 xor dynet!head(S1Id,GovS1Id,GovS1Label) xor GovS1Id = -1, GovS1Label=0),
    (mst!in(S2Id,GovS2Id,_,_), GovS2Label=0 xor dynet!head(S2Id,GovS2Id,GovS2Label) xor GovS2Id = -1, GovS2Label=0),
    every((
		 dynet!state_words(NWords),
		 (NWords = 2 ->
		      domain(Id,
			     [S0Id,
			      S1Id
			     ]
			    )
		 ; NWords = 3 ->
		      domain(Id,
			     [Id1,
			      S0Id,
			      S1Id
			     ]
			    )
		 ; NWords = 10 ->
		   domain(Id,
			     [Id1,%Id2,Id3,Id4,
			      S0Id,A0Id,B0Id,%A02Id,B02Id,
			      S1Id,A1Id,B1Id,%A12Id,B12Id,
			      S2Id,A2Id,B2Id% A22Id,B22Id,
			      %LeftHead0Id, RightHead0Id,
			      %LeftHead1Id, RightHead1Id
			     ])
		 ; NWords = 14 ->
		   domain(Id,
			  [Id1,%Id2,Id3,Id4,
			   S0Id,A0Id,B0Id,%A02Id,B02Id,
			   S1Id,A1Id,B1Id,%A12Id,B12Id,
			   S2Id,A2Id,B2Id,% A22Id,B22Id,
			   LeftHead0Id, RightHead0Id,
			   LeftHead1Id, RightHead1Id
			  ])
		 ; NWords = 15 ->
		   domain(Id,
			  [Id1,%Id2,Id3,Id4,
			   S0Id,A0Id,B0Id,%A02Id,B02Id,
			   S1Id,A1Id,B1Id,%A12Id,B12Id,
			   S2Id,A2Id,B2Id,% A22Id,B22Id,
			   LeftHead0Id, RightHead0Id,
			   LeftHead1Id, RightHead1Id,
			   GovS0Id
			  ])
		 ; NWords = 16 ->
		   domain(Id,
			  [Id1,%Id2,Id3,Id4,
			   S0Id,A0Id,B0Id,%A02Id,B02Id,
			   S1Id,A1Id,B1Id,%A12Id,B12Id,
			   S2Id,A2Id,B2Id,% A22Id,B22Id,
			   LeftHead0Id, RightHead0Id,
			   LeftHead1Id, RightHead1Id,
			   GovS0Id,
			   GovS1Id
			  ])
		 ; fail,
		   NWords = 16 ->
		   domain(Id,
			  [Id1,%Id2,Id3,Id4,
			   S0Id,A0Id,B0Id,A02Id,B02Id,
			   S1Id,A1Id,B1Id,A12Id,B12Id,
			   S2Id,A2Id,B2Id,A22Id,B22Id
			  ])
		 ; NWords = 17 ->
		   domain(Id,
			  [Id1,%Id2,Id3,Id4,
			   S0Id,A0Id,B0Id,%A02Id,B02Id,
			   S1Id,A1Id,B1Id,%A12Id,B12Id,
			   S2Id,A2Id,B2Id,% A22Id,B22Id,
			   LeftHead0Id, RightHead0Id,
			   LeftHead1Id, RightHead1Id,
			   GovS0Id,
			   GovS1Id,
			   GovS2Id
			  ])
		 ; NWords = 20 ->
		   domain(Id,
			  [Id1,%Id2,Id3,Id4,
			   S0Id,A0Id,B0Id,A02Id,B02Id,
			   S1Id,A1Id,B1Id,A12Id,B12Id,
			   S2Id,A2Id,B2Id,A22Id,B22Id,
			   LeftHead0Id, RightHead0Id,
			   LeftHead1Id, RightHead1Id
			  ])
		 ; NWords = 22 ->
		   domain(Id,
			  [Id1,%Id2,Id3,Id4,
			   S0Id,A0Id,B0Id,A02Id,B02Id,
			   S1Id,A1Id,B1Id,A12Id,B12Id,
			   S2Id,A2Id,B2Id,A22Id,B22Id,
			   LeftHead0Id, RightHead0Id,
			   LeftHead1Id, RightHead1Id,
			   GovS0Id,
			   GovS1Id
			  ])
		 ;
		  domain(Id,
			  [Id1,Id2,Id3,Id4,
			   S0Id,A0Id,B0Id,A02Id,B02Id,
			   S1Id,A1Id,B1Id,A12Id,B12Id,
			   S2Id,A2Id,B2Id,A22Id,B22Id
			  ])
		 ),
		 (dynet_map(Id,XId) ->
		      true
		 ; Id = void ->
		   dynet_map(end,XId)
		 ; format('*** missing dynet map for ~w\n',[Id]),
		   dynet_map('<null>',XId)
		 ),
%		 format('add word input id=~w xid=~w\n',[Id,XId]),
		 dynet!add_word_input(XId)
	     )),
    every((
		 domain(Id,
			[
			    BLabel0,
			    B2Label0,
			    ALabel0,
			    A2Label0,
			    
			    BLabel1,
			    B2Label1,
			    ALabel1,
			    A2Label1,

			    BLabel2,
			    B2Label2,
			    ALabel2,
			    A2Label2,

			    LastLabel,

			    id(GovS0Label),
			    id(GovS1Label)
			]),
		 (Id = id(XId) ->
		      true
		 ; label2dynet(Id,XId) -> true
		 ; XId = 0
		 ),
		 dynet!add_label_input(XId)
	     )),
    every((
		 ( action2dynet(LastBase,XId) -> true ; XId = 0),
%		 format('lastbase=~w lastlabel=~w xid=~w\n',[LastBase,LastLabel,XId]),
		 dynet!add_action_input(XId)
	     )),
    (Update \== 0 ->
	 (tagset!a2i(Action,Label,GoldActionLabel)
	     %tagset!action2index(Action,Label,GoldActionLabel,_,_)
	     xor format('*** pb gold action_label index for action=~w label=~w\n',[Action,Label]),
	     GoldActionLabel = -1
	 ),
	 ( action_decompose(NAction,NBase,NLabel),
	   tagset!a2i(NBase,NLabel,NActionLabel)
	  % tagset!action2index(NBase,NLabel,NActionLabel,_,_)
	   xor format('*** pb naction_label index for action=~w\n',[NAction]),
	   NActionLabel = -1
	 ),
	 true
    ; \+ var(Action),
      \+ var(Label) ->
      (tagset!a2i(Action,Label,GoldActionLabel)
       % tagset!action2index(Action,Label,GoldActionLabel,_,_)
       xor
       format('*** pb gold action_label index for action=~w label=~w\n',[Action,Label]),
       GoldActionLabel = -1
      ),
      NActionLabel = -1,
      true
    ;
    GoldActionLabel = -1,
    NActionLabel = -1
    ),
    %    format('dynet predict\n',[]),
    (beam(Beam) xor Beam=1),
    (opt(train:_) -> Train = 1 ; Train = 0),
    '$interface'('Dynet_Predict_Action'(GoldActionLabel:int, NActionLabel:int, Update:int,Beam:int,Train:int),[return(PredActionLabel:int)]),
    dynet!last_weight(Weight),
%%    format('dyalog dynet pal=~w last weight ~w\n',[PredActionLabel,Weight]),
    ( %tagset!i2a(PredActionLabel,PredAction,PredLabel)
      tagset!index2action(PredActionLabel,PredAction,PredLabel)
      xor
      format('*** pb pred action_label ~w\n',[PredActionLabel]),
      PredAction = none, PredLabel = none
    ),
%    format('dyalog dynet 1st pal=~w action=~w(~w) last weight ~w\n',[PredActionLabel,PredAction,PredLabel,Weight]),
    '$interface'('Dynet_Second_Best_Action'(),[return(PredSecondActionLabel:int)]),
    ( %tagset!i2a(PredSecondActionLabel,PredSecondAction,PredSecondLabel)
      tagset!index2action(PredSecondActionLabel,PredSecondAction,PredSecondLabel)
	xor
      format('*** pb pred second action_label ~w\n',[PredSecondActionLabel]),
%      PredSecondAction = none, PredSecondLabel = none,
      true
    ),
%    format('dyalog dynet 2nd pal=~w action=~w(~w)\n',[PredSecondActionLabel,PredSecondAction,PredSecondLabel]),
%    format('=> dynpred action=~w label=~w\n',[PredAction,PredLabel]),
    true
.

%:-std_prolog dynet!add_word_input/1.

:-xcompiler
dynet!add_word_input(Id) :-
    '$interface'('Dynet_Add_Word_Input'(Id:int), [return(none)])
    .

%:-std_prolog dynet!add_label_input/1.

:-xcompiler
dynet!add_label_input(Id) :-
    '$interface'('Dynet_Add_Label_Input'(Id:int), [return(none)])
    .

%:-std_prolog dynet!add_action_input/1.

:-xcompiler
dynet!add_action_input(Id) :-
    '$interface'('Dynet_Add_Action_Input'(Id:int), [return(none)])
    .

%:-std_prolog dynet!last_weight/1.

:-xcompiler
dynet!last_weight(W) :-
    '$interface'('Dynet_Last_Weight'(),[return(W:int)])
    .

:-xcompiler
dynet!get_weight(ActionLabel,W) :-
    %    '$interface'('Dynet_Get_Weight'(ActionLabel:int),[return(W:int)]),
    '$interface'('Dynet_Safe_Get_Weight'(ActionLabel:int,W:term),[]),
    % discard Actions taht have already been tried
    W >  -100000000,
    true
    .

:-xcompiler
dynet!get_nbest_action(Rank,ActionLabel) :-
    '$interface'('Dynet_Get_NBest_Action'(Rank:int),[return(ActionLabel:int)]),
    ActionLabel >= 0,
    true
    .

:-xcompiler
dynet!get_nbest_beam_action(Rank,ActionLabel) :-
    '$interface'('Dynet_Get_NBest_Beam_Action'(Rank:int),[return(ActionLabel:int)]),
    ActionLabel >= 0,
    true
    .

:-std_prolog dynet!apply_updates/0.

dynet!apply_updates :-
    '$interface'('Dynet_Apply_Updates'(),[return(none)])
.
  
:-light_tabular dynet!head/3.
:-mode(dynet!head/3,+(+,-,-)).

dynet!head(Dep,Gov,Label) :-
    Dep \== 0,
    '$interface'('Dynet_Predicted_Head'(Dep:int),[return(Gov:int)]),
    '$interface'('Dynet_Predicted_Label'(Dep:int),[return(Label:int)]),
    true
.


:-light_tabular dynet!head_rank/3.
:-mode(dynet!head_ranks/3,+(+,+,-)).

dynet!head_rank(Dep,Gov,Rank) :-
    Dep \== 0,
    '$interface'('Dynet_Predicted_Head_Rank'(Dep:int,Gov:int),[return(Rank:int)]),
    Rank < 5,
    true
.

:-light_tabular dynet!head_weight/3.
:-mode(dynet!head_weight/3,+(+,+,-)).

dynet!head_weight(Dep,Gov,W) :-
    Dep \== 0,
%    dynet!head_rank(Dep,Gov,Rank),
    '$interface'('Dynet_Safe_Predicted_Head_Weight'(Dep:int,Gov:int,_W:term),[]),
    W is round(1000 * _W).
%    W is 100 - 3 * Rank,
    true
.

:-std_prolog dynet!update_epoch/0.

dynet!update_epoch :-
    '$interface'('Dynet_Update_Epoch'(),[return(none)])
.

