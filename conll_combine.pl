#!/usr/bin/env perl

# add a last column with GOLD POS

use strict;

my $gold = shift;

open(G,"<",$gold)
  or die "can't read gold file $gold: $!";

while (<>) {
  my $g = <G>;
  if (/^\d+\t+/) {
    chomp;
    my @f = split(/\t/,$_);
    my @g = split(/\t/,$g);
    my $pos = $g[3];
    # my $pos = $g[4];
    # pos eq '_' and $pos = $g[3];
    my $mstag = $g[5];
    my @mstag = grep {/^(Case|Mood)=/} split(m{\|},$mstag);
    push(@f,join('|',$pos,@mstag));
    my $f = join("\t",@f);
    print "$f\n";
  } else {
    print $_;
  }
}
