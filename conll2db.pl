#!/usr/bin/env perl

use strict;
use AppConfig qw/:argcount :expand/;
use Text::Scan;
use List::Util qw{max};

my $config = AppConfig->new(
			    "oracle!" => {DEFAULT => 0},
			    "lattice!" => {DEFAULT => 0},
			    "graph!" => {DEFAULT => 0},
			    "tagset=f",
			    "mode=s" => {DEFAULT => 'gold'},
			    "oracle_seg=s",
			    "random_skip=d" => {DEFAULT => 0 }
			   );

$config->args();

my $oracle = $config->oracle();
my $lattice = $config->lattice();
my $graph = $config->graph();	# sagae style data
my $tagset = $config->tagset();
my $mode = $config->mode();
my $skip = $config->random_skip();

my $oracle_seg = [];

if ($config->oracle_seg) {
  my $file = $config->oracle_seg;
  open(SEG,"<",$file) 
    || die "can open oracle_set $file: $!";
  my @sent = ();
  print "%% loading oracle seg $file\n";
  while (<SEG>) {
    # SEG should be in CONLL format with dependency information
    if (/^\s*$/) {
      push(@$oracle_seg,[@sent]);
      @sent = ();
      next;
    }
    chomp;
    push(@sent,[split(/\t+/,$_)]);
  }
  close(SEG);
}

my $mstag_fset=0;

my $lexer_info = {
		  xfullcat => []
		 };

if ($tagset && -f $tagset) {
  open(TAGSET,"<",$tagset) || die "can't read tagset $tagset: $!";
  my $print = 0;
  while(<TAGSET>) {
    if (/^:-finite_set/) {
      # deactivate finite set for mstag
      #      /mstag/ and $mstag_fset=1;
      $print = 1;
    }
    $print and print $_;
    if ($print and /\)\./) {
      print "\n";
      $print = 0;
    }
    if (s/^%%\s+lexer\s*:\s*//o) {
      chomp;
      # lexer directives
      if (s/^xfullcat\s*//o) {
	my @f = split(/\s+/,$_);
	$lexer_info->{xfullcat} = \@f;
	next;
      }

      if ($mode eq 'pred' && /^compounds\s+(\S+)/) {
	my $scan = $lexer_info->{compounds} = Text::Scan->new;
	$scan->ignorecase();
	$scan->restore($1);
	$lexer_info->{collect_sentence} = 1;
      }

      if ($mode eq 'pred' && /^dict\s+(\S+)/) {
	my $scan = $lexer_info->{dict} = Text::Scan->new;
	$scan->ignorecase();
	$scan->restore($1);
	$lexer_info->{collect_sentence} = 1;
      }

      if (/^subcat\s+(\S+)/) {
	my $scan = $lexer_info->{subcat} = Text::Scan->new;
	$scan->ignorecase();
	$scan->restore($1);
	$lexer_info->{collect_sentence} = 1;
      }

      if (s/^split\s*//o) {
	my @f = split(/\s+/,$_);
	$lexer_info->{split}{$_} = 1 foreach (@f);
      }

      if (s/^mstag_complete\s*//o) {
	$lexer_info->{mstag_complete} = 1;
      }

      if (s/^extra\s+(\S+)//o) {
	$lexer_info->{extra}{$1} = 1;
      }

      if (s/^mstag_filter\s+'(\S+)'//o) {
	$lexer_info->{mstag_filter}{$1} = 1;
      }

      if (s/^cluster\s+(\S+)$//o) {
	my $cluster = $1;
#	print "loading cluster file $cluster\n";
	my $scan = $lexer_info->{cluster} = Text::Scan->new;
	$scan->restore($cluster);
#	print "done loading\n";
	$lexer_info->{collect_sentence} = 1;
      }

    }
  }
  close(TAGSET);
}


my $sid=1;
my $max=0;
my $tree={};
my $segmap = {};
my $root;
my $rootlabel;
my $count=0;
my @dag = ();
my $path = {};
my $maxlattice;
my $ids={};
my $memoactions={};

my $nfields;

while(<>) {
  chomp;
  /^#/ and next;		# comments
  if (/^\s*$/) {
    # white lines: sentence separators

    if ($skip && int(rand($skip))) {
      print "%% Skipping sentence E$sid\n\n";
      goto RESET;
    }

FLUSH:
    if (exists $lexer_info->{collect_sentence}) {
      my $sentence = '';
      my $pos2elt = {};
      foreach my $elt (@dag) {
	my ($left,$form) = @{$elt->{data}};
	$pos2elt->{length($sentence)} = $elt;
	$sentence .= "$form ";
      }

      if (my $scanner = $lexer_info->{compounds}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($mwe,$pos,$cat) = @$occ;
	  my $elt = $pos2elt->{$pos};
	  my $left = $elt->{data}[0]-1;
	  ## print "%% found mwe at pos=$pos: <$mwe> $cat left=$left\n";
	  $elt->{data}[5] =~ /mwehead=/ or $elt->{data}[5] .= "|mwehead=${cat}+";
	  foreach my $w (split(/\s+/,$mwe)) {
	    my $elt = $pos2elt->{$pos};
	    $elt->{data}[5] =~ /pred=y/ or $elt->{data}[5] .= "|pred=y";
	    $pos += 1 + length($w);
	  }
	}
      }

      if (my $scanner = $lexer_info->{dict}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($dform,$pos,$cats) = @$occ;
	  my $elt = $pos2elt->{$pos};
	  my $form = quote($elt->{data}[1]);
	  #	print "%% found mwe at pos=$pos: <$mwe> $cat left=$left\n";
	  $cats = join(',',map {quote($_)} grep {$_} split(/\|/,$cats));
	  $elt->{dict} = $cats;
	}
      }

      if (my $scanner = $lexer_info->{subcat}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($dform,$pos,$subcat) = @$occ;
	  my $elt = $pos2elt->{$pos};
	  my $form = quote($elt->{data}[1]);
	  #	print "%% found mwe at pos=$pos: <$mwe> $cat left=$left\n";
##	  $subcat = join(',',map {quote($_)} grep {$_} split(/\|/,$subcat));
	  $elt->{subcat} = $subcat;
	}
      }

      if (my $scanner = $lexer_info->{cluster}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($dform,$pos,$cid) = @$occ;
##	  print "cluster $dform $cid in <$sentence>\n";
	  my $elt = $pos2elt->{$pos};
	  my $form = quote($elt->{data}[1]);
	  $elt->{cluster} = $cid;
	}
      }


    }

    if (keys %$path) {
      # lattice mode: check dead ends
      my $alive = {$maxlattice => 1};
      keep_alive(0,$path,$alive);
      @dag = grep {$alive->{$_->{right}}} @dag;
    }

    if (@$oracle_seg) {
      my $current_seg = shift @$oracle_seg;
#      print "%% using oracle seg\n";
      my @stack = ([1,0]);
      while (@stack) {
	my $info = shift @stack;
	my $current_pos = $info->[0];
	my $current_tok = $current_seg->[$info->[1]];
	$current_tok or next;
	print "%% search candidates at pos $current_pos: oracle lex = $current_tok->[1]\n";
	my @candidates = grep {$_->{data}[0] == $current_pos} @dag;
	my $cost = 16;
	my $lex = $current_tok->[1];
	$lex =~ s/\@\S+$//o;					   # @suff is Hebrew data !
	my @candidates2 = grep {$_->{data}[1] eq $lex # lex
				  && $_->{data}[2] eq $current_tok->[2] # lemma
				    && $_->{data}[3] eq $current_tok->[3] # cat
				      && $_->{data}[4] eq $current_tok->[4] # fullcat
					&& $_->{data}[5] eq $current_tok->[5] # mstag
				      } @candidates;
	if (!@candidates2) {
	  $cost = 8;
	  @candidates2 = grep {$_->{data}[1] eq $lex
				 && $_->{data}[2] eq $current_tok->[2]
				   && $_->{data}[3] eq $current_tok->[3]
				     && $_->{data}[4] eq $current_tok->[4]
				     } @candidates;
	}
	if (!@candidates2) {
	  $cost = 4;
	  @candidates2 = grep {$_->{data}[1] eq $lex
				 && $_->{data}[2] eq $current_tok->[2]
				   && $_->{data}[3] eq $current_tok->[3]
				 } @candidates;
	}
	if (!@candidates2) {
	  $cost = 2;
	  @candidates2 = grep {$_->{data}[1] eq $lex
				 && $_->{data}[2] eq $current_tok->[2]
			       } @candidates;
	}
	if (0 && !@candidates2) {
	  $cost = 0;
	  @candidates2 = @candidates;
	}
	foreach my $candidate (@candidates2) {
	  (!exists $candidate->{segcost} || $cost > $candidate->{segcost}) or next;
	  $candidate->{segcost} = $cost;
	  my $right = $info->[1];
	  $right < $maxlattice or next;
	  push(@stack,[$candidate->{right}+1,$right+1]);
	}
      }
      my $max = max map {find_best_path($_,\@dag)} grep {exists $_->{segcost}} @dag;
      print "%% best path max is $max\n";
      my @first = grep {($_->{data}[0] == 1) 
			  && exists $_->{segbest}
			    && $_->{segbest} == $max
			  } @dag;
      my $first = $first[0];
      while ($first && @$current_seg) {
	my $current_tok = shift @$current_seg;
	my $left = $first->{data}[0]-1;
	my $right = $first->{right};
	print "%% align $left $right $first->{data}[1] vs $current_tok->[1]\n";
	$first->{segalign} = $current_tok;
	exists $first->{segbest} or last;
	$first = $first->{segnext};
      }
      $first and print "%% *** bad align: missing gold tokens\n";
      @$current_seg and print "%% *** bad align: missing lattice tokens\n";
      ($first || @$current_seg) and print "oracle!skip_sentence.\n\n";
    }

    elt_process($_) foreach (@dag);

    foreach my $i (0..3) {
  print <<EOF;
'D'($i,
    lemma{ lex => $i, 
           lemma => $i, 
           cat => $i, 
           mstag => '_',
           fullcat => $i,
           xfullcat => $i,
           dict => [],
           length => 0
         }
).

EOF

    }

    print <<EOF;

'N'($max).
'S'('E$sid').
%% line=$.

EOF

    if ($oracle ## && $root
       ) {
	print <<EOF;
oracle!action(0,init).
EOF
	my $i=0;
	my @actions = ();
	if ($graph) {
	  @actions = graph_get_actions();
	} else {
	  @actions = get_actions(0);
	};
      foreach my $xaction (@actions) {
	my ($action,$info,$length) = @$xaction;
	$length ||= 1;
#	$i += 2 * $length -1;
	$i++;
	print <<EOF;
oracle!action($i,$action). %% $info
EOF
##	$i++;
      }

    }

    print <<EOF;

%% SENTENCE DIV %%
%%%EOF

EOF

  RESET:
    $sid++;
    $max=0;
    $tree ={};
    $segmap = {};
    $root=undef;
    $rootlabel=undef;
    $count = 0;
    @dag = ();
    $path = {};
    $maxlattice = 0;
    $ids={};
    $memoactions={};
    next;			# done sentence
  }
  my @elt = split(/\t/,$_);
  my $tok;
  my $right;
  if ($lattice) {
    my $left = shift(@elt);
    $right = shift(@elt);
    $path->{$left}{$right} ||= 1;
    $right > $maxlattice and $maxlattice = $right;
    $left++;
##    $right++;
    unshift(@elt,$left);
    $tok = pop(@elt);
  }
  $nfields ||= scalar(@elt);
  if ( scalar(@elt) != $nfields 
       && $elt[5] =~ /^\d+/
     ) {
    warn "pb in CONLL file mismatch mstag/head: @elt\n";
    splice(@elt,5,0,'_');
  }
  my $entry = { data => [@elt], dict => '' };
  $tok and $entry->{tok} = $tok;
  $right and $entry->{right} = $right;
  push(@dag,$entry);
  eof() and goto FLUSH;		# this case used when no empty line at the end of the file !
}


if ($oracle) {
  print <<EOF;

oracle!end.

%% SENTENCE DIV %%
%%%EOF

EOF
}

sub elt_process {
  my ($elt) = @_;
  my ($left,$lex,$lemma,$cat,$fullcat,$mstag,@other) = @{$elt->{data}};
  if ($graph && exists $ids->{$left}) {
    # multi line in graph format
    if ($oracle) {
      my $id = $left;
      $segmap->{$id}=$id;
      if (@other) {
	my ($head,$label) = @other;
	$label = quote($label);
	$tree->{$head}{$id}{$label} = 1;
      }
    }
    return; 
  }
  my $xfullcat = $fullcat;
  $left--;
  $max = (exists $elt->{right}) ? $elt->{right} : ($left+1);
  $count++;
  my $fs = "'_'";
  $mstag =~ /atbpos/ and $mstag = Morpho::Arabic::handle($mstag);
  ($cat eq 'V' && $elt->{subcat}) and $mstag .= "|$elt->{subcat}";
  (exists $elt->{cluster}) and $mstag .= "|cluster=$elt->{cluster}";
  if ($mstag =~ /\|/) {
    my @fs = split(/\|/,$mstag);
    if (exists $lexer_info->{mstag_filter}) {
      @fs = grep {exists $lexer_info->{mstag_filter}{$_}} @fs;
    }
    if (@{$lexer_info->{xfullcat}}) {
      my %fs = (map {my @u = split(/=/,$_,2); $u[0] => $u[1]} grep {/.=./} @fs);
      foreach my $f (grep {exists $fs{$_}} @{$lexer_info->{xfullcat}}) {
	$xfullcat .= $fs{$f};
	delete $fs{$f};
      }
      @fs = grep {my @u=split(/=/,$_,2); exists $fs{$u[0]}} @fs;
    }
    $lexer_info->{mstag_complete} and push(@fs,$mstag);
    if (@fs) {
      $fs = join(',',  map {quote($_)} grep {$_} @fs );
      $fs = "[$fs]";
      $mstag_fset and $fs = "mstag$fs";
    } else {
      $fs = "'_'"
    }
  } else {
    if (@{$lexer_info->{xfullcat}} && $mstag =~ /(\S+)=(\S+)/) {
      my ($f,$v) = ($1,$2);
      if (grep {$f eq $_} @{$lexer_info->{xfullcat}}) {
	$xfullcat .= $v;
	$mstag = '_';
      }
    }
    $fs = quote($mstag || '_');
  }
  if ($lexer_info->{extra}{number} && $lex =~ /^\d+(?:[.,]\d*)?/) {
    $xfullcat .= "_num";
  }
  if ($lexer_info->{extra}{capitalize} && $lex eq ucfirst($lex)) {
    $xfullcat .= "_cap";
  }
  my ($qlex,$qlemma,$qcat,$qfullcat,$qxfullcat) = map {quote($_)} ($lex,$lemma,$cat,$fullcat,$xfullcat);
  exists $lexer_info->{split}{cat} and $qcat = qsplit($cat);
  exists $lexer_info->{split}{fullcat} and $qfullcat = qsplit($fullcat);
  exists $lexer_info->{split}{xfullcat} and $qxfullcat = qsplit_pos($xfullcat);
  my $dinfo = $elt->{dict};
  my $length = $max - $left;
  my $id = $count;
  exists $elt->{tok} and $id .= "+$elt->{tok}";
  print <<EOF;
'C'($left,
    $id,
    lemma{ lex => $qlex, 
           lemma => $qlemma, 
           cat => $qcat, 
           mstag => $fs,
           fullcat => $qfullcat,
           xfullcat => $qxfullcat,
           dict => [$dinfo],
           length => $length
         },
    $max).

EOF

  $ids->{$id} = $length;

  my $conllmax = $max;

  if (exists $elt->{segalign}) {
    my $oracle_tok = $elt->{segalign};
    @other = @{$oracle_tok}[6,7];
    $conllmax = $oracle_tok->[0];
    print "%% getting from oracle token: @other\n";
  }

  if ($oracle && (@other || $graph)) {
    $segmap->{$conllmax}=$id;
    if (@other) {
      my ($head,$label) = @other;
      $label = quote($label);
      $tree->{$head}{$conllmax}{$label} = 1;
    }
  }
}

sub quote {
  my $string = shift;
  return $string if ($string =~ /^\d+$/ && !$string =~ /^0\d+/);
##  return $string if ($string =~ /^[a-z]\w*$/o);
  return $string if ($string =~ /^[a-z][a-zA-Z��������������������������������������]*$/o);
  $string =~ s/\'/\'\'/og;
  # $string =~ s/\'(?!\')/\'\'/og;
  return "\'$string\'";
}

sub qsplit {
  my $s = shift;
  my @s = map {quote($_)} split(/\+/,$s);
  if (scalar(@s) == 1) {
    return $s[0]
  } else {
    return '['.join(',',@s).']';
  }
}

sub qsplit_pos {
  my $s = shift;
  my $i = 1;
  my @s = map {quote($i++.":$_")} split(/[+_]/,$s);
  if (scalar(@s) == 1) {
    return $s[0]
  } else {
    return '['.join(',',@s).']';
  }
}

sub get_actions {
  my ($head,$min,$max) = @_;
  exists $memoactions->{$head} and return;
  $memoactions->{$head} = 1;
  my $entry = $tree->{$head};
  my @before = sort {$a <=> $b} grep {$_ < $head} keys %$entry;
  my @after = sort {$a <=> $b} grep {$_ > $head} keys %$entry;
  if (defined $min) {
    foreach my $b (grep {$_ <= $min} @before) {
      print <<EOF;
%% not projective E$sid $head -> $b : min=$min
oracle!notproj($b,$head).
EOF
    }
  }
  if (defined $max) {
    foreach my $a (grep {$_ >= $max} @after) {
      print <<EOF;
%% not projective E$sid $head -> $a : max=$max
oracle!notproj($a,$head).
EOF
    }
  }
  my @actions = ();
  my $lmin = $min;
  my @before2 = @before;
  while (@before2) {
    my $dep = shift(@before2);
    my $lmax = @before2 ? $before2[0] : $head;
    push(@actions,get_actions($dep,$lmin,$lmax));
    $lmin = $dep;
  }
  if ($head) {
    my $id = exists $segmap->{$head} ? $segmap->{$head} : 0;
    my $id2 = exists $segmap->{$head+1} ? $segmap->{$head+1} : 0;
    my $id3 = exists $segmap->{$head+2} ? $segmap->{$head+2} : 0;
    push(@actions,["shift($id,$id2,$id3)","$head",$ids->{$id}]);
print <<EOF;
oracle!shift($id,$id2,$id3).
EOF
  }
  foreach my $dep (reverse @before) {
    foreach my $label (sort {$a cmp $b} keys %{$entry->{$dep}}) {
      push(@actions,["right($label)","$dep $head"]);
    }
  }
  $lmin = $head;
  while (@after) {
    my $dep = shift(@after);
    my $lmax = @after ? $after[0] : $max;
    my @tmp =  get_actions($dep,$lmin,$lmax);
    foreach my $label (sort {$a cmp $b} keys %{$entry->{$dep}}){
      push(@actions,
	   @tmp,
	   ["left($label)","$dep $head"]
	  );
    }
    $lmin = $dep;
  }
  return @actions;
}

sub check_void {
  my ($data,$key) = @_;
  return (!exists $data->{$key}) || !%{$data->{$key}};
}

sub graph_get_actions {
  ## produce oracle for DAG actions (shift, reduce(L), attach(L), pop0, pop1
  my @stack = (0);
  my $pos  = 1;
  my @actions=();
  my $rtree={};
  foreach my $head (keys %$tree) {
    foreach my $dep (keys %{$tree->{$head}}) {
      $rtree->{$dep}{$head} = 1;
    }
  }
  my $step = 0;
  my $register = sub {
    my ($action,$msg) = @_;
    push(@actions,[$action,$msg]);
    $step++;
    print "%% $step action=$action pos=$pos stack=<@stack>\n";
    if (@stack > 0) {
      my $s = $stack[0];
      my @deps = keys %{$tree->{$s}};
      my @govs = keys %{$rtree->{$s}};
      print "%%\tstack0 $s deps=<@deps> govs=<@govs>\n";
    }
    if (@stack > 1) {
      my $s = $stack[1];
      my @deps = keys %{$tree->{$s}};
      my @govs = keys %{$rtree->{$s}};
      print "%%\tstack1 $s deps=<@deps> govs=<@govs>\n";
    }
  };
  while (1) {
#    print "state pos=$pos stack=<@stack>\n";
    if (@stack > 1
	&& exists $tree->{$stack[0]}{$stack[1]}
       ) {
      ## left reduce or attach
      my $head = $stack[0];
      my $dep = $stack[1];
      my $entry = $tree->{$head}{$dep};
      my @labels = sort {$a cmp $b} keys %$entry;
      delete $tree->{$head}{$dep};
      delete $rtree->{$dep}{$head};
      my $lastaction = (check_void($tree,$dep) && check_void($rtree,$dep)) ? 'right' : 'righta';
      while (@labels) {
	my $label = shift(@labels);
	my $action = scalar(@labels) ? 'righta' : $lastaction;
	$register->("${action}($label)","$dep $head");
      }
      if ($lastaction eq 'right') {
	shift(@stack);
	$stack[0] = $head;
      }
    } elsif (@stack > 1
	     && exists $tree->{$stack[1]}{$stack[0]}
	     && (!(grep {$_ > $stack[0]} keys %{$tree->{$stack[0]}})
		 ||
		 ## we do a lefta action is s1 has no free governor
		 ## followed by a pop1, it allows to get rid of s1 quickly !
		 ( $stack[1]
		   && check_void($rtree,$stack[1])
##		   && grep {$_ < $stack[1]} keys %{$tree->{$stack[0]}}
		 )
		)
	    ) {
      ## right reduce or attach
      my $head = $stack[1];
      my $dep = $stack[0];
      my $entry = $tree->{$head}{$dep};
      my @labels = sort {$a cmp $b} keys %$entry;
      delete $tree->{$head}{$dep};
      %{$tree->{$head}} or delete $tree->{$head};
      delete $rtree->{$dep}{$head};
      %{$rtree->{$dep}} or delete $rtree->{$dep};
      my $lastaction = (check_void($tree,$dep) && check_void($rtree,$dep)) ? 'left' : 'lefta';
      while (@labels) {
	my $label = shift(@labels);
	my $action = scalar(@labels) ? 'lefta' : $lastaction;
	$register->("${action}($label)","$dep $head");
      }
      if ($lastaction eq 'left') {
	shift(@stack);
	$stack[0] = $head;
      }
    } elsif (@stack > 0
	     && $stack[0]
	     && check_void($tree,$stack[0])
	     && check_void($rtree,$stack[0])
	    ) {
      ## topmost element has no governor or governee left
      ## we can pop it
      $register->("pop0","$stack[0]");
      shift(@stack);
    } elsif (@stack > 1
	     && $stack[1]
	     && check_void($tree,$stack[1])
	     && check_void($rtree,$stack[1])
	    ) {
      ## second element has no governor or governee left
      ## we can pop it
      $register->("pop1","$stack[1]");
      my $head = shift(@stack);
      $stack[0] = $head;
    } elsif (exists $segmap->{$pos}) {
      ## we shift, if 
      my $id = $segmap->{$pos};
      my $id2 = exists $segmap->{$pos+1} ? $segmap->{$pos+1} : 0;
      my $id3 = exists $segmap->{$pos+2} ? $segmap->{$pos+2} : 0;
      $register->("shift($id,$id2,$id3)","$pos");
      1 and print <<EOF;
oracle!shift($id,$id2,$id3).
EOF
      unshift(@stack,$pos);
      $pos++;
    } else {
      return @actions;
    }
  }
}

sub keep_alive {
  my ($left,$path,$alive) = @_;
  if (!exists $alive->{$left}) {
    $alive->{$left} = 1;
    my $keep = 0;
    foreach my $right (sort keys %{$path->{$left}}) {
      if (keep_alive($right,$path,$alive)) {
	$keep ||= 1;
      } else {
	delete $path->{$left}{$right};
      }
    }
    if (!$keep) {
      print "%% $left is dead !\n";
      delete $path->{$left};
      $alive->{$left} = 0;
    }
  }
  return $alive->{$left};
}

sub find_best_path {
  my ($elt,$dag) = @_;
  exists $elt->{segcost} or return -1;
  if (!exists $elt->{segbest}) {
    exists $elt->{segcall} and return -1;
    $elt->{segcall} = 1;
    my $right = $elt->{right} + 1;
    my @next = grep {($_->{data}[0] == $right) && exists $_->{segcost}} @$dag;
    my $max = 0;
    my $maxelt;
    foreach my $next (@next) {
      my $m = find_best_path($next,$dag);
      $m > $max or next;
      $max = $m;
      $maxelt = $next;
    }
    $elt->{segbest} = $elt->{segcost} + $max;
    $maxelt and $elt->{segnext} = $maxelt;
  }
  return $elt->{segbest};
}

######################################################################
package Morpho::Arabic;

## inspired from script add_arabic_morph_features_labels.pl
## by authors : Djam� Seddah and Miguel Ballesteros
## distributed in SPMRL 2013

## to be moved in some external module

our $map;

sub init {
  $map or
    $map = {
	    'ADJ.VN' => 'subcat=vn',
	    'ADJ.COMP' => 'subcat=comp',
	    'ADJ.NUM' => 'subcat=num',
	    'ADJ.NUM' => 'subcat=num',
	    'CASE_DEF_ACC' => 'case=acc|casedefinitness=y',
	    'CASE_DEF_GEN' => 'case=gen|casedefinitness=y',
	    'CASE_DEF_NOM' => 'case=nom|casedefinitness=y',
	    'CASE_INDEF_ACC' => 'case=acc|casedefinitness=n',
	    'CASE_INDEF_GEN' => 'case=gen|casedefinitness=n',
	    'CASE_INDEF_NOM' => 'case=nom|casedefinitness=n',
	    'CONNEC_PART' => 'subcat=connec',
	    'CV' => 'aspect=imperative|aspect=imperative',
	    'CVSUFF_DO:1p' => 'catsuff=CVSUFF|aspect=imperative||func=do||pers=1|number=p',
	    'CVSUFF_DO:1'  => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=1|number=s',
	    'CVSUFF_DO:3F' => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=3|gender=f|number=s',
	    'CVSUFF_DO:3MP' => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=3|gender=m|number=p',
	    'CVSUFF_DO:3M' => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=3|gender=m|number=s',
	    'CVSUFF_SUBJ:2F' => 'catsuff=CVSUFF|aspect=imperative||func=subj|pers=2|gender=f|number=s',
	    'CVSUFF_SUBJ:2MP' => 'catsuff=CVSUFF|aspect=imperative||func=subj|pers=2|gender=m|number=p',
	    'CVSUFF_SUBJ:2M' => 'catsuff=CVSUFF|aspect=imperative||func=subj|pers=2|gender=m|number=s',
	    'DEM_PRON_FD' => 'gender=f|number=d',
	    'DEM_PRON_F' => 'gender=f|number=s',
	    'DEM_PRON_MD' => 'gender=m|number=d',
	    'DEM_PRON_MP' => 'gender=m|number=p',
	    'DEM_PRON_P' => 'number=p',
	    'DET_ADJ.VN' => 'subcat=vn',
	    'DET_NOUN_VN' => 'subcat=vn',
	    'DET_NOUN_NUM' => 'subcat=num',
	    'DET_NOUN_PROP' => 'subcat=prop',
	    'DET_NOUN_QUANT' => 'subcat=quant',
	    'EMPHATIC_PART' => 'subcat=emphatic',
	    'FOCUS_PART' => 'subcat=focus',
	    'FUT_PART' => 'subcat=fut',
	    'INTERROG_ADV' => 'subcat=interrog',
	    'INTERROG_PART' => 'subcat=interrog',
	    'INTERROG_PRON' => 'subcat=interrog',
	    'IV1P' => 'aspect=imperfect|pers=1|number=p',
	    'IV1' => 'aspect=imperfect|pers=1|number=s',
	    'IV2D' => 'aspect=imperfect|pers=2|number=d',
	    'IV2FP' => 'aspect=imperfect|pers=2|gender=f|number=p',
	    'IV2F' => 'aspect=imperfect|pers=2|gender=f|number=s',
	    'IV2MP' => 'aspect=imperfect|pers=2|gender=m|number=p',
	    'IV2M' => 'aspect=imperfect|pers=2|gender=m|number=s',
	    'IV3FD' => 'aspect=imperfect|pers=3|gender=f|number=d',
	    'IV3MD' => 'aspect=imperfect|pers=3|gender=m|number=d',
	    'IV3MP' => 'aspect=imperfect|pers=3|gender=m|number=p',
	    'IV3M' => 'aspect=imperfect|pers=3|gender=m|number=s',
	    'IVSUFF_DO:1P' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=1|number=p',
	    'IVSUFF_DO:1' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=1|number=s',
	    'IVSUFF_DO:2F' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=2|gender=f|number=s',
	    'IVSUFF_DO:2MP' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=2|gender=m|number=p',
	    'IVSUFF_DO:2M' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=2|gender=m|number=s',
	    'IVSUFF_DO:3D' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|number=d',
	    'IVSUFF_DO:3F' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|gender=f|number=s',
	    'IVSUFF_DO:3MP' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|gender=m|number=p',
	    'IVSUFF_DO:3M' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|gender=m|number=s',
	    'IVSUFF_MOOD:I' => 'catsuff=IVSUFF|aspect=imperfect|mood=i',
	    'IVSUFF_MOOD:J' => 'catsuff=IVSUFF|aspect=imperfect|mood=j',
	    'IVSUFF_MOOD:' => 'catsuff=IVSUFF|aspect=imperfect|mood=s',
	    'IVSUFF_SUBJ:2FS_MOOD:I' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|pers=2|fs|mood=i',
	    'IVSUFF_SUBJ:2FS_MOOD:SJ' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|pers=2|fs|mood=sj',
	    'IVSUFF_SUBJ:3F' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|pers=3|gender=f|number=s',
	    'IVSUFF_SUBJ:D_MOOD:I' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|number=d|mood=i',
	    'IVSUFF_SUBJ:D_MOOD:SJ' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|number=d|mood=sj',
	    'IV_PAS' => 'iv=pass',
	    'IV' => 'aspect=imperfect',
	    'JUS_PART' => 'subcat=JU',
	    'NEG_PART' => 'subcat=NEG',
	    'NOUN.VN' => 'subcat=vn',
	    'NOUN_NUM' => 'subcat=num',
	    'NOUN_PROP' => 'subcat=prop',
	    'NOUN_QUANT' => 'subcat=quant',
	    'NSUFF_FEM_DU_ACC' => 'catsuff=NSUFF|gender=fem|number=d|case=acc',
	    'NSUFF_FEM_DU_ACCGEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=accgen',
	    'NSUFF_FEM_DU_ACC_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=acc',
	    'NSUFF_FEM_DU_GEN' => 'catsuff=NSUFF|gender=fem|number=d|case=gen',
	    'NSUFF_FEM_DU_GEN' => 'catsuff=NSUFF|gender=fem|number=d|case=gen',
	    'NSUFF_FEM_DU_GEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=gen',
	    'NSUFF_FEM_DU_NOM' => 'catsuff=NSUFF|gender=fem|number=d|case=nom',
	    'NSUFF_FEM_DU_NOM_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=nom',
	    'NSUFF_FEM_PL' => 'catsuff=NSUFF|gender=fem|number=p',
	    'NSUFF_FEM_SG' => 'catsuff=NSUFF|gender=fem|number=s',
	    'NSUFF_FEM_SG' => 'catsuff=NSUFF|gender=fem|number=s',
	    'NSUFF_M_SG' => 'catsuff=NSUFF|gender=fem|number=s',
	    'NSUFF_MASC_DU_ACC' => 'catsuff=NSUFF|gender=masc|number=d|case=acc',
	    'NSUFF_MASC_DU_ACCGEN' => 'catsuff=NSUFF|gender=masc|number=d|case=accgen',
	    'NSUFF_MASC_DU_ACCGEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=accgen',
	    'NSUFF_MASC_DU_ACC_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=acc',
	    'NSUFF_MASC_DU_GEN' => 'catsuff=NSUFF|gender=masc|number=d|case=gen',
	    'NSUFF_MASC_DU_GEN' => 'catsuff=NSUFF|gender=masc|number=d|case=gen',
	    'NSUFF_MASC_DU_GEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=gen',
	    'NSUFF_MASC_DU_NOM' => 'catsuff=NSUFF|gender=masc|number=d|case=nom',
	    'NSUFF_MASC_DU_NOM_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=nom',
	    'NSUFF_MASC_PL' => 'catsuff=NSUFF|gender=masc|number=p',
	    'NSUFF_MASC_PL_ACC' => 'catsuff=NSUFF|gender=masc|number=p|case=acc',
	    'NSUFF_MASC_PL_ACCGEN' => 'catsuff=NSUFF|gender=masc|number=p|case=accgen',
	    'NSUFF_MASC_PL_ACCGEN_PO' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=accgen',
	    'NSUFF_MASC_PL_ACC_PO' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=acc',
	    'NSUFF_MASC_PL_GEN' => 'catsuff=NSUFF|gender=masc|number=p|case=gen',
	    'NSUFF_MASC_PL_GEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=gen',
	    'NSUFF_MASC_PL_NOM' => 'catsuff=NSUFF|gender=masc|number=p|case=nom',
	    'NSUFF_MASC_PL_NOM_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=nom',
	    'POSS_PRON1P' => 'subcat=poss|pers=1|number=p',
	    'POSS_PRON1S' => 'subcat=poss|pers=1|number=s',
	    'POSS_PRON2D' => 'subcat=poss|pers=2|number=d',
	    'POSS_PRON2FP' => 'subcat=poss|pers=2|gender=f|number=p',
	    'POSS_PRON2FS' => 'subcat=poss|pers=2|gender=f|number=s',
	    'POSS_PRON2MP' => 'subcat=poss|pers=2|gender=m|number=p',
	    'POSS_PRON2MS' => 'subcat=poss|pers=2|gender=m|number=s',
	    'POSS_PRON3D' => 'subcat=poss|pers=3|number=d',
	    'POSS_PRON3FP' => 'subcat=poss|pers=3|gender=f|number=p',
	    'POSS_PRON3FS' => 'subcat=poss|pers=3|gender=f|number=s',
	    'POSS_PRON3MP' => 'subcat=poss|pers=3|gender=m|number=p',
	    'POSS_PRON3MS' => 'subcat=poss|pers=3|gender=m|number=s',
	    'POSS_PRON_1P' => 'subcat=poss|pers=1|number=p',
	    'POSS_PRON_1S' => 'subcat=poss|pers=1|number=s',
	    'POSS_PRON_2D' => 'subcat=poss|pers=2|number=d',
	    'POSS_PRON_2FP' => 'subcat=poss|pers=2|gender=f|number=p',
	    'POSS_PRON_2FS' => 'subcat=poss|pers=2|gender=f|number=s',
	    'POSS_PRON_2MP' => 'subcat=poss|pers=2|gender=m|number=p',
	    'POSS_PRON_2MS' => 'subcat=poss|pers=2|gender=m|number=s',
	    'POSS_PRON_3D' => 'subcat=poss|pers=3|number=d',
	    'POSS_PRON_3FP' => 'subcat=poss|pers=3|gender=f|number=p',
	    'POSS_PRON_3FS' => 'subcat=poss|pers=3|gender=f|number=s',
	    'POSS_PRON_3MP' => 'subcat=poss|pers=3|gender=m|number=p',
	    'POSS_PRON_3MS' => 'subcat=poss|pers=3|gender=m|number=s',
	    'PRON1P' => 'pers=1|number=p',
	    'PRON1S' => 'pers=1|number=s',
	    'PRON2FP' => 'pers=2|gender=f|number=p',
	    'PRON2FS' => 'pers=2|gender=f|number=s',
	    'PRON2MP' => 'pers=2|gender=m|number=p',
	    'PRON2MS' => 'pers=2|gender=m|number=s',
	    'PRON3D' => 'pers=3|number=d',
	    'PRON3FP' => 'pers=3|gender=f|number=p',
	    'PRON3FS' => 'pers=3|gender=f|number=s',
	    'PRON3MP' => 'pers=3|gender=m|number=p',
	    'PRON3MP' => 'pers=3|gender=m|number=p',
	    'PRON_1P' => 'pers=1|number=p',
	    'PRON_1S' => 'pers=1|number=s',
	    'PRON_2FP' => 'pers=2|gender=f|number=p',
	    'PRON_2FS' => 'pers=2|gender=f|number=s',
	    'PRON_2MP' => 'pers=2|gender=m|number=p',
	    'PRON_2MS' => 'pers=2|gender=m|number=s',
	    'PRON_3D' => 'pers=3|number=d',
	    'PRON_3FP' => 'pers=3|gender=f|number=p',
	    'PRON_3FS' => 'pers=3|gender=f|number=s',
	    'PRON_3MP' => 'pers=3|gender=m|number=p',
	    'PRON_3MS' => 'pers=3|gender=m|number=s',
	    'PVSUFF|pers=3|M' => 'catsuff=PVSUFF|aspect=perfect|pers=3|gender=m|number=s',
	    'PVSUFF_DO:1P' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=1|number=p',
	    'PVSUFF_DO:1S' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=1|number=s',
	    'PVSUFF_DO:2FS' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=2|gender=f|number=s',
	    'PVSUFF_DO:2MP' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=2|gender=m|number=p',
	    'PVSUFF_DO:2MS' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=2|gender=m|number=s',
	    'PVSUFF_DO:3D' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=3|number=d',
	    'PVSUFF_DO:3FS' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=3|gender=f|number=s',
	    'PVSUFF_DO:3MP' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=3|gender=m|number=p',
	    'PVSUFF_SUBJ:1P' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=1|number=p',
	    'PVSUFF_SUBJ:1S' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=1|number=s',
	    'PVSUFF_SUBJ:2FS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=2|gender=f|number=s',
	    'PVSUFF_SUBJ:2MP' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=2|gender=m|number=p',
	    'PVSUFF_SUBJ:2MS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=2|gender=m|number=s',
	    'PVSUFF_SUBJ:3FD' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=f|number=d',
	    'PVSUFF_SUBJ:3FP' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=f|number=p',
	    'PVSUFF_SUBJ:3FS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=f|number=s',
	    'PVSUFF_SUBJ:3MD' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=m|number=d',
	    'PVSUFF_SUBJ:3MP' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=m|number=p',
	    'PVSUFF_SUBJ:3MS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=m|number=s',
	    'PV_PAS' => 'voice=pasive',
	    'RC_PART' => 'subcat=RC',
	    'RESTRIC_PART' => 'subcat=RESTRIC',
	    'VERB_PART' => 'subcat=VERB',
	    'VOC_PART' => 'subcat=VOC',
	   };
}

sub morpho2conll {
  my $s = shift;
  $s =~ s/^DET\+/DET_/o;
  return join('|', grep {$_} map {$map->{$_}} split(/\+/,$s));
}

sub handle {
  my $s = shift;
  init;
  $s =~ s/atbpos=([^\s\|]+)/&morpho2conll($1)/e;
  $s =~ s/^\|//o;
#  print "converted to <$s>\n";
  return $s;
}

1;
