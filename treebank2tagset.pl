#!/usr/bin/env perl

## build a tagset from a CONLL file

use strict;
use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    "old=f",
                            "ldir=f",
			    "enhanced!" => {DEFAULT => 0}
			   );

$config->args();

my $old = $config->old;
my $ldir = $config->ldir; # language directory

my $enhanced = $config->enhanced;

my $deprels = {};
my $fullcats = {};
my $cats = {};
my $feats = {};

while (<>) {
  chomp;
  /^\s*$/ and next;
  my @info = split(/\t+/,$_);
  if ($enhanced) {
    foreach my $deprel (map {$_->[1]}
			map {[split(/:/,$_,2)]}
			split(/\|/,$info[8])) {
      $deprels->{$deprel}++;
    }
  } else {
    my $deprel = $info[7];
    $deprels->{$deprel}++;
  }
  my $fullcat = $info[4];
  $fullcats->{$fullcat}++;
  my $cat = $info[3];
  $cats->{$cat}++;
  foreach my $fv (split(/\|/,$info[5])) {
##    $fv =~ /=/ or next;
    $fv =~ /sentid=/ and next;
    $feats->{$fv}++;
  }
}

print <<EOF;
/* -*- mode:prolog; -*-
 ******************************************************************
 * Copyright (C) 2013, 2014, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie\@inria.fr>
 * ----------------------------------------------------------------
 *
 *  spmrl tagset -- Tagset for SPMRL Data
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

EOF

my $nlabels = scalar(keys %$deprels);

print <<EOF;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dependency labels

% $nlabels labels

EOF

foreach my $deprel (sort {$deprels->{$b} <=> $deprels->{$a}} keys %$deprels) {
  my $x = quote($deprel);
  print <<EOF;
tagset!label($x). %% n=$deprels->{$deprel}
EOF
}


my $nfcats = scalar(keys %$fullcats);

print <<EOF;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Full Cats

% $nfcats full cats

EOF


my $i=0;

foreach my $fullcat (sort {$fullcats->{$b} <=> $fullcats->{$a}} keys %$fullcats) {
  my $x = quote($fullcat);
  print <<EOF;
tagset!fullcat($x,$i). %% n=$fullcats->{$fullcat}
EOF
  $i++;
}

my $ncats = scalar(keys %$cats);

print <<EOF;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simplified Cats

% $ncats cats

EOF

my $i=0;

foreach my $cat (sort {$cats->{$b} <=> $cats->{$a}} keys %$cats) {
  my $x = quote($cat);
  print <<EOF;
tagset!cat($x,$i). %% n=$cats->{$cat}
EOF
  $i++;
}

print <<EOF;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Features
/*
:-x_finite_set(mstag,['-'
EOF

foreach my $fv (sort {$feats->{$b} <=> $feats->{$a}} keys %$feats) {
  my $x = quote($fv);
  print <<EOF;
        , $x    %% n=$feats->{$fv}
EOF
}

print <<EOF;
   ]).
*/
EOF


foreach my $fv (sort {$feats->{$b} <=> $feats->{$a}}
		grep {/^(\w|frmg_label)=/}
		keys %$feats) {
  my ($f,$v) = split(/=/,$fv);
  my $qf = quote($f);
  my $qv = quote($v);
  print <<EOF;
tagset!mstag($qf,$qv).
EOF
}

print "\n";


if (defined $old && -f $old) {
  open(OLD,"<",$old) || die "can't open old $old: $!";
  my $print = 0;
  while(<OLD>) {
    if (/^%%\s*BEGIN\s+KEEP/) {
      $print = 1;
    }
    if ($ldir) {
	s/<ldir>/$ldir/og;
    }
    $print and print $_;
    if (/^%%\s*END\s+KEEP/) {
      $print = 0;
    }
  }
}

sub quote {
  my $string = shift;
  return $string if ($string =~ /^\d+$/);
##  return $string if ($string =~ /^[a-z]\w*$/o);
  return $string if ($string =~ /^[a-z][a-zA-Z��������������������������������������]*$/o);
  $string =~ s/\'/\'\'/og;
  # $string =~ s/\'(?!\')/\'\'/og;
  return "\'$string\'";
}




