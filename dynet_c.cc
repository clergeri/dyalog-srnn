#include "dynet/nodes.h"
#include "dynet/dynet.h"
#include "dynet/training.h"
#include "dynet/timing.h"
#include "dynet/rnn.h"
#include "dynet/gru.h"
#include "dynet/lstm.h"
#include "dynet/dict.h"
#include "dynet/expr.h"
#include "dynet/globals.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/algorithm/string.hpp>

#include <algorithm>
#include <iostream>
#include <iomanip>

#include <fstream>
#include <sstream>
#include <cstdlib>

#include <random>

using namespace std;
using namespace dynet;

// potential char lstm
unsigned CHAR_DIM = 8;
unsigned CHAR_HIDDEN_DIM = 64;
unsigned CHAR_LAYERS = 2;
unsigned CHAR_SIZE= 0;

unsigned LAYERS = 2;

unsigned EMBED_DIM = 100;
unsigned TAG_DIM = 32;
unsigned LABEL_DIM = 16;
//unsigned LABEL_DIM = 32;
unsigned ACTION_DIM = 8;
unsigned DELTA_DIM = 8;

unsigned TAG_SIZE = 0;
unsigned VOCAB_SIZE = 0;
unsigned LABEL_SIZE = 0;
unsigned ACTION_SIZE = 0;

unsigned HIDDEN_DIM = 125;
unsigned TAG_HIDDEN_DIM = 32;

//unsigned STATE_WORDS = 19;      // number of words involved in states (some empty)
//unsigned STATE_WORDS = 16;      // number of words involved in states (some empty)
unsigned STATE_WORDS = 10;      // number of words involved in states (some empty)
//unsigned STATE_WORDS = 4;      // number of words involved in states (some empty)
//unsigned STATE_LABELS = 13;      // number of labels involved in states (some empty)
unsigned STATE_LABELS = 15;      // number of labels involved in states (some empty)
unsigned STATE_ACTIONS = 1;      // number of actions involved in states (some empty)
unsigned STATE_HIDDEN_DIM = 100;

unsigned GPOS_HIDDEN_DIM = 100;

unsigned GMSTAG_HIDDEN_DIM = 50;
unsigned MSTAG_DIM = 8;

unsigned EDGE_HIDDEN_DIM = 150;
//unsigned EDGE_HIDDEN_DIM = 200;
unsigned LABEL_HIDDEN_DIM = 50;
//unsigned LABEL_HIDDEN_DIM = 100;

//double DROPOUT = 0.3;
double DROPOUT = 0.2;

float DYNET_WCOEF = 2.0;       // dynet weight pondering when used with dyalog-sr weights
//Parameter p_wcoef;          // to load/save DYNET_WCOEF as a parameter

unsigned HINGE_LOSS = 0;
unsigned FREEZE_CF = 0;

bool eval = false;
dynet::Dict dword, dtag, dlabel, daction, dchar ;
int kNOTAG, kNOLABEL, kNOACTION;
int kSOS;
int kEOS;
int kUNK;
int kUNKC;
int kSTARTC;
int kENDC;
int kSPACE;
int kTOK;

vector <dynet::Dict*> dmstag;

vector <vector<int>> sentence;
vector <vector<int>> chars;
vector <vector<int>> sentence_mstags;
std::unordered_map<int,pair<int,int>> oracle_edges;
vector <pair<int,int>> edges;
vector <vector<pair<unsigned int,float>>> edge_ranks;

std::unordered_map<int, double> frequencies;
unsigned allfreq = 0;

Model model;
//ParameterCollection model;
Trainer* sgd = nullptr;

float last_weight=0.0;
int second_best_action;
vector <float> weights;
vector <unsigned> ranks;
vector <unsigned> branks;

vector <unsigned> action2base;

bool use_pretrained_embedding = false;
string pretrained_embedding = "";

unsigned MAX_ACTIONS = 0;
unsigned SIMPLE_ACTIONS = 0;
unsigned COMPLEX_ACTIONS = 0;

int stats_pos_errs=0;
int stats_edge_errs=0;
int stats_tokens=0;

// found on http://stackoverflow.com/questions/1577475/c-sorting-and-keeping-track-of-indexes
template <typename T>
vector<unsigned> sort_indexes(const vector<T> &v) {

  // initialize original index locations
  vector<unsigned> idx(v.size());
  iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  sort(idx.begin(), idx.end(),
       [&v](unsigned i1, unsigned i2) {return v[i1] > v[i2];});

  return idx;
}

// given the first character of a UTF8 block, find out how wide it is
// see http://en.wikipedia.org/wiki/UTF-8 for more info
// found in dynet cpp examples
inline unsigned int UTF8Len(unsigned char x) {
  if (x < 0x80) return 1;
  else if ((x >> 5) == 0x06) return 2;
  else if ((x >> 4) == 0x0e) return 3;
  else if ((x >> 3) == 0x1e) return 4;
  else if ((x >> 2) == 0x3e) return 5;
  else if ((x >> 1) == 0x7e) return 6;
  else abort();
}

unsigned _action2base (unsigned action) {
    if (action < SIMPLE_ACTIONS) return action;
    int nlabel = (MAX_ACTIONS - SIMPLE_ACTIONS) / COMPLEX_ACTIONS;
    unsigned base = SIMPLE_ACTIONS + ((action - SIMPLE_ACTIONS) / nlabel);
//    cout << "action2base " << action << " => " << base << endl;
    return base;
}


struct SymbolEmbedding {
  SymbolEmbedding(Model& m, unsigned n, unsigned dim) {
    p_labels = m.add_lookup_parameters(n, {dim});
  }
  void load_embedding(dynet::Dict& d, string pretrain_path){
    ifstream fin(pretrain_path);
       string s;
       while( getline(fin,s) )
       {
        vector <string> fields;
        boost::algorithm::trim(s);
        boost::algorithm::split( fields, s, boost::algorithm::is_any_of(" ") );
        string word = fields[0];
        if (fields.size() == EMBED_DIM+1) {
                // sometimes, word embeddings are badly formatted, because of
                // extra spaces in words
            vector<float> p_embedding;
            for (int ind = 1; ind < fields.size(); ++ind){
                p_embedding.push_back(std::stod(fields[ind]));
            }
            if (d.contains(word) && p_embedding.size() == EMBED_DIM){
                    //cout << "register we for " << word << "\n";
                p_labels.initialize(d.convert(word), p_embedding);
            }
        }
       }
  }
  void new_graph(ComputationGraph& g) { cg = &g; }
  Expression embed(unsigned label_id) {
    return lookup(*cg, p_labels, label_id);
  }
  Expression const_embed(unsigned label_id) {
//    return const_lookup(*cg, p_labels, label_id);
      return const_lookup(*cg, p_labels, label_id);
  }
  ComputationGraph* cg;
  LookupParameter p_labels;
};

template <class Builder>
struct MyBuilder {
    Parameter p_label;
    Parameter p_action;
    SymbolEmbedding* we;        // word embeddings
    SymbolEmbedding* te;        // tag embeddings
    SymbolEmbedding* le;        // label embeddings
    SymbolEmbedding* ae;        // action embeddings
    SymbolEmbedding* ce;        // char embeddings
    SymbolEmbedding* deltae;    // delta distance embeddings
        
    vector <Parameter> mstag_1c; // mstag parameters
    vector <Parameter> mstag_1c_pos; // mstag parameters
    vector <Parameter> mstag_1b; // mstag bias parameters
    vector <Parameter> mstag_2c; // mstag parameters
    vector <Parameter> mstag_2b; // mstag bias parameters
    vector <Parameter> mstag2embed; // mstag 2 embed vector
    
    Builder cbuilder;           // char lstm
//    VanillaLSTMBuilder cbuilder;
    
    Builder l2rbuilder;         // left 2 right word lstm
    Builder r2lbuilder;         // right 2 left word lstm
    Parameter p_f2c;            // word lstm top parameters
    Parameter p_r2c;
    Parameter p_cb;

    Parameter p_gpos2c;         // parameters for gold pos layer
    Parameter p_gpos2b;
    Parameter p_gpos1c;
    Parameter p_gpos1b;

    Parameter p_gpos2embed;
    
            // layer normalization parameters
//        Parameter p_gpos1_norm_gain;
//        Parameter p_gpos1_norm_bias;

    
        // special parameters for feed-forward layers
    Parameter p_c_start;        // start of sentence
    Parameter p_c_end;          // end of sentence
    Parameter p_c_root;         // virtual root
    Parameter p_c_none;         // empty node

    Parameter p_fw_i2h;
    Parameter p_fw_ilabel2h;
    Parameter p_fw_iaction2h;
    Parameter p_fw_i2h_bias;

        // layer normalization parameters
        Parameter p_fw_norm_gain;
        Parameter p_fw_norm_bias;

    Parameter p_fw2_i2h;
    Parameter p_fw2_i2h_bias;

        // layer normalization parameters
        Parameter p_fw2_norm_gain;
        Parameter p_fw2_norm_bias;
    
    Parameter p_fw_actionlabel;
    Parameter p_fw_actionlabel_bias;

    Parameter p_fw_base;
    Parameter p_fw_base_bias;
    Parameter p_fw_base2action;

    Parameter p_char_attention;

    Parameter p_char2word;

    Parameter p_arc_dep_i1c;
    Parameter p_arc_dep_i1b;

    Parameter p_arc_dep_norm_i1c;
    Parameter p_arc_dep_norm_i1b;
    
    Parameter p_arc_dep_i2c;
    Parameter p_arc_dep_i2b;
    
    Parameter p_arc_head_i1c;
    Parameter p_arc_head_i1b;

    Parameter p_arc_head_norm_i1c;
    Parameter p_arc_head_norm_i1b;

    Parameter p_arc_head_i2c;
    Parameter p_arc_head_i2b;
    Parameter p_arc_i1c;
    Parameter p_arc_i1b;
    Parameter p_delta_i1c;

    Parameter p_arc_ghead_12c;
    
    Parameter p_label_dep_i1c;
    Parameter p_label_dep_i1b;
    Parameter p_label_dep_i2c;
    Parameter p_label_dep_i2b;

    Parameter p_label_head_i1c;
    Parameter p_label_head_i1b;
    Parameter p_label_head_i2c;
    Parameter p_label_head_i2b;

    Parameter p_label_ghead_i1c;
    Parameter p_label_ghead_i1b;
    Parameter p_label_ghead_i2c;
    Parameter p_label_ghead_i2b;

    Parameter p_label_depx_i1c;
    Parameter p_label_headx_i1c;
    Parameter p_label_gheadx_i1c;
    
    Parameter p_label_i1b;
    Parameter p_label_i1t;      // tensor


        
    vector <Expression> BiLSTM_results;
    
    vector <Expression> state;
    vector <Expression> state_labels;
    vector <Expression> state_actions;

    vector <Expression> updates;
    Expression nupdate;
    Expression pupdate;

    unsigned steps;
    unsigned checkpoints = 0;
    unsigned no_checkpoints = 0;

    Expression i_fw_i2h;
    Expression i_fw_ilabel2h;
    Expression i_fw_iaction2h;
    Expression i_fw_i2h_bias;
    Expression i_fw_actionlabel;
    Expression i_fw_actionlabel_bias;
    
    Expression i_fw_norm_gain;
    Expression i_fw_norm_bias;
    
    Expression i_fw2_i2h;
    Expression i_fw2_i2h_bias;
    
    Expression i_fw2_norm_gain;
    Expression i_fw2_norm_bias;
    
    Expression i_fw_base;
    Expression i_fw_base_bias;
    Expression i_fw_base2action;

    
//    ComputationGraph* cgptr;
    
    explicit MyBuilder(Model &model) :
            l2rbuilder(LAYERS, EMBED_DIM+TAG_DIM, HIDDEN_DIM, model),
            r2lbuilder(LAYERS, EMBED_DIM+TAG_DIM, HIDDEN_DIM, model),
            cbuilder(CHAR_LAYERS, CHAR_DIM, CHAR_HIDDEN_DIM, model)
        {

            // base input parameters
            
        p_label = model.add_parameters({LABEL_SIZE,LABEL_DIM});

        we = new SymbolEmbedding(model, VOCAB_SIZE, EMBED_DIM);
        if (use_pretrained_embedding) {
            we->load_embedding(dword,pretrained_embedding);
        }
        te = new SymbolEmbedding(model, TAG_SIZE, TAG_DIM);
        le = new SymbolEmbedding(model, LABEL_SIZE, LABEL_DIM);
        ae = new SymbolEmbedding(model, ACTION_SIZE, ACTION_DIM);
        ce = new SymbolEmbedding(model, CHAR_SIZE, CHAR_DIM);
        deltae = new SymbolEmbedding(model, 40, DELTA_DIM);
        
        unsigned LSTM_OUTPUT_DIM = 2 * HIDDEN_DIM;
//        unsigned STATE_WORD_INPUT_DIM = LSTM_OUTPUT_DIM + TAG_SIZE;
        unsigned STATE_WORD_INPUT_DIM = LSTM_OUTPUT_DIM + TAG_DIM;
            /*
        for (unsigned i=0; i < dmstag.size(); i++) {
            STATE_WORD_INPUT_DIM += dmstag[i]->size();
        }
            */
        unsigned HALF_STATE_WORD_INPUT_DIM = STATE_WORD_INPUT_DIM;
//        STATE_WORD_INPUT_DIM *= 2;
        cout << "STATE_WORD_INPUT_DIM=" << STATE_WORD_INPUT_DIM << endl;
        
        p_char_attention = model.add_parameters({CHAR_HIDDEN_DIM,1});
        p_char2word = model.add_parameters({EMBED_DIM,2*CHAR_HIDDEN_DIM});
        
        
//        unsigned LSTM_OUTPUT_DIM = HIDDEN_DIM;
        
            // parameters for combinator on top of biLSTM
//        p_f2c = model.add_parameters({LSTM_OUTPUT_DIM, HIDDEN_DIM});
//        p_r2c = model.add_parameters({LSTM_OUTPUT_DIM, HIDDEN_DIM});
//        p_cb = model.add_parameters({LSTM_OUTPUT_DIM});

            // parameters for gold pos layer
        p_gpos1c = model.add_parameters({GPOS_HIDDEN_DIM,LSTM_OUTPUT_DIM});
        p_gpos1b = model.add_parameters({GPOS_HIDDEN_DIM});
        p_gpos2c = model.add_parameters({TAG_SIZE,GPOS_HIDDEN_DIM});
        p_gpos2b = model.add_parameters({TAG_SIZE});
        p_gpos2embed = model.add_parameters({TAG_DIM,TAG_SIZE});
        
//        p_gpos1_norm_gain = model.add_parameters({GPOS_HIDDEN_DIM});
//        p_gpos1_norm_bias = model.add_parameters({GPOS_HIDDEN_DIM});
        
            // parameters for mstag vocabs
        for(int i=0; i < dmstag.size(); i++) {
            unsigned vocab_size = dmstag[i]->size();
//            cout << "\tprocess mstag vocab " << i << " vocab_size=" << vocab_size << "\n";
            mstag_1c.push_back(model.add_parameters({GMSTAG_HIDDEN_DIM,LSTM_OUTPUT_DIM}));
//            mstag_1c_pos.push_back(model.add_parameters({GMSTAG_HIDDEN_DIM,TAG_SIZE}));
            mstag_1c_pos.push_back(model.add_parameters({GMSTAG_HIDDEN_DIM,TAG_DIM}));
            mstag_1b.push_back(model.add_parameters({GMSTAG_HIDDEN_DIM}));
            mstag_2c.push_back(model.add_parameters({vocab_size,GMSTAG_HIDDEN_DIM}));
            mstag_2b.push_back(model.add_parameters({vocab_size}));
            mstag2embed.push_back(model.add_parameters({TAG_DIM,vocab_size}));
        }

            // parameters for some special "words" used in parser states
        p_c_start = model.add_parameters({STATE_WORD_INPUT_DIM}); 
        p_c_end = model.add_parameters({STATE_WORD_INPUT_DIM});
        p_c_root = model.add_parameters({HALF_STATE_WORD_INPUT_DIM}); // *** special
        p_c_none = model.add_parameters({STATE_WORD_INPUT_DIM});

            // parameters for the feed-forward network on top of the parser states
        p_fw_i2h = model.add_parameters({STATE_HIDDEN_DIM,STATE_WORDS * STATE_WORD_INPUT_DIM});
        p_fw_i2h_bias = model.add_parameters({STATE_HIDDEN_DIM});
        p_fw_ilabel2h = model.add_parameters({STATE_HIDDEN_DIM,STATE_LABELS * LABEL_DIM});
        p_fw_iaction2h = model.add_parameters({STATE_HIDDEN_DIM,STATE_ACTIONS * ACTION_DIM});

        p_fw_norm_gain = model.add_parameters({STATE_HIDDEN_DIM});
        p_fw_norm_bias = model.add_parameters({STATE_HIDDEN_DIM});
        
        p_fw2_i2h = model.add_parameters({STATE_HIDDEN_DIM,STATE_HIDDEN_DIM});
        p_fw2_i2h_bias = model.add_parameters({STATE_HIDDEN_DIM});

        p_fw2_norm_gain = model.add_parameters({STATE_HIDDEN_DIM});
        p_fw2_norm_bias = model.add_parameters({STATE_HIDDEN_DIM});
        
        p_fw_base = model.add_parameters({SIMPLE_ACTIONS + COMPLEX_ACTIONS,STATE_HIDDEN_DIM});
        p_fw_base_bias = model.add_parameters({SIMPLE_ACTIONS + COMPLEX_ACTIONS});
        p_fw_base2action = model.add_parameters({MAX_ACTIONS,SIMPLE_ACTIONS+COMPLEX_ACTIONS});
                
        p_fw_actionlabel = model.add_parameters({MAX_ACTIONS,STATE_HIDDEN_DIM});
        p_fw_actionlabel_bias = model.add_parameters({MAX_ACTIONS});

        p_arc_dep_i1c = model.add_parameters({EDGE_HIDDEN_DIM,HALF_STATE_WORD_INPUT_DIM});
        p_arc_dep_i1b = model.add_parameters({EDGE_HIDDEN_DIM});
        p_arc_dep_i2c = model.add_parameters({EDGE_HIDDEN_DIM,EDGE_HIDDEN_DIM});
        p_arc_dep_i2b = model.add_parameters({EDGE_HIDDEN_DIM});

        p_arc_dep_norm_i1c = model.add_parameters({EDGE_HIDDEN_DIM});
        p_arc_dep_norm_i1b = model.add_parameters({EDGE_HIDDEN_DIM});

        p_arc_head_i1c = model.add_parameters({EDGE_HIDDEN_DIM,HALF_STATE_WORD_INPUT_DIM});
        p_arc_head_i1b = model.add_parameters({EDGE_HIDDEN_DIM});
        p_arc_head_i2c = model.add_parameters({EDGE_HIDDEN_DIM,EDGE_HIDDEN_DIM});
        p_arc_head_i2b = model.add_parameters({EDGE_HIDDEN_DIM});

        p_arc_head_norm_i1c = model.add_parameters({EDGE_HIDDEN_DIM});
        p_arc_head_norm_i1b = model.add_parameters({EDGE_HIDDEN_DIM});

        p_arc_i1c = model.add_parameters({EDGE_HIDDEN_DIM,EDGE_HIDDEN_DIM},ParameterInitSaxe());
        p_arc_i1b = model.add_parameters({EDGE_HIDDEN_DIM});
        p_delta_i1c = model.add_parameters({DELTA_DIM,EDGE_HIDDEN_DIM});

        p_arc_ghead_12c = model.add_parameters({EDGE_HIDDEN_DIM,EDGE_HIDDEN_DIM},ParameterInitSaxe());
        
        p_label_dep_i1c = model.add_parameters({LABEL_HIDDEN_DIM,HALF_STATE_WORD_INPUT_DIM});
        p_label_dep_i1b = model.add_parameters({LABEL_HIDDEN_DIM});
        p_label_dep_i2c = model.add_parameters({LABEL_HIDDEN_DIM,LABEL_HIDDEN_DIM});
        p_label_dep_i2b = model.add_parameters({LABEL_HIDDEN_DIM});

        p_label_head_i1c = model.add_parameters({LABEL_HIDDEN_DIM,HALF_STATE_WORD_INPUT_DIM});
        p_label_head_i1b = model.add_parameters({LABEL_HIDDEN_DIM});
        p_label_head_i2c = model.add_parameters({LABEL_HIDDEN_DIM,LABEL_HIDDEN_DIM});
        p_label_head_i2b = model.add_parameters({LABEL_HIDDEN_DIM});

        p_label_ghead_i1c = model.add_parameters({LABEL_HIDDEN_DIM,HALF_STATE_WORD_INPUT_DIM});
        p_label_ghead_i1b = model.add_parameters({LABEL_HIDDEN_DIM});
        p_label_ghead_i2c = model.add_parameters({LABEL_HIDDEN_DIM,LABEL_HIDDEN_DIM});
        p_label_ghead_i2b = model.add_parameters({LABEL_HIDDEN_DIM});

        p_label_depx_i1c = model.add_parameters({LABEL_SIZE,LABEL_HIDDEN_DIM});
        p_label_headx_i1c = model.add_parameters({LABEL_SIZE,LABEL_HIDDEN_DIM});
        p_label_gheadx_i1c = model.add_parameters({LABEL_SIZE,LABEL_HIDDEN_DIM});
        
        p_label_i1b = model.add_parameters({LABEL_SIZE});
        p_label_i1t = model.add_parameters({LABEL_SIZE,LABEL_HIDDEN_DIM,LABEL_HIDDEN_DIM});
        
        updates.clear();
        state.resize(STATE_WORDS);
        state_labels.resize(STATE_LABELS);
        state_actions.resize(STATE_ACTIONS);
        
        }
    
    void BuildBiLSTMGraph (const vector<vector<int>> &sent,
                           const vector<vector<int>> &csent,
                           const vector<vector<int>> &msent, // vector of mstags
                                                             // for each word
                           ComputationGraph& cg,
                           int train=0) {
        const unsigned slen = sent.size()-4;
        cout << "building lstm length=" << slen << "\n";
        
        cbuilder.new_graph(cg);
        l2rbuilder.new_graph(cg);  // reset RNN builder for new graph
        r2lbuilder.new_graph(cg);  // reset RNN builder for new graph

        if (1 && train) {
            cbuilder.set_dropout(DROPOUT);
            l2rbuilder.set_dropout(DROPOUT);
            r2lbuilder.set_dropout(DROPOUT);
        }

        cbuilder.start_new_sequence();
        l2rbuilder.start_new_sequence();
        r2lbuilder.start_new_sequence();


        vector<Expression> char_lstm;
        vector<Expression> i_words(slen);
        vector<Expression> fwds(slen);
        vector<Expression> revs(slen);
        vector<Expression> res(4+slen);
        BiLSTM_results = res;
        
        BiLSTM_results[0] = parameter(cg,p_c_root);
        BiLSTM_results[1] = parameter(cg,p_c_none);
        BiLSTM_results[2] = parameter(cg,p_c_start);
        BiLSTM_results[3] = parameter(cg,p_c_end);

        Expression i_char_attention = parameter(cg,p_char_attention);
        Expression i_char2word = parameter(cg,p_char2word);
        
        we->new_graph(cg);
        te->new_graph(cg);
        le->new_graph(cg);
        ae->new_graph(cg);
        ce->new_graph(cg);
        deltae->new_graph(cg);
        
        Expression currc = cbuilder.add_input(ce->embed(kSTARTC));
        Expression currs = cbuilder.c.back().back();
                        
            // read sequence from left to right
        l2rbuilder.add_input(concatenate({we->embed(kSOS) + i_char2word * concatenate({currs,currc}),te->embed(kNOTAG)}));
        
        for (unsigned t = 0; t < slen; ++t) {
            vector <Expression> chars;
            for (auto i=0; i < csent[t+4].size(); ++i) {
                Expression ic = ce->embed(csent[t+4][i]);
                if (0 && train) ic = dropout(ic,DROPOUT);
                Expression c = cbuilder.add_input(ic);
                chars.push_back(c);                
            }
            currc = cbuilder.back();
            currs = cbuilder.c.back().back(); // c vector of last layer of last cell
                        
            Expression word = we->const_embed(sent[t+4][0]);
            Expression tag = te->embed(sent[t+4][1]);

            if (0 && train) {
                std::uniform_real_distribution<> dist(0, 1);
                double p = dist(*rndeng);
                double freq = frequencies[sent[t+4][0]];
                if (p < freq) {
                    word = we->embed(kUNK);
//                    tag = dropout(tag,0.25);
//                    currc = dropout(currc,0.25);
//                    currs = dropout(currs,0.25);
                }
            }
            if (1 && train) {
                std::uniform_real_distribution<> dist(0, 1);
                double pword = dist(*rndeng);
                double ptag = dist(*rndeng);
                if (pword < DROPOUT && ptag < DROPOUT) {
                    word = zeroes(cg,Dim({EMBED_DIM}));
                    tag = zeroes(cg,Dim({TAG_DIM}));
                } else if (pword < DROPOUT) {
                    word = zeroes(cg,Dim({EMBED_DIM}));
                    tag = tag / DROPOUT;
                } else if (ptag < DROPOUT) {
                    tag = zeroes(cg,Dim({TAG_DIM}));
                    word = word / DROPOUT;
                }
            }
            Expression cmat_attn = concatenate_cols(chars);
            Expression weight_attn = softmax(transpose(cmat_attn) * i_char_attention);
            Expression attn = cmat_attn * weight_attn;

//            Expression x = concatenate({word,tag,currs,attn});
//            Expression x = concatenate({word,tag,currc,attn});
            Expression x = concatenate({word+i_char2word * concatenate({currs,attn}),tag});
            if (0 && train) {
//                x = dropout(x,0.2);
                x = dropout(x,DROPOUT);
                    //x = dropout(x,frequencies[sent[t+4].first]);
            }
            i_words[t] = x;
            fwds[t] = l2rbuilder.add_input(x);
        }

//        cout << "here2\n";
        
        currc = cbuilder.add_input(ce->embed(kENDC));
        currs = cbuilder.c.back().back(); // c vector of last layer of last cell
        
//        cout << "here3\n";
        
            // read sequence from right to left
//        r2lbuilder.add_input(concatenate({we->embed(kEOS),te->embed(kNOTAG),currc,currc}));
        r2lbuilder.add_input(concatenate({we->embed(kEOS)+i_char2word * concatenate({currs,currc}),te->embed(kNOTAG)}));
        
        for (unsigned t = 0; t < slen; ++t)
            revs[slen - t - 1] = r2lbuilder.add_input(i_words[slen - t - 1]);

            // top layer
        for (unsigned t = 0; t < slen; ++t) {
            Expression x = BiLSTM_results[t+4] = concatenate({fwds[t], revs[t]});
            cg.incremental_forward(x);
        }

        Expression i_gpos1c = parameter(cg,p_gpos1c);
        Expression i_gpos1b = parameter(cg,p_gpos1b);
        Expression i_gpos2c = parameter(cg,p_gpos2c);
        Expression i_gpos2b = parameter(cg,p_gpos2b);
        Expression i_gpos2embed = parameter(cg,p_gpos2embed);
        
//        Expression i_gpos1_norm_gain = parameter(cg,p_gpos1_norm_gain);
//        Expression i_gpos1_norm_bias = parameter(cg,p_gpos1_norm_bias);

        vector <Expression> errs;
        int nerrs = 0;
        int pos_errs = 0;
        
        for (unsigned t=0; t < slen; ++t) {
            Expression x = BiLSTM_results[t+4];
            int gcat = sent[t+4][2];
            Expression y = elu(affine_transform({i_gpos1b,i_gpos1c,x}));
//            Expression y = tanh(affine_transform({i_gpos1b,i_gpos1c,x}));
//            Expression y = tanh(layer_norm(i_gpos1c * x,i_gpos1_norm_gain, i_gpos1_norm_bias)+i_gpos1b);
            y = affine_transform({i_gpos2b,i_gpos2c,y});
            Expression yy = softmax(y);
//            BiLSTM_results[t+4] = concatenate({BiLSTM_results[t+4],yy});
            Expression yembed = i_gpos2embed * yy;
            Expression yacc = yembed;
//            BiLSTM_results[t+4] = concatenate({BiLSTM_results[t+4],yembed});
            if (train) {
                vector <float> dist = as_vector(cg.incremental_forward(yy));
                double best = -9e99;
                int besti = -1;
                for (auto i=0; i < dist.size(); ++i) {
                    if (dist[i] > best) {
                        best = dist[i];
                        besti = i;
                    }
                }
                if (gcat != kNOTAG) {
                    if (gcat != besti) {
                        nerrs++;
                        pos_errs++;
//                        cout << "pos=" << t << " confusion gcat=" << gcat << " with pcat=" << besti << endl;
                    }
                    errs.push_back(pickneglogsoftmax(y,gcat));
                }
            }
            
            if (1 || train) {
                for (unsigned i=0; i < dmstag.size(); i++) {
//                cout << "graph for mstag vocab " << i << endl;
                    Expression i_1c = parameter(cg,mstag_1c[i]);
                    Expression i_1c_pos = parameter(cg,mstag_1c_pos[i]);
                    Expression i_1b = parameter(cg,mstag_1b[i]);
                    Expression i_2c = parameter(cg,mstag_2c[i]);
                    Expression i_2b = parameter(cg,mstag_2b[i]);
                    Expression i_2embed = parameter(cg,mstag2embed[i]);
//                    Expression z = elu(affine_transform({i_1b,i_1c,x,i_1c_pos,y}));
                    Expression z = elu(affine_transform({i_1b,i_1c,x,i_1c_pos,yembed}));
//                    Expression z = tanh(affine_transform({i_1b,i_1c,x,i_1c_pos,y}));
                    z = affine_transform({i_2b,i_2c,z});
                    Expression zz = softmax(z);
                    yacc = affine_transform({yacc,i_2embed,zz});
//                    BiLSTM_results[t+4] = concatenate({BiLSTM_results[t+4],zz});
                    if (train) {
                        int gmstag = msent[i][t+4];
                        vector <float> dist = as_vector(cg.incremental_forward(zz));
                        double best = -9e99;
                        int besti = -1;
                        for (auto i=0; i < dist.size(); ++i) {
                            if (dist[i] > best) {
                                best = dist[i];
                                besti = i;
                            }
                        }
                        if (gmstag != 0) {
                            if (gmstag != besti) {
                                nerrs++;
                                cout << "mstag=" << t << "." << i  << " confusion gmstag=" << gmstag << " with pmstag=" << besti << endl;
                            }
                            errs.push_back(pickneglogsoftmax(z,gmstag));
                        }
                    }
                }

                BiLSTM_results[t+4] = concatenate({BiLSTM_results[t+4],yacc});
                
            }
        }

            // directly predict edges (no shift-reduce transitions)
        Expression i_arc_dep_i1c = parameter(cg,p_arc_dep_i1c);
        Expression i_arc_dep_i1b = parameter(cg,p_arc_dep_i1b);
        Expression i_arc_dep_i2c = parameter(cg,p_arc_dep_i2c);
        Expression i_arc_dep_i2b = parameter(cg,p_arc_dep_i2b);
        Expression i_arc_head_i1c = parameter(cg,p_arc_head_i1c);
        Expression i_arc_head_i1b = parameter(cg,p_arc_head_i1b);
        Expression i_arc_head_i2c = parameter(cg,p_arc_head_i2c);
        Expression i_arc_head_i2b = parameter(cg,p_arc_head_i2b);

        Expression i_arc_dep_norm_i1c = parameter(cg,p_arc_dep_norm_i1c);
        Expression i_arc_dep_norm_i1b = parameter(cg,p_arc_dep_norm_i1b);
        Expression i_arc_head_norm_i1c = parameter(cg,p_arc_head_norm_i1c);
        Expression i_arc_head_norm_i1b = parameter(cg,p_arc_head_norm_i1b);
        
        Expression i_arc_i1c = parameter(cg,p_arc_i1c);
        Expression i_arc_i1b = parameter(cg,p_arc_i1b);
        Expression i_delta_i1c = parameter(cg,p_delta_i1c);
        Expression i_arc_ghead_12c = parameter(cg,p_arc_ghead_12c);
        
        Expression i_label_dep_i1c = parameter(cg,p_label_dep_i1c);
        Expression i_label_dep_i1b = parameter(cg,p_label_dep_i1b);
        Expression i_label_dep_i2c = parameter(cg,p_label_dep_i2c);
        Expression i_label_dep_i2b = parameter(cg,p_label_dep_i2b);

        Expression i_label_head_i1c = parameter(cg,p_label_head_i1c);
        Expression i_label_head_i1b = parameter(cg,p_label_head_i1b);
        Expression i_label_head_i2c = parameter(cg,p_label_head_i2c);
        Expression i_label_head_i2b = parameter(cg,p_label_head_i2b);

        Expression i_label_ghead_i1c = parameter(cg,p_label_ghead_i1c);
        Expression i_label_ghead_i1b = parameter(cg,p_label_ghead_i1b);
        Expression i_label_ghead_i2c = parameter(cg,p_label_ghead_i2c);
        Expression i_label_ghead_i2b = parameter(cg,p_label_ghead_i2b);
        
        Expression i_label_depx_i1c = parameter(cg,p_label_depx_i1c);
        Expression i_label_headx_i1c = parameter(cg,p_label_headx_i1c);
        Expression i_label_gheadx_i1c = parameter(cg,p_label_gheadx_i1c);
        
        Expression i_label_i1b = parameter(cg,p_label_i1b);
        Expression i_label_i1t = parameter(cg,p_label_i1t);
        
        vector <Expression> deps;
        vector <Expression> heads;
        
        vector <Expression> label_deps;
        vector <Expression> label_heads;
        vector <Expression> label_gheads;
        
        int edge_errs=0;

        vector <Expression> tmp_results;
        
        if (1) {
                // root node
            Expression w = BiLSTM_results[0];
//            tmp_results.push_back(w);
            Expression h_arc_head = i_arc_head_i1c * w;
            h_arc_head = elu(layer_norm(h_arc_head,i_arc_head_norm_i1c,i_arc_head_norm_i1b) + i_arc_head_i1b);
//            Expression h_arc_head = elu(affine_transform({i_arc_head_i1b,i_arc_head_i1c,w}));
            h_arc_head = affine_transform({i_arc_head_i2b,i_arc_head_i2c,h_arc_head});
            heads.push_back(h_arc_head);

            Expression h_label_head = elu(affine_transform({i_label_head_i1b,i_label_head_i1c,w}));
            h_label_head = affine_transform({i_label_head_i2b,i_label_head_i2c,h_label_head});
            label_heads.push_back(h_label_head);

            Expression h_label_ghead = elu(affine_transform({i_label_ghead_i1b,i_label_ghead_i1c,w}));
            h_label_ghead = affine_transform({i_label_ghead_i2b,i_label_ghead_i2c,h_label_ghead});
            label_gheads.push_back(h_label_ghead);

        }

        for (unsigned t=0; t < slen; ++t) {
            Expression w = BiLSTM_results[t+4];
            Expression w2 = w;
            Expression w3 = w;
            Expression w4 = w;
            Expression w5 = w;

            if (train) w = dropout(w,DROPOUT);
            if (train) w2 = dropout(w2,DROPOUT);
            if (train) w3 = dropout(w3,DROPOUT);
            if (train) w4 = dropout(w4,DROPOUT);
            if (train) w5 = dropout(w5,DROPOUT);
            
//            tmp_results.push_back(w);
//            Expression h_arc_dep = elu(affine_transform({i_arc_dep_i1b,i_arc_dep_i1c,w}));
            Expression h_arc_dep = i_arc_dep_i1c * w;
            h_arc_dep = elu(layer_norm(h_arc_dep,i_arc_dep_norm_i1c,i_arc_dep_norm_i1b) + i_arc_dep_i1b);
            h_arc_dep = affine_transform({i_arc_dep_i2b,i_arc_dep_i2c,h_arc_dep});

//            Expression h_arc_head = elu(affine_transform({i_arc_head_i1b,i_arc_head_i1c,w}));
            Expression h_arc_head = i_arc_head_i1c * w2;
            h_arc_head = elu(layer_norm(h_arc_head,i_arc_head_norm_i1c,i_arc_head_norm_i1b) + i_arc_head_i1b);
//            Expression h_arc_head = elu(affine_transform({i_arc_head_i1b,i_arc_head_i1c,w}));
            h_arc_head = affine_transform({i_arc_head_i2b,i_arc_head_i2c,h_arc_head});
            
            deps.push_back(h_arc_dep);
            heads.push_back(h_arc_head);
            
            Expression h_label_dep = elu(affine_transform({i_label_dep_i1b,i_label_dep_i1c,w3}));
            h_label_dep = affine_transform({i_label_dep_i2b,i_label_dep_i2c,h_label_dep});

            Expression h_label_head = elu(affine_transform({i_label_head_i1b,i_label_head_i1c,w4}));
            h_label_head = affine_transform({i_label_head_i2b,i_label_head_i2c,h_label_head});

            Expression h_label_ghead = elu(affine_transform({i_label_ghead_i1b,i_label_ghead_i1c,w5}));
            h_label_ghead = affine_transform({i_label_ghead_i2b,i_label_ghead_i2c,h_label_ghead});
            
            label_deps.push_back(h_label_dep);
            label_heads.push_back(h_label_head);
            label_gheads.push_back(h_label_ghead);
            
        }
        Expression head_mat = transpose(concatenate_cols(heads));
//        Expression res_mat = concatenate_cols(tmp_results);
//        vector <Expression> head_sketches;
        for (unsigned t=0; t < slen; ++t) {

            vector <Expression> deltas;
            deltas.push_back(deltae->embed(0));
            for (unsigned t2=0; t2 < slen; ++t2) {
                int d = t2-t;
                if (d > 19)
                    d = 19;
                else if (d < -19)
                    d = -19;
                deltas.push_back(deltae->embed(20+d));
            }
            Expression delta_mat = transpose(concatenate_cols(deltas));

            Expression h_arc_dep = deps[t];
            Expression h_arc = head_mat * i_arc_i1c * h_arc_dep + head_mat * i_arc_i1b;
            Expression h_delta = delta_mat * i_delta_i1c * h_arc_dep;
            h_arc = cmult(h_arc,h_delta);
            Expression h_weights = softmax(h_arc);
            vector <float> weights = as_vector(cg.incremental_forward(h_weights));
//            vector <float> weights = as_vector(cg.incremental_forward(h_arc));
            vector <unsigned int> ranks = sort_indexes(weights);
            unsigned int head = ranks[0];
//            Expression head_sketch = res_mat * h_weights;
//            head_sketches.push_back(head_sketch);

                // we update head vectors with info about their own head
            heads[t+1] = heads[t+1] + i_arc_ghead_12c * heads[head];
            head_mat = transpose(concatenate_cols(heads));
            
            Expression h_label_dep = label_deps[t];
            Expression h_label_head = label_heads[head];
            if (1 && train) {
                h_label_dep = dropout(h_label_dep,DROPOUT);
                h_label_head = dropout(h_label_head,DROPOUT);
            }

            Expression h_label = contract3d_1d_1d(i_label_i1t,h_label_dep,h_label_head,
                                                  affine_transform({i_label_i1b,i_label_depx_i1c,h_label_dep,i_label_headx_i1c,h_label_head}));

            if (head > 0 && head <= t) {
                    // try to incoporate info from grand-parent
                h_label = h_label + i_label_gheadx_i1c * label_gheads[edges[head-1].first];
            }
//            h_label = elu(h_label);
            vector <float> label_weights = as_vector(cg.incremental_forward(softmax(h_label)));
            vector <unsigned int> label_ranks = sort_indexes(label_weights);
            unsigned int label = label_ranks[0];
            
            edges.push_back(make_pair(head,label));
            vector <pair<unsigned int,float>> ranks2(ranks.size());
            for (auto i=0; i < ranks.size(); i++) {
                ranks2[ranks[i]] = make_pair(i+1,weights[ranks[i]]);
            }
            edge_ranks.push_back(ranks2);
            
//            edges.push_back(make_pair(head,0));
            if (train) {
                unsigned lerr=0;
                if (oracle_edges[t+1].first != head) {
//                cout << "edge error dep=" << t+1 << " pgov=" << head << "
//                ggov=" << oracle_edges[t+1].first << endl;
                    lerr++;
                    errs.push_back(pickneglogsoftmax(h_arc,oracle_edges[t+1].first));
                }
                if (oracle_edges[t+1].second != label) {
                    lerr++;
                    errs.push_back(pickneglogsoftmax(h_label,oracle_edges[t+1].second));
                }
                if (lerr) edge_errs++;
            }
            
        }
        
        if (train) {
            unsigned xlen = slen * (1+dmstag.size());
            stats_pos_errs += pos_errs;
            stats_edge_errs += edge_errs;
            stats_tokens += slen;
            float las = 100 * (stats_tokens - stats_edge_errs) / (1.0 * stats_tokens);
            float f1 = 100 * (stats_tokens - stats_pos_errs) / (1.0 * stats_tokens);
            cout << "update " << nerrs << " gold pos; slength= " << slen << " score " << setiosflags(ios::fixed) << setprecision(2) << 100 * (xlen-nerrs) / xlen << "% global f1=" << f1 <<"%\n";
            cout << "edge update " << edge_errs << setiosflags(ios::fixed) << setprecision(2) << " LAS=" << 100 * (slen - edge_errs) / (1.0 * slen) << "% global LAS=" << las <<"% f1=" << f1 << "%\n";
            if (errs.size() > 0) {
                Expression loss = sum(errs);
                float xloss = as_scalar(cg.incremental_forward(loss));
                cg.backward(loss);
                cout << "loss " << xloss << endl;
            }
        }

/*
         BiLSTM_results[0] = concatenate({BiLSTM_results[0],BiLSTM_results[0]});
         for (unsigned t=0; t < slen; ++t) {
             BiLSTM_results[t+4] = concatenate({BiLSTM_results[t+4],head_sketches[t]});
         }
*/        
                    // the following expressions are stable for a given sentence
                    // (moved from predict)
                
        i_fw_i2h = parameter(cg,p_fw_i2h);
        i_fw_ilabel2h = parameter(cg,p_fw_ilabel2h);
        i_fw_iaction2h = parameter(cg,p_fw_iaction2h);
        i_fw_i2h_bias = parameter(cg,p_fw_i2h_bias);
        i_fw_actionlabel = parameter(cg,p_fw_actionlabel);
        i_fw_actionlabel_bias = parameter(cg,p_fw_actionlabel_bias);

        i_fw_norm_gain = parameter(cg,p_fw_norm_gain);
        i_fw_norm_bias = parameter(cg,p_fw_norm_bias);

        i_fw2_i2h = parameter(cg,p_fw2_i2h);
        i_fw2_i2h_bias = parameter(cg,p_fw2_i2h_bias);

        i_fw2_norm_gain = parameter(cg,p_fw2_norm_gain);
        i_fw2_norm_bias = parameter(cg,p_fw2_norm_bias);

        i_fw_base = parameter(cg,p_fw_base);
        i_fw_base_bias = parameter(cg,p_fw_base_bias);
        i_fw_base2action = parameter(cg,p_fw_base2action);
                
//        cg.print_graphviz();

        cg.checkpoint();
        checkpoints=1;
        steps=0;
        
        cout << "setting bilstm\n";
        cout.flush();
        return;
    }

        unsigned state_wi;
    unsigned state_li;
    unsigned state_ai;
    
    void state_reset (ComputationGraph *cgptr) {
//        cout << "state reset\n";
        state_wi=0;
        state_li=0;
        state_ai=0;
        ranks.clear();
        weights.clear();
    }
    
    void add_state_word (const int i) {
        state[state_wi++] = BiLSTM_results[i];
    }

    void add_state_label (const int i) {
        state_labels[state_li++] = le->embed(i);
    }

    void add_state_action (const int i) {
        state_actions[state_ai++] = ae->embed(i);
    }
    
    int predict (ComputationGraph *cgptr, const int gal, const int nal, const int update, const int beam, const int train) {
//        cout << "predict gal=" << gal << " state_length=" << state.size() << endl;
        Expression sw = concatenate(state);
        Expression sl = concatenate(state_labels);
        Expression sa = concatenate(state_actions);
        
        Expression h1 = i_fw_i2h * sw +  i_fw_ilabel2h * sl + i_fw_iaction2h * sa;
        h1 = elu(layer_norm(h1,i_fw_norm_gain,i_fw_norm_bias) + i_fw_i2h_bias);
//        h1 = tanh(layer_norm(h1,i_fw_norm_gain,i_fw_norm_bias) + i_fw_i2h_bias);
        Expression h = elu(layer_norm(i_fw2_i2h * h1, i_fw2_norm_gain, i_fw2_norm_bias) + i_fw2_i2h_bias);
//        Expression h = tanh(layer_norm(i_fw2_i2h * h1, i_fw2_norm_gain, i_fw2_norm_bias) + i_fw2_i2h_bias);
        
        Expression base = affine_transform({i_fw_base_bias,i_fw_base,h});
        Expression base2 = softmax(base);

        Expression actionlabel = affine_transform({i_fw_actionlabel_bias,i_fw_actionlabel,h,i_fw_base2action,base2});

        weights = as_vector(cgptr->incremental_forward(softmax(actionlabel)));
        ranks = sort_indexes(weights);

        unsigned besti = ranks[0];
        last_weight = weights[besti];
        second_best_action = ranks[1];

        if (gal != -1 && update) { // update case
            vector <float> distbase = as_vector(cgptr->incremental_forward(base2));
            int gbase = action2base[gal];
            double bestbase = -9e99;
            int bestbasei = -1;

            for (int i = 0; i < distbase.size(); ++i) {
                if (distbase[i] > bestbase) {
                    bestbase = distbase[i];
                    bestbasei = i;
                }
            }

            if (update < 0) {
                    // should penalize gal
//                cout << "nupdate action " << gal << endl;
                if (gbase == bestbasei) updates.push_back(update * pickneglogsoftmax(base,gbase));
                if (gal == besti) {
                    if (!FREEZE_CF) DYNET_WCOEF /= 1.001;
                    updates.push_back(update * pickneglogsoftmax(actionlabel,gal));
                }
                cgptr->checkpoint();
                checkpoints++;
            } else {
                    // should favor gal
                updates.push_back(update * pickneglogsoftmax(base,gbase));
                if (gal == besti && !FREEZE_CF) DYNET_WCOEF *= 1.001;
                updates.push_back(update * pickneglogsoftmax(actionlabel,gal));
                if (updates.size() > 100) {
                    apply_updates(cgptr,0);
                } else {
                    cgptr->checkpoint();
                    checkpoints++;
                }
                
            }
            
        } else {
                // no update
            cgptr->revert();
            cgptr->checkpoint();
            
                // store the n-best actions ordered in branks
            vector <int> bactions(SIMPLE_ACTIONS+COMPLEX_ACTIONS);
            unsigned branks_i=0;
            unsigned l = SIMPLE_ACTIONS+(beam+1)*COMPLEX_ACTIONS;
                // size of brank could change when using dynamic beam size (sbeam)
            if (branks.size() != l) branks.resize(l);
            for (auto &a: ranks) {
                if (a < SIMPLE_ACTIONS)
                    branks[branks_i++] = a;
                else if (bactions[action2base[a]]++ <= beam) {
                    branks[branks_i++] = a;
                }
            }
        }
        return besti;
    }

    void apply_updates (ComputationGraph *cgptr, int end) {
        cout << "applying " << updates.size() << " updates ck=" << checkpoints << " end=" << end << endl;
        if (updates.size()) {
            Expression errs = sum(updates);
            cgptr->incremental_forward(errs);
            cgptr->backward(errs);
            sgd->update();
            updates.clear();
        } else if (end) {
            sgd->update();
        }
        
//        cout << "here0\n";
        if (end) {
            checkpoints = 0;
        } else {
            while (checkpoints-- > 0) {
                cgptr->revert();
            }
            cgptr->checkpoint();
            checkpoints++;
        }
//        cout << "here2 ck=" << checkpoints << "\n";
        steps = 0;
        cout << "done updates" << endl;
    }

};

MyBuilder<LSTMBuilder> *mybuilder = nullptr;
//MyBuilder<VanillaLSTMBuilder> *mybuilder = nullptr;
ComputationGraph *mycg = nullptr;

extern "C" void Dynet_Init(int epoch);

void Dynet_Init (int epoch) {
    int argc = 0;
    char **argv = NULL;
    DynetParams params = DynetParams();
    char *mem = std::getenv("DYALOGSR_DYNET_MEM");
    if (mem == NULL) mem = (char *)"2048";
    params.mem_descriptor = mem;
//    params.autobatch= true;
    initialize(params);
//    sgd = new SimpleSGDTrainer(model);
//    sgd = new AdamTrainer(model,0.001,0.9,0.9);
//    sgd = new AdamTrainer(model,0.002,0.9,0.9);
    sgd = new AdamTrainer(model);
//    sgd->clipping_enabled=false;
    if (0 && epoch) {
        sgd->update_epoch();
    }
//    sgd = new MomentumSGDTrainer(model);
//    sgd = new AdadeltaTrainer(model);
    kSOS = dword.convert("<s>");
    kEOS = dword.convert("</s>");
    kUNK = dword.convert("<unk>");
    kNOTAG = dtag.convert("<none>");
    kNOLABEL = dlabel.convert("<none>");
    kNOACTION = daction.convert("<none>");
    kSTARTC = dchar.convert("<^>");
    kENDC = dchar.convert("<$>");
    kSPACE = dchar.convert(" ");
//    kTOK = dchar.convert("<tok>");
    kUNKC = dchar.convert("<unk>");
}

extern "C" int Dynet_Form(char *f);

int Dynet_Form(char *f) 
{
    return dword.convert(f);
}

extern "C" int Dynet_Form_Freq(char *f, int freq);

int Dynet_Form_Freq(char *f, int freq) 
{
    unsigned i = dword.convert(f);
    frequencies[i] = freq;
    allfreq += freq;
    string form = string(f);
    size_t cur = 0;
    while (cur < form.size()) {    // register chars in form
        size_t len = UTF8Len(form[cur]);
        dchar.convert(form.substr(cur,len));
        cur += len;
    }
    
//    cout << "register word " << f << " freq=" << freq << endl;
    return i;
}

extern "C" int Dynet_Label(char *f);

int Dynet_Label(char *l) 
{
    return dlabel.convert(l);
}

extern "C" int Dynet_Tag(char *c);

int Dynet_Tag(char *c) 
{
    return dtag.convert(c);
}

extern "C" int Dynet_Action(char *a);

int Dynet_Action(char *a) 
{
    return daction.convert(a);
}


extern "C" void Dynet_Save_Model(const char * name);

void Dynet_Save_Model (const char *fname) 
{
    ofstream out(fname);
    boost::archive::text_oarchive oa(out);
    oa << DYNET_WCOEF << model;
    out.close();
}

extern "C" void Dynet_Load_Model(const char * name);

void Dynet_Load_Model (const char *fname) 
{
    ifstream in(fname);
    if (in.good()) {
        cout << "loading model from " << fname << "\n";
        boost::archive::text_iarchive ia(in);
        ia >> DYNET_WCOEF >> model;
        in.close();
        use_pretrained_embedding = false;
        cout << "model loaded coef=" << DYNET_WCOEF << "\n";
//        Dynet_Save_Model("loaded.mdl");
    } else {
        cerr << "*** missing dynet model " << fname << "\n";
    }
}

extern "C" void Dynet_Add_Parameters(int actions, int nsimple, int ncomplex,
                                     int layers, int lstm_dim, int tag_dim, int label_dim, int state_dim, int coef,
                                     int char_dim, int char_lstm_dim, int char_lstm_layers,
                                     int state_words,
                                     int hinge_loss,
                                     int freeze_cf,
                                     int dropout,
                                     int edge_hdim,
                                     int label_hdim
                                     ) ;

void Dynet_Add_Parameters (int actions, int nsimple, int ncomplex,
                           int layers, int lstm_dim, int tag_dim, int label_dim, int state_dim, int coef,
                           int char_dim, int char_lstm_dim, int char_lstm_layers,
                           int state_words,
                           int hinge_loss,
                           int freeze_cf,
                           int dropout,
                           int edge_hdim,
                           int label_hdim
                           ) {
    dword.freeze();
    dword.set_unk("<unk>");
    dtag.freeze();
    dtag.set_unk("<unk>");
    dlabel.freeze();
    dlabel.set_unk("<unk>");
    dchar.freeze();
    dchar.set_unk("<unk>");
    for(int i=0; i < dmstag.size(); i++) {
        dmstag[i]->convert("<unk>");
        dmstag[i]->freeze();
        dmstag[i]->set_unk("<unk>");
    }
    CHAR_SIZE = dchar.size();
    VOCAB_SIZE = dword.size();
    TAG_SIZE = dtag.size();
    LABEL_SIZE = dlabel.size();
    ACTION_SIZE = daction.size(); // number of base actions (without labels)
    MAX_ACTIONS = actions;        // number of actions (with labels)
    SIMPLE_ACTIONS = nsimple;
    COMPLEX_ACTIONS = ncomplex;
    LAYERS = layers;
    HIDDEN_DIM = lstm_dim;
    TAG_DIM = tag_dim;
    LABEL_DIM = label_dim;
    STATE_HIDDEN_DIM = state_dim;
    DYNET_WCOEF = 1.0 * coef;
    CHAR_DIM = char_dim;
    CHAR_HIDDEN_DIM = char_lstm_dim;
    CHAR_LAYERS = char_lstm_layers;
    STATE_WORDS = state_words;
    HINGE_LOSS = hinge_loss;
    FREEZE_CF = freeze_cf;
    DROPOUT = (1.0 * dropout) / 100.0;
    EDGE_HIDDEN_DIM = edge_hdim;
    LABEL_HIDDEN_DIM = label_hdim;
    for (auto it = frequencies.begin(); it != frequencies.end(); ++it) {
        double count = it->second;
//        it->second = 0.25 / (5000 * (count/allfreq) + 0.25);
//        it->second = 0.25 / (count + 0.25);
        it->second = 0.25 / (sqrt(1+count) + 0.25);
    }
    delete mybuilder;
    mybuilder = new MyBuilder<LSTMBuilder> (model);
//    mybuilder = new MyBuilder<VanillaLSTMBuilder> (model);
    mycg = nullptr;
    for (auto a=0; a < MAX_ACTIONS; a++) {
        action2base.push_back(_action2base(a));
    }
    cout << VOCAB_SIZE << " words " << TAG_SIZE << " tags " << LABEL_SIZE << " labels " << ACTION_SIZE << " actions " << CHAR_SIZE << " chars\n";
}


extern "C" void Dynet_Register_Embedding(const char *name);

void Dynet_Register_Embedding (const char *fname) 
{
    use_pretrained_embedding = true;
    pretrained_embedding = fname;
}

extern "C" void Dynet_Sentence_Init();

void Dynet_Sentence_Init () 
{
    for(auto x : sentence) x.clear();
    sentence.clear();
    for(auto schars : chars) schars.clear();
    chars.clear();
    for(auto x : sentence_mstags) x.clear();
    oracle_edges.clear();
    edges.clear();
    edge_ranks.clear();
    cout << "start sentence init\n";
}

extern "C" int Dynet_Register_Sentence_Word(const char *form, const char *cat, const char *gcat, const char *form2, int space_after);

int Dynet_Register_Sentence_Word(const char *form, const char *cat, const char *gcat, const char *form2, int space_after)
{
//    cout << "register sentence word " << form << " " << cat << " " << gcat << endl;
    sentence.push_back({dword.convert(form2),dtag.convert(cat),dtag.convert(gcat)});
    string sform = string(form);
    size_t cur = 0;
    vector <int> schars;
    while (cur < sform.size()) {    // register chars in form
        size_t len = UTF8Len(sform[cur]);
        schars.push_back(dchar.convert(sform.substr(cur,len)));
        cur += len;
    }
    if (space_after) {
        schars.push_back(dchar.convert(" "));
        cur++;
    }
    chars.push_back(schars);
    return sentence.size()-1;
}

extern "C" void Dynet_Register_Sentence_Mstag(int mstag_vocab, const char *value);

void Dynet_Register_Sentence_Mstag(int mstag_vocab, const char *value) 
{
    unsigned i = sentence.size()-1;
//    cout << "add sentence mstag at " << i << " vocab=" << mstag_vocab << " value=" << value << endl;
    sentence_mstags[mstag_vocab].push_back(dmstag[mstag_vocab]->convert(value));
}

extern "C" void Dynet_Run_BiLSTM(int train);

void Dynet_Run_BiLSTM(int train){
//    if (mycg) mycg->clear();
    delete mycg;
    mycg = new ComputationGraph;
//    mycg->set_immediate_compute(true);
//    mycg->set_check_validity(true);
    mybuilder->BuildBiLSTMGraph(sentence,chars,sentence_mstags,*mycg,train);
//    cg.forward(expr);
}

extern "C" void Dynet_State_Reset();

void Dynet_State_Reset () 
{
    mybuilder->state_reset(mycg);
}

extern "C" void Dynet_Add_Word_Input(int i);

void Dynet_Add_Word_Input(int i) 
{
    mybuilder->add_state_word(i);
}

extern "C" void Dynet_Add_Label_Input(int i);

void Dynet_Add_Label_Input(int i) 
{
    mybuilder->add_state_label(i);
}

extern "C" void Dynet_Add_Action_Input(int i);

void Dynet_Add_Action_Input(int i) 
{
    mybuilder->add_state_action(i);
}

extern "C" int Dynet_Predict_Action (int gal, int nal, int update, int beam, int train);

int Dynet_Predict_Action (int gal, int nal, int update, int beam=1, int train=0) 
{
//    cout << "predict gal=" << gal << " nal=" << nal << endl;
    int pal = mybuilder->predict(mycg,gal,nal,update,beam,train);
//    cout << "dynet update=" << update << " gal=" << gal << " pal=" << pal << endl;
    return pal;
}

extern "C" void Dynet_Apply_Updates();

void Dynet_Apply_Updates() 
{
    mybuilder->apply_updates(mycg,1);
}

extern "C" long Dynet_Last_Weight();

long Dynet_Last_Weight () 
{
    long w = static_cast<long>(DYNET_WCOEF * last_weight);
//    cout << "best weight " << last_weight << " => " << w << endl;
    return w;
}

extern "C" int Dynet_Second_Best_Action();

int Dynet_Second_Best_Action() 
{
    return second_best_action;
}

extern "C" float Dynet_Get_Weight(int al);

float Dynet_Get_Weight (int al) 
{
    if (al >= MAX_ACTIONS) return -1e10;
//    long res = static_cast<long>(DYNET_WCOEF * weights[al]);
    float res = static_cast<float>(DYNET_WCOEF * weights[al]);
//    cout << "weight " << al << " raw=" << weights[al] << " scaled=" << res << endl;
    weights[al] = -1e10;
    return res;
}

extern "C" int Dynet_Get_NBest_Action(int r);

int Dynet_Get_NBest_Action(int r) 
{
    if (r < MAX_ACTIONS) return ranks[r];
    return -1;
}

extern "C" int Dynet_Get_NBest_Beam_Action(int r);

int Dynet_Get_NBest_Beam_Action(int r) 
{
    if (r < branks.size()) return branks[r];
    return -1;
}

extern "C" int Dynet_New_Mstag_Vocab();

int Dynet_New_Mstag_Vocab () 
{
    cout << "Register new mstag vocab\n";
    dynet::Dict *m = new dynet::Dict();
    dmstag.push_back(m);
    m->convert("<none>");
    sentence_mstags.push_back({});
    return dmstag.size()-1;
}

extern "C" int Dynet_Mstag(int mstag,char *value);

int Dynet_Mstag(int mstag, char *value) 
{
    int id = dmstag[mstag]->convert(value);
//    cout << "dynet mstag " << mstag << " " << value << " => " << id << endl;
    return id;
}

extern "C" void Dynet_Oracle_Edge(const int dep, const int gov, const int label);

void Dynet_Oracle_Edge(const int dep, const int gov, const int label) 
{
    oracle_edges[dep] = make_pair(gov,label);
}

extern "C" int Dynet_Predicted_Head(const int dep);

int Dynet_Predicted_Head(const int dep) 
{
    return edges[dep-1].first;
}

extern "C" int Dynet_Predicted_Label(const int dep);

int Dynet_Predicted_Label(const int dep) 
{
    return edges[dep-1].second;
}


extern "C" int Dynet_Predicted_Head_Rank(const int dep, const int gov);

int Dynet_Predicted_Head_Rank(const int dep, const int gov) 
{
    return edge_ranks[dep-1][gov].first;
}

extern "C" float Dynet_Predicted_Head_Weight(const int dep, const int gov);

float Dynet_Predicted_Head_Weight(const int dep, const int gov)
{
    return edge_ranks[dep-1][gov].second;
}

extern "C" void Dynet_Update_Epoch();

void Dynet_Update_Epoch() 
{
    sgd->update_epoch();
}
