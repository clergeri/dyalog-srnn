/** 
 * ----------------------------------------------------------------
 * $Id$
 * Copyright (C) 2013, 2014, 2015, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  \file  engine_c.c 
 *  \brief C support for engine.pl
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <regex.h>
#include <math.h>
#include <ctype.h>

#undef PACKAGE_VERSION
#undef PACKAGE_TARNAME
#undef PACKAGE_STRING
#undef PACKAGE_NAME
#undef VERSION
#undef PACKAGE

#include "libdyalog.h"
#include "builtins.h"

extern char *getenv();

#define SR_TRIM_MIN 0.1
#define SR_TRIM_N 1
#define SR_TRIM_BATCH 10000
#define SR_FEATURE_CACHE 500
#define SR_LIST_MAX 1

// for testing uthash (with keystats)
// #define HASH_EMIT_KEYS 3

// the following hash function seems to work well with fol_t keys
//#define HASH_FUNCTION HASH_FNV

// test bloom filter (not useful here)
// #define HASH_BLOOM 10

// my own hash function for fol_t (see DyALog/Runtime/hash.h)
// seems to be the best one and the fastest one for fol_t keys !
#define HASH_FOL(key,keylen,num_bkts,hashv,bkt)                 \
do {                                                            \
    (hashv) = *(unsigned long *)(key);                          \
    (hashv) = ((hashv) ^ ((hashv) >>10) ^ ((hashv) >>20));      \
    bkt = (hashv) & (num_bkts-1);                               \
} while (0)

#define HASH_FUNCTION HASH_FOL
// Best so far is FNV (116)
//#define HASH_FUNCTION HASH_FNV

#include "uthash/src/uthash.h"

#ifndef HAVE_STRNDUP

size_t
my_strnlen (const char *string, size_t maxlen)
{
  const char *end = memchr (string, '\0', maxlen);
  return end ? (size_t) (end - string) : maxlen;
}

char *
strndup (s, n)
     const char *s;
     size_t n;
{
  size_t len = my_strnlen (s, n);
  char *new = (char *) GC_MALLOC_ATOMIC(sizeof(char) * (len + 1));

  if (new == NULL)
    return NULL;

  new[len] = '\0';
  return (char *) memcpy (new, s, len);
}

#endif



static fol_t
sfol_full_copy( fol_t t, fkey_t Sk(t) )
{
    Deref(t);

    if (FOL_GROUNDP(t)) {
        return t;
    } else if (FOL_DEREFP(t)) {
        dyalog_printf("** pb variable in sfol_full_copy %&f\n",t);
        return t;
    } else {
        unsigned long arity = FOLCMP_ARITY( t );
        fol_t *arg = &FOLCMP_REF(t,1);
        fol_t *stop = arg + arity;

        FOLCMP_WRITE_START( FOLCMP_FUNCTOR( t ), arity );

        for (; arg < stop ;)
            FOLCMP_WRITE( sfol_full_copy( *arg++, Sk(t)) );

        return FOLCMP_WRITE_STOP;
    }
}

Bool
DyALog_Copy( sfol_t src, sfol_t dest )
{
    return Unify(dest->t,dest->k,sfol_full_copy(src->t,src->k),Key0);
}

/*
 * indexing templates+values via hash trees
 */

static unsigned long perceptron_count=1;

static unsigned long sr_nactions;
static unsigned long sr_nsimple;
static unsigned long sr_ncomplex;
static double *sr_action_results;
static unsigned long *sr_action_filter;
static unsigned long sr_nlabels;
static unsigned long sr_beam_size;
static double sr_trim_min=SR_TRIM_MIN;
static unsigned long sr_trim_n=SR_TRIM_N;
static unsigned long sr_trim_batch=SR_TRIM_BATCH;

struct feature_cache_cell 
{
    fol_t a;
    fkey_t Sk(a);
};

static struct feature_cache_cell feature_cache[SR_FEATURE_CACHE];

typedef struct sr_template_value * sr_template_value_t;

struct sr_template_value 
{
    long key;
    sr_template_value_t next;
    double weight;
    double acc;
    unsigned long n;            /* update frequency */
    unsigned long f;            /* visit frequency */
    UT_hash_handle hh;
};


static sr_template_value_t sr_template_value=NULL;

void
perceptron_reset ()
{
    perceptron_count = 0;
}

static void FSet_Model_Trim(void);

void
perceptron_advance ()
{
    perceptron_count++;
//    if (perceptron_count % 10000 == 0) {
    if (perceptron_count % sr_trim_batch == 0) {
//        dyalog_printf("trimming model at %d\n",perceptron_count);
            /* we collapse the model very 10_000 sentences, to save memory */
        FSet_Model_Trim();
    }
}

void
set_max_action (unsigned long n,
                unsigned long nlabels,
                unsigned long nsimple,
                unsigned long ncomplex,
                unsigned long beam_size)
{
    char *info;
//    printf("setting number of actions to %d\n",n);
    sr_nactions = n;
    sr_nlabels = nlabels;
    sr_ncomplex = ncomplex;
    sr_nsimple = nsimple;
    sr_beam_size = beam_size;
    sr_action_results = (double *) GC_MALLOC_ATOMIC(sizeof(double) * sr_nactions);
    sr_action_filter = (unsigned long *) GC_MALLOC_ATOMIC(sizeof(unsigned long) * sr_ncomplex);
    if ((info=getenv("DYALOGSR_TRIM_MIN"))) {
        sr_trim_min = atof(info);
//        dyalog_printf("setting sr_trim_min to %g\n",sr_trim_min);
    }
    if ((info=getenv("DYALOGSR_TRIM_N"))) {
        sr_trim_n = atoi(info);
//        dyalog_printf("setting sr_trim_min to %g\n",sr_trim_min);
    }
    if ((info=getenv("DYALOGSR_TRIM_BATCH"))) {
        sr_trim_batch = atoi(info);
//        dyalog_printf("setting sr_trim_min to %g\n",sr_trim_min);
    }
}

static
double *
get_action_results () 
{
    unsigned long i=0;
    for(i=0; i < sr_nactions; )
        sr_action_results[i++] = 0.0;
    for(i=0; i < sr_ncomplex; )
        sr_action_filter[i++] = 0;
    for (i=0; i < SR_FEATURE_CACHE;)
        feature_cache[i++].a=(fol_t)0;
    return sr_action_results;
}

int
sort_by_freq (sr_template_value_t a, sr_template_value_t b)
{
    return b->f - a->f;
}

inline static sr_template_value_t
fset_value_new_entry(long key,sr_template_value_t *table,double w) 
{
    sr_template_value_t entry = (sr_template_value_t) GC_MALLOC(sizeof(struct sr_template_value));
    entry->key = key;
    entry->next = NULL;
    entry->weight = w;
    entry->acc = 0.0;
    entry->n=0;
    entry->f=0;
    HASH_ADD_INT(*table,key,entry);
    return entry;
}

inline static sr_template_value_t
fset_value_find_entry(long key,sr_template_value_t table)
{
//    printf("find entry key=%d table=%x\n",key,table);
    sr_template_value_t entry=NULL;
    if (!table)
        return NULL;
    else if (table->key == key)
        entry = table;
    else if (HASH_COUNT(table) < 5) {
        for(entry = table->hh.next; entry && entry->key != key; entry=entry->hh.next);
    } else {
        HASH_FIND_INT(table,&key,entry);
    }
    if (entry) {entry->f++;}
    return entry;
}

inline static sr_template_value_t
fset_value_find_or_add_entry(long key,sr_template_value_t *table) 
{
    sr_template_value_t entry = fset_value_find_entry(key,*table);
    return (entry) ? entry : fset_value_new_entry(key,table,0.0);
}


//#define Delta(_entry) (perceptron_count < 100 ? 0 : _entry->acc / (1.0 * perceptron_count))
#define Delta(_entry) 0.0

typedef struct weight_info 
{
    fol_t action;
    fol_t label;
    double *results;
    short bound_action;
} *weight_info_t;

static void
FSet_Value_Weight_Simple(sr_template_value_t table, SP(values), weight_info_t info)
{
//    long key;
        /* values is a sequence of values acting as a path to a final weight w
           we assume values has been derefrenced at this point
         */
        //   dyalog_printf("entering weight simple\n");
    while (1) {
        sr_template_value_t entry=NULL;        
//        Deref(values);
//        printf("next loop values=%x table=%x\n",(long)values,*table);
        
        if (FOLNILP(values)) {
            double * results = info->results;
                /* final step */
//            if (FOLINTP(action) && FOLINTP(label)) {
            if (info->bound_action) {
//            printf("entry fset=%p entry=%p w=%ld\n",fset,entry,entry->weight);
                    /* need to follow path further down for action and label */
                
                if (!(entry = fset_value_find_entry((long)info->action,table))) 
                    return;
                results[0] += entry->weight;

                    /* we are dealing with a labelled action */
                if (info->label != DFOLINT(0)
                    && (entry = fset_value_find_entry((long)info->label,entry->next)))
                    results[0] += entry->weight;

                return;
            } else {
                    /* we iter to fill the result table */
//                printf("iterate\n");
                
                for(; table ; table = (sr_template_value_t) (table->hh.next)) {
                    double weight = table->weight;
                    long xaction = CFOLINT(table->key);
                          //printf("fill result table xaction=%d nsimple=%d ncomplex=%d nlabels=%d delta=%f\n",xaction,sr_nsimple,sr_ncomplex,sr_nlabels,weight);
                    if (xaction < sr_nsimple) {
                        results[xaction] += weight;
                    } else {
                        unsigned long i= (xaction-sr_nsimple) * sr_nlabels+sr_nsimple;
                        unsigned long stop = i+sr_nlabels;

                        for(; i < stop; )
                            results[i++] += weight;
                        for( entry = table->next; entry; entry = (sr_template_value_t) (entry->hh.next) ) {
                            results[CFOLINT(entry->key)] += entry->weight;
                              // dyalog_printf("\tfill cell xaction=%d cell=%&f delta=%g => w=%g\n",xaction,entry->key,entry->weight,results[CFOLINT(entry->key)]);
                        }
                    }
                }
            }
            return;
        }

        if (FOLPAIRP(values)) {
            long length=FOLINFO_LENGTH(values);
            
//            Deref(v);
//            key = (long)v;

//           values = FOLPAIR_CDR(values);
//           Deref(values);

            while (length-- > 0) {
                fol_t v = FOLPAIR_CAR(values);
                fkey_t Sk(v);                

                if (FOLCMPP(v) && FOLCMP_FUNCTOR(v) == FOLCOMA) {
                    unsigned long i = (unsigned long) UCFOLINT(FOLCMP_REF(v,1));
                    if (feature_cache[i].a) {
                        v = feature_cache[i].a;
                        Sk(v) = feature_cache[i].Sk(a);
                    } else {
                        v = FOLCMP_REF(v,2);
                        Sk(v) = Sk(values);
                        Deref(v);
                        feature_cache[i].a = v;
                        feature_cache[i].Sk(a) = Sk(v);
                    }
                } else {
                    Sk(v) = Sk(values);
                    Deref(v);
                }
                
                
                if (FOLPAIRP(v)) {
                        /* if v may be a list of values (for the same feature),
                           we return the sum on all results
                        */
//                    long length=FOLINFO_LENGTH(v);
                    values = FOLPAIR_CDR(values);
                    Deref(values);
//                    while (FOLPAIRP(v)) {
                    do {
                        fol_t x = FOLPAIR_CAR(v);
//                        fkey_t Sk(x) = Sk(v);
//                        Deref(x);
                        if ((entry = fset_value_find_entry((long)x,table)))
                            FSet_Value_Weight_Simple(entry->next,values,Sk(values),info);
                        v = FOLPAIR_CDR(v);
//                        Deref(v);
                    } while (!FOLNILP(v));
                    return;
                }
                
                if (FOLFSETP(v)) {
                        /* if v may be a finite set seen as a list of values (for the same feature),
                           we return the sum on all results as done for lists (see below)
                        */
                    fol_t ftable = FOLCMP_REF(v,2);
                    fol_t *ftable_arg= &FOLCMP_REF(ftable,1);
                    fol_t *tmp_ftable_arg = ftable_arg;
                    fol_t *ftable_stop= ftable_arg + FOLCMP_ARITY(ftable);
                    fol_t *value_arg=&FOLCMP_REF(v,3);
                    fol_t *value_stop=&FOLCMP_REF(v,1) + FOLCMP_ARITY(v);
//                dyalog_printf("dealing with finite set value %&f\n",v);
                    values = FOLPAIR_CDR(values);
                    Deref(values);
                    for(; value_arg < value_stop; value_arg++,ftable_arg=tmp_ftable_arg+FSET_BIT_PER_WORD) {
                        unsigned long value = UCFOLINT(*value_arg);
                        tmp_ftable_arg=ftable_arg;
                        for( ; value && ftable_arg < ftable_stop ; ftable_arg++, value >>= 1 ) {
                            if (value & 1) {
                                    /*  process this element */
//                            dyalog_printf("\tfset v=%&f\n",*ftable_arg);
                                entry = fset_value_find_entry((long)*ftable_arg,table);
                                if (entry) {
                                    FSet_Value_Weight_Simple(entry->next,values,Sk(values),info);
                                }
                            }
                        }
                    }
                    return;
                }
                

//        printf("adding fset=%p table=%p key=%d v=%ld\n",fset,table,key,(long)v);
                entry = fset_value_find_entry((long)v,table);
                if (!entry)
                    return;
                values = FOLPAIR_CDR(values);
                Deref(values);
                table = entry->next;
            }
            continue;
        }

        if (FOLCMPP(values)) {
                /* if values is a compound term (but not a list)
                   then its arguments are subpaths to follow in parallel
                   we return the sum on all results
                */
            fol_t *arg = &FOLCMP_REF( values, 1 );
            fol_t *stop = arg+FOLCMP_ARITY(values)-1;
            for( ; arg < stop ; ) {
                fol_t v = *arg++;
                fkey_t Sk(v) = Sk(values);
                Deref(v);
                FSet_Value_Weight_Simple(table,v,Sk(v),info);
            }
            values = *arg;
//            Deref(values);
            continue;
//            return weight;
        }


            /* should not reach this point */
       dyalog_printf("should not be here values=%&s\n",values,Sk(values));
        return;
        
    }
    
    return;
}


unsigned long
FSet_Value_Weight_Aux(sr_template_value_t *table, fol_t action, fol_t label, SP(values), double update, double acc_update) 
{
    long key;
    unsigned long res=0;
        /* values is a sequence of values acting as a path to a final weight w
           we (possibly) update w and return it
         */
    while (1) {
        sr_template_value_t entry=NULL;        
        Deref(values);
//        dyalog_printf("next loop values=%&s table=%x\n",values,Sk(values),*table);
        
        if (FOLNILP(values)) {
                /* final step */
                /* need to follow path further down for action and label */
            entry=fset_value_find_or_add_entry((long)action,table);
            if (label != DFOLINT(0)) {
                    /* we are dealing with a labelled action */
                entry->weight += update;
                entry->acc += acc_update;
//                entry->acc += 0.7*acc_update + 0.3*(update * entry->n);
                entry->n++;
                res++;
                table = &entry->next;
                entry=fset_value_find_or_add_entry((long)label,table);
            }
            entry->weight += update;
            entry->acc += acc_update;
//            entry->acc += 0.7*acc_update + 0.3*(update * entry->n);
            entry->n++;
            res++;
            return res;
        }
        
        if (FOLPAIRP(values)) {
            fol_t v = FOLPAIR_CAR(values);
            fkey_t Sk(v) = Sk(values);
                //Deref(v);
                //key = (long)v;


            if (FOLCMPP(v) && FOLCMP_FUNCTOR(v) == FOLCOMA)
                v = FOLCMP_REF(v,2);
            
            Deref(v);
            key = (long)v;
            
            if (FOLPAIRP(v)) {
                /* if v may be a list of values (for the same feature),
                   we update following each coponent and return the sum on all results
                 */
                values = FOLPAIR_CDR(values);
                while (FOLPAIRP(v)) {
                    fol_t x = FOLPAIR_CAR(v);
                    fkey_t Sk(x) = Sk(v);
                    Deref(x);
                    entry = fset_value_find_or_add_entry((long)x,table);
                    res += FSet_Value_Weight_Aux(&entry->next,action,label,values,Sk(values),update,acc_update);
                    v = FOLPAIR_CDR(v);
                    Deref(v);
                }
                return res;
            }


            if (FOLFSETP(v)) {
                    /* if v may be a finite set seen as a list of values (for the same feature),
                       we return the sum on all results as done for lists (see below)
                    */
                fol_t ftable = FOLCMP_REF(v,2);
                fol_t *ftable_arg= &FOLCMP_REF(ftable,1);
                fol_t *tmp_ftable_arg = ftable_arg;
                fol_t *ftable_stop= ftable_arg + FOLCMP_ARITY(ftable);
                fol_t *value_arg=&FOLCMP_REF(v,3);
                fol_t *value_stop=&FOLCMP_REF(v,1) + FOLCMP_ARITY(v);
//                dyalog_printf("dealing with finite set value %&f\n",v);
                values = FOLPAIR_CDR(values);
                for(; value_arg < value_stop; value_arg++,ftable_arg=tmp_ftable_arg+FSET_BIT_PER_WORD) {
                    unsigned long value = UCFOLINT(*value_arg);
                    tmp_ftable_arg=ftable_arg;
                    for( ; value && ftable_arg < ftable_stop ; ftable_arg++, value >>= 1 ) {
                        if (value & 1) {
                                /*  process this element */
//                            dyalog_printf("\tfset v=%&f\n",*ftable_arg);
                            entry = fset_value_find_or_add_entry((long)*ftable_arg,table);
                            res += FSet_Value_Weight_Aux(&entry->next,action,label,values,Sk(values),update,acc_update);
                        }
                    }
                }
                return res;
            }
            
//        printf("adding fset=%p table=%p key=%d v=%ld\n",fset,table,key,(long)v);
            entry=fset_value_find_or_add_entry(key,table);
            values = FOLPAIR_CDR(values);
            table = &entry->next;
            continue;
        }

        if (FOLCMPP(values)) {
                /* if values is a compound term (but not a list)
                   then its arguments are subpaths to follow in parallel
                   we update following each coponent and return the sum on all results
                */
            fol_t *arg = &FOLCMP_REF( values, 1 );
            fol_t *stop = arg+FOLCMP_ARITY(values);
            for( ; arg < stop ;) {
                res += FSet_Value_Weight_Aux(table,action,label,*arg++,Sk(values),update,acc_update);
            }
            return res;
        }

            /* should not reach this point */
//        dyalog_printf("should not be here values=%&s\n",values,Sk(values));
        return res;
    }
    return res;
}

unsigned long
FSet_Value_Weight_Update(sfol_t action, sfol_t label, sfol_t values,long update) 
{
             //    dyalog_printf("update action=%&f label=%&f update=%d\n",action->t,label->t,update);
    SFOL_Deref(action);
    SFOL_Deref(label);
    return FSet_Value_Weight_Aux(&sr_template_value,action->t, label->t, values->t,values->k,1.0 * update,1.0*update*perceptron_count);
}

Bool
FSet_Value_Weight(sfol_t action, sfol_t label, sfol_t values, sfol_t res, unsigned long mask) 
{
    int n = Get_Choice_Counter();
    double **results = Get_Choice_Buffer(double **);
    double min = -1000001.00;
    double max;
    unsigned long i;
    long max_i=-1;
//    dyalog_printf("here n=%d nactions=%d\n",n,sr_nactions);
    if (n==0) {
        int all = 0;
        struct weight_info info;
        *results = get_action_results();
        SFOL_Deref(action);
        SFOL_Deref(label);
        SFOL_Deref(values);
        info.action = action->t;
        info.label = label->t;
        info.bound_action = (FOLINTP(action->t) && FOLINTP(label->t));
        info.results = (info.bound_action) ? &((*results)[CFOLINT(label->t)]) : *results;
        FSet_Value_Weight_Simple(sr_template_value,values->t,values->k,&info);
        if (FOLINTP(action->t) && FOLINTP(label->t)) {
//            dyalog_printf("simple case action=%d label=%d\n",action->t,label->t);
//            *res = round((*results)[CFOLINT(label->t)]);
            No_More_Choice();
            return Unify(res->t,res->k, DFOLFLT((*results)[CFOLINT(label->t)]),Key0);
//            return Unify(res->t,res->k, DFOLINT(round((*results)[CFOLINT(label->t)])),Key0);
//            Succeed;
        }
        if (FOLINTP(action->t)) {
                /* if the action is a complex one but partially specified, we remove impossible choices */
            long b = CFOLINT(action->t);
            long a;
//            dyalog_printf("here action=%d\n",b);
            for(a=0 ; a < sr_ncomplex ; a++ ) {
                if ((a+sr_nsimple) != b) {
                    unsigned long i = a * sr_nlabels + sr_nsimple;
                    unsigned long stop = i+sr_nlabels;
                    for(; i < stop ; ) {
                        (*results)[i++] = min;
                    }
                }
            }
        } else if (mask) {
            /* if a mask is set, it denotes the left reduce or attach base,
            we use it to block non interesting actions
            */
            unsigned long a;
            for(a=0 ; a < sr_ncomplex ; a++ ) {
             if ((a+sr_nsimple) != mask && (a+sr_nsimple) != (mask+1)) {
                    unsigned long i = a * sr_nlabels + sr_nsimple;
                    unsigned long stop = i+sr_nlabels;
                    for(; i < stop ; ) {
                        (*results)[i++] = min;
                    }
                 }
            }
        } else {
//            dyalog_printf("mode all active\n");
            all = 1;
        }
        (*results)[0] = min; 
        if (!all) {
                /* no action, no mask => at this point, we skip simple actions */
            for(i=1; i < sr_nsimple; ) {
                (*results)[i++] = min; 
            }
        }
    }

//    printf("turn %d results=%p buffer=%p\n",n,*results,sr_action_results);
        
        /* selecting best remaining result, starting first with complex actions
         * and then listing simple ones */
//    dyalog_printf("weight n=%d\n",n);
    for( i=0; i < sr_nactions; i++) {
            /*  we skip simple actions, and only consider complex ones */
        if ((max_i == -1) || (*results)[i] > max) {
            max = (*results)[i];
            max_i = i;
        }
    }
    if (max == min) {
//        printf("*** min=max beam size=%ld n=%d max_i=%ld
//        max=%lg\n",sr_beam_size,n,max_i,max);
        No_More_Choice();
        Fail;
    } else if (max_i >= sr_nsimple) {
        unsigned long c = (max_i - sr_nsimple) / sr_nlabels;
        sr_action_filter[c]++;
//        dyalog_printf("filtering out i=%d => c=%d v=%d (nsimple=%d ncomplex=%d nlabels=%d beam=%d)\n",max_i,c,sr_action_filter[c],sr_nsimple,sr_ncomplex,sr_nlabels,sr_beam_size);
        if (n >= sr_beam_size - 1) {
            unsigned long j=sr_nsimple+c*sr_nlabels;
            unsigned long stop = j+sr_nlabels;
//            printf("removing c=%d min=%f start=%d stop=%d\n",c,min,j,stop);
            for(; j < stop ;j++){
                (*results)[j] = min;
            }
        }
    }
//    *res = round(max);
    (*results)[max_i] = min;
//    printf("beam size=%ld n=%d max_i=%ld max=%lg\n",sr_beam_size,n,max_i,max);
//    if ( (n >= sr_nactions - (sr_nsimple+1))  || (n >= sr_beam_size)) {
    if ( n >= sr_beam_size-1 ) {
            unsigned int active=0;
                /* check  if some complex actions have not been tried */
            for(i=0; i < sr_ncomplex ; i++ ) {
                if (sr_action_filter[i] == 0) {
//                    dyalog_printf("complex action %d is still active\n",i);
                    active=1;
                    break;
                } 
            }
                /* check if at least one simple action remains active */
            if (!active) {
                for(i=0; i < sr_nsimple ; ) {
                    if ((*results)[i++] > min) {
                        active=1;
                        break;
                    }
                }
            }
            if (!active) {
//                dyalog_printf("closing at %d\n",n);
                No_More_Choice();
            } 
    }
//    dyalog_printf("weight n=%d => i=%d w=%g\n",n,max_i,max);
    return Unify(label->t,label->k,DFOLINT(max_i),Key0)
        && Unify(res->t,res->k,DFOLFLT(max),Key0)
//        Unify(res->t,res->k,DFOLINT(round(max)),Key0)
        ;
}

Bool
FSet_All_Value_Weights(sfol_t values, sfol_t res) 
{
    int n = Get_Choice_Counter();
    double **results = Get_Choice_Buffer(double **);
//    dyalog_printf("here n=%d nactions=%d\n",n,sr_nactions);
    if (n==0) {
        int all = 0;
        struct weight_info info;
        *results = get_action_results();
        SFOL_Deref(values);
        info.bound_action = 0;
        info.results = *results;
        FSet_Value_Weight_Simple(sr_template_value,values->t,values->k,&info);
        return Unify(res->t,res->k,LIGHT_POINTER_TO_DYALOG(info.results),Key0);
    }
    No_More_Choice();
    Fail;
}

Bool
FSet_Get_Value_Weight(double *results, sfol_t label, sfol_t weight) 
{
    SFOL_Deref(label);
    if (FOLINTP(label->t)) {
        return Unify(DFOLFLT(results[CFOLINT(label->t)]),Key0,weight->t,weight->k);
    } else
        Fail;
}


        


unsigned long debug_trimmed=0;

static sr_template_value_t
model_trim_fset_value(sr_template_value_t table) 
{
    sr_template_value_t entry,tmp;
    HASH_ITER(hh,table,entry,tmp) {
        if (entry->next)
            entry->next = model_trim_fset_value(entry->next);
        if (entry->weight || !entry->next) {
            entry->weight = entry->weight - (entry->acc / (1.0 * perceptron_count));
//            entry->weight = entry->weight - (entry->acc / (1.0 * entry->n));
            entry->acc = 0.0;
        }
        if (!entry->next
            && (fabs(entry->weight) < sr_trim_min || entry->n < sr_trim_n)
            ){
//            printf("deleting void entry\n");
            debug_trimmed++;
            HASH_DEL(table,entry);
        }
    }
    if (HASH_COUNT(table))
        return table;
    else
        return NULL;
}

static sr_template_value_t
model_trim_fset_value_alt(sr_template_value_t table) 
{
    sr_template_value_t entry,tmp;
    HASH_ITER(hh,table,entry,tmp) {
        if (entry->next)
            entry->next = model_trim_fset_value_alt(entry->next);

         if (!entry->next
            && (fabs(entry->weight - (entry->acc / (1.0 * perceptron_count))) < sr_trim_min
                || entry->n < sr_trim_n
                )){
            HASH_DEL(table,entry);
         }
         
    }
    if (HASH_COUNT(table))
        return table;
    else
        return NULL;
}


static void
FSet_Model_Trim() 
{
    sr_template_value = model_trim_fset_value_alt(sr_template_value);
}

static void
model_save_fset_value(FILE *model, sr_template_value_t p, unsigned long depth) 
{
    HASH_SORT(p,sort_by_freq);
    for( ; p ; p = (sr_template_value_t) (p->hh.next)) {
        sr_template_value_t next = p->next;
        double weight = 0.0;
        if (p->weight || !next) {
                //    weight = p->weight - (p->acc / (1.0 * perceptron_count));
            weight = p->weight;
        }
        if (weight || next) {
            if (FOLSMBP(p->key)) {
                fprintf(model,"fvaluesmb:d%ld:w%.4lg:%s\n",depth,weight,SymbolName(p->key));
            } else {
                fprintf(model,"fvalueint:d%ld:w%.4lg:%ld\n",depth,weight,CFOLINT(p->key));
            }
            if (next)
                model_save_fset_value(model,next,depth+1);
        }
    }
}

Bool
model_save(char *modelfile) 
{
    FILE *model = fopen(modelfile,"w");
    if (!model) {
        dyalog_printf("error opening model file %s\n",modelfile);
        Fail;
    }
    sr_template_value = model_trim_fset_value(sr_template_value);
    dyalog_printf("%% trimmed entries: %d\n",debug_trimmed);
    model_save_fset_value(model,sr_template_value,0);
    if (fclose(model)) {
        dyalog_printf("error closing model file %s\n",modelfile);
        Fail;
    }
    Succeed;
}

Bool
model_load(char *modelfile)
{
        /* reset hash tables */
    sr_template_value=NULL;
    sr_template_value_t vstack[100]; /* hard coded parameter: may induce strange
                                     * behaviors for very long feature templates */
    char line [256];
    long n=0;
    long i=0;
    double w=0;
    long vdepth=0;
    char buff[256];
    FILE *model = fopen(modelfile,"r");
    if (!model) {
        dyalog_printf("error opening model file %s\n",modelfile);
        Fail;
    }
    while (fgets(line,256,model) != NULL) {
        if (sscanf(line,"fvalueint:d%ld:w%lf:%ld",&vdepth,&w,&i) == 3) {
            sr_template_value_t *table = (vdepth > 0) ? &(vstack[vdepth-1]->next) : &sr_template_value;
            vstack[vdepth] = fset_value_new_entry((long)DFOLINT(i),table,w);
        } else if (sscanf(line,"fvaluesmb:d%ld:w%lf:%s",&vdepth,&w,buff) == 3) {
            sr_template_value_t *table = (vdepth > 0) ? &(vstack[vdepth-1]->next) : &sr_template_value;
            vstack[vdepth] = fset_value_new_entry((long)Create_Atom((char *)strndup(buff,256)),table,w);
        } else if (sscanf(line,"fvaluesmb:d%ld:w%lf:",&vdepth,&w) == 2) {
            sr_template_value_t *table = (vdepth > 0) ? &(vstack[vdepth-1]->next) : &sr_template_value;
            vstack[vdepth] = fset_value_new_entry((long)Create_Atom((char *)""),table,w);
        } else {
            printf("unexpected cmd: %s\n",line);
//            exit(1);
        }
    }
//    sr_template_value = model_trim_fset_value(sr_template_value);
    if (fclose(model)) {
        dyalog_printf("error closing model file %s\n",modelfile);
        Fail;
    }
    dyalog_printf("model loaded\n");
    Succeed;
}


Bool is_capitalized (const char *s) 
{
    char c = s[0];
    return (isupper(c) && !index("����������������������ߵ�",c));
}

Bool is_number (const char *s) 
{
    char *endptr;
    double val;
    errno = 0;
    val = strtod(s,&endptr);

    if ( errno
         || endptr == s
             // || *endptr != '\0'
         )
        Fail;
    
    Succeed;
}

Bool is_acronym (const char *s, sfol_t res)
{
    fol_t l = FOLNIL;
    long n = strlen(s);
    long i = n-1;
    for(; i >= 0 ; i--) {
        if (s[i] != '.' && s[i] != toupper(s[i])) {
            Fail;
        }
        if (s[i] != '.') {
            l = Create_Pair(DFOLCHAR(tolower(s[i])),l);
        }
    }
    return sfol_unify(l,Key0,res->t,res->k);
}

/* Beam handling */

struct beam_slot 
{
    fol_t item;
    tabobj_t o;
    long weight;
};

typedef struct beam_slot *beam_slot_t;

struct beam_cell 
{
    unsigned long step;
    beam_slot_t *members;
};

typedef struct beam_cell *beam_cell_t;

beam_cell_t *beam_cells;        /* a vector of beam cells */
unsigned long beam_size;

void
Beam_Cells_Allocate (unsigned long nsteps, unsigned long bsize) 
{
    unsigned long i = 0;
    beam_size = bsize;
    beam_cells = (beam_cell_t *) GC_MALLOC(sizeof(beam_cell_t) *  nsteps);
    for(; i < nsteps;) {
        unsigned long j=0;
        beam_cell_t cell = beam_cells[i] = (beam_cell_t) GC_MALLOC(sizeof(struct beam_cell));
        cell->step = i++;
        cell->members = (beam_slot_t *) GC_MALLOC(sizeof(beam_slot_t) * (beam_size+1));
        for(; j < beam_size+1 ; ) {
            cell->members[j++] = NULL;
        }
    }
}

extern Bool object_delete(tabobj_t);

static void
beam_free_slot(beam_slot_t slot) 
{
    if (slot) {
            /* free last unempty shifted item */
        object_delete((tabobj_t) (slot->o));
    }
}

beam_slot_t
beam_cell_add (unsigned long step, long weight, sfol_t Item) 
{
        /*  try to add item in beam cell at step
            item is assumed to be ground
            return: beam slot if added
                    NULL otherwise
            if added, the last item in beam cell has to be discarded if falling
            out from the beam        
        */
    beam_cell_t cell = beam_cells[step];
    unsigned long i = 0;
    beam_slot_t *current=cell->members;
    beam_slot_t shifted;
    fol_t item = sfol_full_copy(Item->t,Item->k);
    
    for(; (i < beam_size+1) && *current ; i++, current++) {
        if (weight > (*current)->weight) {
                /*  install item at current slot
                    and shift rightwards the remaining slots
                */
            beam_slot_t new = (beam_slot_t) GC_MALLOC(sizeof(struct beam_slot));
            shifted = *current;
            *current = new;
            new->item = item;
            new->weight = weight;
            new->o = NULL;
            i++;
            for( ; (i < beam_size+1) && shifted ; i++, current++ ) {
                beam_slot_t tmp = *current;
                *current = shifted;
                shifted = tmp;
            }
            beam_free_slot(shifted);
            return new;
        }
    }
        /* we have reached either:
           - the first empty slot of the beam
           - or the last slot of the beam: reject the item
         */
    if (i > beam_size) {
        return NULL;
    } else {
        beam_slot_t new = (beam_slot_t) GC_MALLOC(sizeof(struct beam_slot));
        shifted = *current;
        *current = new;
        new->item = item;
        new->weight = weight;
        new->o = NULL;
        beam_free_slot(shifted);
        return new;
    }
}

void
beam_slot_fill_address (beam_slot_t slot, tabobj_t o) 
{
    slot->o = o;
}

/*
Bool
beam_cell_enumerate (unsigned long step) 
{
    int n = Get_Choice_Counter();
    beam_slot_t *current = Get_Choice_Buffer(beam_slot_t *);
    fol_t item;
    tabobj_t o;
    if (n==0) {
        *current = beam_cells[step]->members;
    }
    Succeed;
}
*/

void
test_float(float f) 
{
    printf("test %g\n",f);
}

struct slist_cell
{
    double weight;
    fol_t e;
    struct slist_cell *next;
};

typedef struct slist_cell *slist_t;

struct bslist
{
    unsigned long n;
    unsigned long count;
    unsigned long cloned;
    slist_t oracle;             /* null or ptr to oracle cell in list */
    slist_t list;
};

typedef struct bslist *bslist_t;

bslist_t
Slist_New(unsigned long n) 
{
    bslist_t bslist = (bslist_t) GC_MALLOC(sizeof(struct bslist));
    bslist->n = n;
    bslist->count = 0;
    bslist->cloned = 0;
    bslist->oracle = NULL;
    bslist->list = NULL;
//    dyalog_printf("slist allocate %x\n",bslist);
    return bslist;
}

bslist_t
Slist_Clone(bslist_t bslist) 
{
    bslist_t clone = (bslist_t) GC_MALLOC(sizeof(struct bslist));
    clone->n=bslist->n;
    clone->count=bslist->count;
    clone->cloned = 1;
    clone->oracle = bslist->oracle;
    clone->list = bslist->list;
    return clone;
}

static void
slist_copy(bslist_t clone) 
{
    slist_t ptr, *clone_ptr;
         //    printf("slist cloning\n");
    clone->cloned = 0;
    clone_ptr = &(clone->list);
    for(ptr = clone->list; ptr; ptr = ptr->next) {
        slist_t cell = *clone_ptr = (slist_t) GC_MALLOC(sizeof(struct slist_cell));
        cell->weight = ptr->weight;
        cell->e = ptr->e;
        cell->next = NULL;
        if (clone->oracle == ptr)
            clone->oracle = cell;
        clone_ptr = &(cell->next);
    }
}

Bool
Slist_Add(sfol_t xweight, sfol_t e, bslist_t bslist, long oracle) 
{
    slist_t *ptr;
    double weight;
    
    SFOL_Deref(xweight);
    weight=CFOLFLT(xweight->t);

    
    if (bslist->cloned)
         slist_copy(bslist);

    if (oracle) {
            /* oracle tail is kept in addition of n-best tails */
        if (bslist->oracle) {   /* unset previous oracle */
            bslist->oracle = NULL;
        } else {
            bslist->n++;
        }
    }
//  dyalog_printf("slist start add bslist=%x list=%x e=%&f w=%d count=%d\n",bslist,bslist->list,e->t,weight,bslist->count);
    for(ptr = &(bslist->list); *ptr && (weight < (*ptr)->weight); ptr = &((*ptr)->next)){
                                  if ((*ptr)->e==e->t)
                                      Succeed;
                      //        printf("\tadvance weight=%d ptr=%x ptr_weight=%d ptr_e=%ld\n",weight,*ptr,(*ptr)->weight,(*ptr)->e);
    }
    for(; *ptr && (weight == (*ptr)->weight); ptr = &((*ptr)->next)){
                            //       printf("\tadvance2 weight=%d ptr=%x ptr_weight=%d ptr_e=%ld\n",weight,*ptr,(*ptr)->weight,(*ptr)->e);
       if ((*ptr)->e == e->t)
            Succeed;
    }
    if ((bslist->count < bslist->n) || oracle) {
            /* we have room in the bounded sorted list or we handle an oracle tail
               we add the new component
             */
        slist_t cell = (slist_t) GC_MALLOC(sizeof(struct slist_cell));
//         dyalog_printf("caseA ptr=%x e=%&f\n",*ptr, (*ptr) ? (*ptr)->e : FOLNIL);
        cell->weight = weight;
        cell->e = e->t;         /* we assume e is ground */
        cell->next = *ptr;
        *ptr = cell;
        bslist->count++;
        if (oracle)
            bslist->oracle = cell; 
       //        dyalog_printf("added ptr=%x e=%&f next=%x count=%d n=%d\n",*ptr,(*ptr)->e,(*ptr)->next,bslist->count,bslist->n);
        Succeed;
    } else if (!(*ptr)) {
            /* we have no more room and the component would be the last one
               we skip it
             */
         // printf("caseB\n");
        Fail;
    } else {
            /* we have no more room but the component is not the last one
               we keep it but need to remove the last one
               to avoid a malloc, we reuse the cell of the last component
             */
        fol_t t = e->t;
        slist_t cell = (*ptr);
                                                                      // printf("caseC ptr=%x\n",*ptr);
        for(; cell; cell = cell->next) {
            fol_t tmp_t = cell->e;
            long tmp_w = cell->weight;
            cell->e=t;
            cell->weight = weight;
            if (tmp_t == e->t) {
                                 /* if there exists a smaller tail for e->t, we
                                 * suppress it and no need to continue the relocation */
                Succeed;
            } else if (bslist->oracle == cell) {
                    /* we need to preserve the old oracle cell
                       by allocating a new cell and copy to it
                     */
                slist_t new_oracle_cell = (slist_t) GC_MALLOC(sizeof(struct slist_cell));
                new_oracle_cell->e = tmp_t;
                new_oracle_cell->weight = tmp_w;
                new_oracle_cell->next = cell->next;
                cell->next = new_oracle_cell;
                bslist->oracle = new_oracle_cell;
                Succeed;
            } 
            t=tmp_t;
            weight = tmp_w;
       }
        Succeed;
    }
}

Bool
Slist_Enumerate(bslist_t bslist, sfol_t e, sfol_t w) 
{
    int n = Get_Choice_Counter();
    unsigned long count = bslist->count;
    unsigned long maxl = bslist->n;
    slist_t *ptr = Get_Choice_Buffer(slist_t *);
    slist_t list;
    double tmp_w;
//    dyalog_printf("slist enumerate bslist=%x count=%d n=%d ptr=%x\n",bslist,bslist->count,n,*ptr);
    if (!count) {
        No_More_Choice();
        Fail;
    } else if (n==0) {
        list = bslist->list;
    } else if (n > count || n > maxl) {
        printf("length of sorted list too long count=%ld max=%ld n=%d\n",count,maxl,n);
        No_More_Choice();
        Fail;
    } else {
        list = *ptr;
    }
    tmp_w = list->weight;
    if (list->next) {
        *ptr = list->next;
    } else {
        No_More_Choice();
    }
    return Unify(e->t,e->k,list->e,Key0) && Unify(w->t,w->k,DFOLFLT(tmp_w),Key0);
}

extern float Dynet_Get_Weight(int al);

Bool Dynet_Safe_Get_Weight (int al, sfol_t w) 
{
    return Unify(w->t,w->k,DFOLFLT(Dynet_Get_Weight(al)),Key0);
}
    



typedef struct entering 
{
    fol_t source;
    fol_t target;
    long weight;
    struct entering *next;
} * entering_t;

entering_t
entering_new () 
{
    entering_t first = (entering_t) GC_MALLOC(sizeof(struct entering));
    first->target = NULL;
    first->source = NULL;
    first->weight = 0;
    return first;
}
    
void
entering_add (entering_t first, sfol_t source, sfol_t target, long weight) 
{
    entering_t cell = (entering_t) GC_MALLOC(sizeof(struct entering));
    entering_t *current = &(first->next);
    SFOL_Deref(source);
    SFOL_Deref(target);
    cell->source = source->t;
    cell->target = target->t;
    cell->weight = weight;
    for(; *current && (*current)->weight > weight; current = &((*current)->next));
    cell->next = *current;
    *current = cell;
}

void
entering_merge (entering_t firstA, entering_t firstB, long delta) 
{
    entering_t *current = &(firstA->next);
    entering_t p = firstB->next;
    entering_t next=NULL;
    for(; p; ) {
        p->weight -= delta;
        for(; *current && (*current)->weight > p->weight; current = &((*current)->next));
        next = p->next;
        p->next = *current;
        *current = p;
        p = next;
        current = &((*current)->next);
    }
}

Bool
entering_first(entering_t e, sfol_t source, sfol_t target, sfol_t weight) 
{
    entering_t cell = e->next;
    if (!cell)
        Fail;
    e->next = cell->next;
    return Unify(cell->source,Key0,source->t,source->k)
        && Unify(cell->target,Key0,target->t,target->k)
        && Unify(DFOLINT(cell->weight),Key0,weight->t,weight->k)
        ;
}

extern float Dynet_Predicted_Head_Weight(int dep, int gov);

Bool Dynet_Safe_Predicted_Head_Weight (int dep, int gov, sfol_t w) 
{
    return Unify(w->t,w->k,DFOLFLT(Dynet_Predicted_Head_Weight(dep,gov)),Key0);
}






    
