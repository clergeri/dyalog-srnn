#!/usr/bin/env perl

# extract compliant CONLL from the answers provided by dpsr_parser
# rename ids for lattices

while(<>) {
  if (/##\s+E\d+/) {
    my @dag = ();
    my %map = (0 => 0);
    my $i = 1;
    my $prev;
    while(<>) {
      chomp;
      if (/^\d+/) {
	my @elt = split(/\t/,$_);
	if (@elt == 9) {
	  my $tok = pop(@elt);
	  push(@elt,'_','_');
	  if ($tok =~ /\d+\s*\+\s*(\d+)/) {
	    #  token indication for lattices
	    push(@elt,$1);
	  }
	} else {
	  # old style: deprecated
	  push(@elt,'_','_');
	}
	push(@dag,\@elt);
	unless (defined $prev && $prev == $elt[0]) {
	  $map{$elt[0]} = $i++;
	}
	$prev = $elt[0];
	$elt[0] = $map{$elt[0]};
      }
      if (/^\s*$/) {
	foreach my $elt (@dag) {
	  $elt->[6] = $map{$elt->[6]};
	  my $x = join("\t",@$elt);
	  print "$x\n";
	}
	print "\n";
	last;
      }
    }
  }
}

