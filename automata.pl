/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  automata.pl -- 
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require('format.pl').
:-require('utils.pl').
:-require('features.pl').
:-require('dynet.pl').

:-extensional guide!head/2.
:-extensional guide!head/3.
:-extensional guide!label/2.
:-extensional guide!action/5.
:-extensional guide!xaction/3.

:-extensional oracle!action/2.
:-extensional oracle!item/2.
:-extensional oracle!select/2.
:-extensional oracle!end.
:-extensional oracle!skip_sentence/0.
:-extensional oracle!notproj/2.
:-extensional oracle!shift/3.
:-extensional oracle!redirect/3.
:-extensional oracle!tail/3.
:-extensional oracle!edge/4.
:-extensional oracle!redge/3.
:-extensional oracle!ngov/3.
:-extensional oracle!agree/3.
:-extensional oracle!violation/3.

:-extensional dynoracle!potential_action/2.

:-extensional item!stack/2.
:-extensional item!back/6.
%:-extensional item!step/3.

:-extensional item!nocost_tail/2.

:-extensional beam/1.
:-extensional templates/2.
:-extensional sbeam/1.

:-extensional avgweight/3.

:-extensional maxstep_factor/1.

:-extensional rC/2.

:-extensional complete_mode/0.

:-extensional mst!in/4.

:-finite_set(train,[early,late,best]).

:-finite_set(action,[shift,pop0,pop1,swap,noop,left,right,lefta,righta,left2,right2,swap2,swap3]).
:-subset(simple_action,action[shift,pop0,pop1,swap,noop,swap2,swap3]).
:-subset(complex_action,action[left,right,lefta,righta,left2,right2]).


:-features(item,
	   [
	    step,
	    generation,
	    right,
	    left,
	    stack,
	    stack1,
	    prefix,
	    inside,
	    lk1,		% lookahead
	    lk2,
	    lk3,
	    action,
	    swap
	   ]
	  ).

:-std_prolog weight_update/5.

:-std_prolog item!terminal/2.

item!terminal(N,
	      item{ right => N,
		    left => 0,
		    stack => tree(0,[],_,_,_,_,_),
		    stack1 => [],
		    lk1 => 0,
		    swap => []
		  }
	     ).

:-std_prolog oracle!representative_item/2.

oracle!representative_item(OracleItem::item{ step => S, prefix => OracleW},RepOracleItem) :-
    ( beam!enumerate(S,Item::item{ prefix => W}),
      (Item = OracleItem ->
	   RepOracleItem = OracleItem
       ; more_general_item(Item,OracleItem),
	 ( recorded(Item,ItemAddr),
	   oracle!action(S,Action),
	   item!back(ItemAddr,Action,ItemAddr0,Info1,W,NAction),
	   recorded(Item0::item{ step => S0},ItemAddr0),
	   oracle!item(S0,OracleItem0),
	   more_general_item(Item0,OracleItem0),
	   (Info1 = none 
	    xor
	    ( Info1 = consult: ItemAddr1 xor Info1 = ItemAdrr1),
	    recorded(Item1::item{ step => S1},ItemAddr1),
	    oracle!item(S1,OracleItem1),
	    more_general_item(Item1,OracleItem1)
	   )
	   -> 
	       format('step=~w use rep oracle item w=~w vs oraclew=~w action=~w\n',[S,W,OracleW,Action]),
	       RepOracleItem = Item
	   ;
	   RepOracleItem = OracleItem
	 )
       ) -> 
	  true
      ;
      RepOracleItem = OracleItem
    )
	.

:-std_prolog update_items/5.

%:-xcompiler
update_items(BestItem::item{ step => BestS,
			     right => BestRight,
			     left => BestLeft,
			     stack => BestStack0,
			     stack1 => BestStack1,
			     lk1 => BestId1,
			     lk2 => BestId2,
			     lk3 => BestId3,
			     prefix => BestPrefix,
			     inside => BestInside,
			     action => BestAction,
			     swap => BestSwap
			   },
	     OracleItem::item{ step => _OracleS,
			       right => OracleRight,
			       left => OracleLeft,
			       stack => OracleStack0,
			       stack1 => OracleStack1,
			       lk1 => OracleId1,
			       lk2 => OracleId2,
			       lk3 => OracleId3,
			       prefix => OraclePrefix,
			       inside => OracleInside,
			       action => OracleAction,
			       swap => OracleSwap
			     },
	     OracleItemAddr,
	     BestItem0::item{ step => Step0 },
	     OracleItem0
	    ) :-
    (_OracleS = none -> OracleS = BestS ; OracleS = _OracleS),
%    verbose('update items step=~w oldoracle=~w oaction=~w baction=~w\n',[OracleS,OracleItemAddr,OracleAction,BestAction]),
%    format('update items step=~w oldoracle=~w oaction=~w baction=~w\n',[OracleS,OracleItemAddr,OracleAction,BestAction]),
%    format('update items\n\tbest=~w\n\toracle=~w\n',[BestItem,OracleItem]),
    ( _OracleS = none ->
      PDelta = 1,
      NDelta = -1
    ;
     (oracle!violation(BestS,BestItem,_)
       xor 
       equiv_item(BestItem,OracleItem),
       format('step ~w update between equiv items\n',[BestS]),
       true
      ) ->
%	 format('update with violation\n',[]),
	 PDelta = 2,
	 NDelta = -2
      ; \+ tagset!noop,
	item!terminal(OracleRight,OracleItem),
	OraclePrefix > BestPrefix ->
	    PDelta is 1+OracleRight-BestRight,
%	    format('delta ~w for update5\n',[PDelta]),
	    NDelta is - PDelta
     ;
	 PDelta = 1,
	 NDelta = -1
    ),
%    (weight_update(OracleItem0,OracleItem,PDelta,NPos,_) xor format('failed oracle update ~w\n',[OracleItem]), fail),
    %    (weight_update(BestItem0,BestItem,NDelta,NNeg,_) xor format('failed pred update ~w\n',[BestItem]), fail ),
%    format('update oracle ~w:~w\n',[OracleItemAddr,OracleItem]),
    weight_update(OracleItem0,OracleItem,PDelta,NPos,_),
%    format('done oracle update ~w\n',[OracleItemAddr]),
    recorded(BestItem,BestItemAddr),
%    format('update pred ~w:~w\n',[BestItemAddr,BestItem]),
    weight_update(BestItem0,BestItem,NDelta,NNeg,_),
%    format('done pred update ~w\n',[BestItemAddr]),
    
    %    verbose('update cost npos=~w nneg=~w\n',[NPos,NNeg]),
    (_OracleS = none ->
	 true
    ;
%    format('update old items _OracleS=~w\n',[_OracleS]),
    (beam!cell(OracleS,MBeamList) xor fail), % clean old beam
    mutable_read(MBeamList,BeamList),
    mutable(MBeamList,[]),
    %% actually, OracleItem is not always the oracle item
    %% we change the name (into PosItem vs NegItemm, for instance) !
    %% in the meantime, we introduce TrueOracleItem
    (oracle!item(OracleS,TrueOracleItem) ->
	 recorded(TrueOracleItem,TrueOracleItemAddr),
	 erase(oracle!item(OracleS,_)), % clean old oracle item
	 true
    ;
%    format('update phase with no oracle item at step=~w\n',[OracleS]),
    TrueOracleItem = none,
    TrueOracleItemAddr = 0
    ),
    update_counter(generation,CurrentGen),
    value_counter(generation,NextGen),

    mutable(MNewItems,0),
    


    every(( 

		 ( domain(Item::item{ step => S,
				      generation => CurrentGen,
				      right => Right,
				      left => Left,
				      stack => Stack0,
				      stack1 => Stack1,
				      lk1 => Id1,
				      lk2 => Id2,
				      lk3 => Id3,
				      prefix => OldPrefix, 
				      inside  => OldInside,
				      action => Action,
				      swap => Swap
				    },
			  BeamList),
		   Item \== TrueOracleItem,
		   recorded(Item,ItemAddr)
		 ;
		 TrueOracleItem \== none,
		 Item = TrueOracleItem,
		 ItemAddr = TrueOracleItemAddr,
		 verbose('updating oracle item ~w: ~w\n',[ItemAddr,TrueOracleItem])
		 ),

	     verbose('update process item ~w\n',[ItemAddr]),
	     ( item!back(ItemAddr,Action,ItemAddr0,_,OldPrefix,_),
               \+ ( item!back(ItemAddr,_,_,_,_OldPrefix,_),
		    _OldPrefix > OldPrefix
		  ) -> 
		   true 
	       ; 
	       fail
	     ),
	     verbose('\tupdate from back ~w ~w\n',[ItemAddr,ItemAddr0]),
	     recorded(Item0::item{ prefix => OldPrefix0 },ItemAddr0),
	     OldCost is OldPrefix - OldPrefix0,
	     verbose('\tupdate phase0\n',[]),
	     weight_update(Item0,Item,0,_,NewCost),
	     verbose('\tupdate phase1 newcost=~w\n',[NewCost]),
	     Inc is NewCost - OldCost,
	     verbose('\tupdate phase2 inc=~w\n',[Inc]),
%	     \+ Inc = 0,
	     NewPrefix is OldPrefix + Inc,
	     ( Action = shift(_,_,_) ->
		   NewInside = 0
	       ;
	       NewInside is OldInside + Inc
	     ),
	     verbose('\tupdate phase3 newinside=~w newprefix=~w\n',[NewInside,NewPrefix]),
	     (Item = TrueOracleItem ->
	%	 oracle!item(OracleS,Item) ->
	%	  erase(oracle!item(OracleS,_)),
		  record_without_doublon(oracle!item(OracleS,NewItem))
	      ;
	     true
	     ),
	     %    oracle!register_best_action(OracleS,OracleAction,NewOraclePrefix,NewOracleItem),
	     verbose('\tupdate item step=~w old=~w new=~w cost=~w action=~w\n',[S,OldPrefix,NewPrefix,NewCost,Action]),
/*
	     on_verbose(
		     every(( item!tail(ItemAddr,_Y,_WY),
			     format('\ttail ~w w=~w\n',[_Y,_WY])
			     ;
			     oracle!tail(ItemAddr,_Y,_WY),
			     format('\toracle tail ~w w=~w\n',[_Y,_WY])
			  ))
		 ),
*/
	     
	     register_item(NewItem::item{ step => S,
					  generation => NextGen,
					  right => Right,
					  left => Left,
					  stack => Stack0,
					  stack1 => Stack1,
					  lk1 => Id1,
					  lk2 => Id2,
					  lk3 => Id3,
					  prefix => NewPrefix, 
					  inside  => NewInside,
					  action => Action,
					  swap => Swap
					  },
			   NewItemAddr),

	     mutable_inc(MNewItems,_),

	     verbose('update registered ~w => ~w:~w\n',[ItemAddr,NewItemAddr,NewItem]),
	       every(( item!back(ItemAddr,_Action,A,B,_Prefix,_NAction),
		       _NewPrefix is _Prefix + Inc,
		       record_without_doublon(item!back(NewItemAddr,_Action,A,B,_NewPrefix,_NAction))
		    )),
	       %	     verbose('here before reroot oracle tails\n',[]),
	       ( Action = shift(_,_,_) ->
		     every((item!tail(ItemAddr,_Y,_WY),
			    _NewWY is _WY + Inc,
			    item!add_tail(NewItemAddr,ItemAddr0,_NewWY)
			  )),
		     once_xor_true(( 
					 oracle!item(S,NewItem),
					 oracle!tail(ItemAddr,_X,_WX),
					 \+ ( oracle!tail(ItemAddr,__X,__WX), 
					     __WX > _WX,
					     format('multiple oracle tails step=~w item=~w:~w\n',[S,NewItemAddr,NewItem])
					    ),
					 %				verbose('\treroot oracle tail ~w w=~w\n',[_X,_WX]),
					 _NewWX is _WX + Inc,
					 record_without_doublon(oracle!tail(NewItemAddr,_X,_NewWX))
				 )),
		     true
		 ;
		 /*
			      item!tails(ItemAddr,OldTList),
			      '$interface'('Slist_Clone'(OldTList:ptr),[return(TList:ptr)]),
			      record_without_doublon(item!tails(NewItemAddr,TList)),
*/
		 every((item!tail(ItemAddr,_Y,_WY),
			%				  verbose('\treroot tail ~w ~w\n',[_Y,_WY]),
			item!add_tail(NewItemAddr,_Y,_WY)
		      )),
		 once_xor_true((  oracle!item(S,NewItem),
				  oracle!tail(ItemAddr,_X,_WX),
					 \+ ( oracle!tail(ItemAddr,__X,__WX), 
					     __WX > _WX,
					     format('multiple oracle tails step=~w item=~w:~w\n',[S,NewItemAddr,NewItem])
					    ),
				  %% verbose('\treroot oracle tail ~w w=~w\n',[_X,_WX]),
				  record_without_doublon(oracle!tail(NewItemAddr,_X,_WX))
			      )),
		 true
	     )

	     )),

    mutable_read(MNewItems,NewItems),
    (NewItems > 0 ->
	 %% everything seems ok
	 record_without_doublon(item!delete_generation(OracleS,CurrentGen)),
	 true
    ;
    format('*** the update of the old items seems to have failed ! we reinstall old beam\n',[]),
    mutable(MBeamList,BeamList),
    true
    )

    ),
	 %    verbose('done update step=~w',[OracleS]),
	 fail
	.


:-std_prolog check_info/1.

check_info(Step) :-
    NStep is Step+1,
%    format('check step=~w\n',[NStep]),
    ( oracle!item(NStep,OracleItem), OracleItem=item{ prefix => OracleW } xor OracleItem = []),
    ( oracle!action(NStep,OracleAction) xor OracleAction = []),
    every((
		 beam!enumerate(NStep,Item::item{ prefix => W}),
		 recorded(Item,ItemAddr),
		 (Item = OracleItem -> Mark = '*' ; Mark = ''),
		 (item!back(_ItemAddr,Action,ItemAddr0,_,W,NAction) xor true),
		 format('\tcheck step=~w item=~w w=~w action=~w parent=~w ~w\n',
			[NStep,ItemAddr,W,Action,ItemAddr0,Mark])
	 )),
    ( beam!enumerate(NStep,OracleItem) 
	  xor 
	  oracle!item(Step,PrevOracleItem),
          beam!enumerate(Step,PrevOracleItem),
	  format('*** check step=~w oracle item not in beam (w=~w action=~w)\n',[NStep,OracleW,OracleAction]))
.

:-extensional item{}.

:-std_prolog generate_shift/5.

%% shift next word to the stack
generate_shift( Item0::item{ step => S,
			     right => J,
			     stack => Stack0,
			     stack1 => Stack1,
			     prefix => Prefix,
			     lk1 => _Id1,
			     lk2 => _Id2,
			     lk3 => _Id3,
			     prefix => Prefix0,
			     action => Action0,
			     swap => Swap
			     %swap => Swap
			   },
		ItemAddr0,
		BuildType,
		Action::shift(Id1,Id2,Id3),
		Loss
	      ) :-
    %    (\+ Action0 = action[swap2,swap3] xor Swap = []),
    (tagset!allow_swap_on_shift xor Swap = []),
    (fail,
     tagset!ensure_tree ->
        %% force a reduction if Stack0 is governor of Stack1
        %% and therefore block shift action
          \+ (Stack0 = tree(Head0,child_info(_,Head1,_,_,_,_,_),After0,Gov0,_,_,_),
	     Stack1 = tree(Head1,Before1,After1,_,_,_,_)
	    )
    ;
    true
    ),
    %	verbose('try shift step=~w item0=~w\n',[XS,Item0]),
    %    	format('try shift step=~w action=~w item0=~w\n',[XS,Action,Item0]),
	(J = 0 xor Id1=_Id1, Id2 = _Id2, Id3 = _Id3),
	recorded('C'(J,Id1,Entry1,K),Addr),
	K > J,	   % block loops !
	'N'(N),
	(K=N -> K2=N, Id2 = 0, Entry2 = [] ; 'C'(K,Id2,Entry2,K2)),
	(K2=N -> Id3 = 0, K3=N, Entry3 = [] ; 'C'(K2,Id3,Entry3,K3)),
	( BuildType = true ->
	  XS is S+1,
	  value_counter(generation,CurrentGen),
	  safe_action_decompose(Action0,Base0,Label0),
	  pre_table_lookup(J,Id1,Id2,Id3,Stack0,Stack1,Feat0,Feat1,EntryI,EntryI2,EntryI3,PoncFeatsI1,Guide,I4), 
	  (K3 = N -> Id4 = 0, Entry4 = [] ; 'C'(K3,Id4,Entry4,_)),
	  \+ tagset!block(Entry2,Entry3,Entry4),
	  post_table_lookup(shift,0,Id4,0,Feat0,Feat1,[], Id1: EntryI, Id2 : EntryI2, Id3 : EntryI3,PoncFeatsI1,Guide,I4,S,J,Cost,0,0,[Base0,Label0],0,NAction),
	  XPrefix is Prefix+Cost,
	  _K=K,
	  once_xor_true((
			       oracle!item(S,Item0),
			       try_register_oracle(XS,shift(Id1,Id2,Id3),Item,Cost)
			   ))
	;
	pre_pseudo_table_lookup(J,Id1,Id2,Id3,Id4),
	Cost = nocost,
	XPrefix = nocost,
	XS = none,
	CurrentGen = none,
	NAction = Action,
	_K=0,
	true
	),
	register_item( Item::item{ step => XS,
				   generation => CurrentGen,
				   right => K,
				   left => _K,
				   %left => 0,
				   stack => NewStack::tree(Addr,[],[],[],[],Addr,Addr),
				   stack1 => Stack0,
				   prefix => XPrefix,
				   inside => 0,
				   lk1 => Id2,
				   lk2 => Id3,
				   lk3 => Id4,
				   action => Action,
				   swap => []
				 },
		       ItemAddr
		     ),
	item!add_tail(ItemAddr,ItemAddr0,XPrefix),
	(BuildType = true ->
	     ( oracle!item(XS,Item),
	       oracle!item(S,Item0) 
	     ->
	     record_without_doublon(oracle!tail(ItemAddr,ItemAddr0,XPrefix))
	     ;
	     true
	     ),
	     record_without_doublon( B::item!back(ItemAddr,Action,ItemAddr0,none,XPrefix,NAction)),
	     Loss = 0,
	     true
	;
	item!check_loss(ItemAddr,Item,Loss),
	(Loss = 0 %, S \== none
	->
	     record(item!noloss_descendant(S,ItemAddr0,ItemAddr)),
	     record_without_doublon( B )
	;
	true
	)
	),
	true
	.

:-xcompiler
item!check_loss(ItemAddr,Item,Loss) :-
    (item!noloss(ItemAddr) ->
	 Loss = 0 ;
     erase_item(Item),
     Loss = 1)
.
  
:-std_prolog generate_other/5.


generate_other(Item::item{ step => S,
			   right => K,
%			   left => J,
			   stack => Stack0,
			   stack1 => Stack1,
			   lk1 => Id1,
			   lk2 => Id2,
			   lk3 => Id3,
			   prefix => Prefix0,
			   action => Action0,
			   swap => Swap
			 },
	       ItemAddr,
	       BuildType,
	       Action,
	       Loss
	      ) :-
    K > 0,
%    (var(Action) xor safe_action_decompose(Action,Base,Label)),
    (BuildType == true ->
	 safe_action_decompose(Action0,Base0,Label0),
	 pre_table_lookup(K,Id1,Id2,Id3,Stack0,Stack1,Feat0,Feat1,EntryI,EntryI2,EntryI3,PoncFeatsI1,Guide,I4),
	 ( oracle!item(S,Item),
	   XS is S+1,
	   oracle!action(XS,OracleAction),
	   safe_action_decompose(OracleAction,OracleBaseAction,OracleLabel),
	   transition_type(OracleAction,OracleType),
%	   format('oracle action step=~w oaction=~w action=~w\n',[XS,_Action,Action]),
	   %% be sure to apply the oracle actions, despite the beam
	   Oracle=1
	 ;
	 recorded(beam!inside(ItemAddr,S)),
	 Oracle=0,
	 AllM = [(left : MLeft), (right : MRight), (left2 : MLeft2), (right2 : MRight2), (pop0 : MPop0) , (pop1 : MPop1)],
	 mutable(MLeft,0,true),
	 mutable(MRight,0,true),
	 mutable(MLeft2,0,true),
	 mutable(MRight2,0,true),
	 mutable(MPop0,0,true),
	 mutable(MPop1,0,true),
	 beam(Beam),
	 %     mutable(M,0,true),
	 mutable(MMax,_,true)
	 )
    ;
    pre_pseudo_table_lookup(K,Id1,Id2,Id3,Id4),
    Oracle=0,
    true
    ),
%%    format('Step ~w other oracle=~w item=~w\n',[S,Oracle,Item]),
    ( 				%% actions using Stack2: left/right left2/right2 pop0 pop1 swap2 swap3
	(Swap = [] -> XStack2 = Stack2 ; XStack2 = Swap),
%%	format('Step ~w branch2 beam ~w\n',[S,Beam]),
	item!xtail(ItemAddr,ItemAddr1,ShiftPrefix1),
%%	format('Step ~w branch2 xtail ~w\n',[S,ItemAddr1]),
	recorded(
		Item1::item{
			 step => S1,
			 stack1 => Stack2
		     },
		ItemAddr1
	    ),
%	format('Step ~w branch2 shiftprefix=~w item1 ~w:~w\n',[S,ShiftPrefix1,ItemAddr1,Item1]),
	( Oracle = 1
	 ->	  
	     %	     format('branch2 oracle XS=~w Action=~w BaseAction=~w\n',[XS,Action,BaseAction]),
	     OracleType = tail,
	     oracle!item(S1,Item1),
	     Action=OracleAction,
%	     BaseAction = OracleBaseAction,
	     Label = OracleLabel,
	     Base=OracleBaseAction
	 ;
	 true
	),
%	format('Step ~w try post table2\n',[S]),
	(BuildType = true ->
	     ShiftPrefix1 \== nocost,
	     post_table_lookup(Base,0,Id4,Label,Feat0,Feat1,XStack2,Id1 : EntryI, Id2 : EntryI2, Id3 : EntryI3,PoncFeatsI1,Guide,I4,S,K,Cost,BaseAction,0,[Base0,Label0],Oracle,NAction),
	     true
	;
%	format('Step ~w branch2 shiftprefix=~w item1 ~w:~w\n',[S,ShiftPrefix1,ItemAddr1,Item1]),
	Cost = nocost,
	(Prefix0 = nocost xor ShiftPrefix1 \== nocost),
	domain(Base,action[left,right,left2,right2,pop0,pop1,swap2,swap3]),
	NAction = Action,
	true
	),
	action_recompose(Base,Label,Action),
	transition_type(Action,tail),
%	format('\t=> PreCost=~w Action=~w\n',[Cost,Action]),
	( Oracle = 1 -> true
	; BuildType = true ->
	  mutable_read(MMax,_Max),
	  ( var(_Max) ->
	    true
	  ; Cost > _Max,
	    true
	  ),
	  (domain(Base:M,AllM) ->
		mutable_read(M,_V),
		_V < Beam
	  ;
	  true
	  ),
	  true
	;
	true
	),
%	format('\t=> Step=~w Cost=~w Action=~w\n',[S,Cost,Action]),
	generate_tail_transition(Base,Item,ItemAddr,Action,Cost,Item1,ItemAddr1,ShiftPrefix1,Loss,NAction)
	    
     ;				%% actions not using Stack2: lefta/righta swap noop
     ( Oracle = 1 ->
	   %	   format('oracle XS=~w Action=~w\n',[XS,Action]),
	   OracleType = notail,
	   Action = OracleAction,
%	   BaseAction = OracleBaseAction,
	   Label = OracleLabel,
	   Base=OracleBaseAction,
%	   format('Step ~w try post table1 action=~w\n',[S,Action]),
	   true
      ;
      true
     ),
     %% format('Step ~w try post table1 action=~w\n',[S,Action]),
     ( BuildType = true ->
       ShiftPrefix1 \== nocost,
       post_table_lookup(Base,0,Id4,Label,Feat0,Feat1,Swap, Id1 : EntryI, Id2 : EntryI2, Id3 : EntryI3,PoncFeatsI1,Guide,I4,S,K,Cost,BaseAction,0,[Base0,Label0],Oracle,NAction),
       true
     ;
     Cost = nocost,
     (Prefix0 = nocost xor ShiftPrefix1 \== nocost),
     %     domain(Base,action[lefta,righta,swap,noop]),
     domain(Base,[lefta,righta,noop,swap]),
     NAction = Action,
     true
     ),
     action_recompose(Base,Label,Action),
     transition_type(Action,notail),
     %     format('\t=> Cost=~w Action=~w\n',[Cost,Action]),
     ( Oracle = 1 -> true
     ; BuildType = true ->
       mutable_read(MMax,_Max),
	( var(_Max) ->
	      true
	 ; Cost > _Max,
	   true
	),
	true
     ;
     true
     ),
     generate_notail_transition(Base,Item,ItemAddr,Action,Cost,Loss,NAction)
    ),

    %% at least one item has been generated
    ( %fail,
	BuildType = true,
	Oracle = 0 ->
	%% if the item is not an oracle one, we update bounds (number of items and lower cost)
	%	  mutable_inc(M,_),
	( (var(_Max) xor Cost - 100 > _Max ) ->
	  _NewMax is Cost - 100,
	  mutable(MMax,_NewMax)
	;
	true
	),
	(domain(Base:M,AllM) ->
	     mutable_inc(M,_)
	;
	true
	)
    ;
    true
    )

.

:-rec_prolog generate_tail_transition/10.
:-rec_prolog generate_notail_transition/7.

:-extensional transition_type/2.

transition_type(left(_),tail).
transition_type(right(_),tail).

%:-std_prolog generate_reduce/7.

generate_tail_transition(
	Base::action[left,right],
	Item0::item{ step => S,
		     right => K,
		     stack => Stack0::tree(Head0,Before0,After0,Gov0,GovHead0,LeftHead0,RightHead0),
		     stack1 => Stack1::tree(Head1,Before1,After1,Gov1,GovHead1,LeftHead1,RightHead1),
		     lk1 => Id1,
		     lk2 => Id2,
		     lk3 => Id3,
		     prefix => Prefix0,
		     action => Action0,
		     swap => Swap
		   },
	ItemAddr0,
	Action,
	Cost,
	Item1::item{
	    step => S1,
	    right => J,
	    left => I,
	    stack => _Stack1,
	    stack1 => Stack2,
	    swap => Swap1
	},
	ItemAddr1,
	ShiftPrefix1,
	Loss,
	NAction
    ) :-
    tagset!reduce,
    Head0 \== 0,
    (Action=left(Label), Base=left xor Action=right(Label), Base=right),
    (tagset!ensure_tree ->
	 %% we ensure that each node has at most one governor
	 ( Gov0 = [], Gov1 = [] -> true
	  ; Gov0 = [_], Gov1 = [] -> Action = right(Label)
	  ; Gov1 = [_], Gov0 = [] -> Action = left(Label)
	  ; fail
	 )
     ;
     true
    ),
    %% in case of (non virtual) single root
    %% the root should stay on the stack until the very end (to be attached to the virtual root)
    ( tagset!single_root ->
      (Head1 \== 0 xor Id1 = 0, 'N'(K), Action = left(Label))
    ;
    true
    ),
    (Cost == nocost ->
	 recorded('C'(_,Pos0,_,_),Head0),
	 (Head1 = 0 -> Pos1 = 0 ; recorded('C'(_,Pos1,_,_),Head1)),
	 (Action = left(Label),
	  oracle!edge(Pos1,Pos0,Label,_)
	 ;
	 Action = right(Label),
	 oracle!edge(Pos0,Pos1,Label,_)
	 ),
	 XS = none,
	 CurrentGen = none,
	 %	 format('try reduce0 step=~w action=~w cost=~w item=~w\n',[S,Action,Cost,Item0]),
	 true
    ;
    XS is S + 1,
    value_counter(generation,CurrentGen),
    true
    ),
    %	verbose('try reduce step=~w item0=~w\n',[XS,Item0]),
    ( Head1 \== 0 ->  true ; Action=left(Label) ),
    
    %% Some hard constraints
    %% No multiedges (at this stage) and no cycle
    ( After1 = child_info(_,Head0,_,_,_,_,_) ->
	  fail
     ; Before0 = child_info(_,Head1,_,_,_,_,_) ->
	   fail
     ;
     true
    ),
    (Swap = [] -> XStack2 = Stack2 ; XStack2 = Swap),
    (Action = left(Label),
     %% left reduce
     (After1 = child_info(_PLabel , _PHead , AV1 , AD1,_,_,_) ->
	  XAV1 is AV1 + 1
	  ;
	  XAV1 = 1,
	  AD1 = [],
	  _PLabel = 0,
	  _PHead = []
     ),
     NewStack = tree(Head1,Before1,child_info(Label,Head0,XAV1,XAD1,Gov0,_PLabel,_PHead),Gov1,GovHead1,LeftHead1,RightHead0),
     child_extend_alt(Cost,Label,AD1,XAD1)
     ; %%After0 = [],
     %% right reduce
     %% can only apply before any shift operation
     Action = right(Label),
     (Before0 = child_info(_PLabel , _PHead , BV0 , BD0,_,_,_) -> 
	  XBV0 is BV0 + 1 
      ; XBV0 = 1, BD0=[], _PLabel = 0, _PHead = []
     ),
     NewStack = tree(Head0,child_info(Label,Head1,XBV0,XBD0,Gov1,_PLabel,_PHead),After0,Gov0,GovHead0,LeftHead1,RightHead0),
     child_extend_alt(Cost,Label,BD0,XBD0)
    ),
    (opt(train:_) ->
	 recorded('C'(_,Pos0,_,_),Head0),
	 (Head1 = 0 -> Pos1 = 0 ; recorded('C'(_,Pos1,_,_),Head1)),
	 (Base = left -> 
	      PosH=Pos1, PosD=Pos0, InfoD=Stack0
	  ; PosH=Pos0, PosD=Pos1, InfoD=Stack1
	 ),
	 InfoD = tree(_,BeforeD,AfterD,_,_,_,_),
	 (BeforeD = child_info(_,_,NBD,DBD,_,_,_) xor NBD=0, DBD=[]),
	 (AfterD = child_info(_,_,NAD,DAD,_,_,_) xor NAD=0, DAD=[]),
	 ND is NBD+NAD,
	 (oracle!edge(PosH,PosD,Label,_ND) ->
	      ( 
%		  pos_before(PosH,PosD), 
		  oracle!edge(PosD,PosX,LabelX,_), 
%		  pos_before(PosD,PosX),
		  rC(PosX,U),
		  U >= K,
		  true
	      ->
		  %% reduction is too early: could have waited to collect the descendants of PosD
%		  format('violation1 ~w-~w->~w\n',[PosD,LabelX,PosX]),
	      	  Edge = violate
               ; \+ ND = _ND ->
		     Edge = []
	       ; oracle!edge(PosD,_,LabelX,_),
                      \+ domain(LabelX,DBD),
                      \+ domain(LabelX,DAD)
		 ->
		     Edge = []
	       ;
	       Edge = agree
	      )
	 ;
%	 format('violation1 badlabel\n',[]),
	  Edge = violate
	 )
     ;
     Edge = []
    ),
    ( Cost == nocost, Edge = violate ->
      fail
    ;
    true
    ),
    %	verbose('potential reduce step=~w item0=~w => ~w\n',[XS,Item0,Item]),
    full_register_item_two_parents(
	    Action,
	    Item::item{ step => XS,
			generation => CurrentGen,
			right => K,
			left => I,
			stack => NewStack,
			stack1 => XStack2,
			lk1 => Id1,
			lk2 => Id2,
			lk3 => Id3,
			action => Action,
			swap => Swap1
		      },
	    ItemAddr0 : Item0,
	    ItemAddr1 : Item1,
	    Cost,
	    ShiftPrefix1,
	    Edge,
	    Loss,
	    NAction
    ),
    %	verbose('register reduce item ~w\n',[Item]),
    true
.


:-xcompiler
child_extend_alt(Cost,Label,Set,XSet) :-
    child_extend(Label,Set,XSet)
.

:-std_prolog should_reduce/2.

should_reduce(Head0,Head1) :-
    Head0 \== 0,
    recorded('C'(_,Pos0,_,_),Head0),
    (Head1 = 0 -> Pos1 = 0 ; recorded('C'(_,Pos1,_,_),Head1)),
    Max is max(Pos0,Pos1),
    (oracle!edge(Pos0,Pos1,_,_),
     \+ (oracle!redge(Pos1,Pos2,_), Pos2 > Max)
    ; oracle!edge(Pos1,Pos0,_,_),
      \+ (oracle!redge(Pos0,Pos2,_), Pos2 > Max)
    )
    .

:-light_tabular is_not_projective/1.
:-mode(is_not_projective/1,+(+)).

is_not_projective(Head) :-
    Head \== 0,
    recorded('C'(_,Pos,_,_),Head),
    oracle!redge(Pos,GPos,_),
    Min is min(Pos,GPos),
    Max is max(Pos,GPos),
    term_range(Min,Max,_Pos),
    (oracle!edge(_Pos,Pos2,_,_) ; oracle!redge(_Pos,Pos2,_)),
    _Pos > Min,
    _Pos < Max,
    (Pos2 < Min ; Pos2 > Max),
    format('** not projective ~w\n',[Head]),
    true
.


:-std_prolog suggest_oracle_action/2.

suggest_oracle_action(item{
			  right => Right,
			  stack => Stack0::tree(Head0,Before0,After0,Gov0,GovHead0,LeftHead0,RightHead0),
			  stack1 => Stack1::tree(Head1,Before1,After1,Gov1,GovHead1,LeftHead1,RightHead1),
			  action => Action0,
			  lk1 => Id1,
			  lk2 => Id2,
			  lk3 => Id3
		      },Action) :-
    recorded('C'(_,Pos0,_,_),Head0),
    (Head1 = 0 -> Pos1 = 0 ; recorded('C'(_,Pos1,_,_),Head1)),
    Max is max(Pos0,Pos1),
    (Action = left(Label),
     oracle!edge(Pos1,Pos0,Label,_),
      \+ (oracle!edge(Pos0,Pos2,_,_), Pos2 > Max)
    xor
    Action = right(Label),
    oracle!edge(Pos0,Pos1,Label,_),
    \+ (oracle!edge(Pos1,Pos2,_,_), Pos2 > Max)
    xor
    'N'(N),
    Right < N,
    (Id1 = 0 -> _Id1 = Id1, _Id2 = Id2, _Id3 = Id3 ; _Id1 = Id1, _Id2 = Id2, _Id3 = Id3 ),
    Action = shift(_Id1,_Id2,_Id3),
    record_without_doublon(oracle!shift(_Id1,_Id2,_Id3))
    xor
    Action = left(Label),
    oracle!edge(Pos1,Pos0,Label,_)
    xor
    Action = right(Label),
    oracle!edge(Pos0,Pos1,Label,_)
    ),
    true
    .

transition_type(lefta(_),notail).
transition_type(righta(_),notail).

%:-std_prolog generate_attach/4.

generate_notail_transition(
	Base::action[lefta,righta],
	Item0::item{ step => S,
		     right => K,
		     left => I,
		     stack => Stack0::tree(Head0,Before0,After0,Gov0,GovHead0,LeftHead0,RightHead0),
		     stack1 => Stack1::tree(Head1,Before1,After1,Gov1,GovHead1,LeftHead1,RightHead1),
		     lk1 => Id1,
		     lk2 => Id2,
		     lk3 => Id3,
		     prefix => Prefix0,
		     action => Action0,
		     swap => Swap
		   },
	ItemAddr0,
	Action,
	Cost,
	Loss,
	NAction
    ) :-
    tagset!attach,
    Head0 \== 0,
    %%  no need to attach after a swap, because we can reverse the order
    \+ Action0=swap,
    %%  not possible to do 2 attach between the same pair of nodes (otherwise, cycle or multitail)
    (tagset!allow_cycle xor \+ domain(Action0,[lefta(_),righta(_)])),
    (Action=lefta(Label), Base=lefta xor Action = righta(Label), Base = righta, Head1 \== 0),
%    (Cost \== nocost xor dynoracle!potential_action(S,Action)),
    (tagset!ensure_tree ->
	 %% we ensure that each node has at most one governor
	 ( Gov0 = [], Gov1 = [] -> true
	  ; Gov0 = [_], Gov1 = [] -> Action = righta(Label)
	  ; Gov1 = [_], Gov0 = [] -> Action = lefta(Label)
	  ; fail
	 ),
	 (%fail,
              \+ tagset!late_attach, 
	      After0 = child_info(_,_,_,_,_,_,_ ),
	      Gov1 = [_]
	  ->
	      %% early attach for trees: lefta only possible if topmost element has no after child
	      Action = righta(Label)
	  ;
	  true
	 )
     ; tagset!allow_cycle ->
	   %% allow cycle, but block multiedge
           (After1 = child_info(_,Head0,_,_,_,_,_),
	    Before0 = child_info(_,Head1,_,_,_,_,_) ->
		fail
	    ; After1 = child_info(_,Head0,_,_,_,_,_) ->
		Action = righta(Label)
	    ; Before0 = child_info(_,Head1,_,_,_,_,_) ->
		  Action = lefta(Label)
	    ;
	    true
	   )
     ;
     %% avoid direct cycle and multi-edges
     (After1 = child_info(_,Head0,_,_,_,_,_) xor Before0 = child_info(_,Head1,_,_,_,_,_)) -> fail
     ;
     true
    ),
    (Cost == nocost ->
	 recorded('C'(_,Pos0,_,_),Head0),
	 (Head1 = 0 -> Pos1 = 0 ; recorded('C'(_,Pos1,_,_),Head1)),
	 (Action = lefta(Label),
	  oracle!edge(Pos1,Pos0,Label,_)
	 ;
	 Action = righta(Label),
	 oracle!edge(Pos0,Pos1,Label,_)
	 )
    ;
    true
    ),
    (Cost == nocost -> XS = none, CurrentGen = none ; XS is S + 1, value_counter(generation,CurrentGen)),
    %% Some hard constraints
    %% No multiedges (at this stage) and no cycle
    ( Action = lefta(Label),
      %% left attach
      (After1 = child_info(_PLabel , _PHead , AV1 , AD1,_,_,_ ) ->
	   XAV1 is AV1 + 1
       ;
       XAV1 = 1,
       AD1 = [],
       _PLabel = 0,
       _PHead = []
      ),
      %	  (GovHead0 = [] -> NewGovHead0 = Head1 ; NewGovHead0 = GovHead0),
      NewGovHead0 = Head1,
      NewStack0 = tree(Head0,Before0,After0,[Label|Gov0],NewGovHead0,LeftHead0,RightHead0),
      NewStack1 = tree(Head1,Before1,child_info(Label,Head0,XAV1,XAD1,Gov0,_PLabel,_PHead),Gov1,GovHead1,LeftHead1,RightHead0),
      child_extend_alt(Cost,Label,AD1,XAD1)
     ; %% After0 = [],
     %% right attach
     Action = righta(Label),
     (Before0 = child_info(_PLabel , _PHead , BV0 , BD0,_,_,_) -> 
	  XBV0 is BV0 + 1 
      ; XBV0 = 1, BD0=[], _PLabel = 0, _PHead = []
     ),
     NewStack0 = tree(Head0,child_info(Label,Head1,XBV0,XBD0,Gov1,_PLabel,_PHead),After0,Gov0,GovHead0,LeftHead0,RightHead1),
     child_extend_alt(Cost,Label,BD0,XBD0),
     %	  (GovHead1 = [] -> NewGovHead1 = Head0 ; NewGovHead1 = GovHead1),
     NewGovHead1 = Head0,
     NewStack1 = tree(Head1,Before1,After1,[Label|Gov1],NewGovHead1,LeftHead1,RightHead1)
    ),
    %% Violations
    (opt(train:_) ->
	 recorded('C'(_,Pos0,_,_),Head0),
	 (Head1 = 0 -> Pos1 = 0 ; recorded('C'(_,Pos1,_,_),Head1)),
	 (Base = lefta -> 
	      PosH=Pos1, PosD=Pos0, InfoD=Stack0
	  ; PosH=Pos0, PosD=Pos1, InfoD=Stack1
	 ),
	 InfoD = tree(_,BeforeD,AfterD,_,_,_,_),
	 (BeforeD = child_info(_,_,NBD,DBD,_,_,_) xor NBD=0, DBD=[]),
	 (AfterD = child_info(_,_,NAD,DAD,_,_,_) xor NAD=0, DAD=[]),
	 ND is NBD+NAD,
	 ( oracle!edge(PosH,PosD,Label,_ND) ->
	   %	       Edge = []
	   Edge = agree
	  ; 
	  Edge = violate
	 )
     ;
     Edge = []
    ),
    %	format('potential attach step=~w item0=~w => cost=~w ~w\n',[XS,Item0,Cost,Item]),
    full_register_item_single_parent(
	    Action,
	    Item::item{ step => XS,
			generation => CurrentGen,
			right => K,
			left => I,
			stack => NewStack0,
			stack1 => NewStack1,
			lk1 => Id1,
			lk2 => Id2,
			lk3 => Id3,
			action => Action,
			swap => Swap
		      },
	    ItemAddr0 : Item0,
	    Cost,
	    Edge,
	    Loss,
	    NAction
	),
    true
.


transition_type(left2(_),tail).
transition_type(right2(_),tail).

%:-std_prolog generate_reduce2/7.

generate_tail_transition(
	Base::action[left2,right2],
	Item0::item{ step => S,
		     right => K,
		     stack => Stack0::tree(Head0,Before0,After0,Gov0,GovHead0,LeftHead0,RightHead0),
		     stack1 => Stack1::tree(Head1,Before1,After1,Gov1,GovHead1,LeftHead1,RightHead1),
		     lk1 => Id1,
		     lk2 => Id2,
		     lk3 => Id3,
		     prefix => Prefix0,
		     action => Action0,
		     swap => []
			    },
	ItemAddr0,
	Action,
	Cost,
	Item1::item{
		 step => S1,
		 right => J,
		 left => I,
		 stack => _Stack1,
		 stack1 => Stack2::tree(Head2,Before2,After2,Gov2,GovHead2,LeftHead1,RightHead2),
		 swap => Swap1
	     },
	ItemAddr1,
	ShiftPrefix1,
	Loss,
	NAction
    ) :-
    tagset!reduce2,
    Head0 \== 0,
    Head1 \== 0,
    (Action = left2(Label), Base=left2 xor Action = right2(Label), Base=right2),
%    (Cost \== nocost xor dynoracle!potential_action(S,Action)),
    (Cost == nocost -> XS = none, CurrentGen = none ; XS is S + 1, value_counter(generation,CurrentGen)),
    %	verbose('try reduce2 step=~w item0=~w:~w\n',[XS,ItemAddr0,Item0]),

    %    safe_action_decompose(Action0,Base0,Label0),
    %	format('try1 reduce step=~w item0=~w oracle=~w action=~w\n',[XS,Item0,Oracle,Action]),
    %	format('potential reduce step=~w item0=~w item1=~w base=~w\n',[XS,Item0,Item1,Base]),
    %	pre_table_lookup(K,Id1,Id2,Id3,Stack0,Stack1,Feat0,Feat1,EntryI,EntryI2,EntryI3,PoncFeatsI1,Guide,I4), 
    %	verbose('try2 reduce2 step=~w item0=~w item1=~w\n',[XS,Item0,Item1]),
	
    (tagset!ensure_tree ->
	     %% we ensure that each node has at most one governor
	 ( Gov0 = [], Gov2 = [] -> true
	  ; Gov0 = [_], Gov2 = [] -> Action = right2(Label)
	  ; Gov2 = [_], Gov0 = [] -> Action = left2(Label)
	  ; fail
	 )
     ;
     true
    ),

    (Cost == nocost ->
	 recorded('C'(_,Pos0,_,_),Head0),
	 (Head2 = 0 -> Pos2 = 0 ; recorded('C'(_,Pos2,_,_),Head2)),
	 (Action = left2(Label),
	  oracle!edge(Pos1,Pos0,Label,_)
	 ;
	 Action = right2(Label),
	 oracle!edge(Pos0,Pos1,Label,_)
	 )
    ;
    true
    ),

    ( (%After0 = [], 
	    Head2 \== 0) ->  true ; Action=left2(Label) ),
    
    %	verbose('try3 reduce2 step=~w item0=~w item1=~w\n',[XS,Item0,Item1]),
	
    %% Some hard constraints
    %% No multiedges (at this stage) and no cycle
    ( After2 = child_info(_,Head0,_,_,_,_,_) ->
	  fail
     ; Before0 = child_info(_,Head2,_,_,_,_,_) ->
	   fail
     ;
     true
    ),

%	verbose('try4 reduce2 step=~w item0=~w item1=~w\n',[XS,Item0,Item1]),
%%	format('\treduce step cost ~w base=~w label=~w oracle1=~w\n',[Cost,Base,Label,Oracle1]),
%%	format('reduce base=~w label=~w cost=~w\n',[Base,Label,Cost]),
    ( Action = left2(Label),
      %% left reduce
      (After2 = child_info(_PLabel , _PHead , AV2 , AD2,_,_,_) ->
	   XAV2 is AV2 + 1
       ;
       XAV2 = 1,
       AD2 = [],
       _PLabel = 0,
       _PHead = []
      ),
      NewStack1 = tree(Head2,Before2,child_info(Label,Head0,XAV2,XAD2,Gov0,_PLabel,_PHead),Gov2,GovHead2,LeftHead2,RightHead0),
      child_extend_alt(Cost,Label,AD2,XAD2),
      NewStack0 = Stack1
     ; %%After0 = [],
     %% right reduce
     %% can only apply before any shift operation
     Action = right2(Label),
     (Before0 = child_info(_PLabel , _PHead , BV0 , BD0,_,_,_) -> 
	  XBV0 is BV0 + 1 
      ; XBV0 = 1, BD0=[], _PLabel = 0, _PHead = []
     ),
     NewStack0 = tree(Head0,child_info(Label,Head2,XBV0,XBD0,Gov2,_PLabel,_PHead),After0,Gov0,GovHead0,LeftHead2,RightHead0),
     child_extend_alt(Cost,Label,BD0,XBD0),
     NewStack1 = Stack1
    ),
    (opt(train:_) ->
	 recorded('C'(_,Pos0,_,_),Head0),
	 (Head2 = 0 -> Pos2 = 0 ; recorded('C'(_,Pos2,_,_),Head2)),
	 (Base = left2 -> 
	      PosH=Pos2, PosD=Pos0, InfoD=Stack0
	  ; PosH=Pos0, PosD=Pos2, InfoD=Stack2
	 ),
	 InfoD = tree(_,BeforeD,AfterD,_,_,_,_),
	 (BeforeD = child_info(_,_,NBD,DBD,_,_,_) xor NBD=0, DBD=[]),
	 (AfterD = child_info(_,_,NAD,DAD,_,_,_) xor NAD=0, DAD=[]),
	 ND is NBD+NAD,
	 (oracle!edge(PosH,PosD,Label,_ND) ->
	      (
%		  pos_before(PosH,PosD), 
		  oracle!edge(PosD,PosX,_,_), 
%		  pos_before(PosD,PosX),
		  rC(PosX,U),
		  U >= K
	      ->
		      %% reduction is too early: could have waited to collect the descendants of PosD
		      Edge = violate
               ; \+ ND = _ND ->
		     Edge = []
	       ; oracle!edge(PosD,_,LabelX,_),
                      \+ domain(LabelX,DBD),
                      \+ domain(LabelX,DAD)
		 ->
		     Edge = []
	       ;
	       Edge = agree
	      )
	  ;
	  Edge = violate
	 )
     ;
     Edge = []
    ),
    ( Cost == nocost, Edge = violate -> fail ; true),
    %	verbose('potential reduce2 step=~w item0=~w => ~w\n',[XS,Item0,Item]),
    full_register_item_two_parents(
	    Action,
	    Item::item{ step => XS,
			generation => CurrentGen,
			right => K,
			left => I,
			stack => NewStack0,
			stack1 => NewStack1,
			lk1 => Id1,
			lk2 => Id2,
			lk3 => Id3,
			action => Action,
			swap => Swap1
		      },
	    ItemAddr0 : Item0,
	    ItemAddr1 : Item1,
	    Cost,
	    ShiftPrefix1,
	    Edge,
	    Loss,
	    NAction
	),
%    verbose('register reduce2 item ~w\n',[Item]),
    true
.

:-xcompiler
pos_before(PosA,PosB) :-
    (PosA < PosB xor (PosA=(_PosA+_), PosB=(_PosB+_), _PosA < _PosB))
	.

transition_type(pop0,tail).

%:-std_prolog generate_pop0/7.

generate_tail_transition(
	Base::pop0,
	Item0::item{ step => S,
		     right => K,
		     stack => Stack0::tree(Head0,Before0,After0,Gov0,GovHead0,LeftHead0,RightHead0),
		     stack1 => Stack1,
		     lk1 => Id1,
		     lk2 => Id2,
		     lk3 => Id3,
		     prefix => Prefix0,
		     action => Action0,
		     swap => Swap
			 },
	ItemAddr0,
	Action::pop0,
	Cost,
	Item1::item{
		 step => S1,
		 left => I,
		 stack1 => Stack2,
		 swap => Swap1
	     },
	ItemAddr1,
	ShiftPrefix1,
	Loss,
	NAction
    ) :-
    tagset!pop0,
    Head0 \== 0,
    (tagset!ensure_tree ->
	 %% when requiring a tree, reduction is only possible if a governor (and only one) is present
	 %% we provide a safety net when reaching the end of the sentence 
	 Gov0 = [_]
     ;
     true
    ),
    (Cost == nocost -> XS = none, CurrentGen = none ; XS is S + 1, value_counter(generation,CurrentGen)),
    %	format('try pop0 step=~w item0=~w:~w\n',[XS,ItemAddr0,Item0]),
    
    %% Violations
    ( opt(train:_) ->
	  recorded('C'(_,Pos0,_,_),Head0),
	  (Before0 = child_info(_,_,NB0,DB0,_,_,_) xor NB0=0, DB0=[]),
	  (After0 = child_info(_,_,NA0,DA0,_,_,_) xor NA0=0, DA0=[]),
	  N0 is NB0+NA0,
	  length(Gov0,NGov0),
	  ( oracle!ngov(Pos0,_,_ND), _ND \== N0 -> % not efficient
		Edge = violate
	   ; oracle!ngov(Pos0,_NGov0,_), NGov0 < _NGov0 ->
		 Edge = violate
	   ; 
	   Edge = []
	  )
     ;
     Edge = []
    ),
    (Swap = [] -> XStack2 = Stack2 ; XStack2 = Swap),
    full_register_item_two_parents(
	    Action,
	    Item::item{ step => XS,
			generation => CurrentGen,
			right => K,
			left => I,
			stack => Stack1,
			stack1 => XStack2,
			lk1 => Id1,
			lk2 => Id2,
			lk3 => Id3,
			action => Action,
			swap => Swap1
		      },
	    ItemAddr0 : Item0,
	    ItemAddr1 : Item1,
	    Cost,
	    ShiftPrefix1,
	    Edge,
	    Loss,
	    NAction
	),
    %	format('register pop0 item ~w\n',[Item]),
    true
.

transition_type(pop1,tail).

%:-std_prolog generate_pop1/7.

generate_tail_transition(
	Base::pop1,
	Item0::item{ step => S,
		     right => K,
		     stack => Stack0::tree(Head0,Before0,After0,_,GovHead0,LeftHead0,RightHead0),
		     stack1 => Stack1::tree(Head1,Before1,After1,Gov1,GovHead1,LeftHead1,RightHead1),
		     lk1 => Id1,
		     lk2 => Id2,
		     lk3 => Id3,
		     prefix => Prefix0,
		     action => Action0,
		     swap => Swap
		   },
	ItemAddr0,
	Action::pop1,
	Cost,
	Item1::item{
		 step => S1,
		 left => I,
		 stack1 => Stack2,
		 swap => Swap1
	     },
	ItemAddr1,
	ShiftPrefix1,
	Loss,
	NAction
    ) :-
    tagset!pop1,
    Head0 \== 0,
    Head1 \== 0,
        %% block pop1 afer shift, because equivalent to pop0,shift
    \+ Action0 = shift(_,_,_),
    %% when requiring a tree, Gov1 must have a governor before reduction
    %	verbose('start pop1 item0=~w\n',[ItemAddr0]),
    %	format('try pop1 step=~w item0=~w:~w\n',[XS,ItemAddr0,Item0]),
    (tagset!ensure_tree ->
	 Gov1 = [_],
	 \+ After1 = child_info(_,Head0,_,_,_,_,_),
	 %% except if late attach is allowed, pop1 is done before attach on Head0
	 %%	     (tagset!late_attach xor After0 = []),
	 true
     ;
     true
    ),
    (Cost == nocost -> XS = none, CurrentGen = none ; XS is S + 1, value_counter(generation,CurrentGen)),
    %% Violations
    ( opt(train:_) ->
	  recorded('C'(_,Pos1,_,_),Head1),
	  (Before1 = child_info(_,_,NB1,DB1,_,_,_) xor NB1=0, DB1=[]),
	  (After1 = child_info(_,_,NA1,DA1,_,_,_) xor NA1=0, DA1=[]),
	  N1 is NB1+NA1,
	  length(Gov1,NGov1),
	  ( oracle!ngov(Pos1,_,_ND), _ND \== N1 -> 
		Edge = violate
	   ; oracle!ngov(Pos1,_NGov1,_), NGov1 < _NGov1 ->
		 Edge = violate
	   ;
	   Edge = []
	  )
     ;
     Edge = []
    ),
    (Swap = [] -> XStack2 = Stack2 ; XStack2 = Swap),
    %% format('step=~w in pop1 cost=~w item0=~w item1=~w oracle1=~w  oracle2=~w item=~w\n',[S,Cost,ItemAddr0,ItemAddr1,Oracle1,Oracle2,Item]),

    full_register_item_two_parents(
	    Action,
	    Item::item{ step => XS,
			generation => CurrentGen,
			right => K,
			left => I,
			stack => Stack0,
			stack1 => XStack2,
			lk1 => Id1,
			lk2 => Id2,
			lk3 => Id3,
			action => Action,
			swap => Swap1
		      },
	    ItemAddr0 : Item0,
	    ItemAddr1 : Item1,
	    Cost,
	    ShiftPrefix1,
	    Edge,
	    Loss,
	    NAction
	),
    %	format('register pop1 item ~w\n',[Item]),
    true
.

transition_type(swap,notail).

%:-std_prolog generate_swap/4.

generate_notail_transition(
	Base::swap,
	Item0::item{ step => S,
		     right => K,
		     left => I,
		     stack => Stack0::tree(Head0,Before0,After0,_,GovHead0,LeftHead0,RightHead0),
		     stack1 => Stack1::tree(Head1,Before1,After1,_,GovHead1,LeftHead1,RightHead1),
		     prefix => Prefix,
		     inside => Inside,
		     lk1 => Id1,
		     lk2 => Id2,
		     lk3 => Id3,
		     prefix => Prefix0,
		     action => Action0,
		     swap => Swap
		   },
	ItemAddr0,
	Action::swap,
	Cost,
	Loss,
	NAction
    ) :-
    tagset!swap,
    %    (Cost \== nocost xor dynoracle!potential_action(S,Action)),
    Head0 \== 0,
    Head1 \== 0,
    \+ Action0 = action[swap],
%    (Cost \== nocost xor 0 is rand(10)),
    %   format('try0 swap step=~w\n',[S,Item0]),
    (Cost == nocost ->
%	 fail,
	 \+ Action0 = action[swap,swap2,swap3],
%	 \+ should_reduce(Head0,Head1),
	 is_not_projective(Head0),
	 is_not_projective(Head1),
	 % 0 is rand(10),
	 XS = none,
	 CurrentGen = none
    ;
    XS is S + 1,
    value_counter(generation,CurrentGen),
    \+ similar_ancestor(Item,ItemAddr0,0),
    true
    ),
%	format('try swap step=~w item0=~w\n',[XS,Item0]),
    full_register_item_single_parent(
	Action,
	Item::item{ step => XS,
		    generation => CurrentGen,
		    right => K,
		    left => I,
		    stack => Stack1,
		    stack1 => Stack0,
		    lk1 => Id1,
		    lk2 => Id2,
		    lk3 => Id3,
		    action => Action,
		    swap => Swap
		  },
	ItemAddr0 : Item0,
	Cost,
	[],
	Loss,
	NAction
    ),
    %	format('registered swap item=~w\n',[Item]),
    true
.

:-std_prolog similar_ancestor/3.

similar_ancestor(Item,AncestorAddr,L) :-
    %% use to avoid long sequence of swap-like trans,
    %% possibly equivalent to identity
%    format('test similar ancestor ~w ~w\n',[AncestorAddr,Item]),
    recorded(Ancestor,AncestorAddr),
    (similar_items(Item,Ancestor)
     xor
     L > 5
     xor
     XL is L+1,
     item!back(AncestorAddr,action[swap,swap2,swap3],NewAncestorAddr,_,_,_),
     similar_ancestor(Item,NewAncestorAddr,XL)
    )
.

:-extensional similar_items/2.

similar_items(item{ %step => XS,
		  %generation => CurrentGen,
		  right => K,
		  %		  left => I,
		  stack => Stack1,
		  stack1 => Stack0,
		  lk1 => Id1,
		  lk2 => Id2,
		  lk3 => Id3,
		  %action => Action,
		  swap => Swap
	      },
	      item{ %step => XS,
		  %generation => CurrentGen,
		  right => K,
		  % left => I,
		  stack => Stack1,
		  stack1 => Stack0,
		  lk1 => Id1,
		  lk2 => Id2,
		  lk3 => Id3,
		  %action => Action,
		  swap => Swap
	      }
	     ).

transition_type(noop,notail).

%:-std_prolog generate_noop/4.

generate_notail_transition(
	Base::noop,
	Item0::item{ step => S,
		     right => K,
		     left => I::0,
		     stack => Stack0::tree(Head0::0,Before0::[],After0,Gov0,GovHead0,LeftHead0,RightHead0),
		     stack1 => Stack1::[],
		     prefix => Prefix,
		     inside => Inside,
		     lk1 => Id1::0,
		     lk2 => Id2::0,
		     lk3 => Id3,
		     prefix => Prefix0,
		     action => Action0,
		     swap => []
		   },
	ItemAddr0,
	Action::noop,
	Cost,
	Loss,
	NAction
    ) :-
    tagset!noop,
    %%    format('step ~w try noop [~w] ~w\n',[S,Oracle,Item0]),
    %% noop on terminal item
    'N'(K),
    XId3 is Id3 -1,
    %%	NewStack0 = tree(0,[],[],[],[])
    NewStack0 = Stack0,
    %%   format('step ~w possible noop ~w\n',[S,Item]),
    (Cost == nocost -> XS = none, CurrentGen = none ; XS is S + 1, value_counter(generation,CurrentGen)),
    full_register_item_single_parent(
	    Action,
	    Item::item{ step => XS,
			generation => CurrentGen,
			right => K,
			left => I,
			stack => NewStack0,
			stack1 => Stack1,
			lk1 => Id1,
			lk2 => Id2,
			lk3 => XId3,
			action => Action,
			swap => []
		      },
		ItemAddr0 : Item0,
		Cost,
		[],
		Loss,
		NAction
	    ),
%	format('registered noop item=~w\n',[Item]),
	true
	.


transition_type(swap2,tail).

%:-std_prolog generate_swap2/7.

generate_tail_transition(
	Base::swap2,
	Item0::item{ step => S,
		     right => K,
		     stack => Stack0::tree(Head0,Before0,After0,Gov0,GovHead0,LeftHead0,RightHead0),
		     stack1 => Stack1::tree(Head1,Before1,After1,Gov1,GovHead1,LeftHead1,RightHead1),
		     prefix => Prefix,
		     inside => Inside,
		     lk1 => Id1,
		     lk2 => Id2,
		     lk3 => Id3,
		     prefix => Prefix0,
		     action => Action0,
		     swap => Swap
		   },
	ItemAddr0,
	Action::swap2,
	Cost,
	Item1::item{
		 step => S1,
		 left => I,
		 stack1 => Stack2::tree(Head2,Before2,After2,Gov2,GovHead2,LeftHead2,RightHead2),
		 swap => Swap1
	     },
	ItemAddr1,
	ShiftPrefix1,
	Loss,
	NAction
    ) :-
    tagset!swap2,
    
    Head0 \== 0,
    Head1 \== 0,
    Head2 \== 0,
    (tagset!allow_swap_on_shift xor Swap = []),
%    \+ Action0 = action[swap2],   % swap2 . swap2 = Id

    (Swap = [] -> XStack2 = Stack2, XSwap = Stack1
    ; Stack1 = Stack2 -> XStack2 = Swap, XSwap = []
    ; XStack2 = Swap, XSwap = Stack1
    ),
    
    (Cost == nocost ->
	 \+ Action0 = action[swap2,swap3],   % swap2 . swap2 = Id
	 %	 \+ Action0 = shift(_,_,_),	     % shift . swap2 = swap . shift (should check that swap is allowed)
	 %	 \+ should_reduce(Head0,Head1),
	 is_not_projective(Head0),
	 is_not_projective(Head1),
	 is_not_projective(Head2),
	 XS = none,
	 CurrentGen = none
    ;
    XS is S + 1,
    value_counter(generation,CurrentGen),
    \+ similar_ancestor(Item,ItemAddr0,0),
    true
    ),


    
    full_register_item_pseudo_two_parents(
	    Action,
	    Item::item{ step => XS,
			generation => CurrentGen,
			right => K,
			left => I,
			stack => Stack0,
			stack1 => XStack2,
			lk1 => Id1,
			lk2 => Id2,
			lk3 => Id3,
			action => Action,
			swap => XSwap
		      },
	    ItemAddr0 : Item0,
	    ItemAddr1 : Item1,
	    Cost,
	    ShiftPrefix1,
	    Edge::[],
	    Loss,
	    NAction
	),
%    verbose('register swap2 item ~w\n',[Item]),
%    format('register swap2 item ~w\n',[Item]),
    true
.


transition_type(swap3,tail).

%:-std_prolog generate_swap2/7.

generate_tail_transition(
	Base::swap3,
	Item0::item{ step => S,
		     right => K,
		     stack => Stack0::tree(Head0,Before0,After0,Gov0,GovHead0,LeftHead0,RightHead0),
		     stack1 => Stack1::tree(Head1,Before1,After1,Gov1,GovHead1,LeftHead1,RightHead1),
		     prefix => Prefix,
		     inside => Inside,
		     lk1 => Id1,
		     lk2 => Id2,
		     lk3 => Id3,
		     prefix => Prefix0,
		     action => Action0,
		     swap => Swap
		   },
	ItemAddr0,
	Action::swap3,
	Cost,
	Item1::item{
		 step => S1,
		 left => I,
		 stack1 => Stack2::tree(Head2,Before2,After2,Gov2,GovHead2,LeftHead2,RightHead2),
		 swap => Swap1
	     },
	ItemAddr1,
	ShiftPrefix1,
	Loss,
	NAction
    ) :-
    tagset!swap3,
    
    Head0 \== 0,
    Head1 \== 0,
    Head2 \== 0,
    
    (tagset!allow_swap_on_shift xor Swap = []),
    
    \+ Action0 = action[swap3],		% swap3 . swap3 = Id
    
    (Swap = [] -> XStack2 = Stack2, XSwap = Stack0
    ; Stack0 = Stack2 -> XStack2 = Swap, XSwap = []
    ; XStack2 = Swap, XSwap = Stack0
    ),

    (Cost == nocost ->
	 XS = none,
	 CurrentGen = none,
	 \+ Action0 = action[swap2,swap3],		% swap2 . swap2 = Id
	 \+ Action0 = shift(_,_,_),	% shift . swap2 = swap . shift (should check that swap is allowed)
%	 \+ should_reduce(Head0,Head1),
	 is_not_projective(Head0),
	 is_not_projective(Head1),
	 is_not_projective(Head2),
	 true
    ;
    XS is S + 1,
    value_counter(generation,CurrentGen),
    \+ similar_ancestor(Item,ItemAddr0,0),
    true
    ),

    
    full_register_item_pseudo_two_parents(
	    Action,
	    Item::item{ step => XS,
			generation => CurrentGen,
			right => K,
			left => I,
			stack => XStack2,
			stack1 => Stack1,
			lk1 => Id1,
			lk2 => Id2,
			lk3 => Id3,
			action => Action,
			swap => XSwap
		      },
	    ItemAddr0 : Item0,
	    ItemAddr1 : Item1,
	    Cost,
	    ShiftPrefix1,
	    Edge::[],
	    Loss,
	    NAction
	),
%    verbose('register swap3 item ~w\n',[Item]),
%    format('register swap3 item ~w\n',[Item]),
    true
.


:-std_prolog complete_item/2.

%% we have reached the maximal number of allowed step but not yet reached a terminal steps
complete_item(Item::item{ step => S, right => N },FinalItem::item{ stack => tree(0,[],_,_,_,_,_), stack1 => [], lk1 => 0 }) :-
    format('complete item step=~w item=~w\n',[S,Item]),
    ( Item = FinalItem -> true
    ;
    XS is S+1,
    beam!new_cell(XS,_),
    (recorded(Item,ItemAddr) xor fail), % should not arise to have several addresses for a same item !
    every((
		 tagset!pop0,
		 generate_other(Item,ItemAddr,true,pop0,Loss)
	     ; 'N'(N),
	       (tagset!label(Label) xor fail),
	       domain(Action,[right(Label),left(Label)]),
	       generate_other(Item,ItemAddr,true,Action,Loss)
	     ; \+ 'N'(N),
	     generate_shift(Item,ItemAddr,true,_,Loss)
	     )),
    (beam!best(XS,NewItem) xor fail),
    (recorded(NewItem,NewItemAddr) xor fail), % should not arise to have several addresses for a same item !
    record(beam!inside(NewItemAddr,XS)),
    complete_item(NewItem,FinalItem)
    )
.

%:-light_tabular item!noloss/1.
%:-mode(item!noloss/1,+(+)).

:-std_prolog item!noloss/1.

item!noloss(ItemAddr) :-
    %% retry noloss until a success has been already found ! (costly)
    ( recorded(item!success_noloss(ItemAddr)) ->
      true
    ; recorded(item!call_noloss(ItemAddr)) -> % to avoid loop
      fail
    ;
    record_without_doublon(item!call_noloss(ItemAddr)),
    recorded(Item::item{ right => N},ItemAddr),
    %    format('try compute loss from ~w:~w\n',[ItemAddr,Item]),
    ( Item = item{ %left => 0,
		   stack => tree(0,[],_,_,_,_,_), stack1 => [], lk1 => 0, swap => [] },
      'N'(N) ->
      % final item => no loss
      format('*** found final nocost item\n',[]),
      true
    ; generate_other(Item,ItemAddr,loss,_,0) ->
      true
    ; generate_shift(Item,ItemAddr,loss,_,0) ->
      true
    ;
%    erase(item!call_noloss(ItemAddr)),
    fail
    ),
    erase(item!call_noloss(ItemAddr)),
    record(item!success_noloss(ItemAddr)),
    true
    )
.


:-xcompiler
full_register_item_single_parent(Action,
				 Item::item{ step => S, prefix => Prefix, action => Action },
				 ItemAddr0 : (Item0::item{ step => S0 }),
				 Cost,
				 Edge,
				 Loss,
				 NAction
				) :-
    register_item_aux(Action,ItemAddr:Item,Item0,Cost,Cost, (Cost \== nocost, oracle!item(S0,Item0)), Edge ),
    %    verbose('item step=~w addr=~w action=~w 0=~w\n',[S,ItemAddr,Action,ItemAddr0]),
%    format('item step=~w addr=~w action=~w 0=~w edge=~w cost=~w\n',[S,ItemAddr,Action,ItemAddr0,Edge,Cost]),
    tails_attach(ItemAddr,ItemAddr0),
    ( Cost \== nocost,
      oracle!item(S,Item),
      oracle!item(S0,Item0),
      oracle!tail(ItemAddr0,_Addr,_W),
         \+ ( oracle!tail(ItemAddr0,__Addr,__W), 
	      __W > _W,
	      format('multiple oracle tails step=~w item=~w:~w\n',[S,ItemAddr0,Item0])
	    )
    ->
	  %		  verbose('\treroot oracle tail ~w ~w\n',[ItemAddr0,_Addr]),
	  record_without_doublon(oracle!tail(ItemAddr,_Addr,_W))
      ;
      true
    ),
%    verbose('back ~w\n',[Back]),
    ( Cost == nocost ->
      item!check_violation_and_loss(Edge,ItemAddr,Item,Loss),
      (Loss = 0 %, S0 \== none
      ->
	   record(item!noloss_descendant(S0,ItemAddr0,ItemAddr)),
	   record_without_doublon( Back )
      ;
      true
      )
    ;
    record_without_doublon( Back::item!back(ItemAddr,Action,ItemAddr0,none,Prefix,NAction)),
    Loss = 0
    ),
    true
.

:-xcompiler
item!check_violation_and_loss(Edge,ItemAddr,Item,Loss) :-
    ((Edge = [] xor Edge = agree) ->
	 item!check_loss(ItemAddr,Item,Loss)
    ;
    erase_item(Item),
    Loss = 1
    )
.

:-xcompiler
full_register_item_pseudo_two_parents(Action,
				 Item::item{ step => S, prefix => Prefix, action => Action, inside => Inside },
				 ItemAddr0 : (Item0::item{ step => S0, inside => Inside0 }),
				 ItemAddr1 : (Item1::item{ step => S1 }),
				 Cost, ShiftPrefix1,
				 Edge,
				 Loss,
				 NAction
				     ) :-
    ( Cost == nocost ->
      Inside = nocost,
      Prefix = nocost,
      true
    ;
    Inside is Inside0+Cost,
    Prefix is ShiftPrefix1+Inside,
    once_xor_true((
		   oracle!item(S0,Item0), 
		   oracle!item(S1,Item1),
		   oracle!tail(ItemAddr0,ItemAddr1,_),
		   try_register_oracle(S,Action,Item,Cost)
		     )),
    (Edge = agree ->
	 oracle!register_agree(Item,Cost)
    ; Edge = violate ->
      oracle!register_violation(Item,Cost)
    ;
     true
    )
    ),
    register_item(Item,ItemAddr),

    %   verbose('item step=~w addr=~w action=~w 0=~w cost=~w\n',[S,ItemAddr,Action,ItemAddr0,Cost]),
%    format('item step=~w addr=~w action=~w 0=~w edge=~w cost=~w\n',[S,ItemAddr,Action,ItemAddr0,Edge,Cost]),
%    tails_attach(ItemAddr,ItemAddr0),
    every((
		 item!tail(ItemAddr0,ItemAddr1,_W),
		 item!add_tail(ItemAddr,ItemAddr1,_W),
%		 format('swap2 adding tail ~w ~w ~w\n',[ItemAddr,ItemAddr1,_W]),
		 true
	 )),
    ( Cost \== nocost,
      oracle!item(S,Item),
      oracle!item(S0,Item0),
      oracle!item(S1,Item1),
      oracle!tail(ItemAddr0,ItemAddr1,_W),
         \+ ( oracle!tail(ItemAddr0,__Addr,__W), 
	      __W > _W,
	      format('multiple oracle tails step=~w item=~w:~w\n',[S,ItemAddr0,Item0])
	    )
    ->
	  %		  verbose('\treroot oracle tail ~w ~w\n',[ItemAddr0,_Addr]),
	  record_without_doublon(oracle!tail(ItemAddr,ItemAddr1,_W))
      ;
      true
    ),
%    verbose('back ~w\n',[Back]),
    ( Cost == nocost ->
      item!check_violation_and_loss(Edge,ItemAddr,Item,Loss),
      (Loss = 0 %, S0 \== none
      ->
	   record(item!noloss_descendant(S0,ItemAddr0,ItemAddr)),
	   record_without_doublon( Back )
      ;
      true
      )
    ;
    record_without_doublon( Back::item!back(ItemAddr,Action,ItemAddr0,consult:ItemAddr1,Prefix,NAction)),
%    format('swap2 adding back ~w ~w\n',[ItemAddr,Back]),
    Loss = 0
    ),
    true
	.

:-xcompiler
full_register_item_two_parents(Action,
			       Item::item{ step => S, prefix => Prefix, action => Action },
			       ItemAddr0 : (Item0::item{ step => S0, inside => Inside0 }),
			       ItemAddr1 : (Item1::item{ step => S1, prefix => Prefix1 }),
			       Cost,
			       ShiftPrefix1,
			       Edge,
			       Loss,
			       NAction
			      ) :-
    %    verbose('preitem0 step=~w action=~w 0=~w 1=~w\n',[S,Action,ItemAddr0,ItemAddr1]),
%    format('try full register iterm 2 parents action=~w cost=~w edge=~w item=~w\n',[Action,Cost,Edge,Item]),
    ( Cost == nocost ->
      New = nocost,
      true
    ;
    (New is Inside0+Cost + (ShiftPrefix1-Prefix1) xor format('*** pb adding costs sprefix1=~w item0=~w item1=~w\n',[ShiftPrefix1,Item0,Item1])),
    true
    ),
%    verbose('preitem1 step=~w action=~w 0=~w 1=~w\n',[S,Action,ItemAddr0,ItemAddr1]),
    register_item_aux(Action,ItemAddr:Item,Item1,New,Cost, 
		      ( Cost \== nocost,
			oracle!item(S0,Item0), 
			%		       verbose('potential0 oracle item addr1=~w:~w addr0=~w:~w Item=~w\n',[ItemAddr1,Item1,ItemAddr0,Item0,Item]),
			oracle!item(S1,Item1),
			%		       verbose('potential1 oracle item addr1=~w:~w addr0=~w:~w Item=~w\n',[ItemAddr1,Item1,ItemAddr0,Item0,Item]),
			oracle!tail(ItemAddr0,ItemAddr1,_),
			true
		      ),
		      Edge
		     ),
    %    verbose('item step=~w addr=~w action=~w 0=~w 1=~w\n',[S,ItemAddr,Action,ItemAddr0,ItemAddr1]),
%    format('item step=~w addr=~w action=~w 0=~w 1=~w edge=~w cost=~w\n',[S,ItemAddr,Action,ItemAddr0,ItemAddr1,Edge,Cost]),
    tails_attach(ItemAddr,ItemAddr1),
%    verbose('back ~w\n',[Back]),
    ( Cost == nocost ->
      item!check_violation_and_loss(Edge,ItemAddr,Item,Loss),
      (Loss = 0 %, S0 \== none
      ->
	   record(item!noloss_descendant(S0,ItemAddr0,ItemAddr)),
	   record_without_doublon( Back )
      ;
      true
      )
    ;
    ( oracle!item(S,Item),
      oracle!item(S0,Item0),
      oracle!item(S1,Item1),
      oracle!tail(ItemAddr1,_Addr,_W),
      \+ ( oracle!tail(ItemAddr1,__Addr,__W), 
	   __W > _W,
	   format('multiple oracle tails step=~w item=~w:~w\n',[S,ItemAddr1,Item1])
	 )
    ->
    %		  verbose('\treroot oracle tail ~w ~w\n',[ItemAddr1,_Addr]),
    record_without_doublon(oracle!tail(ItemAddr,_Addr,_W))
    ;
    true
    ),
    record_without_doublon( Back::item!back(ItemAddr,Action,ItemAddr0,ItemAddr1,Prefix,NAction)),
    Loss = 0
    ),
    true
	.

:-xcompiler
register_item_aux(Action,
		  ItemAddr: (Item::item{ step => XS, 
					 prefix => Prefix,
					 inside => Inside,
					 action => Action
			    }),
		  Item0::item{ prefix => OldPrefix, inside => OldInside },
		  New,
		  Cost,
		  OracleTest,
		  Edge
		 ) :-
    ( Cost == nocost ->
      Prefix = nocost,
      Inside = nocost
    ;
    (OldPrefix \== nocost xor format('*** expecting a true item ~w\n',[Item0])),
    Prefix is OldPrefix+New,
    Inside is OldInside+New,
    once_xor_true((OracleTest,
		   try_register_oracle(XS,Action,Item,Cost)
		  )),
    (Edge = agree ->
	 oracle!register_agree(Item,Cost)
    ; Edge = violate ->
      oracle!register_violation(Item,Cost)
    ;
    true
    )
    ),
    register_item(Item,ItemAddr)
	.

:-xcompiler
tails_attach(NewAddr,OldAddr) :-
%    verbose('tails_attach ~w ~w\n',[NewAddr,OldAddr]),
/*
    on_verbose((format('tails_attach new=~w old=~w\n',[NewAddr,OldAddr]),
		every(( item!tail(OldAddr,_Addr,_W),
			format('\told tail ~w w=~w\n',[_Addr,_W])
			;
			oracle!tail(OldAddr,_Addr,_W),
			format('\told oracle tail ~w w=~w\n',[_Addr,_W])
		     ))
	      )),
*/
	( item!tails(NewAddr,_) ->
	      %% Item was already present with its tail
	      %% we add the tail components from Old
	      every((  item!tail(OldAddr,_ItemAddr,_W),
		       item!add_tail(NewAddr,_ItemAddr,_W),
		       true
		   )),
	      true
	 ; item!tails(OldAddr,OldTList) ->
	       %% Item is new
	       %% we can inherit its tail list from OldItem (by cloning)
	       '$interface'('Slist_Clone'(OldTList:ptr),[return(TList:ptr)]),
	       record_without_doublon(item!tails(NewAddr,TList))
	 ;
%	 recorded(OldItem,OldAddr),
%	 format('missing tails for ~w: ~w\n',[OldAddr,OldItem]),
	 true
	),
	every(( recorded(item!nocost_tail(OldAddr,TailAddr)),
		(recorded(item!nocost_tail(NewAddr,TailAddr)) ->
		     true
		;
		/*
		mutable(MTails,0,true),
		every((
			     recorded(item!nocost_tail(NewAddr,_)),
			     mutable_inc(MTails,_)
			 )),
		mutable_read(MTails,NTails),
		(NTails > 5 xor erase(item!call_noloss(NewAddr))),
		*/
		erase(item!call_noloss(NewAddr)),
		record(item!nocost_tail(NewAddr,TailAddr))
		)
	      )),
	true
	.

:-xcompiler
register_oracle(S,Item::item{ prefix => W, action => Action},Action) :-
	every(( recorded(oracle!item(S,_Item::item{ prefix => _W }),_Addr),
		\+ _Item = Item,
		format('warning: step=~w action=~w multiple oracle items ~w and ~w\n',[S,Action,_Item,Item]),
		_W < W,
		delete_address(_Addr)
	      )),
%	format('register oracle step=~w item=~w\n',[S,Item]),
	( oracle!item(S,_Item::item{ prefix => _W }),
	  _W >= W ->
	  fail
	;
        verbose('register oracle step=~w action=~w item=~w\n',[S,Action,Item]),
%	format('register oracle step=~w action=~w item=~w\n',[S,Action,Item]),
	  record_without_doublon(oracle!item(S,Item))
	)
	.

:-xcompiler
try_register_oracle(S,Action,Item::item{lk1 => Id1, lk2 => Id2, lk3 => Id3, action => Action},Cost) :-
%	format('step ~w action ~w cost ~w generating oracle ~w\n',[S,Action,Cost,Item]),
	%% parent items of Item are oracle items
	( oracle!action(S,Action),
	  (Action = shift(_,_,_) -> (oracle!shift(Id1,Id2,Id3) xor Id1=0, Id2=0, Id3=0) ; true)
	->
	  %% Action is the action leading to step S
	  %% => Item is THE oracle item at step S
	  register_oracle(S,Item,Action)
	;
	  true
	),
	%% We record if Action is the best action leading to step S, with oracle parents
	oracle!register_best_action(S,Action,Cost,Item),
%%	oracle!register_best_action(S,Action,Prefix,Item),
	true
	.

:-xcompiler
register_item(Item::item{ step => S, prefix => W },ItemAddr) :-
    verbose('checking item ~w\n',[Item]),
	update_counter(checking,_),
	( W == nocost ->
	  (recorded(Item,ItemAddr) ->
	       %% need to do some cleaning
	       true
	  ; record(Item,ItemAddr),
%	    format('register nocost item ~w:~w\n',[ItemAddr,Item]),
	    true
	  )
	; recorded(Item,ItemAddr) ->
	  true
	; beam!check(Item,ItemAddr) ->
	  true
	; (oracle!item(S,Item) xor oracle!get_best_action(S,_,_,Item)),
	  %%	format('new item ~w\n',[Item]),
	  (recorded(Item,ItemAddr) xor record(Item,ItemAddr)),
	  true
	),
/*
	on_verbose((  recorded(_Item,ItemAddr),
		      verbose('kept item ~w: ~w  registered ~w\n',[ItemAddr,Item,_Item])
		  )),
*/
	true
	.

:-xcompiler
oracle!register_agree(Item::item{ step => S, prefix => W },Cost) :-
%    format('register agree oracle step=~w item=~w\n',[S,Item]),
    ( fail,
      recorded(oracle!agree(S,item{ prefix => _W},_Cost),_Addr) ->
	  (Cost > _Cost ->
	       delete_address(_Addr),
	       record(oracle!agree(S,Item,Cost))
	   ;
	   true
	  )
      ;
      record(oracle!agree(S,Item,Cost))
    ),
%    format('register done\n',[]),
    true
.

:-xcompiler
oracle!register_violation(Item::item{ step => S, prefix => W },Cost) :-
%    format('register violation oracle step=~w item=~w\n',[S,Item]),
    ( fail,
      recorded(oracle!violation(S,item{ prefix => _W},_Cost),_Addr) ->
	  (Cost > _Cost ->
	       delete_address(_Addr),
	       record(oracle!violation(S,Item,Cost))
	   ;
	   true
	  )
      ;
      record(oracle!violation(S,Item,Cost))
    ),
%    format('register done\n',[]),
    true
.

:-std_prolog build_guide_action/3.

build_guide_action(Action,Label,Guide) :-
    name_builder('~w_~w',[Action,Label],Guide),
    record_without_doublon(guide!xaction(Action,Label,Guide))
.

:-xcompiler
guide!yaction(Action,Label,Guide) :-
    (guide!xaction(Action,Label,Guide) xor build_guide_action(Action,Label,Guide)).

%:-std_prolog pre_table_lookup/14.
%:-light_tabular pre_table_lookup/14.
%:-mode(pre_table_lookup/14,+(+,+,+,+,+,+,-,-,-,-,-,-,-,-)).

:-xcompiler
pre_table_lookup(I1,Id1,Id2,Id3,Stack0,Stack1,Feat0,Feat1,EntryI,EntryI2,EntryI3,PoncFeatsI1,Guide,I4) :-
    (Stack0 = tree(Head0,_,_,_,_,_,_) xor Head0 = []),
    (Stack1 = tree(Head1,_,_,_,_,_,_) xor Head1 = []),
    pre_table_lookup_aux(Head0,Head1,I1,Id1,Id2,Id3,Stack0,Stack1,Feat0,Feat1,EntryI,EntryI2,EntryI3,PoncFeatsI1,Guide,I4)
.

%:-std_prolog pre_table_lookup_aux/16.
:-light_tabular pre_table_lookup_aux/16.
:-mode(pre_table_lookup_aux/16,+(+,+,+,+,+,+,+,+,-,-,-,-,-,-,-,-)).

	
%% out: Feat0 Feat1 EntryI EntryI2 EntryI3 PonctFeatsI1 Guide
pre_table_lookup_aux(_Head0,_Head1,I1,Id1,Id2,Id3,Stack0,Stack1,Feat0,Feat1,EntryI,EntryI2,EntryI3,PoncFeatsI1,XGuide,I4) :-
    stack_features(Stack0,Feat0::[Left0,_, S0Id : _ |_]),
    stack_features(Stack1,Feat1::[Left1,_, S1Id : _ |_]),
    _Delta01 is Left1-Left0, discrete_distance(_Delta01,Delta01),
    _Delta1 is I1 - Left1, discrete_distance(_Delta1,Delta1),
    _Delta0 is I1 - Left0, discrete_distance(_Delta0,Delta0),
%    format('try guiding left0=~w left1=~w\n',[Left0,Left1]),
    ( (guide!head(Left0,Left1) xor guide!head(Left0,Left1,_)) -> 
	  ( guide!action(Left1,Left0,_I1,reduce_right,Label) ->
		( I1 = _I1 ->
		      _XGuide = []
		  ; I1 < _I1 ->
			_XGuide = [shift]
		 ;
		    _XGuide = [potential_reduce_right]
		),
		guide!yaction(reduce_right,Label,_Guide),
		Guide = [reduce_right,_Guide|_XGuide]
	    ;
	    Guide = [reduce_right]
	  ),
	  true
      ; guide!head(Left1,Left0) -> 
	    (guide!label(Left1,Label) ->
		 guide!yaction(reduce_left,Label,_Guide),
		 Guide = [reduce_left,_Guide]
	     ;
	     Guide = [reduce_left]
	    )
      ; guide!head(Left1,Left0,Label) -> 
	    guide!yaction(reduce_left,Label,_Guide),
	    Guide = [reduce_left,_Guide]
      ; (guide!head(Left0,_Right0) ; guide!action(Left0,_,_Right0,_,_)),
	Left0 < _Right0 ->
	    ( (guide!head(Left1,_Right1) ; guide!action(Left1,_,_Right1,_,_)),
	      Left1 < _Right1 ->
		  Guide = [no_reduce0,no_reduce1]
	     ;
	     Guide = [no_reduce0]
	    )
      ; (guide!head(Left1,_Right1) ; guide!action(Left1,_,_Right1,_,_)),
	Left1 < _Right1 ->
	Guide = [no_reduce1]
      ;
      Guide = []
      ),

      ( mst!in(S0Id,S1Id,S0Id,_) ->
	format('%% predict mst_reduce_right ~w ~w\n',[S0Id,S1Id]),
%	guide!yaction(dynet_reduce_right,Label,_Guide),
	Guide1 = [mst_reduce_right|Guide]
      ;  mst!in(S1Id,S0Id,S1Id,_) ->
	 format('%% predict mst_reduce_left ~w ~w\n',[S1Id,S0Id]),
	 %	 guide!yaction(dynet_reduce_left,Label,_Guide),
	 Guide1 = [mst_reduce_left|Guide]
      
      ; dynet!head(S0Id,S1Id,Label) ->
	format('%% predict dynet_reduce_right ~w ~w ~w\n',[S0Id,S1Id,Label]),
%	guide!yaction(dynet_reduce_right,Label,_Guide),
	Guide1 = [dynet_reduce_right|Guide]
      ;  dynet!head(S1Id,S0Id,Label) ->
	 format('%% predict dynet_reduce_left ~w ~w ~w\n',[S1Id,S0Id,Label]),
	 %	 guide!yaction(dynet_reduce_left,Label,_Guide),
	 Guide1 = [dynet_reduce_left|Guide]
      /*
      ; dynet!head_rank(S0Id,S1Id,Rank0),
	dynet!head_rank(S1Id,S0Id,Rank1) ->
	guide!yaction(dynet_reduce_right,Rank0,Guide0),
      	guide!yaction(dynet_reduce_left,Rank1,Guide1),
	Guide = [Guide0,Guide1]
      ; dynet!head_rank(S0Id,S1Id,Rank) ->
	guide!yaction(dynet_reduce_right,Rank,Guide)
      ; dynet!head_rank(S1Id,S0Id,Rank) ->
	guide!yaction(dynet_reduce_left,Rank,Guide)
	*/	
      ;
      Guide1 = Guide
    ),
    feature_strip(Guide1,XGuide),
 %  format('%% Pred action ~w ~w ~w => ~w label=~w\n',[Left0:Lemma0,Left1:Lemma1,I1,Guide,Label]),
    get_word_features(I1,Id1,EntryI::lemma{}),
    ('C'(I1,Id1,_,I2) xor I2 is I1+1),
    get_word_features(I2,Id2,EntryI2::lemma{}),
    ('C'(I2,Id2,_,I3) xor I3 is I2+1),
    get_word_features(I3,Id3,EntryI3::lemma{}),
    ('C'(I3,Id3,_,I4) xor I4 is I3+1),
    ponct_features(Left0,I1,PoncFeatsI1),
    true
.

:-xcompiler
pre_pseudo_table_lookup(I1,Id1,Id2,Id3,Id4) :-
%    format('try pseudo table lookup i1=~w id1=~w id2=~w id3=~w\n',[I1,Id1,Id2,Id3]),    
    ('C'(I1,Id1,_,I2) xor I2 is I1+1),
    ('C'(I2,Id2,_,I3) xor I3 is I2+1),
    ('C'(I3,Id3,_,I4) xor I4 is I3+1),
    ('C'(I4,Id4,_,_) xor Id4 = 0),
%    format('=> pseudo table lookup id1=~w id2=~w id3=~w id4=~w\n',[Id1,Id2,Id3,Id4]),    
    true
.

:-xcompiler
table_lookup(Action,
	     Update,
	     Label,
	     I1,
	     Id1,Id2,Id3,Id4,
	     Stack0,
	     Stack1,
	     Stack2,
	     S,
	     Cost,
	     NUpdates,
	     LastActionFeatures,
	     NAction
	    ) :-
    pre_table_lookup(I1,Id1,Id2,Id3,Stack0,Stack1,Feat0,Feat1,EntryI,EntryI2,EntryI3,PoncFeatsI1,Guide,I4), 
    post_table_lookup(Action,Update,Id4,Label,Feat0,Feat1,Stack2, Id1 : EntryI, Id2 : EntryI2, Id3 : EntryI3,PoncFeatsI1,Guide,I4,S,I1,Cost,0,NUpdates,LastActionFeatures,0,NAction)
.
    

:-xcompiler
post_table_lookup(Action,Update,Id4,Label,Feat0,Feat1,Stack2,Id1 : EntryI, Id2 : EntryI2, Id3 : EntryI3,PoncFeatsI1,Guide,I4,Step,Pos,Cost,ActionBase,NUpdates,LastActionFeatures,Oracle,NAction) :-
    stack_features(Stack2,Feat2),
    (Pos = 0 -> DeltaStep = 0 ; DeltaStep is 10 * Step / Pos),
    ( Id4 = void -> 
      null_entry(EntryI4,0),
      FormI5 = 0, AllCatsI5 = 0
    ; 
    get_word_features(I4,Id4,EntryI4::lemma{}),
    'C'(I4,Id4,_,I5) xor I5 is I4+1,
    get_xword_features(I5,FormI5,AllCatsI5)
    ),
    (Guide = [] ->
	 Feat0 = [Left0,_,S0Id : _ | _],
	 Feat2 = [Left2,_,S2Id : _ | _],
	 ( dynet!head(S0Id,S2Id,_) ->
	   Guide2 = dynet_reduce_right2
	 ;  dynet!head(S2Id,S0Id,_) ->
	    Guide2 = dynet_reduce_left2
	 ;
	 Guide2 = []
	 )
    ;
    Guide2 = Guide
    ),
%    format('update action=~w label=~w cost=~w actionbase=~w Update=~w NUpdates=~w\n',[Action,Label,Cost,ActionBase,Update,NUpdates]),
    (opt(dynet_only),
     opt(train:_)
    ->
	%	dynet!epoch(Epoch),
	%	(0 is Epoch mod 2 -> Use = nn ; Use = sr ),
	(0 is rand(10) xor Use = nn),
	%	       Beam = 1,
	true
    ;
    true
    ),
    (tagset!mixed_train(Mode) xor Mode = none),
    (
	Mode == both ->
	X is rand(5),
	(X < 2 -> Use = sr
	; X > 2 -> Use = nn
	; true
	)
    ; Mode == none ->
      true
    ; Use = Mode
    ),
    ( opt(nodynet) ->
	  DynetW = 0,
	  PAction = none,
	  PAction2 = none,
	  PLabel = 0,
	  PLabel2 = 0
    ; Update = nn(_Update) ->
      dynet!state(AllFeatures,_Update,Action,Label,DynetW,NAction)
    ; %(Update = 0 xor \+ \+ Use = nn),
      true ->
%      format('dynet update u=~w a=~w(~w) ab=~w\n',[Update,Action,Label,ActionBase]),
      dynet!state(AllFeatures,Update,Action,Label,DynetW,NAction)
    ;
    %% block update but get weight
    dynet!state(AllFeatures,0,Action,Label,DynetW,NAction)
    ),
    templates(AllFeatures :: [ Delta0,Delta1,Delta01,
				   Id1 : EntryI,
				   Id2 : EntryI2,
				   Id3 : EntryI3,
				   Id4 : EntryI4,
				   Feat0,
				   Feat1,
				   Feat2,
				   Guide2,
				   DeltaStep,
				   [FormI5,AllCatsI5],
				   [PoncFeatsI1],
				   LastActionFeatures,
				   PAction,
				   PLabel,
				   PAction2,
				   PLabel2
				 ],Values),
	%	format('table lookup action=~w update=~w label=~w\n',[Action,Update,Label]),
    (  Update == 0 ->
      %	  format('table lookup action=~w update=~w label=~w\n',[Action,Update,Label]),
      (PAction = shift -> NAction = shift ; action_recompose(PAction,PLabel,NAction)),
      (var(ActionBase) -> Mask = 0
      ; tagset!actionbase2index(ActionBase,Mask) -> true
      ; Mask = 0
      ),
      %% in most cases, we try best actions, weighted by a combination of
      %% the perceptron weight and the neural weight
      (
	  %% try best actions as ranked by feature-based perceptron
	  %% note: always use perceptron when querying for an oracle action
	  ( Oracle = 1 xor Use = sr ),
	  mutable(MRank,0),
	  ( var(Label) ->
	    ( var(Action) -> true
	    ; tagset!actionbase2index(Action,XAction)
	    )
	  ;
	  tagset!action2index(Action,Label,_XActionLabel,XAction,XLabel)
	  ),
	  %% call to perceptron
	  fset_value_weight(XAction,_XActionLabel,Values,_Cost,Mask),
	  (_XActionLabel = 0 -> XActionLabel = XAction ; XActionLabel = _XActionLabel),
	  mutable_inc(MRank,Rank),
	  %% call to dynet if allowed
	  (opt(nodynet) ->
	       DynetW2 = 0 ;
	      dynet!get_weight(XActionLabel,DynetW2)
	  ),
	  ((var(Action) xor var(Label)) ->
	       tagset!index2action(XActionLabel,Action,Label)
	  ;
	  true
	  ),
	  true
      ;
      %% try best actions as ranked by neural layers
      Use = nn,
      % depend on the number of beam size and complex actions
      % max = NSimple + Beam * NComplex
      % note: does not depend on the number of labels
      % cached as last arg of tagset!parameters/6
      fset_all_value_weights(Values,WTable),
      tagset!parameters(_,_,_,_,_,YBeam),
      term_range(0,YBeam,TBeam),
      domain(Rank,TBeam),
      dynet!get_nbest_beam_action(Rank,XActionLabel),
      %% a bug in tagset!index2action which is ambiguous on simple actions
      tagset!index2action(XActionLabel,Action,Label),
      %% call to dynet
      %% the call will fail if the action has been already handled (via the perceptron)
      dynet!get_weight(XActionLabel,DynetW2),
      %% call to perceptron if allowed
      % tagset!action2index(Action,Label,_XActionLabel,XAction,XLabel),
      % fset_value_weight(XAction,_XActionLabel,Values,_Cost,Mask),
      fset_get_value_weight(WTable,XActionLabel,_Cost),
%      format('rank ~w action ~w(~w) dynet ~w fset ~w\n',[Rank,Action,Label,DynetW2,_Cost]),
      %_Cost = 0,
      true
      ),
      Cost is _Cost + DynetW2,
%      format('nbest u=~w r=~w a=~w:~w(~w) na=~w => ~w / ~w xw=~w _cost=~w cost=~w\n',[Use,Rank,XActionLabel,Action,Label,NAction,DynetW2,DynetW,DynetW2,_Cost,Cost]),
      %	  format('=>1 fset value action=~w label=~w cost=~w\n',[Action,Label,Cost]),
      true
    ; Update = nn(_) ->
      %% special case: no perceptron update
      Cost = 0
    ;
    %\+ 0 is rand(5),
    true ->
      %% update case for the perceptron
      %% feature-based perceptron coonverges more quickly than the neural one
      %% so we only perform part of the updates (1 out of 10)
      tagset!action2index(Action,Label,_XActionLabel,XAction,XLabel),
      %	format('update ~w ~w => ~w ~w ~w\n',[Action,Label,XActionLabel,XAction,XLabel]),
      (fset_value_weight_update(XAction,_XActionLabel,Values,Update,NUpdates) xor NUpdates=0),
      Cost = 0
    ;
    Cost = 0
    )
.

:-extensional action_decompose/3.

action_decompose(shift(_,_,_),shift,0).
action_decompose(shift,shift,0).
action_decompose(pop0,pop0,0).
action_decompose(pop1,pop1,0).
action_decompose(swap,swap,0).
action_decompose(left(Label),left,Label).
action_decompose(right(Label),right,Label).
action_decompose(lefta(Label),lefta,Label).
action_decompose(righta(Label),righta,Label).
action_decompose(left2(Label),left2,Label).
action_decompose(right2(Label),right2,Label).
action_decompose(noop,noop,0).
action_decompose(swap2,swap2,0).
action_decompose(swap3,swap3,0).


:-extensional action_recompose/3.

action_recompose(shift,0,shift(_,_,_)).
action_recompose(pop0,0,pop0).
action_recompose(pop1,0,pop1).
action_recompose(swap,0,swap).
action_recompose(left,Label,left(Label)).
action_recompose(right,Label,right(Label)).
action_recompose(lefta,Label,lefta(Label)).
action_recompose(righta,Label,righta(Label)).
action_recompose(left2,Label,left2(Label)).
action_recompose(right2,Label,right2(Label)).
action_recompose(noop,0,noop).
action_recompose(swap2,0,swap2).
action_recompose(swap3,0,swap3).

action_recompose(none,0,none).	% pseudo action (for -nodynet)

weight_update( Item0::item{ step => S,
			    right => I,
			    stack => Stack0,
			    stack1 => Stack1,
			    lk1 => _Id1,
			    lk2 => _Id2,
			    lk3 => _Id3,
			    prefix => Prefix0,
			    action => Action0,
			    swap => Swap0
			  },
	       Item::item{ prefix => Prefix, action => Action, stack => NewStack0, stack1 => NewStack1, swap => Swap },
	       Update,
	       NUpdates,
	       Cost
	     ) :-
%    format('weight update step=~w action=~w update=~w item0=~w item=~w\n',[S,Action,Update,Item0,Item]),
    action_decompose(Action,Base,Label),
    recorded(Item,ItemAddr),
    recorded(Item0,ItemAddr0),
    item!back(ItemAddr,Action,ItemAddr0,_,Prefix,NAction),
    ( Action = shift(Id1,Id2,Id3) ->
	  Item = item{ lk3 => Id4 },
	  Stack2 = []
      ; Base = action[left,right,pop0,pop1] ->
	Id1=_Id1,
	Id2=_Id2,
	Id3=_Id3,
	Id4 = void,
	Item = item{ stack1 => Stack2 }
      ; Base = action[left2,right2] ->
	Id1=_Id1,
	Id2=_Id2,
	Id3=_Id3,
	Id4 = void,
        (item!back(ItemAddr,Action,ItemAddr0,ItemAddr1,Prefix,NAction),
	 recorded(item{ step => S2, stack1 => Stack2 }, ItemAddr1),
	 S2 \== none
	xor
		format('*** no backptr for action=~w ~w:~w to ~w:~w\n',[Action,ItemAddr,Item,ItemAddr0,Item0]),
	 fail),
	XStack1 = Stack2,
	XStack2 = Stack1
      ; Base = action[swap,noop] ->
	    Id1=_Id1,
	    Id2=_Id2,
	    Id3=_Id3,
	    Id4 = void,
	    Stack2 = Swap0
      ; Base = action[lefta,righta] ->
	    Id1=_Id1,
	    Id2=_Id2,
	    Id3=_Id3,
	    Id4 = void,
	    Stack2 = Swap0
      ; Base = action[swap2] ->
	    Id1=_Id1,
	    Id2=_Id2,
	    Id3=_Id3,
	    Id4 = void,
	    XStack1 = Stack1,
	    XStack2 = NewStack1
      ; Base = action[swap3] ->
	    Id1=_Id1,
	    Id2=_Id2,
	    Id3=_Id3,
	    Id4 = void,
	    XStack1 = Stack1,
	    XStack2 = NewStack0
      ;
      %% should not arrive here !
      fail
    ),
    (XStack1 = Stack1, XStack2=Stack2 xor true),
    safe_action_decompose(Action0,Base0,Label0),
%    format('update step=~w action=~w label=~w naction=~w update=~w item=~w\n',[S,Base,Label,NAction,Update,Item]),
    table_lookup(Base,
		 Update,
		 Label,
		 I,
		 Id1, Id2, Id3, Id4,
		 Stack0,
		 XStack1,
		 XStack2,
		 S,
		 Cost,
		 NUpdates,
		 [Base0,Label0],
		 NAction
		),
%    format('done nupdates=~w\n',[NUpdates]),
    true
.

:-xcompiler
safe_action_decompose(Action,Base,Label) :-
    action_decompose(Action,Base,Label) xor Base=0, Label=0
.

:-xcompiler
oracle!tail(ItemAddr,TailAddr) :-
    oracle!tail(ItemAddr,TailAddr,_)
.

:-xcompiler
item!tail(ItemAddr,TailAddr) :-
    ( item!tail(ItemAddr,TailAddr,_)
      ; oracle!tail(ItemAddr,TailAddr,_),
	\+ item!tail(ItemAddr,TailAddr,_)
      ;  item!nocost_tail(ItemAddr,TailAddr)
    )
.

:-xcompiler
item!xtail(ItemAddr,TailAddr,W) :-
   (item!tail(ItemAddr,TailAddr,W)
    ; oracle!tail(ItemAddr,TailAddr,W),
      \+ item!tail(ItemAddr,TailAddr,W)
    ; item!nocost_tail(ItemAddr,TailAddr),
      W = nocost
   )
.


:-xcompiler
item!acceptable_back(ItemAddr,Action,ItemAddr0,ItemAddr1,W,ExitAddr) :-
    item!back(ItemAddr,Action,ItemAddr0,ItemAddr1,W,_),
    \+ Action = shift(_,_,_),
    (ExitAddr = 0 -> true 
     ; ItemAddr1 = none -> item!tail(ItemAddr0,ExitAddr) 
%     ; ItemAddr1 = (consult:_) -> item!tail(ItemAddr0,ExitAddr) 
     ; ItemAddr1 = (consult:ExitAddr) ->
       item!tail(ItemAddr0,ExitAddr),
       true
     ; item!tail(ItemAddr1,ExitAddr)
    )
	.

:-extensional oracle!max/4.

:-std_prolog oracle!register_best_action/4.

oracle!register_best_action(S,Action,Cost,Item) :-
%	format('try register best action step=~w action=~w cost=~w\n',[S,Action,Cost]),
	( oracle!max(S,MMax,MAction,MItem) ->
	  mutable_read(MMax,Max),
	  ( Cost > Max ->
	    mutable(MMax,Cost),
	    mutable(MAction,Action),
	    mutable(MItem,Item)
	  ;
	    true
	  )
	;
	  mutable(MMax,Cost),
	  mutable(MAction,Action),
	  mutable(MItem,Item),
	  record_without_doublon(oracle!max(S,MMax,MAction,MItem))
	)
	.

:-std_prolog oracle!get_best_action/4.

oracle!get_best_action(S,Action,W,Item) :-
	oracle!max(S,MMax,MAction,MItem),
	mutable_read(MAction,Action),
	mutable_read(MMax,W),
	mutable_read(MItem,Item)
	.

:-extensional beam!cell/2.

:-std_prolog beam!enumerate/2.

beam!enumerate(S,Item) :-
	beam!cell(S,MList),
	mutable_read(MList,L),
	domain(Item,L)
	.

:-std_prolog beam!best/2.

beam!best(S,Item) :-
	beam!cell(S,MList),
	mutable_read(MList,[Item|_])
	.


:-std_prolog beam!worst_in/2.

beam!worst_in(S,W) :-
	beam!cell(S,MList),
	mutable_read(MList,L),
	beam!worst_in_aux(L,W)
	.

:-std_prolog beam!worst_in_aux/2.

beam!worst_in_aux(L,W) :-
	( L = [item{ prefix => W}] xor L=[_|L2], beam!worst_in_aux(L2,W) ).

:-extensional 'B'/2.
:-extensional 'B2'/2.

:-light_tabular beam!adjusted_size/2.
:-mode(beam!adjusted_size/2,+(+,-)).

beam!adjusted_size(Step,N) :-
    beam(N0),
    ('B2'(Step,Size) -> N is N0 * Size ; N = N0)
.
	
:-xcompiler
beam!check(Item::item{ step => S},Addr) :-
	( beam!cell(S,MList) xor beam!new_cell(S,MList)),
	mutable_read(MList,L),
	beam!adjusted_size(S,N),
	beam!add(Item,L,NewL,N,Addr),
	%%	  format('add beam S=~w ~w\n',[S,NewL]),
	\+ Addr = 0,
	(L=NewL xor fast_mutable(MList,NewL)),
	true
	.

:-xcompiler
beam!new_cell(S,MList) :-
	mutable(MList,[]),
	record(beam!cell(S,MList))
	.

:-xcompiler
item!record(Item,Addr) :-
%	verbose('adding item\n',[]),
	update_counter(adding,_),
	record(Item,Addr),
	true
	.

:-std_prolog beam!add/5.

beam!add(Item::item{ prefix => W, step => S},L,NewL,N,Addr) :-
	(N > 0 ->
	 ( L=[] ->
	   NewL = [Item],
	   item!record(Item,Addr)
	 ; L=[Item1::item{ prefix => W1} | L2] ->
	       XN is N-1,
	       ( W  > W1 ->
		     item!record(Item,Addr),
		     beam!cut(L,NewL2,XN,Item),
		     NewL = [Item|NewL2]
		 ; %fail,
		 more_general_item(Item1,Item) ->
		     ( oracle!item(S,Item),
		    \+ Item = Item1 ->
			   %% we need to preserve oracle items
			   %% but we don't put it in the beam in that case
			   Addr = 0
		       ;
		       recorded(Item1,Addr)
		     ),
		     %	     format('more general item ~w: ~w vs ~w\n',[_Addr,Item1,Item]),
		     NewL=L,
		     true
		 ;
		 beam!add(Item,L2,NewL2,XN,Addr),
		 NewL = [Item1|NewL2]
	       )
	 ;
	   format('should not arise L=~w\n',[L]),
	   Addr=0,
	   true
	 )
	;
	 Addr = 0,
	 NewL=L
	)
	.
	
:-std_prolog beam!cut/4.

beam!cut(L,NewL,N,Item::item{}) :-
	( L=[] -> NewL=[]
	; N > 0 ->
	  L=[Item2::item{}|L2],
	  ( 
	      more_general_item(Item,Item2) ->
		  %% remove stored items Item2 less general than the newly installed Item
		  %% it avoid to waste space in the beam for two similar items
		  %% in particular, the case may arise when updating oracle items
		  %		verbose('remove subsumed item ~w\n',[Item2]),
		  erase_item(Item2),
		  %% no more item in L2 can be subsumed by Item (would have been also subs. by Item2)
		  %% and no need to remove more item
		  NewL=L2
	      ;
	      XN is N-1,
	      NewL = [Item2|NewL2],
	      beam!cut(L2,NewL2,XN,Item)
	  )
	; 
	  NewL = [],
	  every((
	   	 domain(_Item::item{},L),
%		 verbose('removing from beam ~w\n',[_Item]),
	   	 erase_item(_Item)
	   	)),
	  true
	)
	.

:-std_prolog more_general_item/2.

more_general_item(item{ step => S,
			generation => Gen,
			right => Right,
			left => Left,
			stack => Stack0,
			stack1 => Stack1,
			prefix => PrefixA,
			inside => InsideA,
			lk1 => Id1,
			lk2 => Id2,
			lk3 => Id3,
			action => Action,
			swap => Swap
		      },
		  item{ step => S,
			generation => Gen,
			right => Right,
			left => Left,
			stack => Stack0,
			stack1 => Stack1,
			prefix => PrefixB,
			inside => InsideB,
			lk1 => Id1,
			lk2 => Id2,
			lk3 => Id3,
			action => Action,
			swap => Swap
		      }
		 ) :-
    ( %(Action=lefta(_) xor Action = righta(_))
	% fail,
	tagset!attach,
	true ->
	  PrefixA = PrefixB,
	  InsideA = InsideB
      ;
	 ( PrefixA > PrefixB
	 xor PrefixA=PrefixB, InsideA >= InsideB
	 )
    )
.

:-std_prolog equiv_item/2.

equiv_item(item{ step => S,
		 generation => Gen,
		 right => Right,
		 left => Left,
		 stack => Stack0,
		 stack1 => Stack1,
		 lk1 => Id1,
		 lk2 => Id2,
		 lk3 => Id3,
		 action => Action,
		 swap => Swap
	       },
	   item{ step => S,
		 generation => Gen,
		 right => Right,
		 left => Left,
		 stack => Stack0,
		 stack1 => Stack1,
		 lk1 => Id1,
		 lk2 => Id2,
		 lk3 => Id3,
		 action => Action,
		 swap => Swap
	       }
	  ) 
.


:-extensional item!tails/2.

:-xcompiler
item!tail(ItemAddr,ItemAddr0,W) :-
	item!tails(ItemAddr,L),
	'$interface'('Slist_Enumerate'(L:ptr,ItemAddr0:term,W: term),[choice_size(1)])
	.

:-xcompiler
item!add_tail(ItemAddr,ItemAddr0,W) :-
    %    format('try to add tail ~w ~w\n',[ItemAddr,ItemAddr0]),
    (W  == nocost ->
	 (recorded(item!nocost_tail(ItemAddr,ItemAddr0)) ->
	      true
	 ;
	 erase(item!call_noloss(ItemAddr)),
	 record(item!nocost_tail(ItemAddr,ItemAddr0))
	 )
    ;
    (  
	item!tails(ItemAddr,L),
	%      format('\treusing tail\n',[]),
	true
	xor
        beam(N),
	'$interface'('Slist_New'(N:int), [return(L:ptr)]),
	%	format('\tcreating tail\n',[]),
	record(item!tails(ItemAddr,L))
    ),
    %    safe_recorded(item{},ItemAddr0),
    (fail, item!tail(ItemAddr,ItemAddr0,_W), _W > W -> true
    ; %% '$interface'('Slist_Add'(W:int,ItemAddr0:term,L:ptr), []) -> true
     ( fail,
       opt(train:_),
       recorded(Item0::item{ step => S0}, ItemAddr0),
       oracle!item(S0,Item0),
       recorded(Item::item{ step => S}, ItemAddr),
       oracle!item(S,Item)
       -> Oracle = 1 ; Oracle = 0),
     '$interface'('Slist_Add'(W:term,ItemAddr0:term,L:ptr,Oracle:int), []) -> true
     ; true
    )
    )
	.

:-xcompiler
erase_item(Item::item{ step => S }) :-
	( oracle!item(S,Item)
	xor oracle!get_best_action(S,_,_,Item)
	xor
	  recorded(Item,ItemAddr),
	  verbose('to be deleted item ~w\n',[ItemAddr]),
	  record_without_doublon(item!delete(ItemAddr)),
	  true
	),
	true
	.
