#!/usr/bin/env perl

## a sample training script to be adapted for specific needs

use strict;
use POSIX qw(strftime);
use AppConfig;

my $config = AppConfig->new(
                            'verbose|v!' => {DEFAULT => 0},
			    'train=f' => {DEFAULT => "train.conll"}, # train file
			    'dev=f' => {DEFAULT => "dev.conll"}, # dev file
			    'test=s',			      # test file if any
                            'cdir=f' => { DEFAULT => "data" },	 # path to data set
                            'iter=d' => {DEFAULT => 10},	 # number of epochs
			    'beam=d' => {DEFAULT => 1},		 # beam size
			    'model=s' => {DEFAULT => 'model'},	 # base for naming models
			    'evalcmd=f' => {DEFAULT => "conll_eval.pl"}, # eval command 
			    'log=f' => { DEFAULT => "train.log" },
			    'mdir=s' => {DEFAULT => 'models'}, # where to store result files and models
			    'tagset=s' => {DEFAULT => 'lang.tagset'}, # tagset to use
			    'mode=s' => {DEFAULT => 'early'}, # early, late, best
			    'sbeam=d' => {DEFAULT => 3},      # dynamic beam size if any
			    'random_skip=d' => {DEFAULT => 0}, # possibly to randomly skip some training sentences
			    'parser_options=s',		       # additional parsing options
			    'goldpos=s',		       # using gold pos as oracle
			    'pretrain=d' => {DEFAULT => 0},    # running neural-only pre-train epochs
			    'use_fullcat!' => {DEFAULT => 0}
			   );

$config->args;

my $model_base=$config->model;
my $cdir = $config->cdir;
my $evalcmd = $config->evalcmd;
my $train = $config->train;
my $dev = $config->dev;
my $test = $config->test;
my $beam = $config->beam;
my $maxiter = $config->iter;
my $tagset = $config->tagset;
my $mode = $config->mode;
my $skip = $config->random_skip;
my $use_fullcat = $config->use_fullcat;

my $goldpos = $config->goldpos;

my $pretrain = $config->pretrain;

#my $sbeam = "-sbeam ".$config->sbeam;
my $sbeam = "";

-f $tagset
  or die "missing tagset file '$tagset': $!";

my $mdir = $config->mdir;

-d $mdir 
  or mkdir $mdir;

-d $mdir
  or die "couldn't build model directory '$mdir': $!";

my $hostname = `hostname`;
chomp $hostname;

my $logfile = $config->log;
open(LOG,">$mdir/$logfile") || die "can't open logfile '$mdir/$logfile': $!";

verbose("training hostname=$hostname iter=$maxiter train=$train dev=$dev mode=$mode beam=$beam model=$model_base mdir=$mdir");

my $gtrain = "$cdir/$train";
my $gdev = "$cdir/$dev";
my $gtest;

$goldpos or $goldpos = $gtrain;

$test and $gtest = "$cdir/$test";
  
foreach my $iter (1..$maxiter+$pretrain) {
  my $do_pretrain = $iter < $pretrain;
  verbose($do_pretrain ? "iteration $iter (pretrain)" : "iteration $iter");
  my $prev = $do_pretrain ? 0 : ($iter == $pretrain) ? 0 : $iter-1;
  my $oiter = $do_pretrain ? 0 : $iter;
  my $suff = "b${beam}i${oiter}";
  my $model = "$mdir/$model_base.$suff";
  my $ldev = "$mdir/$dev.$suff";
  my $aldev = "$mdir/$dev.$suff";
  $aldev =~ s/\.dconll/.answer/o;
  my $eval = "$mdir/$dev.eval.$suff";
  my $in = (1 || $do_pretrain || $iter == $pretrain || $prev) ? "-load_model $mdir/$model_base.b${beam}i${prev}" : "";
  my $out = "-save_model $model";
  my $lexer_opts = "--tagset $tagset";
  my $train_lexer_opts = $lexer_opts;
  $train_lexer_opts .= " --oracle --pos_oracle";
  $skip and $train_lexer_opts .= " --random_skip=$skip";
  my $parser_options = '';
  ($config->parser_options) and $parser_options = $config->parser_options;
  my $pretrain_opts = '';
  ($do_pretrain) and $pretrain_opts = '-dynet-stop';
  my $lbeam = $beam;
  ($do_pretrain) and $lbeam = 1;
  verbose("\tbuilding $model on $train");
  my $cmd = "cat $gtrain | perl ./conll_combine.pl $goldpos $use_fullcat | perl ./conll_shuffle.pl | perl ./conllu2db.pl $train_lexer_opts 2>> $mdir/lexer_errs.log | tee $mdir/lexer.log | ./dpsr_parser -loop -utime -- -epoch $iter $pretrain_opts -train $mode -beam $lbeam $sbeam -res $tagset $in $out -multi $parser_options 2>> $mdir/parser_err.log > $mdir/$train.answer.$suff";
  print STDERR "$cmd\n";
#  unless (-f $model) {
  if ($do_pretrain || !-f "$mdir/$train.answer.$suff" || !-f $model) {
    system("$cmd") == 0
      or warn "system failed: $?";
  }
  ($do_pretrain) and $pretrain_opts = '-mst -mst_emit';
  if (1) {
    verbose("\trunning on dev $dev");
    if ($do_pretrain || !-f $ldev || !-f $aldev) {
      system("cat $gdev| perl ./conllu2db.pl $lexer_opts | ./dpsr_parser -loop -utime -- -beam $lbeam $sbeam -res $tagset -load_model $model -multi $parser_options $pretrain_opts | tee $aldev | perl ./answer2conllu.pl| cut -f1-8 > $ldev" ) == 0
	or die "system failed: $?" ;
    }
  }
  if (-f $ldev){
    my $tmp = "$evalcmd -g $gdev -s $ldev";
    verbose("run eval: $tmp\n");
    system("$tmp 2> /dev/null > $eval") == 0
      or warn "system failed: $?";
    my $stat = `grep "Labeled   attachment score" $eval`;
    chomp $stat;
    $stat =~ /(\S+)\s+\%/ and verbose("\tmodel=$model LAS=$1");
  }
  if ($gtest && -f $gtest) {
    my $ltest = "$mdir/$test.$suff";
    my $altest = "$mdir/$test.$suff";
    $altest =~ s/\.dconll/.answer/o;
    verbose("\trunning on test $test");
    if ($do_pretrain || !-f $ltest || !-f $altest) {
      system("cat $gtest| perl ./conllu2db.pl $lexer_opts | ./dpsr_parser -loop -utime -- -beam $lbeam $sbeam -res $tagset -load_model $model -multi $parser_options $pretrain_opts | tee $altest | perl ./answer2conllu.pl| cut -f1-8 > $ltest") == 0
	or warn "system failed: $?" ;
    }
    if (-f $ltest){
      my $eval = "$mdir/$test.eval.$suff";
      my $tmp = "$evalcmd -g $gtest -s $ltest";
      verbose("run eval: $tmp\n");
      system("$tmp 2> /dev/null > $eval") == 0
	or warn "system failed: $?";
      my $stat = `grep "Labeled   attachment score" $eval`;
      chomp $stat;
      $stat =~ /(\S+)\s+\%/ and verbose("\tmodel=$model LAStest=$1");
    }
  }
}

verbose("done");

sub verbose {
    my $x = shift;
    my $date = strftime "[%F %H:%M:%S]", localtime;
    my $msg = "$date : $x\n";
    print LOG $msg;
    print $msg;
}

close(LOG);

1;

__END__

=head1 NAME

train.pl -- a sample train script for dyalog-srnn

=head1 SYNOPSIS

 ./train.pl
                -v                            (verbose mode)
                --cdir <path_to_treebank>     (default: data)
                --tagset <tagset_file>       
                --train <name_of_train_file>  (default: train.conll)
                --dev <name_of_dev_file>      (default: dev.conll)
                --test <name_of_test_file>    (default: test.conll)
                --mdir <path_to_result_dir>   (default: models)
                --evalcmd <path_to_conll_eval_cmd>
                --log <log_file>
                --random_skip <ratio_of_skipped_sentences>    (default: 0)
                --goldpos                   (use gold POS as oracle)
                --iter <number_of_epoch>    (default: 10)
                --pretrain <number_of_pretraining_epoch>    (default: 0)

=head1 DESCRIPTION

B<train.pl> is a generic script to train dyalog-srnn over a dataset.
It should work for basic usages but will need to be adapted for more
specific usages.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2017, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut

