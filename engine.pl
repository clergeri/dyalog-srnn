/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2013, 2014, 2015, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  engine.pl -- Engine for Tabular Shift-Reduce Dependency Parsers
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require('format.pl').
:-require('utils.pl').
:-require('features.pl').
:-require('dynet.pl').
:-require('automata.pl').
:-require('mst.pl').

:-extensional info!comment/1.
:-extensional info!contracted/3.
:-extensional info!ellipse/3.

% info provided by the lexer through dict files
% exploited by features *dict*
:-extensional dict!info/2. 	


?- verbose('new round\n',[]),
   show_time(latency),
   verbose('here1\n',[]),
%%   test_float(3.14159),
   process_options,
   verbose('here2\n',[]),
   ( oracle!end ->
     ( save_avgweight,
       fail
     ;
     \+ opt(dynet_stop),
     % don't try to save the perceptron model when only dynet updates were allowed
     % otherwise we get an empty model because of feature trimming
     save_model,
     fail
     ; dynet!save_model,
       fail
     ;
     show_time(model_save)
     )
   ;
     show_time(model_load),
     parse
   ),
   fail
   .

:-std_prolog parse/0.

parse :-
    'N'(N),
    verbose('processing new sentence N=~w\n',[N]),
    %% dynet init
    every(( 'C'(_L,_Id,_,_),
	    record( 'rC'(_Id,_L))
	  )),
%    (opt(nodynet) xor dynet!sentence_init),
    dynet!sentence_init,
    (opt(dynet_stop) ->
	 once_xor_true((opt(dynet_emit),
			dynet!display
		       )),
	 once_xor_true(( opt(train:_),
			 \+ opt(nodynet),
			 dynet!apply_updates )),
	 fail
    ;
    true
    ),
    every((
		 opt(mst),
		 record(mst!root(0)),
		 mst!init,
		 mst!solve,
		 true
	     )),
    (opt(mst),
     opt(mst_emit) ->
	 %% assume opt(mst) is active
	 mst!display,
	 once_xor_true(( opt(train:_),
			 \+ opt(nodynet),
			 dynet!apply_updates
		       )),
	 fail
    ;
    true
    ),
	( opt(train:_) ->
	  %% we only advance the perceptron in training mode
	  perceptron!advance,
	  /*
	  every(( oracle!action(_Step,_Action),
		  domain(_Action,[swap,swap2,pop0,pop1,left2(_),right2(_),lefta(_),righta(_)]),
		  _LStep is _Step - 2,
		  _RStep is _Step + 2,
		  term_range(_LStep,_RStep,_TStep),
		  domain(_XStep,_TStep),
		  record_without_doublon(dynoracle!potential_action(_XStep,_Action))
		))
		*/
	  true
	;
	  opt(check) xor  abolish(oracle!action/2)
	),
	('S'(SId) xor SId='E1'),
	%%	MaxStep is (2 * N) - 1,
	(avgweight(AvgWeight,_,__MaxSteps) xor
         format('%% *** missing avgweight\n',[]),
	 (recorded(avgweight(AvgWeight)), format('found avgweight ~w\n',[AvgWeight]) xor AvgWeight=700),
	 record(avgweight(700,0,0)),
	 __MaxSteps = 0
	),
	UpdateMarge is min(120.0,AvgWeight / 5),
%	Marge2 is AvgWeight/5,
%	record_without_doublon(marge(Marge2)),
	(opt(train) ->
	     format('%% marge=~w avg=~w nstep=~w\n',[UpdateMarge,AvgWeight,__MaxSteps])
	 ;
	 true
	),
	(opt(train:_),
	 \+ tagset!noop ->
	     oracle!action(MaxStep,_),
	     \+ (oracle!action(_MaxStep,_), _MaxStep > MaxStep)
	 ; maxstep_factor(MaxStepFactor) ->
%	       format('using maxstep factor ~w\n',[MaxStepFactor]),
	       MaxStep is round(MaxStepFactor * N)
	 ; tagset!attach ->
	       %% when accepting multi-governors, it is less clear how many steps we need
	       %% it would be even worse if accepting multiple edges between same nodes
	       %% the current value (3) is hardcoded and seems enough for SemEval PCEDT
	       %% but it should be a parameter or be automatically found
	       MaxStep is (4 * N)
	 ;
	 MaxStep is (2 * N)
	),
%%	beam(Beam),
%%	'$interface'('Beam_Cells_Allocate'(MaxStep:int,Beam:int),[return(none)]),
%%	format('start parsing sid=~w N=~w MaxStep=~w\n',[SId,N,MaxStep]),
	every((
%%	 \+ oracle!skip_sentence,
%%	 \+ oracle!notproj(_,_),
	 beam!new_cell(0,_),
	 ( oracle!action(0,init) ->
	   record_without_doublon(oracle!item(0,InitItem))
	 ;
	   true
	 ),
	 register_item(
		       InitItem::item{ step => 0,
				       generation => 0,
				       right => 0,
				       left => 0,
				       stack => tree(0,[],[],[],[],[],[]),
				       stack1 => [],
				       prefix => 0,
				       inside => 0,
				       lk1 => 0,
				       lk2 => 0,
				       lk3 => 0,
				       action => init,
				       swap => []
				     },
		       _
		      ),
	 fail
	;
	term_range(0,MaxStep,TStep),
	domain(Step,TStep),
	
	 update_counter(generation,_),
	 value_counter(generation,CurrentGen),

	 verbose('Dealing step=~w generation=~w\n',[Step,CurrentGen]),
	 every((
		      erase(oracle!violation(Step,_,_)),
		      erase(oracle!agree(Step,_,_))
	      )),
	 ( recorded(terminal_step(_Step)) ->
%	   format('found terminal step ~w\n',[_Step]),
	       fail
	   ; tagset!noop,
	   \+ ( (beam!enumerate(Step,_Item) ; oracle!item(Step,_Item)),
		\+ item!terminal(N,_Item)
	      ) ->
	     %% if there is an item at step Step that is terminal
	     %% then we record the step as a terminal one, and dont try to go furher 
%	       format('register terminal step ~w\n',[Step]),
	       record_without_doublon(terminal_step(Step)),
	       fail
	   ;
	   true
	 ),
	 ( beam!best(Step,_BestItem) ->
%	       recorded(_BestItem,_BestItemAddr),
%	       (item!back(_BestItemAddr,_BestAction,_BestItemAddr0,_,_BestCost) xor true),
%	       format('**** step ~w bestaction=~w cost=~w item0=~w item=~w:~w\n',[Step,_BestAction,_BestCost,_BestItemAddr0,_BestItemAddr,_BestItem]),
	       true
	   ;
	       format('no item at step ~w\n',[Step])
	 ),
	 NStep is Step+1,
	 beam!new_cell(NStep,_),
/*
	 on_verbose(
		 every(( format('show oracle item at step=~w\n',[Step]),
			 oracle!item(Step,_Item),
			 recorded(_Item,_Addr),
			 format('oracle step=~w item=~w: ~w\n',[Step,_Addr,_Item]),
			 every(( item!tail(_Addr,_Addr2,_W),
				 format('\ttail ~w w=~w\n',[_Addr2,_W])
				 ;
				 oracle!tail(_Addr,_Addr2,_W),
				 format('\toracle tail ~w w=~w\n',[_Addr2,_W])
			      ))
		      ))
	     ),
*/
	 
	 once_xor_true((
			      %fail,
			      opt(train:_),
			      opt(dyn_oracle),
			      \+ oracle!action(NStep,_),
			      format('search for next oracle action at ~w\n',[NStep]),
			      oracle!item(Step,_OracleItem),
			      recorded(_OracleItem,_OracleItemAddr),
			      format('\toracle item ~w:~w\n',[_OracleItemAddr,_OracleItem]),
			      item!noloss(_OracleItemAddr),
			      format('\toracle item is a no loss one !\n',[]),
			      recorded(item!noloss_descendant(Step,_OracleItemAddr,_NextOracleItemAddr)),
			      format('\tfound noloss descendant ~w\n',[_NextOracleItemAddr]),
			      item!back(_NextOracleItemAddr,_NextOracleAction,_OracleItemAddr,_,_,_NextOracleNAction),
			      record(oracle!action(NStep,_NextOracleAction)),
			      format('=> found oracle action at ~w : ~w\n',[NStep,_NextOracleAction]),
			      _NextOracleAction = shift(_Id1,_Id2,_Id3),
			      %				     _OracleItem = item{ lk1 => _Id1, lk2 => _Id2, lk3 => _Id3 },
			      record_without_doublon(oracle!shift(_Id1,_Id2,_Id3)),
			      true
			  )),
	 
	 once_xor_true((
			      opt(train:_),
			      opt(dyn_oracle),
			      \+ oracle!action(NStep,_),
			      oracle!item(Step,_OracleItem),
			      _OracleItem = item{ lk1 => _Id1, lk2 => _Id2, lk3 => _Id3 },
			      (suggest_oracle_action(_OracleItem,_NextOracleAction)
			       xor
			       (_Id1 = 0 -> Id1 = 1, Id2 = 2, Id3 = 3 ; Id1=_Id1, Id2 = _Id2, Id3 = _Id3),
			       _NextOracleAction = shift(Id1,Id2,Id3),
			       record_without_doublon(oracle!shift(Id1,Id2,Id3))
			      ),
			      format('Setting default oracle action at ~w: ~w\n',[NStep,_NextOracleAction]),
			      record(oracle!action(NStep,_NextOracleAction))
			  )),
	 
	 
	 every((
%		      NStep is Step+1,
		      (
			  (opt(train:_),
			   oracle!item(Step,Item),
			   verbose('step ~w oracle item ~w\n',[Step,Item]),
			   ( item!terminal(N,Item) ->
			     record_without_doublon(oracle!action(NStep,noop))
			   ;
			   true
			   ),
			   ( recorded(Item,_) 
			     xor format('oracle item declared but erased at step=~w item=~\n',[Step,Item])
			   ),
			   ( oracle!item(Step,AltItem),
			     \+ AltItem  = Item ->
			     format('more than one oracle at step=~w item=~w altitem=~w\n',[Step,Item,AltItem])
			   ;
			   true
			   )
			   xor 
			   \+ opt(check),
			   oracle!action(Step,PrevOracleAction),
			   format('no oracle item step=~w prevaction=~w\n',[Step,PrevOracleAction]),
			   fail
			  ),
			  %		 oracle!item(Step,Item),
			  \+ beam!enumerate(Step,Item),
			  oracle!action(NStep,Action),
			  verbose('using selected oracle step=~w item=~w\n',[Step,Item]),
			  recorded(Item,ItemAddr)
		      ;
		      beam!enumerate(Step,Item),
		      recorded(Item,ItemAddr),
		      record(beam!inside(ItemAddr,Step))
		      ),


%		item!step(Step,Item,ItemAddr),
		verbose('step ~w process item ~w: ~w\n',[Step,ItemAddr,Item]),
		every((
			     generate_shift(Item,ItemAddr,true,Action,_Loss)
			  ;
			  generate_other(Item,ItemAddr,true,Action,_Loss)
			 ))
		  )),
	 %% Check mode (when activated)
	 every(( opt(check),
		 check_info(Step)
	      )),
	 %% update as soon as possible
	 %% may have an impact for the rest of the sentence
	 ( once((
		     %% update should be done only once for each step
		     %% furthermore, an update may erase some items
		     %% leading to segmentation errors when backtracking on recorded(...)
		     %% (note: this a bug of DyALog: deleting elements in the table while iterating on it)
		     %% to avoid the problem, we delay the erasing of the items, until
		     %% the very end of the step (and first mark the items to be deleted)
		opt(train:early),
		verbose('train at step ~w\n',[Step]),
		(oracle!item(NStep,NextOracleItem) 
		       xor 
		       (oracle!action(NStep,Action) xor true),
		 oracle!item(Step,Item0),
		 format('step ~w training missing oracle item action=~w item0=~w\n',[NStep,Action,Item0]), 
		 fail),
		recorded(NextOracleItem,NextOracleItemAddr),
		once(( item!back(NextOracleItemAddr,Action,ItemAddr0,_,NextOracleW,NAction),
		       safe_recorded(Item0::item{ step => Step, prefix => W0} ,ItemAddr0),
		       oracle!item(Step,Item0)
		     )),
		%% dynet update when NAction (at neural layer) is not the oracle action Action
		((NAction = Action xor NAction = shift, Action = shift(_,_,_)) ->
		     true
		;
		%% do an update only at neural layer
		weight_update(Item0,NextOracleItem,nn(1),_,_)
		),
		oracle!get_best_action(NStep,BestOracleAction,_,BestOracleItem),
		%% (opt(dyn_oracle), 0 is rand(10) -> Use_Oracle = false ; Use_Oracle = true),
		%% verbose('step =~w oracle_action=~w best_oracle_action=~w item0=~w\n',[NStep,Action,BestOracleAction,ItemAddr0]),
		( % Use_Oracle = true,
		  \+ Action = BestOracleAction ->
		      %% the best item reached from the previous oracle item
		      %% is not reached through the oracle action
		      %%	   format('here oracle_item=~w\n',[NextOracleItem]),
		      update_counter(updates,_),
		      update_counter(updates1,_),
		      update_items(BestOracleItem::item{ action => BestOracleAction },
				   NextOracleItem::item{ action => Action },
				   NextOracleItemAddr,
				   Item0,
				   Item0 )

		;
		  % Use_Oracle = true,
		  beam!best(NStep,BestItem::item{ prefix => BestW }),
		  BestW > NextOracleW,
		  oracle!representative_item(NextOracleItem,RepNextOracleItem::item{ prefix => RepNextOracleW }),
%%		  RepNextOracleItem::item{ prefix => RepNextOracleW } = NextOracleItem,
		  \+ BestItem = RepNextOracleItem,
		  recorded(BestItem,BestItemAddr),
		  once((
			      item!back(BestItemAddr,BestAction,BestItemAddr0,_,BestW,BestNAction),
                              \+ ( % should not need this
				     item!back(BestItemAddr,_,_,_,_BestW,_),
				     _BestW > BestW
				 ),
			      safe_recorded(BestItem0::item{ step => Step},BestItemAddr0)
		      )),
		  (
                   %% the oracle item is not in the beam 
                   \+ beam!enumerate(NStep,RepNextOracleItem)
	           xor
		   %% the cost for the oracle item is too low wrt the cost of best item
		   RepNextOracleW < BestW - UpdateMarge
		   xor 
		   %% BestItem is a final item, but not the one predicted by the oracle !
		   BestItem = item{ right => N, left => 0, stack => tree(0,[],_,_,_,_,_), stack1 => [], lk1 => 0, lk3 => Id3, swap=> []},
                   \+ RepNextOracleItem = BestItem
		   xor
		   %% NextOracleItem is a final item, but not the best one
		   RepNextOracleItem = item{ right => N, left => 0, stack => tree(0,[],_,_,_,_,_), stack1 => [], lk1 => 0, lk3 => Id3, swap => []},
		   \+ RepNextOracleItem = BestItem
		   xor 
		   %% the best item clearly violates the oracle
		   oracle!violation(NStep,BestItem,_),
		   update_counter(updates2_violation,_)
		  ) ->
		  %% we try to compensate negatively for the best item and positively for the oracle item
		  update_counter(updates,_),
		  update_counter(updates2,_),
		  recorded(RepNextOracleItem,RepNextOracleItemAddr),
		  once(( item!back(RepNextOracleItemAddr,Action,RepItemAddr0,_,RepNextOracleW,NAction),
			 safe_recorded(RepItem0::item{ step => Step } , RepItemAddr0),
			 true
		      )),
		  verbose('update2 oitem0=~w aitem0=~w oaction=~w baction=~w\n',[ItemAddr0,BestItemAddr0,Action,BestAction]),
		  update_items(BestItem::item{ action => BestAction },
			       RepNextOracleItem::item{ action => Action },
			       RepNextOracleItemAddr,
			       BestItem0,
			       RepItem0
			      )

		; %% violation-based updates are tried before oracle-based updates
%		  fail,
		  %% from a parent item, we derive a violation item (wrt oracle) with better cost than an agree item
		  %% we decide to update
		  %% the gain seems to be marginal !
		  oracle!agree(NStep,AItem::item{},ACost),
		  oracle!violation(NStep,VItem::item{},VCost),
		  beam!enumerate(NStep,VItem),
		  VCost > ACost,
%		  \+ beam!enumerate(NStep,AItem),
		  recorded(VItem,VItemAddr),
		  once((
			      item!back(VItemAddr,VAction,VItemAddr0,_,VW,VNAction),
			      \+ ( % should not need this
				  item!back(VItemAddr,_,_,_,_VW,_),
				  _VW > VW
			      ),
			      safe_recorded(VItem0::item{ step => Step},VItemAddr0)
		      )),
		  recorded(AItem,AItemAddr),
		  once((
			      item!back(AItemAddr,AAction,AItemAddr0,_,AW,ANAction),
			      \+ ( % should not need this
				  item!back(AItemAddr,_,_,_,_AW,_),
				  _AW > AW
			      ),
			      safe_recorded(AItem0::item{ step => Step},AItemAddr0)
		      )),
		  AItem0=VItem0,
		  true ->
		  update_counter(updates,_),
		  update_counter(updates3,_),
		  verbose('update3 aitem0=~w vitem0=~w aaction=~w vaction=~w acost=~w vcost=~w\n',[AItemAddr0,VItemAddr0,AAction,VAction,ACost,VCost]),
		  update_items(VItem::item{ action => VAction},
			       AItem::item{ action => AAction},
			       AItemAddr,
			       VItem0,
			       AItem0
			      )
		;
		opt(dyn_oracle),
		  %		  N < 20,
%		  rand(max(2,N-NStep / 2)) < 2,
%		  format('try update4 at step=~w\n',[NStep]),
		  beam!enumerate(NStep,LossItem::item{ prefix => LossItemW }),
		  recorded(LossItem,LossItemAddr),
		  (oracle!violation(NStep,LossItem,_) xor \+ item!noloss(LossItemAddr)),
%		  format('try1 update4 at step=~w lossitem=~w\n',[NStep,LossItem]),
		  beam!enumerate(NStep,NoLossItem::item{ prefix => NoLossItemW }),
		  LossItemW > NoLossItemW,
		  recorded(NoLossItem,NoLossItemAddr),
		  %		  format('try2 update4 at step=~w lossitem=~w nolossitem=~w\n',[NStep,LossItem,NoLossItem]),
		  \+ oracle!violation(NStep,NoLossItem,_),
		  item!noloss(NoLossItemAddr),
%		  format('potential update4 step=~w lossw=~w nolossw\n',[NStep,LossItemW,NoLossItemW]),
		  once((
			      item!back(LossItemAddr,LossAction,LossItemAddr0,_,LossW,LossNAction),
			      \+ ( % should not need this
				  item!back(LossItemAddr,_,_,_,_LossW,_),
				  _LossW > LossW
				  ),
			      safe_recorded(LossItem0::item{ step => Step},LossItemAddr0)
			  )),
		  once((
			      item!back(NoLossItemAddr,NoLossAction,NoLossItemAddr0,_,NoLossW,NoLossNAction),
			      \+ ( % should not need this
				      item!back(NoLossItemAddr,_,_,_,_NoLossW,_),
				      _NoLossW > NoLossW
				  ),
			      safe_recorded(NoLossItem0::item{ step => Step},NoLossItemAddr0)
			  )),
%		  LossItemItem0=NoLossItem0,
		  true
		  ->
		  update_counter(updates,_),
		  update_counter(updates4,_),
		  format('update4 step=~w nolossitem0=~w lossitem0=~w nolossaction=~w lossaction=~w nolosscost=~w losscost=~w lossitem=~w nolossitem=~w\n',[Step,NoLossItemAddr0,LossItemAddr0,NoLossAction,LossAction,NoLossW,LossW,LossItem,NoLossItem]),
		  update_items(LossItem::item{ action => LossAction},
			       NoLossItem::item{ action => NoLossAction},
			       NoLossItemAddr,
			       LossItem0,
			       NoLossItem0
 			      ),
		  true
		;
%		fail,
		opt(dyn_oracle),
		beam!enumerate(NStep,LossItem::item{ prefix => LossItemW }),
		recorded(LossItem,LossItemAddr),
		(oracle!violation(NStep,LossItem,_) xor \+ item!noloss(LossItemAddr)),
		%% we have a loss item in the beam but no noloss item
		%% (otherwise update4 would have been triggered)
		once((
			    item!back(LossItemAddr,LossAction,LossItemAddr0,_,LossW,LossNAction),
			    \+ ( % should not need this
				    item!back(LossItemAddr,_,_,_,_LossW,_),
				    _LossW > LossW
				),
			    safe_recorded(LossItem0::item{ step => Step},LossItemAddr0)
			)),
		%% but there is a success path from LossItemAddr0 (parent of LossItem), using an item at NStep not in beam !
		item!noloss(LossItemAddr0),
		recorded(item!noloss_descendant(Step,LossItemAddr0,NoLossItemAddr)),
		recorded(NoLossItem,NoLossItemAddr),
		\+ (beam!enumerate(NStep,_NoLossItem),
		    \+ oracle!violation(NStep,_NoLossItem,_),
		    recorded(_NoLossItem,_NoLossItemAddr),
		    item!back(_NoLossItemAddr,_,LossItemAddr0,_,_,_),
		    item!noloss(_NoLossItemAddr)
		   ),
%		\+ NoLossAction = shift(_,_,_),
		true
		->
		NoLossItem0 = LossItem0,
		NoLossItemAddr0 = LossItemAddr0,
		update_counter(updates,_),
		update_counter(updates6,_Updates6),
		format('update6 #=~w step=~w nolossitem0=~w lossitem0=~w nolossaction=~w lossaction=~w nolosscost=~w losscost=~w nolossitem=~w\n',[_Updates6,Step,NoLossItemAddr0,LossItemAddr0,NoLossAction,LossAction,NoLossW,LossW,NoLossItem]),
		update_items(LossItem::item{ action => LossAction},
			     NoLossItem::item{ action => NoLossAction},
			     NoLossItemAddr,
			     LossItem0,
			     NoLossItem0
			    ),
		true

		  ; 
		  fail,
		  beam!enumerate(S,NextOracleItem),
		  beam!enumerate(S,BetterItem::item{ prefix => BetterW }),
		  BetterW > NextOracleW,
		  recorded(BetterItem,BetterItemAddr),
		  once((
			      item!back(BetterItemAddr,BetterAction,BetterItemAddr0,_,BetterW,BetterNAction),
			      \+ ( % should not need this
				  item!back(BetterItemAddr,_,_,_,_BetterW,_),
				  _BetterW > BetterW
			      ),
			      safe_recorded(BetterItem0::item{ step => Step, prefix => BetterW0 },BetterItemAddr0)
		      )),
		  BetterW < W0 ->
		  update_counter(updates,_),
		  update_counter(updates4,_),
		  verbose('update4 oitem0=~w aitem0=~w oaction=~w baction=~w\n',[ItemAddr0,BetterItemAddr0,Action,BetterAction]),
		  update_items(BetterItem::item{ action => BetterAction },
			       NextOracleItem::item{ action => Action },
			       NextOracleItemAddr,
			       BetterItem0,
			       Item0
			      )
		  ;
                  \+ tagset!noop,
		  beam!best(NStep,NextOracleItem),
		  item!terminal(N,NextOracleItem),
%		  format('potential update5-1 N=~w oraclew=~w avgweight=~w\n',[N,NextOracleW,AvgWeight]),
		  ( beam!enumerate(NStep,NonTerminalItem::item{ right => N1, prefix => NTIW }),
		    \+ item!terminal(N,NonTerminalItem),
%		    DeltaS is 2.0*(1+N-N1)*AvgWeight,
		    Bound is NTIW + 2.0 * (1+N-N1) * AvgWeight,
%		    format('\tpotential update5-1-1 oraclew=~w N1=~w NTIW=~w Bound=~w deltaS=~w\n',[NextOracleW,N1,NTIW,Bound,DeltaS]),
		    NextOracleW <  Bound ->
			true
		    ;
		    fail
		  ) ->
		      recorded(NonTerminalItem,NTIAddr),
%		      format('potential update5-2 nitw=~w\n',[NTIW]),
		      once((
				  item!back(NTIAddr,NTIAction,NTIAddr0,_,NTIW,NTINAction),
                            \+ ( % should not need this
				  item!back(NTIAddr,_,_,_,_NTIW,_),
				  _NTIW > NTIW
			      ),
			      safe_recorded(NTI0::item{ step => Step, prefix =>NTIW0 },NTIAddr0)
		      )),

		      update_counter(updates,_),
		      update_counter(updates5,_),
		      format('update5 N=~w N1=~w\n',[N,N1]),
		      update_items(NonTerminalItem::item{ action => NTIAction },
				   NextOracleItem::item{ action => Action },
				   NextOracleItemAddr,
				   NTI0::item{},
				   Item0::item{}
				  )
		  ;
		  fail
		),
		true
	       ))
	     ,
	   fail
	   ;
	   verbose('cleaning step=~w\n',[Step]),
	   every((  ( recorded(item!delete(_ItemAddr)),
		      verbose('try erase item ~w\n',[_ItemAddr]),
                      \+ ( recorded(_Item::item{ step => _S },_ItemAddr),
			   oracle!item(_S,_Item),
			   % verbose('cancel erasing of oracle item ~w: ~w\n',[_ItemAddr,_Item]),
			   true
			 )
		      ;
		      recorded(item!delete_generation(NStep,CurrentGen)),
		      recorded(item{ step => NStep, generation => CurrentGen }, _ItemAddr),
		      verbose('delete step ~w generation ~w item ~w\n',[NStep,CurrentGen,_ItemAddr]),
		      true
		      ;
		      recorded(item{ step => NStep, prefix => nocost },_ItemAddr)
		    ),
		    delete_address(_ItemAddr),
		    verbose('erase item ~w\n',[_ItemAddr]),
		    update_counter(erasing,_),
		   %	  erase(item!stack(ItemAddr,_)),
		    erase(item!tails(_ItemAddr,_)),
		    erase(item!back(_ItemAddr,_,_,_,_,_)),
		    erase(oracle!tail(_ItemAddr,_,_)),
		    erase(item!nocost_tail(_ItemAddr,_)),
		    erase(item!loss(_ItemAddr,_)),
		    verbose('done erase item ~w\n',[_ItemAddr]),
		    true
		)),
	   verbose('done cleaning step=~w\n',[Step]),
	   abolish(item!delete/1),
	   abolish(item!delete_generation/2),
	   abolish(update_processed/1),
	   fail
	 )
	;
	   true
	)),
	%	_MaxStep=MaxStep,
	verbose('completed all steps\n',[]),
	( tagset!noop ->
		 (recorded(terminal_step(_MaxStep)) xor _MaxStep = MaxStep),
		 __MaxStep = _MaxStep,
		 verbose('retrieve terminal step ~w\n',[_MaxStep]),
		 true
	 ;
	     true
	),
	( (beam!enumerate(_MaxStep,
			  FinalItem::item{ step => _MaxStep,
					   right => N,
					   left => 0,
					   stack => tree(0,[],_,_,_,_,_),
					   stack1 => [],
					   prefix => Cost,
					   inside => _Cost,
					   lk1 => 0,
					   lk2 => 0,
					   %				      lk3 => 0,
					   swap => []
					 }
			 )
	  ; oracle!item(_MaxStep,FinalItem)
	   %% ; beam!enumerate(_MaxStep,FinalItem),
	   %%   format('*** step=~w final in beam but not the best !\n',[_MaxStep]),
	   %%   true
	  ),
%         verbose('potential final item step=~w ~w\n',[_MaxStep,FinalItem]),
	  \+ (
	      %% with lattice, we may have final items for various values of step
	      %% we keep the best one
	      beam!enumerate(__MaxStep,_BetterItem::item{ right => N, stack => tree(0,[],_,_,_,_,_), stack1 => [], lk1 => 0, prefix => _BetterCost, swap => [] }),
	      _BetterCost > Cost,
	      %% format('\tbetter item ~w\n',[_BetterItem]),
	      true
	  )
	->
	  format('## ~w cost=~w inside=~w step=~w\n',[SId,Cost,_Cost,_MaxStep]),
	  %% every((
	  %% 	       beam!enumerate(_,_BetterItem),
	  %% 	       _BetterCost > Cost,
	  %% 	       format('** could have chosen better cost ~w ~w\n',[_BetterCost,_BetterItem]),
	  %% 	       true
	  %%      )),
	  every((info!comment(Comment),
		 format('##. ~w\n',[Comment])
		)),
	  (recorded(FinalItem,FinalItemAddr) xor fail), % strange, but sometines several identical final items
	  recorded(InitItem,InitItemAddr),
	  item!tree(FinalItemAddr,0),
	  every(( tree!display(0) )),
	  format('\n',[]),
	  every(( recorded(avgweight(_Avg,_AllCost,_AllStep),_AvgAddr) ->
		      _NewAllCost is _AllCost+abs(Cost),
		      _NewAllStep is _AllStep+_MaxStep,
		      _NewAvg is (_NewAllCost / _NewAllStep),
%		     format('new avg is ~w allcost=~w allstep=~w\n',[_NewAvg,_NewAllCost,_NewAllStep]),
		     %delete_address(_AvgAddr),
		      erase(avgweight(_,_,_)),
		      record(avgweight(_NewAvg,_NewAllCost,_NewAllStep)),
		      true
		  ;
		  fail
	       ))
	  ; (tagset!pop0 xor tagset!reduce),
	    beam!best( MaxStep,
		       PartialBestItem::item{}
		     ),
	    %% we have reached the maximum number of allowed steps with no final item
	    %% we tried to complete some partial item using pop0 and/or reduce steps
	    verbose('try complete with pop0 ations maxstep=~w best=~w\n',[MaxStep,PartialBestItem]),
	    record_without_doublon(complete_mode),
	    complete_item(PartialBestItem,_FinalItem::item{ prefix => Cost, inside => _Cost })
	    ->
	    format('## ~w partial cost=~w inside=~w step=~w\n',[SId,Cost,_Cost,_MaxStep]),
	    recorded(_FinalItem,_FinalItemAddr),
	    recorded(InitItem,InitItemAddr),
	    item!tree(_FinalItemAddr,0),
	    every(( tree!display(0) )),
	    format('\n',[])
	  ; recorded(__FinalItem::item{ step => _MaxStep, prefix => Cost, inside => _Cost },__FinalItemAddr),
	    format('test potential final item ~w\n',[__FinalItem]),
	    item!terminal(N,__FinalItem) ->
	    format('## ~w terminal cost=~w inside=~w step=~w\n',[SId,Cost,_Cost,_MaxStep]),
	    item!tree(__FinalItemAddr,0),
	    recorded(InitItem,InitItemAddr),
	    every(( tree!display(0) )),
	    format('\n',[])
	  ;
	format('## ~w failure\n',[SId,Cost,_Cost]),
	every((info!comment(Comment),
	       format('##. ~w\n',[Comment])
	      )),
	item!failure_tree(0,0),
	every(( tree!display(0) )),
	format('\n',[]),
	true
	),
	once_xor_true(( opt(train:_),
			\+ opt(nodynet),
			dynet!apply_updates )),
	persistent!add_fact(avgweight(_,_,_)),
	show_time(parsing),
	value_counter(checking,Checks),
	value_counter(adding,Adding),
	value_counter(erasing,Erasing),
	value_counter(updates,Updates),
	value_counter(updates1,Updates1),
	value_counter(updates2,Updates2),
	value_counter(updates2_violation,Updates2_Violation),
	value_counter(updates3,Updates3),
	value_counter(updates4,Updates4),
	value_counter(updates6,Updates6),
	format('stats checking=~w adding=~w erasing=~w updates=~w u1=~w u2=~w u3=~w u4=~w u6=~w u2v=~w\n',[Checks,Adding,Erasing,Updates,Updates1,Updates2,Updates3,Updates4,Updates6,Updates2_Violation]),
	fail
	.


:-std_prolog parse_options/1.

parse_options(Options) :-
        ( Options == [] -> true
	;
	  ( Options = ['-verbose'|Rest] -> record_without_doublon( opt(verbose) )
	  ; Options = ['-multi'|Rest] -> record_without_doublon( opt(multi) )
	  ; Options = ['-res',File|Rest] -> read_dbfile(File)
	  ; Options = ['-train',Mode::train[]|Rest] ->
	    %% Training modes in {early,late,best}
	    erase(opt(train:_)),
	    record_without_doublon(opt(train:Mode))
	  ; Options = ['-train'|Rest] ->
	    %% default training mode
	    erase(opt(train:_)),
	    record_without_doublon(opt(train:early))
	  ; Options = ['-load_model',Model|Rest] ->
	    record_without_doublon(opt(load_model(Model))),
	    load_model(Model),
%	    load_avgweight,
	    true
	  ; Options = ['-save_model',Model|Rest] ->
	    %% -save_model option implies -train option
	    record_without_doublon(opt(save_model(Model))),
	    %% record_without_doublon(opt(train)),
	    true
	  ; Options = ['-beam',XN|Rest] ->
	    atom_number(XN,N),
	    record_without_doublon(beam(N))
	  ; Options = ['-sbeam',XN|Rest] ->
	    atom_number(XN,N),
	    record_without_doublon(sbeam(N))
	  ; Options = ['-mode',Mode|Rest] ->
	    %% alias for -train
	    erase(opt(train:_)),
	    record_without_doublon(opt(train:Mode))
	  ; Options = ['-mstag'|Rest] ->
		record_without_doublon(opt(mstag))
	  ; Options = ['-maxstep',XN|Rest] ->
		atom_number(XN,N),
		record_without_doublon(maxstep_factor(N))
	  ; Options = ['-check'|Rest] ->
		%% check wrt oracle, but don't train
	    record_without_doublon(opt(check))
	  ; Options = ['-mixed_train',Kind|Rest] -> % Kind in sr,nn,both
	    erase(tagset!mixed_train(_)),
	    record_without_doublon(tagset!mixed_train(Kind))
	  ; Options = ['-allow_swap_on_shift'|Rest] ->
	    record_without_doublon(tagset!allow_swap_on_shift)
	  ; Options = ['-dyn_oracle'|Rest] ->
	    %% use dynamic oracle
	    record_without_doublon(opt(dyn_oracle))
	  ; Options = ['-embed',File|Rest] ->
	    record_without_doublon(opt(embed(File))),
	    dynet!register_embedding(File),
	    true
	  ; Options = ['-epoch',XN|Rest] ->
	    atom_number(XN,N),
	    record_without_doublon(dynet!epoch(N))
	  ; Options = ['-dynet_lstm_layers',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!lstm_layers(_)),
	    record_without_doublon(dynet!lstm_layers(N))
	  ; Options = ['-dynet_char_lstm_layers',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!char_lstm_layers(_)),
	    record_without_doublon(dynet!char_lstm_layers(N))
	  ; Options = ['-dynet_lstm_dim',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!lstm_dim(_)),
	    record_without_doublon(dynet!lstm_dim(N))
	  ; Options = ['-dynet_char_dim',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!char_dim(_)),
	    record_without_doublon(dynet!char_dim(N))
	  ; Options = ['-dynet_char_lstm_dim',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!char_lstm_dim(_)),
	    record_without_doublon(dynet!char_lstm_dim(N))
	  ; Options = ['-dynet_tag_dim',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!tag_dim(_)),
	    record_without_doublon(dynet!tag_dim(N))
	  ; Options = ['-dynet_label_dim',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!label_dim(_)),
	    record_without_doublon(dynet!label_dim(N))
	  ; Options = ['-dynet_state_dim',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!state_dim(_)),
	    record_without_doublon(dynet!state_dim(N))
	  ; Options = ['-dynet_weight_coef',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!weight_coef(_)),
	    record_without_doublon(dynet!weight_coef(N))
	  ; Options = ['-dynet_cx',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!weight_cx(_)),
	    record_without_doublon(dynet!weight_cx(N))
	  ; Options = ['-dynet_dropout',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!dropout(_)),
	    record_without_doublon(dynet!dropout(N))
	  ; Options = ['-dynet_state_words',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!state_words(_)),
	    record_without_doublon(dynet!state_words(N))
	  ; Options = ['-dynet_hinge_loss'|Rest] -> % deprecated
	    record_without_doublon(dynet!hinge_loss)
	  ; Options = ['-train_on_gold_mstag',MSTag|Rest] ->
	    (recorded(tagset!train_on_gold_mstag(MSTag))
	     xor
	     record(tagset!train_on_gold_mstag(MSTag)),
	     dynet!register_mstag_vocab(MSTag,_)
	    )
	  ; Options = ['-dynet_freeze_cf'|Rest] ->
	    record_without_doublon(dynet!freeze_cf)
	  ; Options = ['-nodynet'|Rest] ->
	    record_without_doublon(opt(nodynet))
	  ; Options = ['-dynet-only-tags'|Rest] ->
	    %% only use it as a tagger (not yet implemented)
	    record_without_doublon(opt(dynet_only_tags))
	  ; Options = ['-dynet-only'|Rest] ->
	    %% only use dynet info during the first epoch (with beam 1)
	    record_without_doublon(opt(dynet_only))
	  ; Options = ['-dynet-stop'|Rest] ->
	    record_without_doublon(opt(dynet_stop))
	  ; Options = ['-mst'|Rest] ->
	    record_without_doublon(opt(mst))
	  ; Options = ['-mst_emit'|Rest] ->
	    record_without_doublon(opt(mst_emit))
	  ; Options = ['-dynet_label_hdim',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!label_hdim(_)),
	    record_without_doublon(dynet!label_hdim(N))
	  ; Options = ['-dynet_edge_hdim',XN|Rest] ->
	    atom_number(XN,N),
	    erase(dynet!edge_hdim(_)),
	    record_without_doublon(dynet!edge_hdim(N))
	  ; Options = [ParserOption|Rest] ->
	    true
	  ;  fail
	  ),
	  parse_options(Rest)
        )
.

:-xcompiler
register_simple_action(MSimple,Action) :-
    atom_module(Directive,tagset,Action),
    recorded(Directive),
    mutable_inc(MSimple,_Action),
    verbose('register ~w action ~w\n',[Action,_Action]),
    record(tagset!action2index(Action,0,0,_Action,0)),
%    record(tagset!action2index(Action,0,_Action,_Action,0)),
%    record(tagset!index2action(0,Action,0)),
    record(tagset!index2action(_Action,Action,0)),
    record(tagset!a2i(Action,0,_Action)),
    record(tagset!i2a(_Action,Action,0)),
    record(tagset!simple_action(Action)),
    true
.
	       

:-xcompiler
process_options :-	
	( recorded(parsed_options)
	xor
	argv(Options),
	  parse_options(Options),
	  dynet!init,
	  format('parsed options\n',[]),
	  (beam(Beam) xor record(beam(1)), Beam=1),
	  %% SBeam = selection beam indicates the max number of actions to consider from one item
	  %% by default, its value is Beam
	  %% but we can use SBeam to simulate a kind of Selectional Branching (see Choi & McCallum)
	  (sbeam(SBeam) xor SBeam = Beam, record(sbeam(SBeam))),
	  (SBeam > Beam -> XBeam = Beam ; XBeam = SBeam ),
	  (maxstep_factor(_) -> true
	   ; recorded(tagset!maxstep(MaxStepFactor)) -> record(maxstep_factor(MaxStepFactor))
	   ; record(maxstep_factor(2))
	  ),
	  generate_templates,
	  every((tagset!projective,
		 %% keep a very simple set of transitions for projective treebanks
		 erase(tagset!swap),
		 erase(tagset!swap2),
		 erase(tagset!swap3),
		 erase(tagset!noop),
		 erase(tagset!pop0),
		 erase(tagset!pop1),
		 erase(tagset!attach),
		 erase(tagset!reduce2),
		 record_without_doublon(tagset!ensure_tree),
		 record_without_doublon(tagset!single_root)
		)),
	  value_counter(labels,NLabels),
	  record(tagset!nlabels(NLabels)),
	  %% registering simple actions 
	  mutable(MSimple,1,true),
	  %% 0=shift
	  record(tagset!action2index(shift,0,0,0,0)),
	  dynet!a2i(shift,_),
	  record(tagset!index2action(0,shift,0)),
	  record(tagset!a2i(shift,0,0)),
	  record(tagset!i2a(0,shift,0)),
	  every(( domain(SimpleAction,simple_action[pop0,pop1,swap,swap2,swap3,noop]),		  
		  register_simple_action(MSimple,SimpleAction),
		  dynet!a2i(SimpleAction,_)
		)),
	  mutable_read(MSimple,NSimple),
	  mutable(MComplex,2,true),
	  %% reduce actions
	  LeftBase = NSimple,
	  RightBase is NSimple + 1,
	  record(tagset!actionbase2index(left,LeftBase)),
	  dynet!a2i(left,_),
	  record(tagset!actionbase2index(right,RightBase)),
	  dynet!a2i(right,_),
	  every(( tagset!label2index(Label,I),
		  LI is NSimple + I -1,
		  RI is LI + NLabels,
		  record(tagset!action2index(left,Label,LI,LeftBase,I)),
		  record(tagset!index2action(LI,left,Label)),
		  record(tagset!action2index(right,Label,RI,RightBase,I)),
		  record(tagset!index2action(RI,right,Label)),
		  record(tagset!a2i(left,Label,LI)),
		  record(tagset!i2a(LI,left,Label)),
		  record(tagset!a2i(right,Label,RI)),
		  record(tagset!i2a(RI,right,Label)),
		  every((domain(_Action,[shift,reduce_right,potential_reduce_right,reduce_left]),
                         name_builder('~w_~w',[_Action,Label],_GuidingAction),
			 record(guide!xaction(_Action,Label,_GuidingAction)),
			 true
		       )),
		  true
		)),
	  ( tagset!attach ->
		mutable_add(MComplex,2),
		LeftABase is NSimple + 2,
		RightABase is NSimple + 3,
		record(tagset!actionbase2index(lefta,LeftABase)),
		dynet!a2i(lefta,_),
		record(tagset!actionbase2index(righta,RightABase)),
		dynet!a2i(righta,_),		
	        every(( tagset!label2index(Label,I),
			LI is NSimple + 2*NLabels + I -1,
			RI is LI + NLabels,
			record(tagset!action2index(lefta,Label,LI,LeftABase,I)),
			record(tagset!index2action(LI,lefta,Label)),
			record(tagset!action2index(righta,Label,RI,RightABase,I)),
			record(tagset!index2action(RI,righta,Label)),
			
			record(tagset!a2i(lefta,Label,LI)),
			record(tagset!i2a(LI,lefta,Label)),
			record(tagset!a2i(righta,Label,RI)),
			record(tagset!i2a(RI,righta,Label)),

			true
		     ))
	   ; 
	   record_without_doublon(tagset!reduce),
	   true
	  ),
	  ( tagset!reduce2 ->
		mutable_add(MComplex,2),
		mutable_read(MComplex,_NComplex),
		LeftBase2 is NSimple + _NComplex - 2,
		RightBase2 is LeftBase2 + 1,
		record(tagset!actionbase2index(left2,LeftBase2)),
		dynet!a2i(left2,_),
		record(tagset!actionbase2index(right2,RightBase2)),
		dynet!a2i(right2,_),
	        every(( tagset!label2index(Label,I),
			%			LI is NSimple + LeftBase2*NLabels + I -1,
			LI is NSimple + (_NComplex-2)*NLabels + I -1,
			RI is LI + NLabels,
			record(tagset!action2index(left2,Label,LI,LeftBase2,I)),
			record(tagset!index2action(LI,left2,Label)),
			record(tagset!action2index(right2,Label,RI,RightBase2,I)),
			record(tagset!index2action(RI,right2,Label)),

			record(tagset!a2i(left2,Label,LI)),
			record(tagset!i2a(LI,left2,Label)),
			record(tagset!a2i(right2,Label,RI)),
			record(tagset!i2a(RI,right2,Label)),

			true
		      
		     ))
	   ; 
	   true
	  ),
	  mutable_read(MComplex,NComplex),
	  MaxActions is NComplex*NLabels+NSimple,
	  '$interface'(set_max_action(MaxActions:int, NLabels:int, NSimple:int, NComplex: int, XBeam: int),[return(none)]),
	  record(tagset!label2index(0,0)),
/*
	  every(( _A::tagset!action2index(_Action,_Label,_LI,_Base,_I),
	   	  format('registered action2index ~w\n',[_A])
	        )),
	  every(( _B::tagset!index2action(_,_,_),
	   	  format('registered index2action ~w\n',[_B])
	        )),

	  every((      term_range(0,MaxActions,_TR),
		       domain(_I,_TR),
		       tagset!i2a(_I,_Action,_L),
		       format('registered i2a ~w ~w ~w\n',[_Action,_L,_I]),
		       tagset!a2i(_Action,_L,_I),
		       format('registered a2i ~w ~w ~w\n',[_Action,_L,_I]),
		       true
		   )),
*/
	  format('maxaction is actions=~w labels=~w\n',[MaxActions,NLabels]),

	  YBeam is NSimple + NComplex * (XBeam + 1),
	  record(tagset!parameters(MaxActions,NLabels,NSimple,NComplex,XBeam,YBeam)),
	  
	  every((%tagset!train_on_gold_mstag(MStag),
		 %dynet!register_mstag_vocab(MStag,_),
		 dynet!mstag_vocab(MSTag,_),
		 recorded(tagset!mstag(MSTag,Value)),
		 dynet!mstag(MSTag,Value,_)
		)),
	  
	  (recorded(avgweight(AvgWeight)) xor AvgWeight=700),
	  record(avgweight(AvgWeight,0,0)),
	  dynet!add_parameters(MaxActions,NSimple,NComplex),
	  every((
		       opt(load_model(Model)),
		       dynet!load_model(Model)
		   )),
	  every((
		       opt(train:_),
		       dynet!update_epoch
		   )),
	  record( parsed_options)
	),
	%%	format('adding persistent\n',[]),
	persistent!add(parsed_options),
	%% the following avoid reparsing options at each loop iteration
	%% using persistent facts
	persistent!add_fact(opt(_)),
	persistent!add_fact(beam(_)),
	persistent!add_fact(sbeam(_)),
	persistent!add_fact(tagset!label(_)),
	persistent!add_fact(tagset!label2index(_,_)),
	persistent!add_fact(tagset!nlabels(_)),
	persistent!add_fact(tagset!action2index(_,_,_,_,_)),
	persistent!add_fact(tagset!index2action(_,_,_)),
	persistent!add_fact(tagset!simple_label(_,_)),
	persistent!add_fact(tagset!simple_action(_)),

	persistent!add_fact(tagset!parameters(_,_,_,_,_,_)),
	
	persistent!add_fact(tagset!a2i(_,_,_)),
	persistent!add_fact(tagset!i2a(_,_,_)),
	
	persistent!add_fact(tagset!block(_,_,_)),
	persistent!add_fact(templates(_,_)),
	persistent!add_fact(guide!xaction(_,_,_)),
	persistent!add_fact(tagset!pop0),
	persistent!add_fact(tagset!pop1),
	persistent!add_fact(tagset!swap),
	persistent!add_fact(tagset!noop),
	persistent!add_fact(tagset!swap2),
	persistent!add_fact(tagset!swap3),
	persistent!add_fact(tagset!allow_swap_on_shift),
	persistent!add_fact(tagset!mixed_train(_)),
	persistent!add_fact(tagset!attach),
	persistent!add_fact(tagset!reduce),
	persistent!add_fact(tagset!reduce2),
	persistent!add_fact(tagset!ensure_tree),
	persistent!add_fact(tagset!late_attach),
	persistent!add_fact(tagset!allow_cycle),
	persistent!add_fact(tagset!single_root),
	persistent!add_fact(tagset!projective),
	persistent!add_fact(tagset!actionbase2index(_,_)),
	persistent!add_fact(maxstep_factor(_)),

	persistent!add_fact(tagset!train_on_gold_mstag(_)),
	persistent!add_fact(dynet!mstag_vocab(_,_)),
	
	persistent!add_fact(label2dynet(_,_)),
	persistent!add_fact(action2dynet(_,_)),
	persistent!add_fact(dynet2label(_,_)),
	persistent!add_fact(dynet2action(_,_)),
	persistent!add_fact(dynet!state_words(_)),
	persistent!add_fact(dynet!weight_cx(_)),
	persistent!add_fact(avgweight(_)),
	persistent!add_fact(dynet!epoch(_)),
%	persistent!add_fact(avgweight(_,_,_)),
	true
	.

:-std_prolog read_dbfile/1.
read_dbfile(File) :-
	open(File,read,S),
	repeat(( read_term(S,T,_),
		 ( T == eof
		 xor T=tagset!label(Label),
		   update_counter(labels,I),
		   J is I+1,
		   record(tagset!label2index(Label,J)),
		   dynet!l2i(Label,_),
		   fail
		 xor T=vocab(Form,Freq),
		   dynet!w2i(Form,Freq,_),
		   fail
		 xor T=tagset!cat(Cat,_),
		   \+ dynet!use_fullcat,
		   dynet!t2i(Cat,_),
		   fail
		 xor T=tagset!fullcat(FCat,_),
		   dynet!use_fullcat,
		   dynet!t2i(FCat,_),
		   fail
		 xor T=tagset!vocab(Vocab),
		   read_dbfile(Vocab),
		   fail
     		 xor T=tagset!embedding(Embed),
		   record_without_doublon(opt(embed(Embed))),
		   dynet!register_embedding(Embed),
		   fail
	         xor T=avgweight(V),
		   format('read db avgweight ~w\n',[V]),
		   record(T),
		   fail
		 xor T=tagset!train_on_gold_mstag(MSTag),
		   dynet!register_mstag_vocab(MSTag,_),
		   record(T),
		   fail
		 xor
		   record(T),
		   fail
		 )
	       )),
	close(S)
	.

:-std_prolog item!tree/2.

item!tree(ItemAddr,ExitAddr) :-
    ( ItemAddr = 0 ->
%	  format('reaching void addr\n',[]),
	  true
      ; recorded(item{ step => 0 }, ItemAddr) ->
	 true
     ;
     safe_recorded( Item::item{ step => S, stack => Stack0::tree(Addr,_,_,_,_,_,_), stack1 => Stack1 }, ItemAddr),
     ( item!back(ItemAddr,K::shift(_,_,_),ExitAddr,ItemAddr1,W,_) ->
	   ItemAddr0 = ExitAddr,
	   Action = K,
	   true
       ; item!acceptable_back(ItemAddr,Action,ItemAddr0,ItemAddr1,W,ExitAddr),
	 \+ ( item!acceptable_back(ItemAddr,_Action,_ItemAddr0,_ItemAddr1,_W,ExitAddr),
	      _W > W
	    ) ->
	     true
       ; ExitAddr = 0 ->
%	     format('** pb reaching void addr\n',[]),
	     fail
       ;
       safe_recorded(item{ step => SExit },ExitAddr),
       format('*** pb item!tree step=~w item=~w exit=~w exit_step=~w\n',[S,ItemAddr,ExitAddr,SExit]),
       every(( _B::item!back(ItemAddr,_,_,_,_,_),
	       format('\tback ~w\n',[_B])
	    )),
       fail
     ),
     verbose('back process step=~w exit=~w addr=~w action=~w addr0=~w addr1=~w\n',[S,ExitAddr,ItemAddr,Action,ItemAddr0,ItemAddr1]),
     ( Action = left(Label) ->
	   safe_recorded( item{ stack => tree(Addr00,_,_,_,_,_,_) }, ItemAddr0),
	   record_without_doublon(result!dep(Addr00,Addr,Label)),
	   item!tree(ItemAddr1,ExitAddr),
	   item!tree(ItemAddr0,ItemAddr1)
       ; Action = right(Label) ->
	     safe_recorded( item{ stack1 => tree(Addr01,_,_,_,_,_,_) }, ItemAddr0),
	     record_without_doublon(result!dep(Addr01,Addr,Label)),
	     item!tree(ItemAddr1,ExitAddr),
	     item!tree(ItemAddr0,ItemAddr1)
       ; Action = lefta(Label) ->
	     Item = item{ stack1 => tree(Addr1,_,_,_,_,_,_) },
	     record_without_doublon(result!dep(Addr,Addr1,Label)),
	     item!tree(ItemAddr0,ExitAddr)
       ; Action = righta(Label) ->
	     Item = item{ stack1 => tree(Addr1,_,_,_,_,_,_) },
	     record_without_doublon(result!dep(Addr1,Addr,Label)),
	     item!tree(ItemAddr0,ExitAddr)
       ; Action = left2(Label) ->
	     Item = item{ stack1 => tree(Gov,_,_,_,_,_,_) },
	     safe_recorded( item{ stack => tree(Dep,_,_,_,_,_,_) }, ItemAddr0),
	     record_without_doublon(result!dep(Dep,Gov,Label)),
	     item!tree(ItemAddr1,ExitAddr),
	     item!tree(ItemAddr0,ItemAddr1)
       ; Action = right2(Label) ->
	     Item = item{ stack => tree(Gov,_,_,_,_,_,_) },
	     safe_recorded( item{ stack1 => tree(Dep,_,_,_,_,_,_) }, ItemAddr1),
	     record_without_doublon(result!dep(Dep,Gov,Label)),
	     item!tree(ItemAddr1,ExitAddr),
	     item!tree(ItemAddr0,ItemAddr1)
       ; Action=shift(_,_,_) ->
	     recorded( 'C'(Left,_,_,Right) , Addr ),
	     record_without_doublon(result!root(Left,Addr,Right)),
	     true
       ; Action=action[pop0,pop1] ->
	     item!tree(ItemAddr1,ExitAddr),
	     item!tree(ItemAddr0,ItemAddr1)
       ; Action=action[swap,noop,swap2,swap3] ->
	     item!tree(ItemAddr0,ExitAddr)
       ;
       %% should not be here !
       fail
    ),

     ((Action = lefta(_) xor Action = righta(_)) ->
	  recorded( 'C'(_,_,lemma{ cat => Cat0 },_), Addr ),
	  ( Stack1 = tree(Addr1,_,_,_,_,_,_), 
            \+ Addr1 = 0 ->
	       recorded( 'C'(_,_,lemma{ cat => Cat1 },_), Addr1 )
	   ; Cat1 = 0
	  ),
	  Info = [Cat0,Cat1]
      ;
      Info = []
     ),
     safe_recorded(item{ prefix => PW0}, ItemAddr0),
     Delta is W - PW0,
     format('%% step ~w action ~w info ~w cost=~w delta=~w\n',[S,Action,Info,W,Delta]),

     true
    )
.

:-std_prolog item!failure_tree/2.

item!failure_tree(Left,Gov) :-
    ('N'(Left) ->
	 true
    ; recorded('C'(Left,_,_,Right), Addr) ->
      (Gov = 0 -> Label = root ; Label = missinghead),
      record(result!root(Left,Addr,Right)),
      record(result!dep(Addr,Gov,Label)),
      item!failure_tree(Right,Addr)
    ;
    fail
    )
.
    

:-extensional result!root/3.
:-extensional result!dep/3.
:-std_prolog tree!display/1.

tree!display(Left) :-
    result!root(Left,Addr,Right),
    recorded( 'C'(Left,Id,lemma{ lex => Lex, lemma => Lemma, fullcat => FCat, cat => Cat, mstag => FS },Right) , Addr ),
    XLeft is Left+1,
    (result!dep(Addr,_,_) xor record(result!dep(Addr,'',''))),
    every((info!contracted(XLeft,XRight,CForm),
	   format('~w-~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,XRight,CForm,'_','_','_','_','_','_','_'] )
	  )),
    every((info!ellipse(XLeft,Beta,CForm),
	   format('~w.~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,Beta,CForm,'_','_','_','_','_','_','_'] )
	  )),
    every(( (tagset!ensure_tree ->
		 %% should not need to check at this point !
		 %% but safety to get onlye one governor
		 (result!dep(Addr,GovAddr,Label) xor true)
	    ;
	    result!dep(Addr,GovAddr,Label)
	    ),
%	    format('** process dep ~w ~w ~w\n',[Addr,GovAddr,Label]),
	    ( GovAddr = 0 ->
		  Gov = 0
	      ; GovAddr = '' ->
		    Gov = ''
	      ;
	      recorded( 'C'(GLeft,_,_,_) , GovAddr ),
	      Gov is GLeft+1
	    ),
	    (opt(mstag) ->
		 (FS = [_|_] -> XFS = FS ; XFS = [FS]),
		 (Left=0,
		  recorded(sentence_id(SentID)) ->
		      XFS2=[SentID|XFS]
		  ;
		  XFS2 = XFS
		 ),
		 format('~w\t~w\t~w\t~w\t~w\t~L\t~w\t~w\t~w\n', [XLeft,Lex,Lemma,Cat,FCat,['~w','|'],XFS2,Gov,Label,Id] )
	     ;
	     format('~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,Lex,Lemma,Cat,FCat,'_',Gov,Label,Id] )
	    )
	 )),
    tree!display(Right)
.

:-std_prolog dynet!display,
  dynet!display/1
  .

dynet!display :-
    ('S'(SId) xor SId='E1'),
    format('## ~w\n',[SId]),
    every((info!comment(Comment),
	   format('##. ~w\n',[Comment])
	  )),
    dynet!display(0),
    format('\n',[]),
    true
.

dynet!display(Left) :-
    (recorded('C'(Left,Id,lemma{ lex => Lex, lemma => Lemma, fullcat => FCat, cat => Cat, mstag => FS },Right), Addr) ->
	 dynet!head(Id,Gov,LabelId),
	 dynet2label(LabelId,Label),
	 XLeft is Left+1,
	 (result!dep(Addr,_,_) xor record(result!dep(Addr,'',''))),
	 every((info!contracted(XLeft,XRight,CForm),
		format('~w-~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,XRight,CForm,'_','_','_','_','_','_','_'] )
	       )),
	 every((info!ellipse(XLeft,Beta,CForm),
		format('~w.~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,Beta,CForm,'_','_','_','_','_','_','_'] )
	       )),
	 (opt(mstag) ->
	      (FS = [_|_] -> XFS = FS ; XFS = [FS]),
	      (Left=0,
	       recorded(sentence_id(SentID)) ->
		   XFS2=[SentID|XFS]
	      ;
	      XFS2 = XFS
	      ),
	      format('~w\t~w\t~w\t~w\t~w\t~L\t~w\t~w\t~w\n', [XLeft,Lex,Lemma,Cat,FCat,['~w','|'],XFS2,Gov,Label,Id] )
	 ;
	 format('~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,Lex,Lemma,Cat,FCat,'_',Gov,Label,Id] )
	 ),
	 dynet!display(Right)
    ;
    true
    )
.


:-std_prolog mst!display,
  mst!display/1
  .

mst!display :-
    ('S'(SId) xor SId='E1'),
    format('## ~w\n',[SId]),
    every((info!comment(Comment),
	   format('##. ~w\n',[Comment])
	  )),
    mst!display(0),
    format('\n',[]),
    true
.

mst!display(Left) :-
    (recorded('C'(Left,Id,lemma{ lex => Lex, lemma => Lemma, fullcat => FCat, cat => Cat, mstag => FS },Right), Addr) ->
	 mst!in(Id,Gov,_,_),
	 (dynet!head(Id,Gov,LabelId) ->
	      dynet2label(LabelId,Label)
	 ;
	 Label = 'missinghead'
	 ),
	 XLeft is Left+1,
	 (result!dep(Addr,_,_) xor record(result!dep(Addr,'',''))),
	 every((info!contracted(XLeft,XRight,CForm),
		format('~w-~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,XRight,CForm,'_','_','_','_','_','_','_'] )
	       )),
	 every((info!ellipse(XLeft,Beta,CForm),
		format('~w.~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,Beta,CForm,'_','_','_','_','_','_','_'] )
	       )),
	 (opt(mstag) ->
	      (FS = [_|_] -> XFS = FS ; XFS = [FS]),
	      (Left=0,
	       recorded(sentence_id(SentID)) ->
		   XFS2=[SentID|XFS]
	      ;
	      XFS2 = XFS
	      ),
	      format('~w\t~w\t~w\t~w\t~w\t~L\t~w\t~w\t~w\n', [XLeft,Lex,Lemma,Cat,FCat,['~w','|'],XFS2,Gov,Label,Id] )
	 ;
	 format('~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n', [XLeft,Lex,Lemma,Cat,FCat,'_',Gov,Label,Id] )
	 ),
	 mst!display(Right)
    ;
    true
    )
.


    


%:-dyalog_ender(table_analysis).

:-std_prolog table_analysis/0.
%:-require 'forest.pl'.

table_analysis :-
    format('table analysis\n',[]),
    mutable(MTable,0,true),
    every(( recorded( O ),
%%	    format('recorded ~w\n',[O]),
	    mutable_inc(MTable,_)
	  )),
    mutable_read(MTable,NObj),
    '$interface'('Dyalog_Smb_Number'(),[return(NSymb:int)]),
    format('recorded ~w objects ~w symbols\n',[NObj,NSymb]),
    true
.
    
