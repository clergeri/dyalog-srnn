#!/usr/bin/env perl

use strict;
use AppConfig qw/:argcount :expand/;
use Text::Scan;
use List::Util qw{max min};
use Text::LevenshteinXS qw(distance);
use PDL::Lite;
use PDL::Primitive;
#use Math::Big;

my $config = AppConfig->new(
			    "oracle!" => {DEFAULT => 0},
			    "lattice!" => {DEFAULT => 0},
			    "graph!" => {DEFAULT => 0},
			    "proj!" => {DEFAULT => 0},
			    "tagset=f",
			    "mode=s" => {DEFAULT => 'gold'},
			    "oracle_seg=s",
			    "pos_oracle!", # last column is GOLD POS
			    "random_skip=d" => {DEFAULT => 0 },
			    "oracle_emit!" => {DEFAULT => 1}, # possibility to emit or not some parts of the oracle info
			    "language=s",
			    ## the following options can also occur in tagset file
			    "vocab=f", # a filtered list of Word Embeddings for a given vocab
			    "we=f", # the full list of WE
			   );

$config->args();

my $oracle = $config->oracle();
my $lattice = $config->lattice();
my $graph = $config->graph();	# sagae style data
my $proj = $config->proj();	# projective or not
my $late_attach=0;
my $tagset = $config->tagset();
my $mode = $config->mode();
my $skip = $config->random_skip();
my $language = $config->language();
my $loss = 0;			# for oracle: edges that are erased because they do not fit in the oracle
my $noop = 0;
my $swap2 = 0;
my $swap3 = 0;
my $attach = 0;
my $reduce = 0;
my $reduce2 = 0;
my $pop2 = 0;
my $swap = 0;
my $pop0 = 0;
my $pop1 = 0;
my $single_root = 0;

my $vocab = {}; 		# word embeddings for a vocab
my $we = {};			# all we
my $we_approx = {};		# cache approximate word

my $pos_oracle = $config->pos_oracle;

my $oracle_seg = [];

if ($config->oracle_seg) {
  my $file = $config->oracle_seg;
  open(SEG,"<",$file) 
    || die "can open oracle_set $file: $!";
  my @sent = ();
  print "%% loading oracle seg $file\n";
  while (<SEG>) {
    # SEG should be in CONLL format with dependency information
    if (/^\s*$/) {
      push(@$oracle_seg,[@sent]);
      @sent = ();
      next;
    }
    chomp;
    push(@sent,[split(/\t+/,$_)]);
  }
  close(SEG);
}

my $mstag_fset=0;

my $lexer_info = {
		  xfullcat => []
		 };

if ($tagset && -f $tagset) {
  open(TAGSET,"<",$tagset) || die "can't read tagset $tagset: $!";
  my $print = 0;
  while(<TAGSET>) {
    if (/^:-finite_set/) {
      # deactivate finite set for mstag
      #      /mstag/ and $mstag_fset=1;
      $print = 1;
    }
    $print and print $_;
    if ($print and /\)\./) {
      print "\n";
      $print = 0;
    }
    if (/^tagset!attach\./) {
      ## activate graph mode !
      $graph = 1;
      $attach = 1;
    }
    if (/^tagset!reduce\./) {
      ## activate graph mode !
      $reduce = 1;
      $graph=1;
    }
    if (/^tagset!ensure_tree\./) {
#      $proj=1;
    }
    if (/^tagset!late_attach\./) {
      $late_attach=1;
    }
    if (0 && /^tagset!noop\./) {
      $noop=1;
    }
    if (/^tagset!reduce2\./) {
      $reduce2 = 1;
      $graph = 1;      
    }
    if (/^tagset!swap\./) {
      $swap = 1;
      $graph = 1;
    }
    if (/^tagset!pop0\./) {
      $pop0 = 1;
      $graph = 1;
    }
    if (/^tagset!pop1\./) {
      $pop1 = 1;
      $graph = 1;
    }
    if (/^tagset!pop2\./) {
      $pop2 = 1;
      $graph = 1;
    }
    if (/^tagset!swap2\./) {
      $swap2 = 1;
      $graph = 1;
    }
    if (/^tagset!swap3\./) {
      $swap3 = 1;
      $graph = 1;
    }

    if (/^tagset!single_root\./) {
      $single_root = 1;
    }

    if (s/^%%\s+lexer\s*:\s*//o) {
      chomp;
      # lexer directives
      if (s/^xfullcat\s*//o) {
	my @f = split(/\s+/,$_);
	$lexer_info->{xfullcat} = \@f;
	next;
      }

      if ($mode eq 'pred' && /^compounds\s+(\S+)/) {
	my $scan = $lexer_info->{compounds} = Text::Scan->new;
	$scan->ignorecase();
	$scan->restore($1);
	$lexer_info->{collect_sentence} = 1;
      }

      if ($mode eq 'pred' && /^dict\s+(\S+)/) {
	my $scan = $lexer_info->{dict} = Text::Scan->new;
	$scan->ignorecase();
	$scan->restore($1);
	$lexer_info->{collect_sentence} = 1;
      }

      if (/^subcat\s+(\S+)/) {
	my $scan = $lexer_info->{subcat} = Text::Scan->new;
	$scan->ignorecase();
	$scan->restore($1);
	$lexer_info->{collect_sentence} = 1;
      }

      if (s/^split\s*//o) {
	my @f = split(/\s+/,$_);
	$lexer_info->{split}{$_} = 1 foreach (@f);
      }

      if (s/^mstag_complete\s*//o) {
	$lexer_info->{mstag_complete} = 1;
      }

      if (s/^mstag_list\s*//o) {
	$lexer_info->{mstag_list} = 1;
      }

      if (s/^extra\s+(\S+)//o) {
	$lexer_info->{extra}{$1} = 1;
      }

      if (s/^mstag_filter\s+'(\S+)'//o) {
	$lexer_info->{mstag_filter}{$1} = 1;
      }

      if (s/^mstag_exclude_features\s+//o) {
	foreach my $f (split(/\s+/,$_)) {
	  $lexer_info->{mstag_exclude_feature}{$f} = 1;
	}
      }

      if (s/^cluster\s+(\S+)$//o) {
	my $cluster = $1;
#	print "loading cluster file $cluster\n";
	if (-f "$cluster.vals"  && -s "$cluster.vals") {
	    my $scan = $lexer_info->{cluster} = Text::Scan->new;
	    $scan->restore($cluster);
#	print "done loading\n";
	    $lexer_info->{collect_sentence} = 1;
	}
      }
      
      if (s/^align\s+(\S+)$//o) {
	$lexer_info->{align} = $1;
      }

      if (s/^delta_reverse\s+//o){
	foreach my $f (split(/\s+/,$_)) {
	  $lexer_info->{delta_reverse}{$f} = 1;
	}
      }

      if (s/^gold\s+//o) {
	# use gold dependencies as features, but forget some of them
	$lexer_info->{gold} = [split(/\s+/,$_)];
      }

      if (s/^fullcat_complete//o) {
	$lexer_info->{fullcat_complete} = 1;
      }

      if (s/^fullcat_add_cat//o) {
	$lexer_info->{fullcat_add_cat} = 1;
      }

      if (s/^overwrite_fullcat//o) {
	$lexer_info->{overwrite_fullcat} = 1;
      }

      if (s/^add_alt_cats\s+(\S+)//o) {
	my $distrib = $1;
	my $forms = {};
	if (-f $distrib) {
	  open(F,"<",$distrib)
	    or die "cannot process $distrib: $!";
	  while (<F>) {
	    chomp;
	    my @f = split(/\t/,$_);
	    $forms->{$f[0]}{$f[1]} = 1;
	  }
	  close(F);
	}
	$lexer_info->{add_alt_cats} = $forms;
      }

      if (s/^cluster_as_lemma//o) {
	$lexer_info->{cluster_as_lemma} = 1;
      }

      if (s/^alt_cats_in_lattice//o) {
	$lexer_info->{alt_cats_in_lattice} = 1;
      }

      if (s/^vocab\s+(\S+)//o) {
	my $file = $1;		# vocab list with Word Embeddings
	if (-f $file) {
	  open(V,'<',$file) or die "can't process vocab file $file: $!";
	  print "%% loading vocab: $file\n";
	  while (<V>) {
	    chomp;
	    my @f = split(/\s+/,$_);
	    my $w = shift @f;
	    scalar(@f) == 100 or next; # assume WE size is 100
	    $vocab->{$w} = norm pdl [@f];
	  }
	  close(V);
	}
      }

      if (s/^we\s+(\S+)//o) {
	my $file = $1;		# very large list of word Embeddings
	if (-f $file) {
	  if ($file =~ /\.xz$/) {
	    open(V,'-|',"xzcat $file") or die "can't process vocab file $file: $!";
	  } else {
	    open(V,'<',$file) or die "can't process vocab file $file: $!";
	  }
	  print "%% loading we: $file\n";
	  while (<V>) {
	    chomp;
	    my @f = split(/\s+/,$_);
	    scalar(@f) > 2 or next;
	    my $w = shift @f;
	    ## keep we only for words not in vocabulary
	    exists $vocab->{$w} and next;
	    scalar(@f) == 100 or next;
	    $we->{$w} = norm pdl [@f];
	  }
	  close(V);
	}
      }
      
      
    }
  }
  close(TAGSET);
}


my $sid=1;
my $sentid;
my $max=0;
my $tree={};
my $segmap = {};
my $root;
my $rootlabel;
my $count=0;
my @dag = ();
my $path = {};
my $maxlattice;
my $ids={};
my $memoactions={};

my $sinfo = {};

my $nfields;
my $oracle_id=0;

while(<>) {
  chomp;
  if (/^##\s*shuffle\s+(\d+)/) {
    $oracle_id = $1;
    next;
  }
  if (s/^#+\s*//) {
    # comments
    my $comment = quote($_);
    print <<EOF;
info!comment($comment).
EOF
    next;
  }
  if (/^\s*$/) {
    # white lines: sentence separators

    if ($skip && int(rand($skip))) {
      print "%% Skipping sentence E$sid\n\n";
      goto RESET;
    }

FLUSH:
    if (exists $lexer_info->{collect_sentence}) {
      my $sentence = '';
      my $altsentence = '';
      my $pos2elt = {};
      my $altpos2elt = {};
      foreach my $elt (@dag) {
	my ($left,$form) = @{$elt->{data}};
	$pos2elt->{length($sentence)} = $elt;
	$sentence .= "$form ";
	my $form2 = find_closest_word($form) || $form;
	$altpos2elt->{length($altsentence)} = $elt;
	$altsentence .= "$form2";
      }

      if (my $scanner = $lexer_info->{compounds}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($mwe,$pos,$cat) = @$occ;
	  my $elt = $pos2elt->{$pos};
	  my $left = $elt->{data}[0]-1;
	  ## print "%% found mwe at pos=$pos: <$mwe> $cat left=$left\n";
	  $elt->{data}[5] =~ /mwehead=/ or $elt->{data}[5] .= "|mwehead=${cat}+";
	  foreach my $w (split(/\s+/,$mwe)) {
	    my $elt = $pos2elt->{$pos};
	    $elt->{data}[5] =~ /pred=y/ or $elt->{data}[5] .= "|pred=y";
	    $pos += 1 + length($w);
	  }
	}
      }

      if (my $scanner = $lexer_info->{dict}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($dform,$pos,$cats) = @$occ;
	  my $elt = $pos2elt->{$pos};
	  my $form = quote($elt->{data}[1]);
	  #	print "%% found mwe at pos=$pos: <$mwe> $cat left=$left\n";
	  $cats = join(',',map {quote($_)} grep {$_} split(/\|/,$cats));
	  $elt->{dict} = $cats;
	}
      }

      if (my $scanner = $lexer_info->{subcat}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($dform,$pos,$subcat) = @$occ;
	  my $elt = $pos2elt->{$pos};
	  my $form = quote($elt->{data}[1]);
	  #	print "%% found mwe at pos=$pos: <$mwe> $cat left=$left\n";
##	  $subcat = join(',',map {quote($_)} grep {$_} split(/\|/,$subcat));
	  $elt->{subcat} = $subcat;
	}
      }

      if (my $scanner = $lexer_info->{cluster}) {
	foreach my $occ ($scanner->multiscan($sentence)) {
	  my ($dform,$pos,$cid) = @$occ;
##	  print "cluster $dform $cid in <$sentence>\n";
	  my $elt = $pos2elt->{$pos};
	  my $form = quote($elt->{data}[1]);
	  $elt->{cluster} = $cid;
	}
	foreach my $occ ($scanner->multiscan(lc($sentence))) {
	  my ($dform,$pos,$cid) = @$occ;
##	  print "cluster $dform $cid in <$sentence>\n";
	  my $elt = $pos2elt->{$pos};
	  my $form = quote($elt->{data}[1]);
	  $elt->{cluster} ||= $cid;
	}
	if ($sentence ne $altsentence) {
	  foreach my $occ ($scanner->multiscan($altsentence)) {
	    my ($dform,$pos,$cid) = @$occ;
	    ##	  print "cluster $dform $cid in <$sentence>\n";
	    my $elt = $altpos2elt->{$pos};
	    my $form = quote($elt->{data}[1]);
	    $elt->{cluster} ||= $cid;
	  }
	}
	
      }


    }

    if (keys %$path) {
      # lattice mode: check dead ends
      my $alive = {$maxlattice => 1};
      keep_alive(0,$path,$alive);
      @dag = grep {$alive->{$_->{right}}} @dag;
    }

    if (@$oracle_seg) {
      (defined $oracle_id) or $oracle_id = $sid-1;
##      my $current_seg = shift @$oracle_seg;
      my $current_seg = $oracle_seg->[$oracle_id];
      undef $oracle_id;
      foreach my $entry (@$current_seg) {
	if ($entry->[7] eq 'aux_pass') {
	  $current_seg->[$entry->[6]-1][5] .= "|diathesis=passive";
	} elsif ($entry->[7] eq 'aux_tps') {
	  $current_seg->[$entry->[6]-1][5] .= "|diathesis=active";
	}
      }
#      foreach my $e (@dag) {
#	my $d = $e->{data};
#	print "%% list cand: $d->[0] $d->[1] $d->[4]\n";
#      }
#      print "%% using oracle seg\n";
      my $seen = {};
      my @stack = ([1,0]);
      while (@stack) {
	my $info = shift @stack;
	my $current_pos = $info->[0];
	exists $seen->{$info->[0]}{$info->[1]} and next;
	$seen->{$info->[0]}{$info->[1]} = 1;
	my $current_tok = $current_seg->[$info->[1]];
	$current_tok or next;
	print "%% search candidates at pos $current_pos: oracle lex = $current_tok->[1] deplabel=$current_tok->[7]\n";
	my @candidates = grep {$_->{data}[0] == $current_pos} @dag;
	my $lex = $current_tok->[1];
	my $lemma = $current_tok->[2];
	($lemma eq '_') and $lemma = $lex;
	my $cat = $current_tok->[3];
	my $fullcat = $current_tok->[4];
#	($fullcat eq '_' && exists $lexer_info->{fullcat_complete}) and $fullcat = $cat;
	my $mstag = $current_tok->[5];
	my $deplabel = $current_tok->[7];
##	$lex =~ s/\@\S+$//o;					   # @suff is Hebrew data !
	foreach my $candidate (@candidates) {
##	  print "%% list2 cand: $candidate->{data}[0] $candidate->{data}[1] $candidate->{data}[4]\n";
	  my $cost = 1;
	  if ($lexer_info->{align} eq 'hebrew') {
	    ## print "%% Hebrew align\n";
	    ## align for Hebrew (no lemma, cat=fulltag)
	    $lex =~ s/\@\S+$//o;
	    ($lex eq $candidate->{data}[1]) or next;
	    if ($cat eq $candidate->{data}[3]
		&& $mstag eq $candidate->{data}[5]) {
	      $cost = 16;
	    } elsif ($cat eq $candidate->{data}[3]) {
	      $cost = 8;
	    } else {
	      $cost = 2;
	    }
	    $cost > 1 or next;
	  } else {
	    ## align for French (default)
	    my $d = distance($lex,$candidate->{data}[1]);
	    ($d < 4) or next;
	    my $cmstag = $candidate->{data}[5];
	    my $ccat = $candidate->{data}[4];
	    ($candidate->{data}[1] eq $lex) and $cost *= (1 + 1 / (1+$d)); #lex
	    $candidate->{data}[2] eq $current_tok->[2] and $cost *= 1.2; # lemma
	    $candidate->{data}[3] eq $current_tok->[3] and $cost *= 2; # cat
	    ($fullcat eq 'VINF' && $cmstag =~ /mode=infinitive/o) and $cost *= 1.2;
	    ($fullcat eq 'VIMP' && $cmstag =~ /mode=imperative/o) and $cost *= 1.2;
	    ($fullcat eq 'VPR' && $cmstag =~ /mode=gerundive/o) and $cost *= 1.2;
	    ($fullcat eq 'VPP' && $cmstag =~ /mode=participle/o) and $cost *= 1.2;
	    ($fullcat eq 'VS' && $cmstag =~ /mode=subjontive/o) and $cost *= 1.2;
	    ($fullcat eq 'CS' &&  $ccat eq 'csu') and $cost *= 1.2;
	    ($fullcat eq 'CC' &&  $ccat eq 'coo') and $cost *= 1.2;
	    ($fullcat eq 'PROREL' &&  $ccat eq 'prel') and $cost *= 1.2;
	    ($fullcat eq 'PROWH' &&  $ccat eq 'pri') and $cost *= 2;
	    ($cat eq 'PRO' &&  $deplabel eq 'suj' && $cmstag =~ /case=nom/o) and $cost *= 1.2;
	    ($cat eq 'PRO' &&  $deplabel eq 'obj' && $cmstag =~ /case=acc/o) and $cost *= 1.2;
	    ($cat eq 'CL' &&  $deplabel eq 'suj' && $cmstag =~ /case=nom/o) and $cost *= 1.2;
	    ($cat eq 'CL' &&  $deplabel eq 'obj' && $cmstag =~ /case=acc/o) and $cost *= 1.2;
	    ($lemma eq 'que' && $fullcat eq 'ADV' && $ccat eq 'que') and $cost *= 2;
	    ($mstag =~ /n=s/o && $cmstag =~ /number=sg/o) and $cost *= 1.2;
	    ($mstag =~ /n=p/o && $cmstag =~ /number=pl/o) and $cost *= 1.2;
	    ($mstag =~ /p=([123])/o && $cmstag =~ /number=$1/o) and $cost *= 1.2;
	    ($mstag =~ /g=m/o && $cmstag =~ /gender=masc/o) and $cost *= 1.2;
	    ($mstag =~ /g=f/o && $cmstag =~ /gender=fem/o) and $cost *= 1.2;
	    ($mstag =~ /m=ind/o && $cmstag =~ /mode=indicative/o) and $cost *= 1.2;
	    ($mstag =~ /t=cond/o && $cmstag =~ /mode=conditional/o) and $cost *= 1.2;
	    ($mstag =~ /t=pst/o && $cmstag =~ /tense=present/o) and $cost *= 1.2;
	    ($mstag =~ /diathesis=active/ && $cmstag =~ /diathesis=active/) and $cost *= 1.2;
	    ($mstag =~ /diathesis=passive/ && $cmstag =~ /diathesis=passive/) and $cost *= 1.2;
	    ($deplabel =~ /^aux_(tps|pass)/o && $ccat eq 'aux' ) and $cost *= 2;
	    # $candidate->{data}[4] eq $current_tok->[4] and $cost *= 2; # fullcat
	    # $candidate->{data}[5] eq $current_tok->[5] and $cost *= 2; # mstag
	  }
	  (!exists $candidate->{segcost} || $cost > $candidate->{segcost}) or next;
	  $candidate->{segcost} = $cost;
	  my $right = $info->[1];
	  $right < $maxlattice or next;
	  push(@stack,[$candidate->{right}+1,$right+1]);
	}
      }
      my $max = max map {find_best_path($_,\@dag)} grep {exists $_->{segcost}} @dag;
      print "%% best path max is $max\n";
      my @first = grep {($_->{data}[0] == 1) 
			  && exists $_->{segbest}
			    && $_->{segbest} == $max
			  } @dag;
      my $first = $first[0];
      while ($first && @$current_seg) {
	my $current_tok = shift @$current_seg;
	my $left = $first->{data}[0]-1;
	my $right = $first->{right};
	print "%% align $left $right $first->{data}[1] vs $current_tok->[1] $current_tok->[3] $current_tok->[4]: $first->{data}[3] $first->{data}[4] $first->{data}[5]\n";
	$first->{segalign} = $current_tok;
	exists $first->{segbest} or last;
	$first = $first->{segnext};
      }
      $first and print "%% *** bad align: missing gold tokens\n";
      @$current_seg and print "%% *** bad align: missing lattice tokens\n";
      ($first || @$current_seg) and print "oracle!skip_sentence.\n\n";
    }

    elt_process($_) foreach (@dag);

    my $maxleft = 0;
    foreach my $left (sort {$a <=> $b} keys %$sinfo) {
      $sinfo->{$left}{size} = $sinfo->{$left}{n} * ($sinfo->{$left+1}{n} || 1) * ($sinfo->{$left+2}{n} || 1);
      $maxleft = $left;
    }

    foreach my $step (0 .. 2 * $maxleft) {
      my $x = 1;
      my $k = int($step/2);
      my $n = 0;
      foreach my $i (($k-2) .. ($k+2)) {
	($i < 0 || $i > $maxleft || !exists $sinfo->{$i}) and next;
	$sinfo->{$i}{size} > $x and $x = $sinfo->{$i}{size};
	$n ++;
      }
      my $avg = int(log($x));
      $avg < 1 and $avg = 1;
      print <<EOF;
'B2'($step,$avg). %% k=$k n=$n x=$x
EOF
    }

    foreach my $left (sort {$a <=> $b} keys %$sinfo) {
      my $entry = $sinfo->{$left};
      my $qform = $entry->{form};
      $qform or next;
      my $size = int(log($entry->{size} || 1));
      $size ||= 1;
      my @cats = sort {$a cmp $b} keys %{$entry->{cat}};
      @cats > 1 and push(@cats,join("_",@cats));
      my $cats = join(',',map {quote($_)} @cats);
      print <<EOF;
'L'($left,$qform,[$cats]).
'B'($left,$size).
EOF
      if (exists $entry->{pred_gov}) {
	my $gov = $entry->{pred_gov};
	my $label = quote($entry->{label} || "none");
	push(@{$sinfo->{$gov}{dep}},$left);
	print <<EOF;
guide!head($left,$gov).
guide!label($left,$label).
EOF
      }

      if (exists $entry->{multi_pred_gov}) {
	foreach my $gov (keys %{$entry->{multi_pred_gov}}) {
	  foreach  my $label (keys %{$entry->{multi_pred_gov}{$gov}}) {
	    my $qlabel =  quote($label);
	    push(@{$sinfo->{$gov}{dep}},$left);
	    print <<EOF;
guide!head($left,$gov,$qlabel).
EOF
	  }
	}
      }
    }
    
      foreach my $left (sort {$a <=> $b}
			grep {exists $sinfo->{$_}{pred_gov}}
			keys %$sinfo) {
      my $entry = $sinfo->{$left};
      my $gov =  $sinfo->{$left}{pred_gov};
      $gov < $left or next;
      my $label = quote($sinfo->{$left}{label});
      my $rightmost = $left;
      while (exists $sinfo->{$rightmost}{dep}) {
	my $tmp = $sinfo->{$rightmost}{dep}[-1];
	$tmp > $rightmost or last;
	$rightmost = $tmp;
      }
      $rightmost++;
      print <<EOF;
guide!action($gov,$left,$rightmost,reduce_right,$label).
EOF
    }

    foreach my $left (sort {$a <=> $b} grep {exists $sinfo->{$_}{multi_pred_gov}} keys %$sinfo) {
      my $entry = $sinfo->{$left};
      foreach my $gov (keys %{$sinfo->{$left}{multi_pred_gov}}) {
	$gov < $left or next;
	foreach my $label (keys %{$sinfo->{$left}{multi_pred_gov}{$gov}}) {
	  my $qlabel = quote($label);
	  my $rightmost = $left;
	  while (exists $sinfo->{$rightmost}{dep}) {
	    my $tmp = $sinfo->{$rightmost}{dep}[-1];
	    $tmp > $rightmost or last;
	    $rightmost = $tmp;
	  }
	  $rightmost++;
	  print <<EOF;
guide!action($gov,$left,$rightmost,reduce_right,$qlabel).
EOF
	}
      }
    }
    
    
    foreach my $i (0..3) {
  print <<EOF;
'D'($i,
    lemma{ lex => $i, 
           lemma => $i, 
           cat => $i, 
           mstag => '_',
           fullcat => $i,
           xfullcat => $i,
           dict => [],
           length => 0
         }
).

EOF

    }

    print <<EOF;

'N'($max).
'S'('E$sid').
%% line=$.

EOF

    if ($oracle ## && $root
       ) {

      my $oracle_emit = $config->oracle_emit;
      ## emit all oracle edges (for building dynamic oracles)
      my $rtree={};
	foreach my $h (sort {$a <=> $b} keys %$tree) {
	  my $idh = $segmap->{$h} || $h;
	  foreach my $d (sort {$a <=> $b} keys %{$tree->{$h}}) {
	    my $idd = $segmap->{$d} || $d;
	    my $nd = scalar(keys %{$tree->{$d}}); # should be adapted for multi-edges
	    foreach my $l (sort {$a cmp $b} keys %{$tree->{$h}{$d}}) {
	      $rtree->{$d}++;
	      print <<EOF;
oracle!edge($idh,$idd,$l,$nd).
oracle!redge($idd,$idh,$l).
EOF
	    }
	  }
	}

      foreach my $p (sort keys %$segmap){
	my $idp = $segmap->{$p};
	my $ngov = $rtree->{$p} || 0;
	my $ndep = scalar(keys %{$tree->{$p}});
	print <<EOF;
oracle!ngov($idp,$ngov,$ndep).
EOF
      }

	print <<EOF;
oracle!action(0,init).
EOF

	my $i=0;
	my @actions = ();
	if ($graph) {
	  @actions = graph_get_actions_alt();
	} else {
	  @actions = get_actions(0);
	};
      foreach my $xaction (@actions) {
	my ($action,$info,$length) = @$xaction;
	$length ||= 1;
#	$i += 2 * $length -1;
	$i++;
	($oracle_emit || $i < 0) and print <<EOF;
oracle!action($i,$action). %% $info
EOF
##	$i++;
      }

    }

    print <<EOF;

%% SENTENCE DIV %%
%%%EOF

EOF

  RESET:
    $sid++;
    undef $sentid;
    $max=0;
    $tree ={};
    $segmap = {};
    $root=undef;
    $rootlabel=undef;
    $count = 0;
    @dag = ();
    $path = {};
    $maxlattice = 0;
    $ids={};
    $memoactions = {};
    $sinfo = {};
    next;			# done sentence
  }
  my @elt = split(/\t/,$_);
  if ($elt[0] =~ /^(\d+)-(\d+)$/) {
    # conllu schema for contracted words
    my $qform = quote($elt[1]);
    print <<EOF;
info!contracted($1,$2,$qform).

EOF
    next;
  }
  if ($elt[0] =~ /^(\d+)\.(\d+)$/) {
    # conllu schema for contracted words
    my $qform = quote($elt[1]);
    print <<EOF;
info!ellipse($1,$2,$qform).

EOF
    next;
  }
  my $tok;
  my $right;
  if ($lattice) {
    my $left = shift(@elt);
    $right = shift(@elt);
    $path->{$left}{$right} ||= 1;
    $right > $maxlattice and $maxlattice = $right;
    $left++;
##    $right++;
    unshift(@elt,$left);
    $tok = pop(@elt);
  }
  $nfields ||= scalar(@elt);
  if ( scalar(@elt) != $nfields 
       && $elt[5] =~ /^\d+/
     ) {
    warn "pb in CONLL file mismatch mstag/head: @elt\n";
    splice(@elt,5,0,'_');
  }
  my $entry = { data => [@elt], dict => '' };
  $tok and $entry->{tok} = $tok;
  $right and $entry->{right} = $right;
  if ($elt[0] =~ /^(\d+)\.\d+/) {
    # hack for CONLLU gap entries with id of the form n.m (such as 2.1)
    $entry->{right} = $1;
    $dag[-1]{right} = $elt[0]-1;
  }
  push(@dag,$entry);
  eof() and goto FLUSH;		# this case used when no empty line at the end of the file !
}


if ($oracle) {
  print <<EOF;

oracle!end.

%% SENTENCE DIV %%
%%%EOF

EOF
}

sub elt_process {
  my ($elt) = @_;
  my ($left,$lex,$lemma,$cat,$fullcat,$mstag,@other) = @{$elt->{data}};
  if ($graph && exists $ids->{$left}) {
    # multi line in graph format
    if ($oracle) {
      my $id = $left;
      $segmap->{$id}=$id;
      if (@other) {
	my ($head,$label) = @other;
	$label = quote($label);
	$head < 0 or $tree->{$head}{$id}{$label} = 1;
	if ($lexer_info->{gold} && rand(1) < $lexer_info->{gold}[0]) {
	  # add a prediction coming from the gold ! (very special case)
	  
	}
      }
    }
    return; 
  }
  ($lemma eq '_') and $lemma = $lex;
  (exists $lexer_info->{cluster_as_lemma}
   && exists $elt->{cluster}
  ) and $lemma = $elt->{cluster};
  my $xfullcat = [];
  ($fullcat ne '_') and push(@$xfullcat,$fullcat);
  (((!@$xfullcat && exists $lexer_info->{fullcat_complete})
    || $lexer_info->{fullcat_add_cat}
   ) && $fullcat ne $cat
  ) and push(@$xfullcat,$cat);
  @$xfullcat or push(@$xfullcat,$fullcat);
  (exists $lexer_info->{overwrite_fullcat}) and $xfullcat = [$cat];
  (exists $lexer_info->{add_alt_cats}) and push(@$xfullcat,
						map {"alt_$_"}
						grep {$_ ne $cat}
						keys %{$lexer_info->{add_alt_cats}{$lex}});
  $left--;
  $max = (exists $elt->{right}) ? $elt->{right} : ($left+1);
  $count++;
  my $fs = "'_'";
  $mstag =~ /atbpos/ and $mstag = Morpho::Arabic::handle($mstag);
  if ($cat eq 'V' && $elt->{subcat}) {
    my $subcat = $elt->{subcat};
    my @mstag = ($mstag,$subcat);
    $subcat !~ /\bObj=/ and push(@mstag,'Obj=none');
    $subcat !~ /\bObjde=/ and push(@mstag,'Objde=none');
    $subcat !~ /\bObj�=/ and push(@mstag,'Obj�=none');
    $subcat !~ /\bAtt=/ and push(@mstag,'Att=none');
    $mstag = join('|',@mstag);
  }
  (exists $elt->{cluster}) and $mstag .= "|cluster=$elt->{cluster}";
  if ($mstag =~ /\|/ || $lexer_info->{mstag_list}) {
    my @fs = split(/\|/,$mstag);
    if ($mstag =~ /sentid=(\d+)/){
      $sentid = $1;
      print <<EOF;
sentence_id('sentid=$sentid').
EOF
      @fs = grep {!/^sentid=/} @fs;
    }
    if (exists $lexer_info->{mstag_filter}) {
      @fs = grep {exists $lexer_info->{mstag_filter}{$_}} @fs;
    }
    if (exists $lexer_info->{mstag_exclude_feature}) {
      @fs = grep {/^(\S+)=/ && !exists $lexer_info->{mstag_exclude_feature}{$1}} @fs;
    }
    ## *** TMP: remove mate_head feature
    @fs = grep {!/^(mate|mate_sem)_head=/} @fs;
    if (@{$lexer_info->{xfullcat}}) {
      my %fs = (map {my @u = split(/=/,$_,2); $u[0] => $u[1]} grep {/.=./} @fs);
      foreach my $f (grep {exists $fs{$_}} @{$lexer_info->{xfullcat}}) {
	$xfullcat = [map {"$_$fs{$f}"} @$xfullcat];
	## delete $fs{$f};
      }
      @fs = grep {my @u=split(/=/,$_,2); exists $fs{$u[0]}} @fs;
    }
    $lexer_info->{mstag_complete} and push(@fs,$mstag);
    if (1) {
      if (my @delta = grep {$_ =~ /^frmg_delta=/} @fs) {
	my ($delta) = $delta[0] =~ /=(\S+)/;
	defined $lexer_info->{delta_reverse}{frmg} and $delta = -$delta;
	$sinfo->{$left}{pred_gov}= $left+$delta;
      }
      if (my @delta = grep {$_ =~ /^mst_delta=/} @fs) {
	my ($delta) = $delta[0] =~ /=(\S+)/;
	$sinfo->{$left}{pred_gov}= $left+$delta;
      }
      if (my @delta = grep {$_ =~ /^gold_delta=/} @fs) {
	my ($delta) = $delta[0] =~ /=(\S+)/;
	$sinfo->{$left}{pred_gov}= $left+$delta;
      }
      if (my @delta = grep {$_ =~ /^spmrl_delta=/} @fs) {
	my ($delta) = $delta[0] =~ /=(\S+)/;
	# use guiding info if none alredady present
	# ($delta && !exists $sinfo->{$left}{pred_gov}) and $sinfo->{$left}{pred_gov}= $left+$delta;
      }
      if (my @delta = grep {$_ =~ /^(mate|mate_sem)_delta=/} @fs) {
	my ($type,$delta) = $delta[0] =~ /(mate|mate_sem)_delta=(\S+)/;
	defined $lexer_info->{delta_reverse}{$type} and $delta = -$delta;
	($left-$delta > 0) and $sinfo->{$left}{pred_gov}= $left-$delta;
      }
      my %ogre = ();
      foreach my $fv (grep {/^ogre_(?:\S+?)=/} @fs) {
	my ($f,$v) = split(/=/,$fv);
	my ($iter,$rule) = $f =~ /^ogre_(\d+)_(\S+)/;
	my @v = split(/,/,$v);
	my $delta = $v[-1];
	if ($left-$delta > 0) {
#	  $sinfo->{$left}{pred_gov} = $left-$delta;
	  #	  $sinfo->{$left}{label} = $v[-2];
	  #	  print "%% process ogre $left $delta $fv\n";
	  if (1) {
	    my $entry = $sinfo->{$left}{multi_pred_gov} ||= {};
	    $entry->{$left-$delta}{"$iter:$v[-2]"} = 1;
	  }
	}
	my $action = 'rename';
	if ($v[-2] =~ /^_->/) {
	  $action = 'add';
	} elsif ($v[-2] =~ /->_$/) {
	  $action = 'del';
	}
	if (1) {
	  foreach my $new (
#			   "delta_$f=$v[-1]",
#			   "label_$f=$v[-2]",
			   #			   "action_$f=$action",
			   "rule_ogre_$iter=$rule",
			   "delta_ogre_$iter=$v[-1]",
			   "label_ogre_$iter=$v[-2]",
			   "action_ogre_$iter=$action"
			  ) {
	    $ogre{$new}=1;
	  }
	}
      }
      @fs = grep {!/^ogre_/} @fs;
      if (keys %ogre) {
	push(@fs,keys %ogre);
      }
      if (@fs && @fs==1) {
	$fs = quote($fs[0]);
      } elsif (@fs) {
	$fs = join(',',  map {quote($_)} grep {$_} @fs );
	$fs = "[$fs]";
	$mstag_fset and $fs = "mstag$fs";
      } else {
	$fs = "'_'"
      }
    }
    # get label info for guiding
    # - favor gold label if present
    # - other labels otherwise
    # should add some kind of sorting info between multiple sources of guiding
    # (or add the functionality to have multiple guides in dyalog-sr)
    if (my @label = grep {$_ =~ /^gold_label=/} @fs) {
      my ($label) = $label[0] =~ /=(\S+)/;
      $sinfo->{$left}{label}= $label;
    }
    if (my @label = grep {$_ =~ /^mst_label=/} @fs) {
      my ($label) = $label[0] =~ /=(\S+)/;
      $sinfo->{$left}{label}= $label;
    }
    if (my @label = grep {$_ =~ /^(\w+)_label=/ && $1 ne 'spmrl'} @fs) {
      my ($label) = $label[0] =~ /=(\S+)/;
      (exists $sinfo->{$left}{label}) or $sinfo->{$left}{label}= $label;
    }

  } else {
    if (@{$lexer_info->{xfullcat}} && $mstag =~ /(\S+)=(\S+)/) {
      my ($f,$v) = ($1,$2);
      if (grep {$f eq $_} @{$lexer_info->{xfullcat}}) {
	$xfullcat = [map {"$_$v"} @$xfullcat];
#	$mstag = '_';
      }
    }
    $fs = quote($mstag || '_');
  }
  if ($lexer_info->{extra}{number} && $lex =~ /^\d+(?:[.,]\d*)?/) {
    $xfullcat = [map {"${_}_num"} @$xfullcat];
  }
  if ($lexer_info->{extra}{capitalize} && lcfirst($lex) ne $lex) {
    $xfullcat = [map {"${_}_cap"} @$xfullcat];
  }
  my ($qlex,$qlemma,$qcat,$qfullcat) = map {quote($_)} ($lex,$lemma,$cat,$fullcat);
  my $qxfullcat = [map {quote($_)} @$xfullcat];
  exists $lexer_info->{split}{cat} and $qcat = qsplit($cat);
  exists $lexer_info->{split}{fullcat} and $qfullcat = qsplit($fullcat);
  exists $lexer_info->{split}{xfullcat} and $qxfullcat = [map {qsplit_pos($_)} @$xfullcat];
  if (scalar(@$qxfullcat) == 1) {
    $qxfullcat = $qxfullcat->[0];
  } else {
    $qxfullcat = "[".join(",",@$qxfullcat)."]";
  }
  my $dinfo = $elt->{dict};
  my $length = $max - $left;
  #  my $id = $count;
  my $id = $elt->{data}[0];
  if (scalar(@$xfullcat) > 1
      && exists $lexer_info->{add_alt_cats}
      && exists $lexer_info->{alt_cats_in_lattice}
     ) {

    my $i = 0;
    foreach my $xcat (@$xfullcat) {
      my $qxcat = quote($xcat);
    print <<EOF;
'C'($left,
    $id + $i,
    lemma{ lex => $qlex, 
           lemma => $qlemma, 
           cat => $qcat, 
           mstag => $fs,
           fullcat => $qfullcat,
           xfullcat => $qxcat,
           dict => [$dinfo],
           length => $length
         },
    $max).

EOF
      if ($i) {
	$ids->{"$id + $i"} = $length;
	$sinfo->{$left}{n}++;
      }
      
      $i++;
    }

    $id .= "+ 0";
    
  } else {
    exists $elt->{tok} and $id .= "+$elt->{tok}";
    print <<EOF;
'C'($left,
    $id,
    lemma{ lex => $qlex, 
           lemma => $qlemma, 
           cat => $qcat, 
           mstag => $fs,
           fullcat => $qfullcat,
           xfullcat => $qxfullcat,
           dict => [$dinfo],
           length => $length
         },
    $max).

EOF
  }

  if ($pos_oracle) {
    my ($gpos,@mstag) = split(m{\|},$other[-1]);
    my $qgpos = quote($gpos);
    print <<EOF;

pos_oracle($id,$qgpos).

EOF
    foreach my $mstag (@mstag) {
      my ($f,$v) = split(/=/,$mstag);
      my $qf = quote($f);
      my $qv = quote($v);
      print <<EOF;

mstag_oracle($id,$qf,$qv).

EOF
    }
  }

  if ($elt->{data}[9] eq "SpaceAfter=No") {
    print <<EOF;

no_space_after($id).

EOF
  }
  
  $ids->{$id} = $length;

  $sinfo->{$left}{form} = $qlex;
  $sinfo->{$left}{cat}{$cat} = 1;
  $sinfo->{$left}{n}++;

  my $conllmax = $max;

  if (exists $elt->{segalign}) {
    my $oracle_tok = $elt->{segalign};
    @other = @{$oracle_tok}[6,7];
    $conllmax = $oracle_tok->[0];
    print "%% getting from oracle token: @other\n";
  }

  if ($oracle && (@other || $graph)) {
    $segmap->{$conllmax}=$id;
    if (@other) {
      my ($head,$label) = @other;
      $label = quote($label);
      unless ($head < 0) {
	$tree->{$head}{$conllmax}{$label} = 1;
	print <<EOF;
oracle!edge($head,$id,$label).

EOF
      }
    }
  }
  
  if (my $lex2 = find_closest_word($lex)) {
    my $qlex2 = quote($lex2);
    print <<EOF;
we_approx($id,$qlex2).
EOF
  }

}

sub quote {
  my $string = shift;
  return $string if ($string =~ /^\d+$/ && !$string =~ /^0\d+/);
##  return $string if ($string =~ /^[a-z]\w*$/o);
  return $string if ($string =~ /^[a-z][a-zA-Z��������������������������������������]*$/o);
  $string =~ s/\\/\\\\/og;
  $string =~ s/\'/\'\'/og;
  # $string =~ s/\'(?!\')/\'\'/og;
  return "\'$string\'";
}

sub qsplit {
  my $s = shift;
  ($s =~ /^P\+/) and return quote($s);
  my @s = map {quote($_)} split(/\+/,$s);
  if (scalar(@s) == 1) {
    return $s[0]
  } else {
    return '['.join(',',@s).']';
  }
}

sub qsplit_pos {
  my $s = shift;
  my $i = 1;
  ($s =~ /^P\+/) and return quote($s);
  my @s = map {quote($i++.":$_")} split(/[+_]/,$s);
  if (scalar(@s) == 1) {
    return $s[0]
  } else {
    return '['.join(',',@s).']';
  }
}

sub get_actions {
  my ($head,$min,$max) = @_;
  exists $memoactions->{$head} and return;
  $memoactions->{$head} = 1;
  my $entry = $tree->{$head};
  my @before = sort {$a <=> $b} grep {$_ < $head} keys %$entry;
  my @after = sort {$a <=> $b} grep {$_ > $head} keys %$entry;
  if (defined $min) {
    foreach my $b (grep {$_ <= $min} @before) {
      print <<EOF;
%% not projective E$sid $head -> $b : min=$min
oracle!notproj($b,$head).
EOF
    }
  }
  if (defined $max) {
    foreach my $a (grep {$_ >= $max} @after) {
      print <<EOF;
%% not projective E$sid $head -> $a : max=$max
oracle!notproj($a,$head).
EOF
    }
  }
  my @actions = ();
  my $lmin = $min;
  my @before2 = @before;
  while (@before2) {
    my $dep = shift(@before2);
    my $lmax = @before2 ? $before2[0] : $head;
    push(@actions,get_actions($dep,$lmin,$lmax));
    $lmin = $dep;
  }
  if ($head) {
    my $id = exists $segmap->{$head} ? $segmap->{$head} : 0;
    my $id2 = exists $segmap->{$head+1} ? $segmap->{$head+1} : 0;
    my $id3 = exists $segmap->{$head+2} ? $segmap->{$head+2} : 0;
    push(@actions,["shift($id,$id2,$id3)","$head",$ids->{$id}]);
print <<EOF;
oracle!shift($id,$id2,$id3).
EOF
  }
  foreach my $dep (reverse @before) {
    foreach my $label (sort {$a cmp $b} keys %{$entry->{$dep}}) {
      push(@actions,["right($label)","$dep $head"]);
    }
  }
  $lmin = $head;
  while (@after) {
    my $dep = shift(@after);
    my $lmax = @after ? $after[0] : $max;
    my @tmp =  get_actions($dep,$lmin,$lmax);
    foreach my $label (sort {$a cmp $b} keys %{$entry->{$dep}}){
      push(@actions,
	   @tmp,
	   ["left($label)","$dep $head"]
	  );
    }
    $lmin = $dep;
  }
  return @actions;
}

sub check_void {
  my ($data,$key,$pos) = @_;
  return (!exists $data->{$key}) || !scalar(keys %{$data->{$key}});
}

sub first_edge_after {
  my ($tree,$rtree,$key,$pos) = @_;
  return min(grep {$_ >= $pos} (keys %{$tree->{$key}}, keys %{$rtree->{$key}}));
}

sub first_gov_after {
  my ($tree,$rtree,$key,$pos) = @_;
  return min(grep {$_ >= $pos} keys %{$rtree->{$key}});
}

sub first_edge_before {
  my ($tree,$rtree,$key,$pos) = @_;
  return min(grep {$_ < $pos} (keys %{$tree->{$key}}, keys %{$rtree->{$key}}));
}

sub first_edge_below {
  my ($tree,$rtree,$key,$stack,$pos) = @_;
  foreach my $i ($pos ... (scalar(@$stack)-1)) {
    (exists $tree->{$key}{$stack->[$i]} || exists $rtree->{$stack->[$i]}{$key}) and return $i;
  }
  return undef;
}

sub has_edge_between {
  my ($tree,$rtree,$key,$min,$max) = @_;
  return min(grep {$_ <= $max && $_ >= $min} (keys %{$tree->{$key}}, keys %{$rtree->{$key}}));
}

sub has_edge {
  my ($tree,$rtree,$key1,$key2) = @_;
  return exists $tree->{$key1}{$key2} || exists $tree->{$key2}{$key1};
}

sub graph_get_actions {
  ## produce oracle for DAG actions (shift, reduce(L), attach(L), pop0, pop1
  my @stack = (0);
  my $pos  = 1;
  my @actions=();
  my $rtree={};
  foreach my $head (keys %$tree) {
    foreach my $dep (keys %{$tree->{$head}}) {
      $rtree->{$dep}{$head} = 1;
    }
  }
  my $step = 0;
  my $register = sub {
    my ($action,$msg) = @_;
    push(@actions,[$action,$msg]);
    $step++;
    my $dstep = 0;
    $pos > 0 and $dstep = $step / $pos;
    print "%% $step action=$action pos=$pos stack=<@stack> dstep=$dstep\n";
    if (@stack > 0) {
      my $s = $stack[0];
      my @deps = sort keys %{$tree->{$s}};
      my @govs = sort keys %{$rtree->{$s}};
      print "%%\tstack0 $s deps=<@deps> govs=<@govs>\n";
    }
    if (@stack > 1) {
      my $s = $stack[1];
      my @deps = sort keys %{$tree->{$s}};
      my @govs = sort keys %{$rtree->{$s}};
      print "%%\tstack1 $s deps=<@deps> govs=<@govs>\n";
    }
  };
  while (1) {
#    print "state pos=$pos stack=<@stack>\n";
    if (@stack > 1
	&& exists $tree->{$stack[0]}{$stack[1]}
       ) {
      ## left reduce or attach
      my $head = $stack[0];
      my $dep = $stack[1];
      my $entry = $tree->{$head}{$dep};
      my @labels = sort {$a cmp $b} keys %$entry;
      delete $tree->{$head}{$dep};
      delete $rtree->{$dep}{$head};
      my $lastaction = (check_void($tree,$dep,$pos) && check_void($rtree,$dep,$pos)) ? 'right' : 'righta';
      while (@labels) {
	my $label = shift(@labels);
	my $action = scalar(@labels) ? 'righta' : $lastaction;
	$register->("${action}($label)","$dep $head");
      }
      if ($lastaction eq 'right') {
	shift(@stack);
	$stack[0] = $head;
      }
    } elsif (@stack > 1
	     && exists $tree->{$stack[1]}{$stack[0]}
	     && (!(grep {$_ > $stack[0]} keys %{$tree->{$stack[0]}})
		 ||
		 ## we do a lefta action is s1 has no free governor
		 ## followed by a pop1, it allows to get rid of s1 quickly !
		 ( $stack[1]
		   && check_void($rtree,$stack[1],$pos)
##		   && grep {$_ < $stack[1]} keys %{$tree->{$stack[0]}}
		 )
		)
	    ) {
      ## right reduce or attach
      my $head = $stack[1];
      my $dep = $stack[0];
      my $entry = $tree->{$head}{$dep};
      my @labels = sort {$a cmp $b} keys %$entry;
      delete $tree->{$head}{$dep};
      %{$tree->{$head}} or delete $tree->{$head};
      delete $rtree->{$dep}{$head};
      %{$rtree->{$dep}} or delete $rtree->{$dep};
      my $lastaction = (check_void($tree,$dep) && check_void($rtree,$dep)) ? 'left' : 'lefta';
      while (@labels) {
	my $label = shift(@labels);
	my $action = scalar(@labels) ? 'lefta' : $lastaction;
	$register->("${action}($label)","$dep $head");
      }
      if ($lastaction eq 'left') {
	shift(@stack);
	$stack[0] = $head;
      }
    } elsif (@stack > 0
	     && $stack[0]
	     && check_void($tree,$stack[0])
	     && check_void($rtree,$stack[0])
	    ) {
      ## topmost element has no governor or governee left
      ## we can pop it
      $register->("pop0","$stack[0]");
      shift(@stack);
    } elsif (@stack > 1
	     && $stack[1]
	     && check_void($tree,$stack[1])
	     && check_void($rtree,$stack[1])
	    ) {
      ## second element has no governor or governee left
      ## we can pop it
      $register->("pop1","$stack[1]");
      my $head = shift(@stack);
      $stack[0] = $head;
    } elsif (exists $segmap->{$pos}) {
      ## we shift, if 
      my $id = $segmap->{$pos};
      my $id2 = exists $segmap->{$pos+1} ? $segmap->{$pos+1} : 0;
      my $id3 = exists $segmap->{$pos+2} ? $segmap->{$pos+2} : 0;
      $register->("shift($id,$id2,$id3)","$pos");
      1 and print <<EOF;
oracle!shift($id,$id2,$id3).
EOF
      unshift(@stack,$pos);
      $pos++;
    } elsif (@stack > 1) {
      print "%% pb oracle E$sid <@stack> pos=$pos\n";
      $register->("pop0","$stack[0]");
      shift(@stack);
    } else {
      return @actions;
    }
  }
}


sub graph_get_actions_alt {
  ## produce oracle for DAG actions, not using any reduce actions (shift, attach(L), pop0, pop1)
  my @stack = (0);
  my $pos  = 1;
  my @actions=();
  my $rtree={};
  my $prev = 'init';
  my $added = {};
  my $radded = {};
  foreach my $head (keys %$tree) {
    foreach my $dep (keys %{$tree->{$head}}) {
      $rtree->{$dep}{$head} = 1;
    }
  }
  my $step = 0;
  my $register = sub {
    my ($action,$msg) = @_;
    push(@actions,[$action,$msg]);
    $step++;
    my $dstep = 0;
    $pos > 0 and $dstep = $step / $pos;
    print "%% $step action=$action pos=$pos stack=<@stack> dstep=$dstep\n";
    if (@stack > 0) {
      my $s = $stack[0];
      my @deps = sort keys %{$tree->{$s}};
      my @govs = sort keys %{$rtree->{$s}};
      print "%%\tstack0 $s deps=<@deps> govs=<@govs>\n";
    }
    if (@stack > 1) {
      my $s = $stack[1];
      my @deps = sort keys %{$tree->{$s}};
      my @govs = sort keys %{$rtree->{$s}};
      print "%%\tstack1 $s deps=<@deps> govs=<@govs>\n";
    }
    $prev = $action;
  };
  while (1) {
    my $mins0;
    my $mins1;
    my $mins2;
#    print "state pos=$pos stack=<@stack>\n";
    if (@stack > 1
	&& exists $tree->{$stack[0]}{$stack[1]}
	&& ($attach ||
	    ($reduce
	     && scalar(keys %{$tree->{$stack[1]}}) == 0
	     && scalar(keys %{$rtree->{$stack[1]}}) == 1
	    )
	   )
       ) {
      ## right reduce or attach
      my $head = $stack[0];
      my $dep = $stack[1];
      my $entry = $tree->{$head}{$dep};
      my @labels = sort {$a cmp $b} keys %$entry;
      delete $tree->{$head}{$dep};
      delete $rtree->{$dep}{$head};
      $added->{$head}{$dep}++;
      $radded->{$dep}{$head}++;
      if (scalar(@labels) == 1
	  && $reduce
	  && !scalar(keys %{$tree->{$dep}})
	  && !scalar(keys %{$rtree->{$dep}})
	 ) {
	my $label = shift(@labels);
	my $action = 'right';
	$register->("${action}($label)","$dep $head");
	my $head = shift(@stack);
	$stack[0] = $head;
      } else {
	while (@labels) {
	  my $label = shift(@labels);
	  my $action = 'righta';
	  $register->("${action}($label)","$dep $head");
	}
      }
    } elsif (@stack > 1
	     && exists $tree->{$stack[1]}{$stack[0]}
	    ## && (!$proj || check_void($tree,$stack[0],$pos))
	     ##	     && check_void($tree,$stack[0],$pos)
	     && (##(!$proj && !$late_attach)
		 !$late_attach
		 || !first_edge_after($tree,$rtree,$stack[0],$pos)
		 || first_gov_after($tree,$rtree,$stack[0],$pos)
		)
	     && ($attach ||
		 ($reduce
		  && scalar(keys %{$tree->{$stack[0]}}) == 0
		  && scalar(keys %{$rtree->{$stack[0]}}) == 1
		  && (!$single_root || $stack[1] || !exists $segmap->{$pos})
		 )
		)
	    ) {
      ## left reduce or attach
      my $head = $stack[1];
      my $dep = $stack[0];
      my $entry = $tree->{$head}{$dep};
      my @labels = sort {$a cmp $b} keys %$entry;
      delete $tree->{$head}{$dep};
      %{$tree->{$head}} or delete $tree->{$head};
      delete $rtree->{$dep}{$head};
      %{$rtree->{$dep}} or delete $rtree->{$dep};
      $added->{$head}{$dep}++;
      $radded->{$dep}{$head}++;
      if (scalar(@labels) == 1
	  && $reduce
	  && !scalar(keys %{$tree->{$dep}})
	  && !scalar(keys %{$rtree->{$dep}})
	 ) {
	my $label = shift(@labels);
	my $action = 'left';
	$register->("${action}($label)","$dep $head");
	shift(@stack);
      } else {
	while (@labels) {
	  my $label = shift(@labels);
	  my $action = 'lefta';
	  $register->("${action}($label)","$dep $head");
	}
      }
    } elsif ($pop0
	     && @stack > 0
	     && $stack[0]
	     && check_void($tree,$stack[0],$pos)
	     && check_void($rtree,$stack[0],$pos)
	    ) {
      ## topmost element has no governor or governee left
      ## we can pop it
      ($noop && !scalar(keys %{$radded->{$stack[0]}})) and $register->("noop","$stack[0]");
      $register->("pop0","$stack[0]");
      shift(@stack);
    } elsif ($pop1
	     && @stack > 1
	     && $stack[1]
	     && check_void($tree,$stack[1],$pos)
	     && check_void($rtree,$stack[1],$pos)
#	     && (!$proj 
#		 || !exists $added->{$stack[1]}{$stack[0]}
#		)
	     && (!exists $added->{$stack[1]}{$stack[0]}
		 || ( !first_edge_after($tree,$rtree,$stack[0],$pos)
		    )
		 || defined first_edge_before($tree,$rtree,$stack[0],$stack[1])
		 ## || defined first_edge_below($tree,$rtree,$stack[0],\@stack,2)
		 || ((scalar(@stack) > 2) && has_edge($tree,$rtree,$stack[0],$stack[2]))
		)
	    ) {
      ## second element has no governor or governee left
      ## we can pop it
      ## if stack1 is a gov for stack0, it seems better to delay reduce of stack1
      ($noop && !scalar(keys %{$radded->{$stack[1]}})) and $register->("noop","$stack[1]");
      $register->("pop1","$stack[1]");
      my $head = shift(@stack);
      $stack[0] = $head;
    } elsif ($pop2
	     && @stack > 2
	     && check_void($tree,$stack[2],$pos)
	     && check_void($rtree,$stack[2],$pos)
	    ) {
      $register->("pop2","$stack[2]");
      splice(@stack,2,1);
    } elsif ( !$proj
	      && $swap
	      && $prev ne 'swap'
#	      && $prev ne 'swap2'
#	      && $prev ne 'swap3'
	      && @stack > 1 
	      && $stack[1]
	      && ($mins1 = first_edge_after($tree,$rtree,$stack[1],$pos))
#	      && print("potential swap s0=$stack[0] s1=$stack[1] mins1=$mins1 ",first_edge_after($tree,$rtree,$stack[0],$pos)," -- ",first_edge_after($tree,$rtree,$stack[0],$pos),"\n")
	      && (!first_edge_after($tree,$rtree,$stack[0],$pos)
		  || first_edge_after($tree,$rtree,$stack[0],$pos) > $mins1
		  || (first_edge_after($tree,$rtree,$stack[0],$pos) == $mins1
		      && ( ( $stack[1] < $stack[0]
			     && defined first_edge_before($tree,$rtree,$stack[0],$stack[1])
			   )
			   ||
			   ( defined first_edge_after($tree,$rtree,$stack[0],$mins1+1)
			     && !defined first_edge_after($tree,$rtree,$stack[1],$mins1+1)
			     && !defined first_edge_before($tree,$rtree,$stack[1],$stack[1])
			   )
			 )
		     )
		 )
	    ) {
      ## we try a swap if we find a non projective dep
      $register->('swap',"$stack[0] $stack[1]");
      my $s0 = shift(@stack);
      my $s1 = shift(@stack);
      unshift(@stack,$s0);
      unshift(@stack,$s1);
    } elsif (1
	     && @stack > 2
	     && $reduce2
	     && exists $tree->{$stack[2]}{$stack[0]}
	     && scalar(keys %{$tree->{$stack[2]}{$stack[0]}}) == 1
	     && scalar(keys %{$tree->{$stack[0]}}) == 0
	     && scalar(keys %{$rtree->{$stack[0]}}) == 1
	    ) {
      my $head = $stack[2];
      my $dep = $stack[0];
      my ($label) = keys %{$tree->{$head}{$dep}};
      delete $tree->{$head}{$dep};
      %{$tree->{$head}} or delete $tree->{$head};
      delete $rtree->{$dep}{$head};
      %{$rtree->{$dep}} or delete $rtree->{$dep};
      $added->{$head}{$dep};
      $radded->{$dep}{$head};
      $register->("left2($label)","$dep $head");
      shift(@stack);
    } elsif (1
	     && @stack > 2
	     && $reduce2
	     && exists $tree->{$stack[0]}{$stack[2]}
	     && scalar(keys %{$tree->{$stack[0]}{$stack[2]}}) == 1
	     && scalar(keys %{$tree->{$stack[2]}}) == 0
	     && scalar(keys %{$rtree->{$stack[2]}}) == 1
	    ) {
      my $head = $stack[0];
      my $dep = $stack[2];
      my ($label) = keys %{$tree->{$head}{$dep}};
      delete $tree->{$head}{$dep};
      %{$tree->{$head}} or delete $tree->{$head};
      delete $rtree->{$dep}{$head};
      %{$rtree->{$dep}} or delete $rtree->{$dep};
      $added->{$head}{$dep};
      $radded->{$dep}{$head};
      $register->("right2($label)","$dep $head");
      splice(@stack,2,1);

    } elsif ($swap2
	     && @stack > 2
	     && $prev ne 'swap2'
	     && $prev ne 'swap3'
	     && $stack[2]
#	     && $reduce2
	     && exists $tree->{$stack[2]}{$stack[0]}
	     && scalar(keys %{$tree->{$stack[2]}{$stack[0]}}) == 1
	     && scalar(keys %{$tree->{$stack[0]}}) == 0
	     && scalar(keys %{$rtree->{$stack[0]}}) == 1
#	     && $stack[1] < $stack[0]
	    ) {
      $register->("swap2","$stack[0] $stack[2]");
      my $s1 = $stack[1];
      my $s2 = $stack[2];
      $stack[1] = $s2;
      $stack[2] = $s1;
    } elsif ($swap2
	     && @stack > 2
	     && $prev ne 'swap2'
	     && $prev ne 'swap3'
	     && $stack[2]
#	     && $reduce2
	     && exists $tree->{$stack[0]}{$stack[2]}
	     && scalar(keys %{$tree->{$stack[0]}{$stack[2]}}) == 1
	     && scalar(keys %{$tree->{$stack[2]}}) == 0
	     && scalar(keys %{$rtree->{$stack[2]}}) == 1
#	     && $stack[1] < $stack[0]
	    ) {
      $register->("swap2","$stack[0] $stack[2]");
      my $s1 = $stack[1];
      my $s2 = $stack[2];
      $stack[1] = $s2;
      $stack[2] = $s1;

    } elsif ($swap3
	     && @stack > 2
	     && $prev ne 'swap2'
	     && $prev ne 'swap3'
	     && $stack[2]
#	     && $reduce2
	     && exists $tree->{$stack[2]}{$stack[1]}
	     && scalar(keys %{$tree->{$stack[2]}{$stack[1]}}) == 1
	     && scalar(keys %{$tree->{$stack[1]}}) == 0
	     && scalar(keys %{$rtree->{$stack[1]}}) == 1
#	     && $stack[1] < $stack[0]
	    ) {
      $register->("swap3","$stack[0] $stack[2]");
      my $s0 = $stack[0];
      my $s2 = $stack[2];
      $stack[0] = $s2;
      $stack[2] = $s0;

    } elsif ($swap3
	     && @stack > 2
	     && $prev ne 'swap2'
	     && $prev ne 'swap3'
	     && $stack[2]
#	     && $reduce2
	     && exists $tree->{$stack[1]}{$stack[2]}
	     && scalar(keys %{$tree->{$stack[1]}{$stack[2]}}) == 1
	     && scalar(keys %{$tree->{$stack[2]}}) == 0
	     && scalar(keys %{$rtree->{$stack[2]}}) == 1
#	     && $stack[1] < $stack[0]
	    ) {
      $register->("swap3","$stack[0] $stack[2]");
      my $s0 = $stack[0];
      my $s2 = $stack[2];
      $stack[0] = $s2;
      $stack[2] = $s0;

    } elsif (0
	     && $swap2
	     && @stack > 2
	     && $prev ne 'swap2'
	     && $prev ne 'swap3'
	     && $stack[2]
	     && (($mins2 = first_edge_after($tree,$rtree,$stack[2],$pos)) > $pos)
#	     && (($mins1 = first_edge_after($tree,$rtree,$stack[1],$pos)) > $mins2)
	     && (first_edge_after($tree,$rtree,$stack[0],$mins2) > $mins2)
#	     && $reduce2
#	     && $stack[1] < $stack[0]
	    ) {
      $register->("swap2","$stack[0] $stack[2]");
      my $s1 = $stack[1];
      my $s2 = $stack[2];
      $stack[1] = $s2;
      $stack[2] = $s1;

    } elsif (0
	     && $swap2
	     && @stack > 2
	     && $prev ne 'swap2'
	     && $prev ne 'swap3'
	     && $stack[2]
	     && (($mins2 = first_edge_after($tree,$rtree,$stack[2],$pos)))
	     && (($mins0 = first_edge_after($tree,$rtree,$stack[0],$pos)) > $mins2)
	     && (first_edge_after($tree,$rtree,$stack[1],$mins2) > $mins2)
#	     && $reduce2
#	     && $stack[1] < $stack[0]
	    ) {
      $register->("swap3","$stack[0] $stack[2]");
      my $s0 = $stack[0];
      my $s2 = $stack[2];
      $stack[0] = $s2;
      $stack[2] = $s0;

      
    } elsif (0
	     && $swap
	     && $swap2
	     && @stack > 1
	     && $stack[1] > $stack[0]
	    ) {
            $register->('swap',"$stack[0] $stack[1]");
      my $s0 = shift(@stack);
      my $s1 = shift(@stack);
      unshift(@stack,$s0);
      unshift(@stack,$s1);
    } elsif (exists $segmap->{$pos}
	     && (!$stack[0] || first_edge_after($tree,$rtree,$stack[0],$pos))
	    ) {
      ## we shift, if 
      my $id = $segmap->{$pos};
      my $id2 = exists $segmap->{$pos+1} ? $segmap->{$pos+1} : 0;
      my $id3 = exists $segmap->{$pos+2} ? $segmap->{$pos+2} : 0;
      $register->("shift($id,$id2,$id3)","$pos");
      1 and print <<EOF;
oracle!shift($id,$id2,$id3).
EOF
      unshift(@stack,$pos);
      $pos++;
    } elsif (@stack > 1) {
      my $s0 = $stack[0];
      my $s1 = $stack[1];
      my $n0 = scalar(keys %{$tree->{$s0}}) + scalar(keys %{$rtree->{$s0}});
      my $n1 = scalar(keys %{$tree->{$s1}}) + scalar(keys %{$rtree->{$s1}});
      my $change = 0;
      if (scalar(@stack) > 1 && $stack[2]) {
	my $s2 = $stack[2];
	has_edge($tree,$rtree,$s0,$s2) and $n0--;
	has_edge($tree,$rtree,$s1,$s2) and $n1--;
      }
      my $s = (!$s1 || ($n0 < $n1) || !$pop1) ? $s0 : $s1;
      foreach my $dep (sort keys %{$tree->{$s}}) {
	print "%% erase $s -> $dep\n";
	delete $rtree->{$dep}{$s};
	$loss++;
	$change++;
      }
      delete $tree->{$s};
      foreach my $gov (sort keys %{$rtree->{$s}}) {
	print "%% erase $s <- $gov\n";
	delete $tree->{$gov}{$s};
	$loss++;
	$change++;
      }
      delete $rtree->{$s};
      print "%% pb oracle E$sid <@stack> pos=$pos accloss=$loss\n";
      if (!$change
	  && !$pop0
	  && !$pop1
	 ) {
	# avoid loop when nothing removed and no available actions
	# should only occur at the end of a sentence (otherwise shift is an option)
	return @actions;
      }
    } else {
      return @actions;
    }
  }
}


sub keep_alive {
  my ($left,$path,$alive) = @_;
  if (!exists $alive->{$left}) {
    $alive->{$left} = 1;
    my $keep = 0;
    foreach my $right (sort keys %{$path->{$left}}) {
      if (keep_alive($right,$path,$alive)) {
	$keep ||= 1;
      } else {
	delete $path->{$left}{$right};
      }
    }
    if (!$keep) {
      print "%% $left is dead !\n";
      delete $path->{$left};
      $alive->{$left} = 0;
    }
  }
  return $alive->{$left};
}

sub find_best_path {
  my ($elt,$dag) = @_;
  exists $elt->{segcost} or return -1;
  if (!exists $elt->{segbest}) {
    exists $elt->{segcall} and return -1;
    $elt->{segcall} = 1;
    my $right = $elt->{right} + 1;
    my @next = grep {($_->{data}[0] == $right) && exists $_->{segcost}} @$dag;
    my $max = 0;
    my $maxelt;
    foreach my $next (@next) {
      my $m = find_best_path($next,$dag);
      $m > $max or next;
      $max = $m;
      $maxelt = $next;
    }
    $elt->{segbest} = $elt->{segcost} + $max;
    $maxelt and $elt->{segnext} = $maxelt;
  }
  return $elt->{segbest};
}

sub find_closest_word {
  my $w = shift;
#  print "%% search0 closest word to $w: $we->{$w}\n";
  (exists $vocab->{$w} || !exists $we->{$w}) and next;
  (exists $vocab->{lc($w)}) and return lc($w);
  (exists $we_approx->{$w}) and return $we_approx->{$w};
#  print "%% search1 closest word to $w\n";
  my $v = $we->{$w};
  my $best;
  my $bestw;
  foreach my $w2 (keys %$vocab) {
    eval {
      my $d = inner($v,$vocab->{$w2});
      unless ($best && $best > $d) {
	$best = $d;
	$bestw = $w2;
      }
      ($best > 0.9) and last;
    }
  }
  if ($best > 0.8) {
    $we_approx->{$w} = $bestw;
#    print "%% found $bestw $best\n";
    return $bestw;
  } else {
    $we_approx->{$w} = 0;
    return 0;
  }
}

######################################################################
package Morpho::Arabic;

## inspired from script add_arabic_morph_features_labels.pl
## by authors : Djam� Seddah and Miguel Ballesteros
## distributed in SPMRL 2013

## to be moved in some external module

our $map;

sub init {
  $map or
    $map = {
	    'ADJ.VN' => 'subcat=vn',
	    'ADJ.COMP' => 'subcat=comp',
	    'ADJ.NUM' => 'subcat=num',
	    'ADJ.NUM' => 'subcat=num',
	    'CASE_DEF_ACC' => 'case=acc|casedefinitness=y',
	    'CASE_DEF_GEN' => 'case=gen|casedefinitness=y',
	    'CASE_DEF_NOM' => 'case=nom|casedefinitness=y',
	    'CASE_INDEF_ACC' => 'case=acc|casedefinitness=n',
	    'CASE_INDEF_GEN' => 'case=gen|casedefinitness=n',
	    'CASE_INDEF_NOM' => 'case=nom|casedefinitness=n',
	    'CONNEC_PART' => 'subcat=connec',
	    'CV' => 'aspect=imperative|aspect=imperative',
	    'CVSUFF_DO:1p' => 'catsuff=CVSUFF|aspect=imperative||func=do||pers=1|number=p',
	    'CVSUFF_DO:1'  => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=1|number=s',
	    'CVSUFF_DO:3F' => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=3|gender=f|number=s',
	    'CVSUFF_DO:3MP' => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=3|gender=m|number=p',
	    'CVSUFF_DO:3M' => 'catsuff=CVSUFF|aspect=imperative||func=do|pers=3|gender=m|number=s',
	    'CVSUFF_SUBJ:2F' => 'catsuff=CVSUFF|aspect=imperative||func=subj|pers=2|gender=f|number=s',
	    'CVSUFF_SUBJ:2MP' => 'catsuff=CVSUFF|aspect=imperative||func=subj|pers=2|gender=m|number=p',
	    'CVSUFF_SUBJ:2M' => 'catsuff=CVSUFF|aspect=imperative||func=subj|pers=2|gender=m|number=s',
	    'DEM_PRON_FD' => 'gender=f|number=d',
	    'DEM_PRON_F' => 'gender=f|number=s',
	    'DEM_PRON_MD' => 'gender=m|number=d',
	    'DEM_PRON_MP' => 'gender=m|number=p',
	    'DEM_PRON_P' => 'number=p',
	    'DET_ADJ.VN' => 'subcat=vn',
	    'DET_NOUN_VN' => 'subcat=vn',
	    'DET_NOUN_NUM' => 'subcat=num',
	    'DET_NOUN_PROP' => 'subcat=prop',
	    'DET_NOUN_QUANT' => 'subcat=quant',
	    'EMPHATIC_PART' => 'subcat=emphatic',
	    'FOCUS_PART' => 'subcat=focus',
	    'FUT_PART' => 'subcat=fut',
	    'INTERROG_ADV' => 'subcat=interrog',
	    'INTERROG_PART' => 'subcat=interrog',
	    'INTERROG_PRON' => 'subcat=interrog',
	    'IV1P' => 'aspect=imperfect|pers=1|number=p',
	    'IV1' => 'aspect=imperfect|pers=1|number=s',
	    'IV2D' => 'aspect=imperfect|pers=2|number=d',
	    'IV2FP' => 'aspect=imperfect|pers=2|gender=f|number=p',
	    'IV2F' => 'aspect=imperfect|pers=2|gender=f|number=s',
	    'IV2MP' => 'aspect=imperfect|pers=2|gender=m|number=p',
	    'IV2M' => 'aspect=imperfect|pers=2|gender=m|number=s',
	    'IV3FD' => 'aspect=imperfect|pers=3|gender=f|number=d',
	    'IV3MD' => 'aspect=imperfect|pers=3|gender=m|number=d',
	    'IV3MP' => 'aspect=imperfect|pers=3|gender=m|number=p',
	    'IV3M' => 'aspect=imperfect|pers=3|gender=m|number=s',
	    'IVSUFF_DO:1P' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=1|number=p',
	    'IVSUFF_DO:1' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=1|number=s',
	    'IVSUFF_DO:2F' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=2|gender=f|number=s',
	    'IVSUFF_DO:2MP' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=2|gender=m|number=p',
	    'IVSUFF_DO:2M' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=2|gender=m|number=s',
	    'IVSUFF_DO:3D' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|number=d',
	    'IVSUFF_DO:3F' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|gender=f|number=s',
	    'IVSUFF_DO:3MP' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|gender=m|number=p',
	    'IVSUFF_DO:3M' => 'catsuff=IVSUFF|aspect=imperfect|function=do|pers=3|gender=m|number=s',
	    'IVSUFF_MOOD:I' => 'catsuff=IVSUFF|aspect=imperfect|mood=i',
	    'IVSUFF_MOOD:J' => 'catsuff=IVSUFF|aspect=imperfect|mood=j',
	    'IVSUFF_MOOD:' => 'catsuff=IVSUFF|aspect=imperfect|mood=s',
	    'IVSUFF_SUBJ:2FS_MOOD:I' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|pers=2|fs|mood=i',
	    'IVSUFF_SUBJ:2FS_MOOD:SJ' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|pers=2|fs|mood=sj',
	    'IVSUFF_SUBJ:3F' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|pers=3|gender=f|number=s',
	    'IVSUFF_SUBJ:D_MOOD:I' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|number=d|mood=i',
	    'IVSUFF_SUBJ:D_MOOD:SJ' => 'catsuff=IVSUFF|aspect=imperfect|function=subj|number=d|mood=sj',
	    'IV_PAS' => 'iv=pass',
	    'IV' => 'aspect=imperfect',
	    'JUS_PART' => 'subcat=JU',
	    'NEG_PART' => 'subcat=NEG',
	    'NOUN.VN' => 'subcat=vn',
	    'NOUN_NUM' => 'subcat=num',
	    'NOUN_PROP' => 'subcat=prop',
	    'NOUN_QUANT' => 'subcat=quant',
	    'NSUFF_FEM_DU_ACC' => 'catsuff=NSUFF|gender=fem|number=d|case=acc',
	    'NSUFF_FEM_DU_ACCGEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=accgen',
	    'NSUFF_FEM_DU_ACC_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=acc',
	    'NSUFF_FEM_DU_GEN' => 'catsuff=NSUFF|gender=fem|number=d|case=gen',
	    'NSUFF_FEM_DU_GEN' => 'catsuff=NSUFF|gender=fem|number=d|case=gen',
	    'NSUFF_FEM_DU_GEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=gen',
	    'NSUFF_FEM_DU_NOM' => 'catsuff=NSUFF|gender=fem|number=d|case=nom',
	    'NSUFF_FEM_DU_NOM_POS' => 'catsuff=NSUFF|subcat=poss|gender=fem|number=d|case=nom',
	    'NSUFF_FEM_PL' => 'catsuff=NSUFF|gender=fem|number=p',
	    'NSUFF_FEM_SG' => 'catsuff=NSUFF|gender=fem|number=s',
	    'NSUFF_FEM_SG' => 'catsuff=NSUFF|gender=fem|number=s',
	    'NSUFF_M_SG' => 'catsuff=NSUFF|gender=fem|number=s',
	    'NSUFF_MASC_DU_ACC' => 'catsuff=NSUFF|gender=masc|number=d|case=acc',
	    'NSUFF_MASC_DU_ACCGEN' => 'catsuff=NSUFF|gender=masc|number=d|case=accgen',
	    'NSUFF_MASC_DU_ACCGEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=accgen',
	    'NSUFF_MASC_DU_ACC_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=acc',
	    'NSUFF_MASC_DU_GEN' => 'catsuff=NSUFF|gender=masc|number=d|case=gen',
	    'NSUFF_MASC_DU_GEN' => 'catsuff=NSUFF|gender=masc|number=d|case=gen',
	    'NSUFF_MASC_DU_GEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=gen',
	    'NSUFF_MASC_DU_NOM' => 'catsuff=NSUFF|gender=masc|number=d|case=nom',
	    'NSUFF_MASC_DU_NOM_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=d|case=nom',
	    'NSUFF_MASC_PL' => 'catsuff=NSUFF|gender=masc|number=p',
	    'NSUFF_MASC_PL_ACC' => 'catsuff=NSUFF|gender=masc|number=p|case=acc',
	    'NSUFF_MASC_PL_ACCGEN' => 'catsuff=NSUFF|gender=masc|number=p|case=accgen',
	    'NSUFF_MASC_PL_ACCGEN_PO' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=accgen',
	    'NSUFF_MASC_PL_ACC_PO' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=acc',
	    'NSUFF_MASC_PL_GEN' => 'catsuff=NSUFF|gender=masc|number=p|case=gen',
	    'NSUFF_MASC_PL_GEN_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=gen',
	    'NSUFF_MASC_PL_NOM' => 'catsuff=NSUFF|gender=masc|number=p|case=nom',
	    'NSUFF_MASC_PL_NOM_POS' => 'catsuff=NSUFF|subcat=poss|gender=masc|number=p|case=nom',
	    'POSS_PRON1P' => 'subcat=poss|pers=1|number=p',
	    'POSS_PRON1S' => 'subcat=poss|pers=1|number=s',
	    'POSS_PRON2D' => 'subcat=poss|pers=2|number=d',
	    'POSS_PRON2FP' => 'subcat=poss|pers=2|gender=f|number=p',
	    'POSS_PRON2FS' => 'subcat=poss|pers=2|gender=f|number=s',
	    'POSS_PRON2MP' => 'subcat=poss|pers=2|gender=m|number=p',
	    'POSS_PRON2MS' => 'subcat=poss|pers=2|gender=m|number=s',
	    'POSS_PRON3D' => 'subcat=poss|pers=3|number=d',
	    'POSS_PRON3FP' => 'subcat=poss|pers=3|gender=f|number=p',
	    'POSS_PRON3FS' => 'subcat=poss|pers=3|gender=f|number=s',
	    'POSS_PRON3MP' => 'subcat=poss|pers=3|gender=m|number=p',
	    'POSS_PRON3MS' => 'subcat=poss|pers=3|gender=m|number=s',
	    'POSS_PRON_1P' => 'subcat=poss|pers=1|number=p',
	    'POSS_PRON_1S' => 'subcat=poss|pers=1|number=s',
	    'POSS_PRON_2D' => 'subcat=poss|pers=2|number=d',
	    'POSS_PRON_2FP' => 'subcat=poss|pers=2|gender=f|number=p',
	    'POSS_PRON_2FS' => 'subcat=poss|pers=2|gender=f|number=s',
	    'POSS_PRON_2MP' => 'subcat=poss|pers=2|gender=m|number=p',
	    'POSS_PRON_2MS' => 'subcat=poss|pers=2|gender=m|number=s',
	    'POSS_PRON_3D' => 'subcat=poss|pers=3|number=d',
	    'POSS_PRON_3FP' => 'subcat=poss|pers=3|gender=f|number=p',
	    'POSS_PRON_3FS' => 'subcat=poss|pers=3|gender=f|number=s',
	    'POSS_PRON_3MP' => 'subcat=poss|pers=3|gender=m|number=p',
	    'POSS_PRON_3MS' => 'subcat=poss|pers=3|gender=m|number=s',
	    'PRON1P' => 'pers=1|number=p',
	    'PRON1S' => 'pers=1|number=s',
	    'PRON2FP' => 'pers=2|gender=f|number=p',
	    'PRON2FS' => 'pers=2|gender=f|number=s',
	    'PRON2MP' => 'pers=2|gender=m|number=p',
	    'PRON2MS' => 'pers=2|gender=m|number=s',
	    'PRON3D' => 'pers=3|number=d',
	    'PRON3FP' => 'pers=3|gender=f|number=p',
	    'PRON3FS' => 'pers=3|gender=f|number=s',
	    'PRON3MP' => 'pers=3|gender=m|number=p',
	    'PRON3MP' => 'pers=3|gender=m|number=p',
	    'PRON_1P' => 'pers=1|number=p',
	    'PRON_1S' => 'pers=1|number=s',
	    'PRON_2FP' => 'pers=2|gender=f|number=p',
	    'PRON_2FS' => 'pers=2|gender=f|number=s',
	    'PRON_2MP' => 'pers=2|gender=m|number=p',
	    'PRON_2MS' => 'pers=2|gender=m|number=s',
	    'PRON_3D' => 'pers=3|number=d',
	    'PRON_3FP' => 'pers=3|gender=f|number=p',
	    'PRON_3FS' => 'pers=3|gender=f|number=s',
	    'PRON_3MP' => 'pers=3|gender=m|number=p',
	    'PRON_3MS' => 'pers=3|gender=m|number=s',
	    'PVSUFF|pers=3|M' => 'catsuff=PVSUFF|aspect=perfect|pers=3|gender=m|number=s',
	    'PVSUFF_DO:1P' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=1|number=p',
	    'PVSUFF_DO:1S' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=1|number=s',
	    'PVSUFF_DO:2FS' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=2|gender=f|number=s',
	    'PVSUFF_DO:2MP' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=2|gender=m|number=p',
	    'PVSUFF_DO:2MS' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=2|gender=m|number=s',
	    'PVSUFF_DO:3D' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=3|number=d',
	    'PVSUFF_DO:3FS' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=3|gender=f|number=s',
	    'PVSUFF_DO:3MP' => 'catsuff=PVSUFF|aspect=perfect|function=do|pers=3|gender=m|number=p',
	    'PVSUFF_SUBJ:1P' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=1|number=p',
	    'PVSUFF_SUBJ:1S' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=1|number=s',
	    'PVSUFF_SUBJ:2FS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=2|gender=f|number=s',
	    'PVSUFF_SUBJ:2MP' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=2|gender=m|number=p',
	    'PVSUFF_SUBJ:2MS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=2|gender=m|number=s',
	    'PVSUFF_SUBJ:3FD' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=f|number=d',
	    'PVSUFF_SUBJ:3FP' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=f|number=p',
	    'PVSUFF_SUBJ:3FS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=f|number=s',
	    'PVSUFF_SUBJ:3MD' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=m|number=d',
	    'PVSUFF_SUBJ:3MP' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=m|number=p',
	    'PVSUFF_SUBJ:3MS' => 'catsuff=PVSUFF|aspect=perfect|function=subj|pers=3|gender=m|number=s',
	    'PV_PAS' => 'voice=pasive',
	    'RC_PART' => 'subcat=RC',
	    'RESTRIC_PART' => 'subcat=RESTRIC',
	    'VERB_PART' => 'subcat=VERB',
	    'VOC_PART' => 'subcat=VOC',
	   };
}

sub morpho2conll {
  my $s = shift;
  $s =~ s/^DET\+/DET_/o;
  return join('|', grep {$_} map {$map->{$_}} split(/\+/,$s));
}

sub handle {
  my $s = shift;
  init;
  $s =~ s/atbpos=([^\s\|]+)/&morpho2conll($1)/e;
  $s =~ s/^\|//o;
#  print "converted to <$s>\n";
  return $s;
}

1;
